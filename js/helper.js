/**
 * Created by локтионов_ав on 24.06.14.
 */

var report_date_range_picker = $('#report_date_range_picker');
function qualifyURL(url) {
    /*var a = document.createElement('a');
    a.href = url;
    return a.href;*/
    var baseUrl = $('#baseUrl').val();
    return baseUrl + '/'+ url;
}
$(document).ready(function () {
    $(document).on('click', '.accept-link', function (e) {
        AcceptClick(e);
    });
    $(document).on('click', '.annul-link', function (e) {
        AnnulClick(e);
    });
    $(document).on('click', '.out-link', function (e) {
        OutClick(e);
    });
    var date_picker = $('#date_picker');


    report_date_range_picker.change(function (e) {
        var target = $(e.target);
        $('#ReportForm_date_range_str').val(target.val());
    });

    date_picker.on('click', '.btn', function (e) {
        //e.stopPropagation();
        var target = $(e.target);
        var filter = target.data('filter');
        var date_range_str = '';
        $('#ReportForm_date_range_alias').val(filter);
        if (filter != 'own') {
            report_date_range_picker.attr('disabled', 'disabled');
            var aj = $.ajax({
                type: "POST",
                url: qualifyURL('report/getdaterange/'),
                data: {"category_filter": filter},
                //async: false,
                success: function (html) {
                    console.log(html);
                    report_date_range_picker.val(html).trigger('change');
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
        else {
            report_date_range_picker.val('').trigger('change');
            report_date_range_picker.attr('disabled', false);
            report_date_range_picker.focus();
        }
        /*if(target.hasClass('active')){
         filter = 0;
         }else{
         target.parent().children('.active').removeClass('active');
         }
         target.toggleClass('active');
         */
        return true;
    });

    var org_picker = $('#org_picker');

    org_picker.on('click', '.btn', function (e) {
        var target = $(e.target);
        var value = target.data('filter');
        var report_category = $('#ReportForm_category').find(':input[value=' + value + ']');
        if (!report_category.attr('checked')) {
            $("#org_filter").html('<img src="/css/ajax-loader.gif">');
            $.ajax({
                async: true,
                data: {'ReportForm[category]': value},
                url: qualifyURL('report/getmolist_ajax/'),
                type: 'post',
                success: function (data) {
                    $('#org_filter').html(data);
                }
            });
            report_category.prop('checked', true);
            $('#ReportForm_kodmo').val('');
        }

    });
});
function MoChanged(v) {
    $('#ReportForm_kodmo').val(v);
}
function testFunction(v) {
    alert(v);
}
function AcceptClick(e) {
    var target = $(e.target);
    var idzap = target.data('idzap');
    $('#accept-modal .modal-body #idzap_accept').val(idzap);
    var fio = target.data('fio');
    $('#accept-modal .modal-header h3').html(fio);
    var dngosp = target.data('dngosp');
    $('#accept-modal .modal-body #PacientView_DNGOSP').val(dngosp);
    var dpogosp = target.data('dpogosp');
    $('#accept-modal .modal-body #PacientView_DPOGOSP').val(dpogosp);
    var dsnapr = target.data('dsnapr');
    var $select = $('#accept-modal .modal-body #PacientView_DSPO')[0].selectize;
    $select.addOption({kod: dsnapr.key, name: dsnapr.val});
    $select.setValue(dsnapr.key);
}
function AnnulClick(e) {
    var target = $(e.target);
    var idzap = target.data('idzap');
    $('#annul-modal .modal-body #idzap_annul').val(idzap);
    var fio = target.data('fio');
    $('#annul-modal .modal-header h3').html(fio);
    $('#annul-modal .modal-body #PacientView_PANUL').val('');
}
function OutClick(e) {
    var target = $(e.target);
    var idzap = target.data('idzap');
    $('#out-modal .modal-body #idzap_out').val(idzap);
    var fio = target.data('fio');
    $('#out-modal .modal-header h3').html(fio);
    var dogosp = target.data('dogosp');
    $('#out-modal .modal-body #dogosp-out').val(dogosp);
    var dspo = target.data('dspo');
    var $select = $('#out-modal .modal-body #PacientView_DS')[0].selectize;
    $select.addOption({kod: dspo.key, name: dspo.val});
    $select.setValue(dspo.key);
}
function SetZL(fam, im, ot, p, dr, tel, vpolis, npolis, ter, smo) {
    $('#Pacient_FAM').val(fam);
    $('#Pacient_IM').val(im);
    $('#Pacient_OT').val(ot);
    $('#Pacient_P').val(p);
    $('#Pacient_DR').val(dr);
    $('#Pacient_TEL').val(tel);
    $('#Pacient_VPOLIS').val(vpolis);
    $('#Pacient_NPOLIS').val(npolis);
    $('#Pacient_TER').val(ter);
    $('#Pacient_SMO').val(smo);
    OpenTab();
}
function OpenTab() {
    $('#bulletin_tabs li.active').removeClass('active');
    $('#bulletin_tabs li:first').toggleClass('active');
}

function MkbLoad(query, callback, url) {
    if (!query.length) return callback();
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {
            'query': encodeURIComponent(query)
        },
        error: function () {
            callback();
        },
        success: function (res) {
            callback(res.slice(0, 10));
        }
    });
}


function printBlock(printDiv) {
    productDesc = $(printDiv).html();
    var body = $('body');
    body.addClass('printSelected');
    body.append('<div class="printSelection">' + productDesc + '</div>');
    window.print();

    window.setTimeout(pageCleaner, 0);

    return false;
}
function pageCleaner() {
    $('body').removeClass('printSelected');
    $('.printSelection').remove();
}
function PacSum() {
    var ipac = parseInt($('#Mesto_IPAC').val()) || 0;
    var opac = parseInt($('#Mesto_OPAC').val()) || 0;
    var ppac = parseInt($('#Mesto_PPAC').val()) || 0;
    $('#Mesto_NPAC').val(ipac + opac + ppac);
}
function KSum() {
    var skm = parseInt($("input[id$='Mesto_SKM']").val()) || 0;
    var skw = parseInt($("input[id$='Mesto_SKW']").val()) || 0;
    var skd = parseInt($("input[id$='Mesto_SKD']").val()) || 0;
    $("input[id$='Mesto_SK']").val(skm + skw + skd);
}

function UpdateGrid(id, typep, href) {
    href = href || false;
    if (href != false) {
        var re_id = /\/(\d)/;
        var re_typep = /\/typep\/(\w+)/i;
        if ((res = re_id.exec(href)) != null) {
            id = res[1];
        }
        else {
            id = 1;
        }
        if ((res = re_typep.exec(href)) != null) {
            typep = res[1];
        }
        else {
            typep = 'planed';
        }
        $(".radio-filter.active").removeClass('active');
        $("#btn_" + typep).toggleClass('active');
    }
    $('#typep').val(typep);
    if (id == 2 || id == 3)
        id = 'h';
    else
        id = 'a';
    var grid_id = $('.grid-view').attr('id');
    $('#' + grid_id).yiiGridView('update', {
        data: {
            typep_ajax: typep,
            q: ''
        }

    });
}
function SetPopErrors(data) {
    var pop = $('#link-' + data.source_action + '-' + data.source_id);
    pop.popover({
        content: data.flashes,
        html: true,
        placement: 'left'
    });
    pop.popover('show');
    var close_link = $('.popover-content #flash-div-' + data.source_action + '-' + data.source_id + ' a.close');
    close_link.on('click', function (e) {
        pop.popover('hide');
        pop.popover('destroy');
    });
    pop.on('click', function (e) {
        pop.popover('hide');
        pop.popover('destroy');
    });
}
/**
 * @return {boolean}
 */
function UpdatePage(route, id, data) {
    var len = $('.grid-view').length;
    if (route == 'cabinet/view' || len == 0) {
        if (data.save)
            location.reload();
        else SetPopErrors(data);
        return false;
    }
    data = data || false;
    if (data != false) {
        if (data.save) {
            $('#flash-place').html(data.flashes);
            UpdatePage(route, id);
        }
        else {
            $('#flash-place').html('');
            SetPopErrors(data)
            return false;
        }
    }

    id = id || 1;
    var typep = $('#typep').val() || 'planed';
    UpdateGrid(id, typep);

    return false;
}
function GetMestaAjax(url, date, params) {
    $.ajax({
        async: false,
        type: "POST",
        url: url,
        dataType: 'json',
        data: {"date": date, "params": params},
        success: function (html) {
            $("#IDMESTO" + date).html(html);
        }
    });
}

function FileDone(e, data) {
    var err_arr;
    var errors;
    if (data.result != null) {
        err_arr = $(data.result[0].model_errors);
        errors = err_arr.length;
        err_arr.each(function (index) {
            errors += data.result[0].model_errors[index];
        });
        $('#files-grid').yiiGridView('update');
    } else {
        errors = 'Неизвестная ошибка сервера. Обратитесь к администратору.';
    }

    if (errors != 0)
        $('#flash-place').html('Всего ошибок: ' + errors);
}
/**
 * @return {string}
 */
function RenderOptHeader(data, escape) {
    return '<div class="my-optgroup-header">' + escape(data.label) + '</div>';
}
function RenderItem(item, escape) {
    var label = item.name;
    var css_class='my-item option-yes';
    if(item.optgroup=='no' || item.optgroup=='0'){
        css_class='my-item option-no';
    }
    return '<div class="'+css_class+'">' + escape(label)  +  '</div>';
}
function RenderOption(item, escape) {
    var label = item.name;
    var css_class='my-option option-yes';
    if(item.optgroup=='no' || item.optgroup=='0'){
        css_class='my-option option-no';
    }
    return '<div class="'+css_class+'">' + escape(label)  +  '</div>';
}
function RenderOptionWithOpt(item, escape) {
    var label = item.name;
    var cat = item.cat ? '<br /><span class="small muted">' + item.cat + '</span>' : '';

    return '<div>' + escape(label)  + cat + '</div>';
}
function RefillSelectizeUser(data){
    var selectize = $("#kodmo")[0].selectize;
    selectize.clear();
    selectize.clearOptions();
    selectize.load(function(callback) {
        callback(data.options);
    });
    if(data.kod != undefined)
        selectize.setValue(data.kod);
}
function show_div($id) {
    var proxy = $("#"+$id);
    if(proxy.is(':visible')){
        proxy.hide('slow');
    } else {
        proxy.show('slow');
    }
}