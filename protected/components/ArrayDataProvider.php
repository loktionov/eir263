<?php
/**
 * User: Локтионов_АВ
 * Date: 04.07.14
 * Time: 11:21
 */
class ArrayDataProvider extends CArrayDataProvider
{
    /**
     * Fetches the data from the persistent data storage.
     * @return array list of data items
     */
    protected function fetchData()
    {
        if(($sort=$this->getSort())!==false && ($order=$sort->getOrderBy())!='')
            $this->sortData($this->getSortDirections($order));

        if(($pagination=$this->getPagination())!==false)
        {
            $pagination->setItemCount($this->getTotalItemCount());
            return $this->rawData;
            //return array_slice($this->rawData, $pagination->getOffset(), $pagination->getLimit());
        }
        else
            return $this->rawData;
    }
}
