<?php

/**
 * User: Loktionov
 * Date: 22.01.14
 * Time: 17:16
 */
class Helpers
{
    public static function GetTypepArray()
    {
        return array(
            'planed' => array(
                'rus' => 'бронь',
                'color' => TbHtml::LABEL_COLOR_INFO,
                'detail' => 'направленные, но не госпитализированные пациенты',
            ),
            'blocked' => array(
                'rus' => 'госпитализирован',
                'color' => TbHtml::LABEL_COLOR_SUCCESS,
                'detail' => 'госпитализированные пациенты',
            ),
            'annuled' => array(
                'rus' => 'аннулирован',
                'color' => TbHtml::LABEL_COLOR_INVERSE,
                'detail' => 'аннулированные пациенты',
            ),
            'out' => array(
                'rus' => 'выписан',
                'color' => TbHtml::LABEL_COLOR_DEFAULT,
                'detail' => 'выписанные пациенты',
            ),
            'extr' => array(
                'rus' => 'экстренно',
                'color' => TbHtml::LABEL_COLOR_WARNING,
                'detail' => 'экстренные пациенты',
            ),
            'all' => array(
                'rus' => 'все',
                'color' => TbHtml::LABEL_COLOR_IMPORTANT,
                'detail' => 'все пациенты',
            ),
        );
    }

    public static function GetPacientLabel($typep, $extr = false, $htmlOptions = array())
    {
        $lbl_param = TbArray::getValue($typep, self::GetTypepArray());
        $return = '';
        if (!empty($lbl_param))
            $return = TbHtml::labelTb($lbl_param['rus'], TbArray::merge($htmlOptions, array('color' => $lbl_param['color'])));

        if ($extr)
            $return = TbHtml::labelTb('э', TbArray::merge($htmlOptions, array('color' => TbHtml::LABEL_COLOR_WARNING))) . $return;
        return $return;
    }

    public static function GetFompArray($fomp)
    {
        if ($fomp == 3)
            return array(3 => 'экстренная',);
        else
            return array(
                1 => 'плановая',
                //2 => 'неотложная',
            );
    }

    /**
     * Возвращает текстовое представление типа полиса
     *
     * @param $tp int|string Цифровое обзначение типа полиса
     * @return bool|string Текстовое представление типа полиса
     */
    public static function GetTypepolis($tp)
    {
        switch ($tp) {
            case 1:
                return 'С';
            case 2:
                return 'В';
            case 3:
                return 'П';
            case 'С':
                return 1;
            case 'В':
                return 2;
            case 'П':
                return 3;
            default:
                return false;
        }
    }

    public static function GetSexArray()
    {
        return array('1' => 'Мужской', '2' => 'Женский');
    }

    public static function GetPolisArray()
    {
        return array('1' => 'Старый полис', '2' => 'Временное свидетельство', '3' => 'Новый полис');
    }

    public static function GetNamepolis($tp)
    {
        switch ($tp) {
            case 1:
                return 'Старый полис';
            case 2:
                return 'Временное свидетельство';
            case 3:
                return 'Единый полис';
            case 'С':
                return 'Старый полис';
            case 'В':
                return 'Временное свидетельство';
            case 'П':
                return 'Единый полис';
            default:
                return false;
        }
    }

    public static function GetEmptyHtmlField()
    {
        return '<span class = "empty-field">пусто</span>';
    }

    public static function GetSafeCockie($cookie_name)
    {
        return Yii::app()->request->cookies->contains($cookie_name) ?
            Yii::app()->request->cookies[$cookie_name]->value : '';
    }

    /**
     * Возвращает текстовое представление пола
     *
     * @param $sex int|string Цифровое обзначение пола
     * @return bool|string Текстовое представление пола
     */
    public static function GetSex($sex)
    {
        if (is_string($sex)) {
            $sex = strtoupper($sex[0]);
        }
        switch ($sex) {
            case 1:
                return 'М';
            case 2:
                return 'Ж';
            case 'М':
                return 1;
            case 'Ж':
                return 2;
            case 'M':
                return 1;
            case 'W':
                return 2;
            default:
                return false;
        }
    }

    /**
     * Возвращает цвет статуса
     *
     * @param $status int|string Цифровое обзначение пола
     * @return bool|string Текстовое представление пола
     */
    public static function GetSpatrahStatusColor($status)
    {
        switch ($status) {
            case 0:
                return TbHtml::LABEL_COLOR_INFO;
            case 1:
                return TbHtml::LABEL_COLOR_WARNING;
            case 2:
                return TbHtml::LABEL_COLOR_SUCCESS;
            case 3:
                return TbHtml::LABEL_COLOR_IMPORTANT;
            case 4:
                return TbHtml::LABEL_COLOR_INVERSE;
            default:
                return false;
        }
    }

    public static function GetSpatrahStatusText($status)
    {
        switch ($status) {
            case 0:
                return 'Добавлен';
            case 1:
                return 'Отправлен';
            case 2:
                return 'Найден';
            case 3:
                return 'Не найден';
            case 4:
                return 'Ошибки флк';
            default:
                return false;
        }
    }

    public static function GetAge($birth)
    {
        $birth = new DateTime($birth);
        $today = new DateTime();
        $dif = $today->diff($birth);
        return $dif->y;
    }

    /**
     * @param $isDateTime bool
     * @param bool $isView
     * @return string
     */
    public static function getDateFormat($isDateTime = false, $isView = false)
    {
        $time = $isDateTime ? 'Time' : '';
        $format = "myDate{$time}Format";
        if (self::getIsMsSql() OR $isView)
            $format = "date{$time}Format";
        return Yii::app()->params[$format];
    }

    public static function getIsWin()
    {
        return strpos(strtolower(php_uname('s')), 'windows') !== false;
    }

    public static function getIsMsSql()
    {
        return Yii::app()->db->driverName == 'sqlsrv';
    }

    public static function getLocale()
    {
        return self::getIsWin() ? 'rus_Russia' : 'ru_RU';
    }

    public static function getEncode()
    {
        return self::getIsWin() ? 'CP1251' : 'ISO-8859-5';
    }

    public static function getCommonMenuExit($controller)
    {
        return array(
            'class' => 'TbNav',
            'items' => array(
                array('label' => "Вход", 'url' => Yii::app()->createAbsoluteUrl('site/login'), 'visible' => Yii::app()->user->isGuest, 'active' => $controller->action->id == 'login'),
                array('label' => "Выход", 'url' => Yii::app()->createAbsoluteUrl('site/logout'), 'visible' => !Yii::app()->user->isGuest, 'align' => 'right',),
            ),
            'htmlOptions' => array('class' => 'pull-right'),
        );
    }

    public static function getCommonMenuManual($controller)
    {
        //$show_update = Helpers::isShowUpdate('27.11.2014 23:59:59', '10 days'); //. ($show_update ? '&nbsp;<i class="fa fa-exclamation-circle" style="color: #f89406"></i>' : ''),
        return array(
            'encodeLabel' => false,
            'class' => 'TbNav',
            'items' => array(
                array(
                    'label' => "Инструкция",
                    'url' => Yii::app()->createAbsoluteUrl('site/manual'),
                    'active' => @$controller->route == 'site/manual',
                    'visible' => !Yii::app()->user->isGuest,
                ),
            ),
            'htmlOptions' => array('class' => 'pull-right'),
        );
    }

    public static function getCommonMenuCabinet($controller)
    {
        return array(
            'class' => 'TbNav',
            'items' => array(
                array(
                    'label' => "Кабинет",
                    'url' => Yii::app()->createAbsoluteUrl('cabinet'),
                    'active' => @$controller->id == 'cabinet',
                    'visible' => !Yii::app()->user->isGuest
                ),
            ),
        );
    }

    public static function getCommonMenuSearchplace($controller)
    {
        return array(
            'class' => 'TbNav',
            'encodeLabel' => false,
            'items' => array(
                array(
                    'label' => "Поиск мест",
                    'active' => $controller->id == 'clinic',
                    'visible' => !Yii::app()->user->isGuest,
                    'url' => Yii::app()->createAbsoluteUrl('clinic'),
                ),
            ),
        );
    }

    public static function getCommonMenuAdmintools($controller)
    {
        return array(
            'class' => 'TbNav',
            'encodeLabel' => false,
            'items' => array(
                array(
                    'label' => "Админ",
                    'active' => $controller->id == 'admin',
                    'visible' => Yii::app()->user->getState('role') == WebUser::ROLE_ADMIN,
                    'url' => Yii::app()->createAbsoluteUrl('admin'),
                ),
            ),
        );
    }

    public static function getCommonMenuUpdates($controller)
    {
        return array(
            'encodeLabel' => false,
            'class' => 'TbNav',
            'items' => array(
                array(
                    'label' => "Обновления",
                    'url' => Yii::app()->createAbsoluteUrl('updates/index'),
                    'active' => @$controller->route == 'updates/index',
                    'visible' => !Yii::app()->user->isGuest,
                ),
            ),
            'htmlOptions' => array('class' => 'pull-right'),
        );
    }

    public static function getCommonMenuPasport($controller)
    {
        return array(
            'encodeLabel' => false,
            'class' => 'TbNav',
            'items' => array(
                array(
                    'label' => "Паспорт",
                    'url' => Yii::app()->createAbsoluteUrl('pasport'),
                    'active' => @in_array(@$controller->id, array('pasport', 'pmo', 'medrab')),
                    'visible' => !Yii::app()->user->isGuest,
                ),
            ),
        );
    }

    public static function getCommonMenuReport($controller)
    {
        return array(
            'class' => 'TbNav',
            'encodeLabel' => false,
            'items' => array(
                array(
                    'label' => "Отчеты",
                    'active' => $controller->id == 'report',
                    'visible' => !Yii::app()->user->isGuest,
                    //'url' => Yii::app()->createAbsoluteUrl('report'),
                    'items' => array(
                        array(
                            'label' => "Сводный отчет о движении пациентов",
                            'active' => $controller->route == 'report/index',
                            'visible' => !Yii::app()->user->isGuest,
                            'url' => Yii::app()->createAbsoluteUrl('report'),
                        ),
                        array(
                            'label' => "Занятость мест в разрезе профиля коек",
                            'active' => $controller->route == 'report/mesta',
                            'visible' => !Yii::app()->user->isGuest,
                            'url' => Yii::app()->createAbsoluteUrl('report/mesta'),
                        ),
                        array(
                            'label' => "Количество и состав коечного фонда",
                            'active' => $controller->route == 'report/analiticmesta',
                            'visible' => !Yii::app()->user->isGuest,
                            'url' => Yii::app()->createAbsoluteUrl('report/analiticmesta'),
                        ),
                        TbHtml::menuDivider(),
                        array(
                            'label' => "Пациенты",
                            'active' => $controller->route == 'report/analitic',
                            'visible' => !Yii::app()->user->isGuest,
                            'url' => Yii::app()->createAbsoluteUrl('report/analitic'),
                        ),
                    )
                ),
                TbHtml::menuDivider(),
            ),
        );
    }

    /**
     * @param $controller Controller
     * @param $menu array
     * @return array
     */
    public static function getCommonMenu($controller, $menu = array())
    {
        //$show_update = Helpers::isShowUpdate('06.11.2014 23:59:59', '10 days');
        //($show_update ? '&nbsp;<i class="fa fa-exclamation-circle" style="color: #f89406"></i>' : '')
        return TbArray::merge($menu, array(
            self::getCommonMenuExit($controller),
            self::getCommonMenuManual($controller),
            self::getCommonMenuUpdates($controller),
            self::getCommonMenuCabinet($controller),
            self::getCommonMenuPasport($controller),
        ));
    }

    /**
     * @param $date_range
     * @throws InvalidArgumentException
     * @return array
     */
    public static function parseRange($date_range)
    {
        $myq = '/^(?<start_date>\d{4}\.\d\d\.\d\d)\s*(?<start_time>\d\d:\d\d:\d\d){0,1}\s*-\s(?<end_date>\d{4}\.\d\d\.\d\d)\s*(?<end_time>\d\d:\d\d:\d\d){0,1}$/';
        if (self::getIsMsSql())
            $q = '/^(?<start_date>\d\d\.\d\d\.\d{4})\s*(?<start_time>\d\d:\d\d:\d\d){0,1}\s*-\s(?<end_date>\d\d\.\d\d\.\d{4})\s*(?<end_time>\d\d:\d\d:\d\d){0,1}$/';
        else
            $q = $myq;
        preg_match($q, $date_range, $matches);
        if (empty($matches)) {


            throw new InvalidArgumentException('Сбой разбора строки ' . $date_range . ' Неверный диапазон дат.');

        }
        $start_str = $matches['start_date'] . self::getTimeFromMatches(@$matches['start_time']);
        $end_str = $matches['end_date'] . self::getTimeFromMatches(@$matches['end_time'], true);
        $start = DateTime::createFromFormat(Helpers::getDateFormat(true), $start_str);
        $end = DateTime::createFromFormat(Helpers::getDateFormat(true), $end_str);
        return array($start, $end);
    }

    private static function getTimeFromMatches($time, $end = false)
    {
        $zeroTime = '00:00:00';
        if ($end) {
            $zeroTime = '23:59:59';
        }
        if (empty($time))
            $time = $zeroTime;
        return ' ' . trim($time);
    }

    public static function DeclinationSpaceArray()
    {
        return array(

            'место',
            'места',
            'мест',

        );
    }

    public static function DeclinationSpace($n)
    {

        return self::getNumEnding($n, self::DeclinationSpaceArray());
    }

    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     * @param  $number Integer Число на основе которого нужно сформировать окончание
     * @param $endingArray Array Массив слов или окончаний для чисел (1, 4, 5), например array('яблоко', 'яблока', 'яблок')
     * @return String
     */
    public static function getNumEnding($number, $endingArray)
    {
        if (!is_numeric($number) || empty($number))
            return '';
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $endingArray[0];
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $endingArray[1];
                    break;
                default:
                    $ending = $endingArray[2];
            }
        }
        return $ending;
    }

    public static function isShowUpdate($strdate, $mod)
    {
        $update_time = new DateTime($strdate);
        $update_time->modify($mod);
        return time() < $update_time->getTimestamp();
    }

    /**
     * @param $key
     * @param $callback
     * @param $args
     * @return mixed
     */
    public static function check_cache($key, $callback, $args = null)
    {
        $res_cache = Yii::app()->cache->get($key);
        if (empty($res_cache))
            $res = call_user_func($callback, $args);
        else {
            $res = $res_cache;
        }
        Yii::app()->cache->set($key, $res, 3600);
        return $res;
    }

    public static function getPass($length = 9, $is_alpha = true, $is_alpha_upper = true, $is_numeric = true, $is_special = true)
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = "";


        // if you want a form like above
        if ($is_alpha)
            $chars .= $alpha;

        if ($is_alpha_upper)
            $chars .= $alpha_upper;

        if ($is_numeric)
            $chars .= $numeric;

        if ($is_special)
            $chars .= $special;


        $len = strlen($chars);
        $pw = '';

        for ($i = 0; $i < $length; $i++)
            $pw .= substr($chars, rand(0, $len - 1), 1);

        return str_shuffle($pw);
    }

    public static function checkSNILS($snils)
    {
        if ($snils == '')
            return true;
        $regex = '/^\d\d\d\-\d\d\d\-\d\d\d\s\d\d$/';
        if (!preg_match($regex, $snils))
            return false;
        $regex = '/(\d)/';
        preg_match_all($regex, $snils, $matches);
        $matches = $matches[0];
        $end_check = (int)($matches[9] . $matches[10]);
        $minsnils = 1001998;
        $oursnils = '';
        $checksum = 0;
        $num = 0;
        for ($i = 0; $i < 9; $i++) {
            $num = (int)$matches[$i];
            $oursnils .= $matches[$i];
            $checksum += $num * (9 - $i);
        }
        //echo $oursnils.' - '.$checksum;
        if ($minsnils >= (int)$oursnils)
            return true;
        if ($checksum < 100) {
            if ($checksum != $end_check)
                return false;
            else return true;
        } elseif ($checksum == 100 OR $checksum == 101) {
            if ($end_check != 0)
                return false;
            else return true;
        } elseif ($checksum > 101) {
            $checksum = $checksum % 101;
            if ($end_check != (int)(substr((string)$checksum, -2)))
                return false;
            else return true;
        } else return false;
    }

    public static function formatSnils($snils)
    {
        $snils = str_pad($snils, 11, '0', STR_PAD_LEFT);
        return mb_substr($snils, 0, 3) . '-' . mb_substr($snils, 3, 3) . '-'
        . mb_substr($snils, 6, 3) . ' ' . mb_substr($snils, 9, 2);
    }

    public static function file_force_download($file, $name = null, $content_type="application/octet-stream")
    {
        if (file_exists($file)) {
            $name = empty($name) ? basename($file) : $name;
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $content_type);
            header('Content-Disposition: attachment; filename="' . $name . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            unlink($file);
            exit;
        }
    }

} 