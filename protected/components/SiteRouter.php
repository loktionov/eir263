<?php

class SiteRouter
{
    public static function routeRequest($event)
    {
        /** @var $sender CWebApplication */
        $sender = & $event->sender;
        if(Yii::app()->user->hasState('role')){
            switch(Yii::app()->user->getState('role')){
                case WebUser::ROLE_TFOMS:
                    $sender->defaultController = 'report';
                    break;
                case WebUser::ROLE_SMO:
                    $sender->defaultController = 'cabinet';
                    break;
                case WebUser::ROLE_ADMIN:
                    $sender->defaultController = 'admin';
                    break;
                default: $sender->defaultController = 'updates';
            }
        }



        //throw new CHttpException(404, 'Unable to find a site hosted here with that hostname');
    }

}