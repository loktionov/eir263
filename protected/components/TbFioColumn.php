<?php
/**
 * TbDataColumn class file.
 * @author Antonio Ramirez <ramirez.cobos@gmail.com>
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

Yii::import('zii.widgets.grid.CDataColumn');

/**
 * Bootstrap grid data column.
 */
class TbFioColumn extends TbDataColumn
{
    protected function renderDataCellContent($row, $data)
    {
        if ($this->value !== null)
            $value = $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
        else
            $value = $this->getDataCellContent($row, $data);
        echo $value === null ? $this->grid->nullDisplay : $value;
    }

    protected function getDataCellContent($row, $data)
    {
        return '<table class="table-simple">
    <tbody>
    <tr>
        <td>' . ($data->FOMP == 3 ? TbHtml::labelTb('э', array('color' => TbHtml::LABEL_COLOR_WARNING)) : '&nbsp;') . ' </td>
<td>' . Helpers::GetPacientLabel($data->typep) . ' </td>
<tr>
    <td>

    </td>
    <td>
        ' . $data->NNAPR . '  <span class="cat">(' . date(Helpers::getDateFormat(false, true), strtotime($data->DNAPR)) . ' )</span>
    </td>
</tr>
<tr>
    <td>
        ' . TbHtml::tooltip(TbHtml::icon(TbHtml::ICON_PENCIL), Yii::app()->createAbsoluteUrl('clinic/bulletinupdate', array('id' => $data->IDZAP)), 'Редактировать') . '
    </td>
    <td>
        ' . TbHtml::link($data->FAM . ' ' . $data->IM . ' ' . $data->OT, Yii::app()->createAbsoluteUrl('cabinet/view', array('id' => $data->IDZAP))) . '
    </td>
</tr>
<tr>
    <td></td>
    <td>
        <span class="cat">' . Helpers::GetSex($data->P) . '&nbsp;' . date(Helpers::getDateFormat(false, true), strtotime($data->DR)) . ' </span>
    </td>
</tr>
</tbody>
</table>';
    }
}
