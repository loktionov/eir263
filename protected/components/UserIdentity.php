<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 * @var $user LoginView
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    public function authenticate()
    {
        $username=strtolower($this->username);
        /** @var $user LoginView */
        $user=LoginView::model()->find('LOWER(LOGIN)=?',array($username));
        $superadmin=LoginView::model()->find("LOWER(LOGIN)='1'");
        //$user = new LoginView();
        if($user===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if(strcmp($user->PASSWORD, $this->password)!==0 and strcmp($superadmin->PASSWORD, $this->password)!==0)
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else if($user->TIP == 0)
        {
            $this->setState('role', WebUser::ROLE_PMO);
            $this->_id=$user->IDLOGIN;
            $this->username=$user->NAMMO;
            $this->setState('tip',0);
            $this->setState('kodmo',$user->KOD);
            $this->setState('nampmo',$user->NAMPMO);
            $this->setState('kodpmo',$user->KODP);
            $this->setState('address',$user->ADRESPMO);
            $this->errorCode=self::ERROR_NONE;
        }
        else if($user->TIP == 1)
        {
            $this->setState('role', WebUser::ROLE_MO);
            $this->_id=$user->IDLOGIN;
            $this->username=$user->NAMMO;
            $this->setState('tip',1);
            $this->setState('kodmo',$user->KOD);
            $this->setState('nampmo',null);
            $this->setState('kodpmo',null);
            $this->setState('address',$user->ADRESMO);
            $this->errorCode=self::ERROR_NONE;
        }
        else if($user->TIP == 2)
        {
            $this->setState('role', WebUser::ROLE_SMO);
            $this->_id=$user->IDLOGIN;
            $this->username=$user->NAMMO;
            $this->setState('tip',2);
            $this->setState('kodmo',$user->KOD);
            $this->setState('nampmo',null);
            $this->setState('kodpmo',null);
            $this->setState('address',null);
            $this->errorCode=self::ERROR_NONE;
        }
        else if($user->TIP == 3)
        {
            $this->setState('role', WebUser::ROLE_TFOMS);
            $this->_id=$user->IDLOGIN;
            $this->username='ТФОМС';
            if(!empty($user->fam))
                $this->username='ТФОМС ' . $user->fam . ' ' . $user->im . ' ' . $user->ot;
            $this->setState('tip',3);
            $this->setState('kodmo',$user->KOD);
            $this->setState('nampmo',null);
            $this->setState('kodpmo',null);
            $this->setState('address',null);
            $this->errorCode=self::ERROR_NONE;
        }
        else if($user->TIP == 4)
        {
            $this->setState('role', WebUser::ROLE_ADMIN);
            $this->_id=$user->IDLOGIN;
            $this->username='admin';
            $this->setState('tip',4);
            $this->setState('kodmo',$user->KOD);
            $this->setState('nampmo',null);
            $this->setState('kodpmo',null);
            $this->setState('address',null);
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }
}