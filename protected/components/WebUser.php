<?php

/**
 * User: локтионов_ав
 * Date: 20.07.14
 * Time: 16:48
 */
class WebUser extends CWebUser
{
    public $role;
    const ROLE_PMO = 'pmo';
    const ROLE_MO = 'mo';
    const ROLE_SMO = 'smo';
    const ROLE_TFOMS = 'tfoms';
    const ROLE_ADMIN = 'admin';
    const ROLE_GUEST = 'guest';

    public function afterLogin($fromCookie)
    {
        if (!Yii::app()->user->hasState('tip')) {
            Yii::app()->user->setState('tip', '4');
        }
        parent::afterLogin($fromCookie);
    }

    public function loginRequired()
    {
        try {
            parent::loginRequired();
        } catch (CHttpException $e) {

        }
    }
    public function setState($key,$value,$defaultValue=null){
        parent::setState($key, $value, $defaultValue);
        if($key == 'role')
            $this->role = $value;
    }
}