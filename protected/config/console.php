<?php
function getMainConfig(){
    return require 'main.php';
}

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

 return  array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
    'import'=>array(
        'application.components.Helpers'
    ),
	// application components
	'components'=>array(
        'db'=>getMainConfig()['components']['db'],
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);
