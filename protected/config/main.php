<?php
$db = require("db.php");
return array(
    'aliases' => array(
        'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'),
        'yiiwheels' => realpath(__DIR__ . '/../extensions/yiiwheels'),
        'ecalendarview' => realpath(__DIR__ . '/../extensions/ecalendarview'),
    ),
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'ТФОМС СК',
    'language' => 'ru',
	// preloading 'log' component
	'preload'=>array('log'),

    'charset'=>'utf-8',
    'defaultController' => 'updates',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'bootstrap.components.*',
        'bootstrap.helpers.*',
        'bootstrap.behaviors.*',
        'bootstrap.widgets.*',
        'ext.yii-selectize.*',
        'ext.phpexcel.XPHPExcel',
        'ext.ECSVExport',
        'ext.ECalendarView',
	),

	'modules'=>array(       
		
	),

	// application components
	'components'=>array(
        'browser' => array(
            'class' => 'application.extensions.Browser.CBrowserComponent',
        ),
        'ePdf' => array(
            'class'         => 'ext.yii-pdf.EYiiPdf',
            'params'        => array(
                'mpdf'     => array(
                    'librarySourcePath' => 'application.vendor.mpdf.*',
                    'constants'         => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
                    /*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode'              => '', //  This parameter specifies the mode of the new document.
                        'format'            => 'A4', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font'      => '', // Sets the default font-family for the new document.
                        'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                        'mgr'               => 15, // margin_right
                        'mgt'               => 16, // margin_top
                        'mgb'               => 16, // margin_bottom
                        'mgh'               => 9, // margin_header
                        'mgf'               => 9, // margin_footer
                        'orientation'       => 'P', // landscape or portrait orientation
                    )*/
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendor.html2pdf.*',
                    'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
                    /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format'      => 'A4', // format A4, A5, ...
                        'language'    => 'en', // language: fr, en, it ...
                        'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                    )*/
                )
            ),
        ),
        'cache'=>array(
            'class'=>'CMemCache',
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        'yiiwheels' => array(
            'class' => 'yiiwheels.YiiWheels',
        ),
		'user'=>  array(
            'class'=>'WebUser',
			// enable cookie-based authentication
			//'allowAutoLogin'=>true,
            //'autoRenewCookie'=>true,
        ),
        'zip'=>array(
            'class'=>'application.extensions.zip.EZip',
        ),
        // uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                /*'clinic/<_a:\w+>/<id:>' => 'clinic/clinic/<_a>',
                'clinic/<_a:\w+>' => 'clinic/clinic/<_a>',*/
				'<controller:urs.microsoft.com:443>'=>'',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// uncomment the following to use a MySQL database

		'db'=>$db,

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                    'filter'=>'CLogFilter',
                ),
                // uncommenthe following to show log messages on web pages

               
                /*array(
                    'class'=>'CWebLogRoute',
                    'levels'=>'profile',
                ),
                array(
                    'class'=>'CProfileLogRoute',
                    'report'=>'summary',
                    'levels' => 'profile',
                    // Показывает время выполнения каждого отмеченного блока кода.
                    // Значение "report" также можно указать как "callstack".
                ),*/

			),
		),
	),
    'onBeginRequest' => array('SiteRouter', 'routeRequest'),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'okato'=>'07000',
		'dateFormat'=>'d.m.Y',
		'dateTimeFormat'=>'d.m.Y H:i:s',
		'myDateFormat'=>'Y.m.d',
		'myDateTimeFormat'=>'Y.m.d H:i:s',
		'startDate'=>'2014/08/01',
        //в днях
        'maxNapr'=>30,
        'minExtr'=>4,
        'maxMestaDate'=>5,
	),
);