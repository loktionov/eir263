<?php

class AdminController extends Controller
{

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?'),
            ),
            array('allow',
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetrandpass(){
        $br = rand(0,100);
        $pass = Helpers::getPass();
        for($i = 0; $i<=$br;$i++){ $pass = Helpers::getPass(); }
        echo $pass;
    }
    public function actionRenewNnapr()
    {
        $nnaprs = Yii::app()->db->createCommand("select nnapr, count(*) c from pacient
group by nnapr
having count(*) > 1")->queryAll();
        foreach($nnaprs as $nn){
            $nnapr = $nn['nnapr'];
            $pac = Pacient::model()->findAll('nnapr =:nnapr', array(':nnapr'=>$nnapr));
            $i=0;
            foreach($pac as $pacient){
                $i++;
                if($i == 1)
                    continue;

                $cccccc = substr($nnapr, 0, 6);
                $yy = substr($nnapr, 6, 2);
                $n = Yii::app()->db->createCommand()
                    ->select('substring(NNAPR, 9, 6)')
                    ->from('Pacient')
                    ->limit(1)
                    ->order('Convert(int, substring(NNAPR, 9, 6)) desc')
                    ->where('substring(nnapr, 1, 8) = :nnapr', array(':nnapr'=>$cccccc.$yy))
                    ->queryScalar();

                $nnnnnn = str_pad((1 + (int)$n), 6, '0', STR_PAD_LEFT);
                $pacient->NNAPR = $cccccc . $yy . $nnnnnn;
                $pacient->save(false, array('NNAPR'));
            }


        }
    }


/*
    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}