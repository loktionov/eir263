<?php

class CabinetController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    public function actionView($id)
    {
        $this->layout = '//layouts/column2';
        $this->pageTitle = 'Пациент - ' . Yii::app()->name;
        $model = $this->loadModelView($id);
        $proxy = $this->loadProxy($model);
        $this->render('view', array('model' => $model, 'proxy' => $proxy));
    }

    public function actionPrint($id)
    {
        $poc = $this->loadModelView($id);
        $this->renderPartial('print_view', array('data' => $poc));
    }

    public function actionDownload()
    {
        $id = Yii::app()->request->getParam('id');
        $date_range = Yii::app()->request->getParam('date_range_picker');
        $typep = Yii::app()->request->getParam('typep_range');
        $crit = new CDbCriteria();

        // 1 - входящие; 2 - исходящие
        if (Yii::app()->user->getState('role') == WebUser::ROLE_PMO) {
            $id == 1 ? $crit->addCondition('MONAPR = :MO') : $crit->addCondition('MO = :MO');
            $crit->params[':MO'] = Yii::app()->user->getState('kodmo');
        } else if (Yii::app()->user->getState('role') == WebUser::ROLE_SMO) {
            $crit->addCondition('SMO=:SMO');
            $crit->params[':SMO'] = Yii::app()->user->getState('kodmo');
        }
        $DATTR = 'DNAPR';
        if (!empty($typep) AND $typep != 'all' AND $typep != 'extr') {
            {
                $crit->params[':typep'] = $typep;
                $crit->addCondition('typep = :typep');
                switch ($typep) {
                    case 'planed':
                        $DATTR = 'DPGOSP';
                        break;
                    case 'blocked':
                        $DATTR = 'DNGOSP';
                        break;
                    case 'annuled':
                        $DATTR = 'DANUL';
                        break;
                    default:
                        $DATTR = 'DNAPR';
                }
            }
        } elseif ($typep == 'extr') {
            $crit->addCondition('fomp = 3');
        }
        if (!empty($date_range)) {
            list($start, $end) = Helpers::parseRange($date_range);
            $crit->addBetweenCondition($DATTR, $start->format('d.m.Y'), $end->format('d.m.Y 23:59:59'));
        } else {
            $date_range = 'all';
        }
        //$crit->limit = '5000';
        //Yii::beginProfile('findAll');
        $pacient = PacientView::model()->findAll($crit);
        //Yii::endProfile('findAll');

        //Yii::beginProfile('GetXML');
        $sxe = Pacient::GetXml($pacient);
        //Yii::endProfile('GetXML');
        //Yii::app()->end();
        if ($sxe) {
            $name = $date_range . '_' . $typep . '.xml';
            $path = "outfiles/" . rand(1000, 100000) . $name;
            file_put_contents($path, $sxe);
            Helpers::file_force_download($path, $name);
        } else {
            $this->redirect(Yii::app()->request->urlReferrer);
        }
    }

    public function actionIndex($id = 1)
    {
        $this->pageTitle = 'Кабинет - ' . Yii::app()->name;
        $filter_ambulance = 'planed';
        $filter_hospital = 'planed';
        if (in_array(Yii::app()->user->getState('tip'), array(2, 3, 4))) {
            $id = 2;
        }
        if (isset($_GET['typep']) AND $id == 1)
            $filter_ambulance = $_GET['typep'];
        if (isset($_GET['typep']) AND ($id == 2 OR $id == 3))
            $filter_hospital = $_GET['typep'];
        if (isset($_GET['typep_ajax'])) {
            $model = new PacientView();
            if (!empty($_GET['q'])) {
                $regexp = '/^\d{14}$/';
                if (!preg_match($regexp, trim($_GET['q']))) {
                    $model->FAM = $_GET['q'];
                    $model->proxy_fam = $_GET['q'];

                } else {
                    $model->NNAPR = trim($_GET['q']);
                }
            }
            if (!empty($_GET['profk'])) {
                $model->PROFK = $_GET['profk'];
            }
            if (!empty($_GET['mo'])) {
                $model->MO = $_GET['mo'];
            }
            switch ($id) {
                case 1:
                    $this->renderPartial('pacient_grid_ambulance', array('model' => $model, 'id' => 1, 'filter' => $_GET['typep_ajax']));
                    break;
                case 2:
                    $this->renderPartial('pacient_grid_hospital', array('model' => $model, 'id' => 2, 'filter' => $_GET['typep_ajax']));
                    break;
                case 3:
                    $this->renderPartial('pacient_grid_smo', array('model' => $model, 'id' => 2, 'filter' => $_GET['typep_ajax']));
                    break;
            }
            Yii::app()->end();
        }
        $this->render('index', array('filter_ambulance' => $filter_ambulance, 'filter_hospital' => $filter_hospital, 'id' => $id));
    }

    public function actionAnnul()
    {
        $poc = new Pacient();
        $res = false;
        $source = '';
        $action = 'annul';
        $s = Yii::app()->request->getParam('source_id');
        if (isset($_POST['PacientView']) AND !empty($_POST['PacientView']['IDZAP'])) {
            $source = $_POST['PacientView']['IDZAP'];
            $poc = $this->loadModel($source);
            $poc->attributes = $_POST['PacientView'];
            $poc->scenario = $this->action->id;
            if (Yii::app()->user->getState('tip') == 2)
                $poc->IANUL = 1;
            else {
                if ($s == 1)
                    $poc->IANUL = 3;
                else if ($s == 2)
                    $poc->IANUL = 2;
            }
            $poc->KANUL = Yii::app()->user->getState('kodmo');
            $res = $poc->save(true, array('DANUL', 'PANUL', 'IANUL', 'KANUL'));
            if ($res) $this->setState('success-annul', $source, $s);
            else  $this->setState('error', $source, $s);
        } else  $this->setState('warning', $source, $s);

        $res_json = $this->setJsonAnswer($poc, $action, $res, $source, $s);
    }

    public function actionAccept()
    {
        $poc = new Pacient();
        $res = false;
        $source = '';
        $action = 'accept';
        $s = Yii::app()->request->getParam('source_id');
        if (isset($_POST['PacientView']) AND !empty($_POST['PacientView']['IDZAP'])) {
            $source = $_POST['PacientView']['IDZAP'];
            $poc = $this->loadModel($source);
            $poc->attributes = $_POST['PacientView'];
            $poc->scenario = $this->action->id;
            $res = $poc->save(true, array('DNGOSP', 'DPOGOSP', 'DSPO'));
            if ($res) $this->setState('success-accept', $source, $s);
            else  $this->setState('error', $source, $s);
        } else  $this->setState('warning', $source, $s);

        $res_json = $this->setJsonAnswer($poc, $action, $res, $source, $s);
    }

    public function actionOut()
    {
        $poc = new Pacient();
        $res = false;
        $source = '';
        $action = 'out';
        $s = Yii::app()->request->getParam('source_id');
        if (isset($_POST['PacientView']) AND !empty($_POST['PacientView']['IDZAP'])) {
            $source = $_POST['PacientView']['IDZAP'];
            $poc = $this->loadModel($source);
            $poc->attributes = $_POST['PacientView'];
            $poc->scenario = $this->action->id;
            $res = $poc->save(true, array('DOGOSP', 'DS'));
            if ($res) $this->setState('success-out', $source, $s);
            else  $this->setState(TbHtml::ALERT_COLOR_ERROR, $source, $s);
        } else  $this->setState(TbHtml::ALERT_COLOR_WARNING, $source, $s);
        $res_json = $this->setJsonAnswer($poc, $action, $res, $source, $s);

    }

    public function actionRollback($id)
    {
        $poc = $this->loadModel($id);
        $poc->scenario = $this->action->id;
        $source = $poc->IDZAP;
        $action = 'rollback';
        $s = Yii::app()->request->getParam('source_id');
        $poc_history = PacientHistory::model()->find(array('condition' => 'IDZAP=:IDZAP', 'order' => 'ID DESC', 'params' => array(':IDZAP' => $id)));
        if (empty($poc_history))
            return false;
        $poc->attributes = $poc_history->attributes;
        $res = $poc->save();
        if ($res) $this->setState('success-rollback', $source, $s);
        else  $this->setState(TbHtml::ALERT_COLOR_ERROR, $source, $s);
        $res_json = $this->setJsonAnswer($poc, $action, $res, $source, $s);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pacient the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Pacient::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }
        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pacient the loaded model
     * @throws CHttpException
     */
    public function loadModelView($id)
    {
        $model = PacientView::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }
        return $model;
    }

    /**
     * @param string $mes Сообщение
     * @param null $id IDZAP
     * @param bool $s Источник действия
     */
    public function setState($mes = '', $id = null, $s = false)
    {
        $rollback_link = '';
        $fio = 'Пациент';
        if (!empty($id)) {
            $poc = $this->loadModel($id);
            $fio .= ' ' . TbHtml::i($poc->FAM . ' ' . $poc->IM . ' ' . $poc->OT);

        }
        if (strpos($mes, 'rollback') === false and !empty($id)) {
            $url = Yii::app()->createAbsoluteUrl('cabinet/rollback', array('id' => $id));
            $link_id = $mes . $id;
            $rollback_link = TbHtml::link('отменить изменения', 'javascript:void(0)', array(
                'id' => $link_id,
                'class' => 'java-link',
            ));
            $rollback_link .= <<<ROLL
<script>
\$('body').on('click','#$link_id',function(){\$.ajax({'data':{source_id:'$s'},'dataType':'json','success':function(data){

                    UpdatePage('{$this->route}', undefined, data);
                },'url':'$url','cache':false});return false;});
</script>
ROLL;

        }
        switch ($mes) {
            case TbHtml::ALERT_COLOR_ERROR:
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR,
                    '<strong>Ошибка!</strong> Направление не отредактировано.');
                break;
            case TbHtml::ALERT_COLOR_WARNING:
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_WARNING,
                    '<strong>Внимание!</strong> Направление не отредактировано.');
                break;
            case 'success-accept':
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                    "<strong>Хорошо!</strong> {$fio} госпитализирован. " . $rollback_link);
                break;
            case 'success-out':
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                    "<strong>Хорошо!</strong> {$fio} выписан. " . $rollback_link);
                break;
            case 'success-annul':
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                    "<strong>Хорошо!</strong> {$fio} аннулирован. " . $rollback_link);
                break;
            case 'success-rollback':
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                    'Изменения отменены.');
                break;
        }
    }

    /**
     * @param $poc
     * @param $action
     * @param $res
     * @param $source
     * @param bool $s
     * @internal param $res_json
     * @return mixed
     */
    public function setJsonAnswer($poc, $action, $res, $source, $s = false)
    {

        $res_json = array();
        $res_json['save'] = $res;
        $res_json['source_id'] = $source;
        $res_json['source_action'] = $action;
        if ($s or !$res) {
            $res_json['flashes'] = $this->renderPartial('_flashes', array('model' => $poc, 'action' => $action), true, true);
        }
        echo json_encode($res_json);
        Yii::app()->end();
    }

    /**
     * @param Pacient $poc
     * @return ProxyMan
     */
    public function loadProxy($poc)
    {
        if (!$poc->isNewRecord) {
            $proxy = ProxyMan::model()->find('idzap=:idzap', array(':idzap' => $poc->IDZAP));
        }
        if (!isset($proxy))
            $proxy = new ProxyMan();
        return $proxy;
    }
}