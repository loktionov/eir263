<?php

class ClinicController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?'),
            ),
            array(
                'deny',
                'actions' => array(
                    'bulletin',
                    'bulletinUpdate'
                ),
                'expression' => function ($user) {
                        if ($user->getState("role") != WebUser::ROLE_PMO AND $user->getState("role") != WebUser::ROLE_MO) {
                            return true;
                        } else {
                            return false;
                        }
                    },
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->pageTitle = 'Пациент - ' . Yii::app()->name;
        //$model_bulletin = new BulletinForm();
        //$model_mo = new Mo();
        $mesta = array();
        $pmo_name = '';
        $profk_name = '';
        $okato_name = '';
        $typek_name = '';
        $remember_params = false;

        if (!empty($_GET['remember_params'])) {
            Yii::app()->request->cookies['remember_params'] = new CHttpCookie('remember_params', serialize($_GET));
            $remember_params = true;
        } else if (!empty($_GET)) {
            unset(Yii::app()->request->cookies['remember_params']);
        }
        $params = Helpers::GetSafeCockie('remember_params');
        if (empty($_GET) && !empty($params)) {
            $_GET = unserialize($params);
            $remember_params = true;
        }
        if (!empty($_GET['pmoview'])) {
            $mesta['PMO'] = $_GET['pmoview'];
            $kodpmo = $_GET['pmoview'];

        } else if (Yii::app()->user->hasState('kodpmo') and !isset($_GET['pmoview'])) {
            $kodpmo = Yii::app()->user->getState('kodpmo');
            $mesta['PMO'] = Yii::app()->user->getState('kodpmo');
        }
        if (!empty($kodpmo)) {
            $pmoView = new PMOView();
            $pmo_name = $pmoView->find('KODPMO = :pmo', array(':pmo' => $kodpmo));
        }

        if (!empty($_GET['okato'])) {
            $mesta['KODOKATO'] = $_GET['okato'];
            $okato_name = new RefFilial();
            $okato_name = $okato_name->find('OKATO = :okato', array(':okato' => $_GET['okato']));
        }
        if (!empty($_GET['profkview'])) {
            $mesta['PROFK'] = $_GET['profkview'];
            $profKView = new ProfKView();
            $profk_name = $profKView->find('IDPK = :pk', array(':pk' => $_GET['profkview']));
        }
        if (!empty($_GET['tpk'])) {
            $mesta['typek'] = $_GET['tpk'];
            switch ($_GET['tpk']) {
                case 'skd':
                    $typek_name = 'Детские';
                    break;
                case 'skm':
                    $typek_name = 'Мужские';
                    break;
                case 'skw':
                    $typek_name = 'Женские';
                    break;
            }
        }
        $this->render('index', array(
            'mesta' => $mesta,
            'pmo_name' => $pmo_name,
            'profk_name' => $profk_name,
            'okato_name' => $okato_name,
            'typek_name' => $typek_name,
            'remember_params' => $remember_params,
        ));
    }

    public function actionBulletin($idmesto = null)
    {
        $this->pageTitle = 'Направление - ' . Yii::app()->name;
        $dt = !empty($_GET['dt']) ? date(Helpers::getDateFormat(), strtotime($_GET['dt'])) : null;
        $poc = new Pacient();
        $poc->DPGOSP = $dt;
        $proxy = $this->loadProxy($poc);
        if (!empty($_POST['pmoview']) && !empty($_POST['profkview']) && !empty($_POST['Pacient'])) {
            $poc->attributes = $_POST['Pacient'];
            $poc->PMO = $_POST['pmoview'];
            $poc->PROFK = @$_POST['profkview'];
            $poc->MEDRAB = empty($_POST['medrab']) ? null : $_POST['medrab'];
            $poc->DNAPR = date(Helpers::getDateFormat());
            $poc->DR = !empty($poc->DR) ? date(Helpers::getDateFormat(), strtotime($poc->DR)) : null;
            /** @var $profk ProfkView */
            $profk = ProfKView::model()->findByPk($poc->PROFK);
            if (empty($profk))
                throw new Exception('Неверный код Профиля койки ' . var_dump($_POST));
            $poc->PROFO = $profk->IDPO;


            $poc->MO = empty($_POST['kodmo']) ? Yii::app()->user->getState('kodmo') : $_POST['kodmo'];
            /** @var $mo Mo */
            $mo = PMOView::model()->find('KODPMO=:kodpmo', array(':kodpmo' => $poc->PMO));
            if (empty($mo))
                throw new Exception('Неверный код МО ' . var_dump($_POST['pmoview']));
            $poc->MO = $mo->KODMO;

            $poc->MONAPR = Yii::app()->user->GetState('kodmo');
            $poc->PMONAPR = Yii::app()->user->GetState('kodpmo');

            $valid = true;
            if(isset($_POST['proxy-button']))
            {
                $proxy->attributes = $_POST['ProxyMan'];
                $proxy->idzap = $poc->IDZAP;
                $valid = $proxy->validate();
            }
            $valid = $poc->validate() && $valid;
            if ($valid) {
                $poc->save(false);
                if(isset($_POST['proxy-button']))
                {
                    $proxy->idzap = $poc->IDZAP;
                    $proxy->save(false);
                }
                $params = $poc->FOMP == 3 ? array('fomp' => 3) : array('idmesto' => $idmesto, 'dt' => $dt);
                $link = Yii::app()->createAbsoluteUrl('clinic/bulletin', $params);
                $a = TbHtml::link('Можно выдать новое направление с теми же параметрами.', $link);
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, '<strong>Хорошо!</strong> Направление выдано. ' . $a);
                $this->redirect(Yii::app()->createAbsoluteUrl('cabinet/view', array('id' => $poc->IDZAP)));
            } else {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR,
                    '<strong>Ошибка!</strong> проверьте правильность заполнения полей');
            }

        }
        $kodpmo = null;
        $nampmo = null;
        $kodprofk = null;
        $namprofk = null;
        $fomp = intval(@$_GET['fomp']) < 3 ? 1 : 3;
        if (!empty($idmesto)) {
            /** @var $mesto MestoView */
            $mesto = MestoView::model()->findByPk($idmesto);
            $kodpmo = $mesto->PMO;
            $nampmo = $mesto->NAMPMO;
            $kodprofk = $mesto->PROFK;
            $namprofk = $mesto->NAMPK;
        } else {
            //$fomp = 3;
            $kodpmo = Yii::app()->user->getState('kodpmo');
            $nampmo = Yii::app()->user->getState('nampmo');
            if (!$poc->isNewRecord) {
                $kodprofk = $poc->PROFK;
                $namprofk = ProfKView::model()->findByPk($poc->PROFK)->NAMPK;
            }
        }

        $poc->FOMP = $fomp;
        $p = Peoplesearch::model();
        $r = Reestrsearch::model();

        $now = new DateTime(date('d-m-Y'));
        $dnapr = date_create_from_format('d.m.Y', $dt);
        $isfuture = $dnapr > $now->modify('+' . Yii::app()->params['maxNapr'] . ' days');


        $rs_form = $this->renderPartial('application.views.rssearch.index', array('peoplemodel' => $p, 'reestrmodel' => $r, 'dataProvider' => null,), true);

        $this->render('bulletin', array(
            'model' => $poc,
            'kodpmo' => $kodpmo,
            'nampmo' => $nampmo,
            'kodprofk' => $kodprofk,
            'namprofk' => $namprofk,
            'dt' => $dt,
            'rs' => $rs_form,
            'isfuture' => $isfuture,
            'proxy' => $proxy,
        ));

    }

    public function actionBulletinUpdate($id)
    {
        $this->pageTitle = 'Направление - ' . Yii::app()->name;
        $poc = $this->loadModel($id);
        $proxy = $this->loadProxy($poc);
        if (isset($_POST['pmoview']) && isset($_POST['profkview']) && isset($_POST['Pacient'])) {
            $poc->attributes = $_POST['Pacient'];
            $poc->PMO = $_POST['pmoview'];
            $poc->PROFK = @$_POST['profkview'];
            $poc->MEDRAB = empty($_POST['medrab']) ? null : $_POST['medrab'];
            $poc->DNAPR = date(Helpers::getDateFormat());
            $poc->DR = !empty($poc->DR) ? date(Helpers::getDateFormat(), strtotime($poc->DR)) : null;
            $profk = ProfKView::model()->find('IDPK=:pk', array(':pk' => $poc->PROFK));
            $poc->PROFO = $profk->IDPO;
            $poc->MO = empty($_POST['kodmo']) ? Yii::app()->user->getState('kodmo') : $_POST['kodmo'];
            $mo = PMOView::model()->find('KODPMO=:kodpmo', array(':kodpmo' => $poc->PMO));
            $poc->MO = $mo->KODMO;

            //$poc->MONAPR = Yii::app()->user->GetState('kodmo');
            //$poc->PMONAPR = Yii::app()->user->GetState('kodpmo');

            //$this->performAjaxValidation($poc);
            if(isset($_POST['proxy-button']))
            {
                $proxy->attributes = $_POST['ProxyMan'];
                $proxy->idzap = $poc->IDZAP;
                $proxy->save();
            }
            else if ( ! $proxy->isNewRecord){
                $proxy->delete();
            }

            //$this->performAjaxValidation($proxy);

            if ($poc->save() && !$proxy->hasErrors()) {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                    '<strong>Хорошо!</strong> Направление отредактировано.');
                $this->redirect(Yii::app()->createAbsoluteUrl('cabinet/view', array('id' => $poc->IDZAP)));
            } else {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR,
                    '<strong>Ошибка!</strong> проверьте правильность заполнения полей');
            }
        }
        $kodpmo = null;
        $nampmo = null;
        $kodprofk = null;
        $namprofk = null;

        $kodpmo = $poc->PMO;
        $nampmo = PMOView::model()->find('KODPMO=:PMO', array(':PMO' => $poc->PMO));
        if (!empty($nampmo))
            $nampmo = $nampmo->NAMPMO;
        $kodprofk = $poc->PROFK;
        $namprofk = ProfKView::model()->find('IDPK=:IDPK', array(':IDPK' => $poc->PROFK));
        if (!empty($namprofk))
            $namprofk = $namprofk->NAMPK;

        if (!isset($poc)) {
            $poc = new Pacient();

        }
        $p = Peoplesearch::model();
        $r = Reestrsearch::model();

        $rs_form = $this->renderPartial('application.views.rssearch.index', array('peoplemodel' => $p, 'reestrmodel' => $r, 'dataProvider' => null,), true);
        $dt = $poc->DPGOSP;
        $this->render('bulletin', array(
            'model' => $poc,
            'kodpmo' => $kodpmo,
            'nampmo' => $nampmo,
            'kodprofk' => $kodprofk,
            'namprofk' => $namprofk,
            'dt' => $dt,
            'rs' => $rs_form,
            'proxy' => $proxy,
        ));

    }

    /**
     * @param Pacient $poc
     * @return ProxyMan
     */
    public function loadProxy($poc)
    {
        if ( ! $poc->isNewRecord) {
            $proxy = ProxyMan::model()->find('idzap=:idzap', array(':idzap' => $poc->IDZAP));
        }
        if(!isset($proxy))
            $proxy = new ProxyMan();
        return $proxy;
    }

    public function actionGetmo()
    {
        $params = Yii::app()->request->getParam('params');
        $date = Yii::app()->request->getParam('date');
        $s = MestoView::GetMestaSum(new DateTime($date), $params);
        $mesta = new CArrayDataProvider($s, array(
            'id' => 'IDMESTO' . date('d-m-Y', strtotime($date)),
            'keyField' => 'IDMESTO',
            'sort' => array(
                'attributes' => array(
                    'NAMMO', 'D',
                ),
            ),
            'totalItemCount' => count($s),
            'pagination' => array(
                'route' => 'clinic/getmo',
                'pageSize' => 4,
                'params' => array(
                    'date' => $date,
                    'params' => $params,
                ),
            ),
        ));
        if (!isset($_GET['ajax'])) {
            $moform = $this->renderPartial('search_result', array('mesta' => $mesta), true, true);
            echo json_encode($moform);
        } else
            $this->renderPartial('search_result', array('mesta' => $mesta));
    }

    public function actionGetsmo()
    {
        $data = Smo::GetSmoArray(@$_POST['Pacient']['TER']);
        //$options = '';
        $options = CHtml::tag('option', array('value' => '', 'selected' => 'selected'), '');
        foreach ($data as $value => $name) {
            $options .= CHtml::tag('option',
                array('value' => $value), CHtml::encode($name), true);
        }
        echo $options;
    }

    public function actionGetmkb()
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();
        $q = Yii::app()->request->getParam('query');
        $mkb10 = MKB10::GetMkbArrayLike(urldecode($q));
        $res = array();
        if ($mkb10)
            foreach ($mkb10 as $k => $v) {
                $res[] = array('kod' => $k, 'name' => $v);
            }
        echo json_encode($res);
    }

    public function actionGetcalajax()
    {
        $l = setlocale(LC_ALL, Helpers::getLocale());
        Yii::import('ecalendarview.ECalendarViewItem');
        $_GET['date'] = date_create_from_format('d.m.Y', $_GET['date']);
        $data = new ECalendarViewItem($_GET);


        echo $this->renderPartial('cal_view', array('data' => $data), true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pacient the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Pacient::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Pacient|ProxyMan $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && strpos($_POST['ajax'], 'pacient-form') !== false) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}