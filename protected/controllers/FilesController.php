<?php

/**
 * User: локтионов_ав
 * Date: 11.07.14
 * Time: 16:17
 */
class FilesController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny', // allow all users to perform 'index' and 'view' actions
                'users' => array('?'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->pageTitle = 'Файлы - ' . Yii::app()->name;
        $files = null;
        if (Yii::app()->user->hasState('kodmo')) {
            $files = new ImpView();
            $files->KODMO = Yii::app()->user->getState('kodmo');
        }
        $this->render('index', array('files' => $files));
    }

    public function actionDownload($id)
    {
        $this->pageTitle = 'Файлы - ' . Yii::app()->name;
        $file = ImpView::model()->findByPk($id);
        if (empty($file)) {
            Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_WARNING, 'Такого файла не было загружено.');
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $pacient = ImpPacView::model()->findAll('IDIMP=:id', array(':id' => $id));

        if (empty($pacient)) {
            Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_WARNING, 'В файле не было пациентов.');
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $sxe = Pacient::GetXml($pacient);
        if ($sxe) {
            /** @var $file ImpView */
            $name = preg_replace('/^(.+?)\.OMS$/', '$1', $file->NAMF) . '.xml';
            if (empty($name))
                $name = date('d-m-Y') . '.xml';
            $path = "outfiles/" . rand(1000, 100000) . $name;
            file_put_contents($path, $sxe);
            Helpers::file_force_download($path, $name);
        } else {
            $this->redirect(Yii::app()->request->urlReferrer);
        }
    }

    public function CheckSender($b, $c)
    {
        if (array_search((int)$b, array(0, 1)) !== false)
            $model = MO::model();
        elseif ($b == 2)
            $model = SMO::model();
        elseif ($b == 3)
            return true;
        $res = $model->findByPk($c);
        if (!isset($res))
            return false;
        return $c == Yii::app()->user->getState('kodmo');
    }

    public function GetXml($xml, $params)
    {
        list($a, $b, $c, $d, $name) = $params;
        if ($a != 6) {
            $imp = new Imp();
            $imp->IDLOGIN = Yii::app()->user->id;
            $imp->NAMF = $name;
            $imp->TIPDAN = $a;
            $imp->D = date('d-m-Y H:i:s');
            $imp->save();
            return $this->pacientImport($xml, $a, $imp);
        } else {
            $imp = new ImpMesto();
            $imp->save();
            return $this->mestoImport($xml, $a);
        }
    }

    protected function fileExists($fileName, $caseSensitive = true)
    {

        if (file_exists($fileName)) {
            return $fileName;
        }
        if ($caseSensitive) return false;

        // Handle case insensitive requests
        $directoryName = dirname($fileName);
        $fileArray = glob($directoryName . '\*', GLOB_NOSORT);
        $fileNameLowerCase = strtolower($fileName);
        foreach ($fileArray as $file) {
            if (strtolower($file) == $fileNameLowerCase) {
                return $file;
            }
        }
        return false;
    }

    public function CheckFile($tmp_name, $oms_name, $size)
    {
        $res = $res = array(
            'name' => $oms_name,
            'size' => $size,
        );
        if ($this->fileExists($tmp_name, false)) {
            //GD11260079_2014-07-02.OMS
            $q = '/^GD([1-6])([0-3])(26\d{4})_(\d{4}-\d{2}-\d{2})\.OMS$/ui';
            if (!preg_match($q, $oms_name, $matches)) {
                $error = 'Неверное имя файла.';
            } else {
                $a = $matches[1];
                $b = $matches[2];
                $c = $matches[3];
                $d = $matches[4];
                $date = date_create($d);
                if ($date === false) {
                    $error = 'Неверная дата в имени файла';
                } else {
                    if ($date > date_create(date('d-m-Y'))) {
                        $error = 'Дата в имени файла больше сегодняшней.';
                    } else {
                        if (!$this->CheckSender($b, $c)) {
                            $error = 'Неверный код отправителя -' . $c;
                        } else {
                            $newname = 'incoming\\' . $oms_name;

                            if (!copy($tmp_name, $newname)) {
                                $error = 'Внутренняя ошибка сервера. 501';
                            } else {
                                $zip = Yii::app()->zip;
                                if (!$zip->extractZip($newname, 'incoming\\unzip')) {
                                    $error = 'Ошибка распаковки файла.';
                                } else {
                                    $xml_name = 'incoming\\unzip\\' . basename($newname, '.OMS') . '.XML';
                                    if (!$this->fileExists($xml_name, false)) {
                                        $error = 'Неожиданное содержимое архива. Имя архива не совпадает с именем файла.';
                                    } else {
                                        libxml_use_internal_errors(true);
                                        $xml = simplexml_load_file($xml_name);
                                        unlink($xml_name);
                                        if ($xml === false) {
                                            $error = "Содержимое архива не является корректным XML-файлом.\n";
                                            $errors = libxml_get_errors();
                                            foreach ($errors as $err) {
                                                /** @var $err LibXMLError */
                                                $error .= $this->display_xml_error($err, $xml) . "\n";
                                            }
                                            libxml_clear_errors();
                                        } else {
                                            $error = false;
                                            $res['xml'] = $xml;
                                            $res['params'] = array($a, $b, $c, $d, $oms_name,);
                                        }
                                        libxml_use_internal_errors(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $error = 'Ошибка загрузки файла.';
        }
        $res['error'] = $error;
        return array($res);
    }

    public function actionUpload()
    {
        $this->pageTitle = 'Файлы - ' . Yii::app()->name;
        if (isset($_FILES['fileuploadui'])) {
            $res = $this->CheckFile(
                $_FILES['fileuploadui']['tmp_name'],
                strtoupper($_FILES['fileuploadui']['name']),
                $_FILES['fileuploadui']['size']
            );
            if (!$res[0]['error']) {
                $res[0]['model_errors'] = $this->GetXml($res[0]['xml'], $res[0]['params']);
            }
            echo json_encode($res);
        } else {
            echo json_encode(array(array('error' => 'Файл не загружен.')));
        }
        //Yii::app()->end();
    }

    /**
     * @param $xml SimpleXmlElement
     * @param $a Integer
     * @param $imp Imp
     * @throws Exception
     * @return Array
     */
    public function pacientImport($xml, $a, $imp)
    {
        $errors = array();
        $pacients = array();
        foreach ($xml->ZAP as $zap) {
            if ($a == 1 OR $a == 3) {
                $pacient = new Pacient();
            } elseif (!empty($zap->NNAPR)) {
                $pacient = Pacient::model()->find('NNAPR=:NNAPR', array(':NNAPR' => (string)$zap->NNAPR));
            }
            if (empty($pacient)) {
                $pacient = new Pacient();
                $pacient->addError('NNAPR', 'Номер направления не найден в системе «Госпитализация». Направление: "'
                    . (string)$zap->NNAPR . '" следует загрузить файлом GD1 прежде, чем проводить следующие операции.');
                $errors[] = $this->renderPartial('application.views.cabinet._flashes',
                    array('model' => $pacient, 'action' => 'fileload', 'nzap' => $zap->NZAP), true);
                continue;
            }
            $pacient->scenario = 'files';
            $pacient->file = 'T' . $a;
            $attrs = null;
            $proxy = null;
            foreach ($zap->children() as $attr => $data) {
                $attr = strtoupper($attr);
                $data = (string)$data;
                if (empty($data))
                {
                    //todo верно?
                    continue;
                    //$data = null;
                }

                /** @var $scheme SXML */
                $scheme = SXML::model()->find('NAME=:ATTR and IDXML=:t', array(':ATTR' => $attr, ':t' => $pacient->file));

                $is_date = false;
                if (!empty($scheme))
                    $is_date = $scheme->FP == 'D';
                if ($is_date AND !empty($data)) {
                    $data = date(Helpers::getDateFormat(), strtotime($data));
                }
                if ($attr == 'NZAP') {
                    continue;
                }
                if (empty($scheme)) {
                    //$pacient->addError('IDZAP', 'Неизвестный тег: ' . $attr . 'для файла ' . $pacient->file);
                    continue;
                }
                if ($attr == 'FOMP' AND $a == 3 and $data == 1) {
                    $pacient->addError('FOMP', 'В файле экстренной госпитализации пацент с признаком плановой госпитализации.
                    FOMP=1');
                }
                if ($attr == 'FOMP' AND $a == 1 and $data == 3) {
                    $pacient->addError('FOMP', 'В файле плановой госпитализации пацент с признаком экстренной госпитализации.
                    FOMP=3');
                }
                if (strpos($attr, 'PROXY_') !== false) {
                    if (!isset($proxy)) {
                        $proxy = new ProxyMan();
                    }
                    $attr = strtolower(str_replace('PROXY_', '', $attr));
                    if ($proxy->hasAttribute($attr)) {
                        $proxy->$attr = $data;
                    }
                } else {
                    $pacient->$attr = $data;
                    $attrs[] = $attr;
                }
            }
            if (!in_array('IDZAP', $attrs))
                $attrs[] = 'IDZAP';
            if (!in_array('NNAPR', $attrs))
                $attrs[] = 'NNAPR';
            $scheme_required = array();
            $scheme_tmp = Yii::app()->db->createCommand("SELECT [name] from sxml_view where IDXML=:t and require = 1 and name not in ('nzap')")
                ->queryAll(false, array(':t' => $pacient->file));
            foreach ($scheme_tmp as $row => $v) {
                $scheme_required[] = $v[0];
            }
            $attrs = array_unique(array_merge($attrs, $scheme_required));
            $pacient->validate($attrs, false);
            if (!$pacient->hasErrors()) {
                $res = $pacient->save(true, $attrs);
                if (!$res) {
                    throw new Exception("Пациент не сохранен. № записи: $zap->NZAP № направления: {$pacient->NNAPR}");
                }
                if(isset($proxy)){
                    $proxy->idzap = $pacient->IDZAP;
                    $proxy->save();
                }
                $impPacient = new ImpPacient();
                $impPacient->IDIMP = $imp->IDIMP;
                $impPacient->NZAP = (string)$zap->NZAP;
                $impPacient->IDPAC = $pacient->IDZAP;
                $impPacient->save();
            } else {
                $errors[] = $this->renderPartial('application.views.cabinet._flashes',
                    array('model' => $pacient, 'action' => 'fileload', 'nzap' => $zap->NZAP), true);
            }
        }
        $imp->REZ = 1;
        $imp->save();
        return $errors;
    }

    /**
     * @param $xml SimpleXmlElement
     * @param $a
     * @return bool
     */
    private function mestoImport($xml, $a)
    {
        $errors = array();
        $mestos = array();
        foreach ($xml->ZAP as $zap) {
            $mesto = new Mesto();
            foreach ($zap->children() as $attr => $data) {
                $data = (string)$zap->$attr;
                if (empty($data))
                    $data = null;
                $scheme = SXML::model()->find('IDXML = :T AND NAME=:ATTR', array(':T' => "T$a", ':ATTR' => $attr));
                $is_date = false;
                if (!empty($scheme))
                    $is_date = $scheme->FP == 'D';
                if ($is_date AND !empty($data)) {
                    $data = date(Helpers::getDateFormat(), strtotime($data));
                }
                //TODO ZAPSMO Что это за поле
                if ($attr == 'NZAP' OR $attr == 'ZAPSMO' OR empty($scheme)) {
                    continue;
                }
                //$attr = $attr == 'RESERVE' ? strtolower($attr) : strtoupper($attr);
                if (preg_match('/reserve/ui', $attr)) {
                    $attr = strtolower($attr);
                } else {
                    $attr = strtoupper($attr);
                }
                $mesto->$attr = $data;
            }
            if ($mesto->validate()) {
                $mestos[] = $mesto;
            } else {
                $errors[] = $this->renderPartial('application.views.cabinet.mesto_flashes',
                    array('model' => $mesto, 'action' => 'fileload', 'nzap' => (string)$zap->NZAP), true);
            }
        }
        if (empty($errors)) {
            foreach ($mestos as $mesto) {
                /** @var $mesto Mesto */
                $mesto->save();
            }
        }
        return $errors;
    }

    function display_xml_error($error, $xml)
    {
        $return = $xml[$error->line - 1] . "\n";
        $return .= str_repeat('-', $error->column) . "^\n";

        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
            "\n  Line: $error->line" .
            "\n  Column: $error->column";

        if ($error->file) {
            $return .= "\n  File: " . basename($error->file);
        }

        return $return;
    }
}