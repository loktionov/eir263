<?php

class MedrabController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Мед. работник - ' . Yii::app()->name;
        $model = new Medrab;
        $model->MO = Yii::app()->user->getState('kodmo');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Medrab'])) {
            $model->attributes = $_POST['Medrab'];
            if ($model->save())
                $this->redirect(array('medrab/admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Мед. работник - ' . Yii::app()->name;
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Medrab'])) {
            $model->attributes = $_POST['Medrab'];
            if ($model->save())
                $this->redirect(array('medrab/admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        //Medrab::model()->deleteAll('kodmedrab=:id and mo=:mo', array(':id' => $id, ':mo' => Yii::app()->user->getState('kodmo')));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->pageTitle = 'Мед. работник - ' . Yii::app()->name;
        $model = new Medrab('search');
        $model->unsetAttributes(); // clear any default values
        $model->MO = Yii::app()->user->getState('kodmo');
        if (isset($_GET['Medrab']))
            $model->attributes = $_GET['Medrab'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Medrab the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Medrab::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        $model->KODMEDRAB = Helpers::formatSnils($model->KODMEDRAB);
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Medrab $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'medrab-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
