<?php

class MestoController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    //public $defaultAction = 'create';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny', // allow all users to perform 'index' and 'view' actions
                'users' => array('?'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Места - ' . Yii::app()->name;
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Места - ' . Yii::app()->name;
        $model = new Mesto;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Mesto'])) {
            $model->attributes = $_POST['Mesto'];
            $model->PROFK = $_POST['ProfKView'];
            if ($model->save()) {
                EUserFlash::setSuccessMessage('Место добавлено!');
                //$this->redirect(Yii::app()->createAbsoluteUrl('mesto'));
            }
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Места - ' . Yii::app()->name;
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Mesto'])) {
            $model->attributes = $_POST['Mesto'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDMESTO));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Места - ' . Yii::app()->name;
        $model = new Mesto;
        //$dataProvider=new CActiveDataProvider('Mesto');
        if (isset($_POST['Mesto'])) {
            $model->attributes = $_POST['Mesto'];
            //$model->PROFK = $_POST['ProfKView'];
            $model->MO = Yii::app()->user->getState('kodmo');
            if (empty($model->PMO))
                $model->PMO = Yii::app()->user->getState('kodpmo');
            $this->performAjaxValidation($model);
            if ($model->save()) {

                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Места добавлены');
                $this->redirect(Yii::app()->createAbsoluteUrl('mesto'));
            } else {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR, 'Места не добавлены');
            }
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->pageTitle = 'Места - ' . Yii::app()->name;
        $model = new Mesto('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Mesto'])) {
            $model->attributes = $_GET['Mesto'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Mesto the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Mesto::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Mesto $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mesto-form') {
            echo TbActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}