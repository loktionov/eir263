<?php

class PasportController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny', // allow all users to perform 'index' and 'view' actions
                'users' => array('?'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
                'expression' => '$user->getState("tip") < 2',

            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->pageTitle = 'Паспорт - ' . Yii::app()->name;
        $this->layout = '//layouts/column2';
        $kodmo = Yii::app()->user->getState('kodmo');
        $mo = Mo::model()->findByPk($kodmo);
        $pasport = Pasport::model()->find('kodmo=:kodmo', array(":kodmo" => $kodmo));
        $pmo = new PMO();
        $pmo->KODMO = $kodmo;
        $medrab = new Medrab();
        $medrab->MO = $kodmo;
        $this->render('index', array('mo' => $mo, 'pmo' => $pmo, 'medrab' => $medrab, 'pasport' => $pasport));
    }

    public function actionUpdatemo()
    {
        $this->pageTitle = 'Паспорт - ' . Yii::app()->name;
        $kodmo = Yii::app()->user->getState('kodmo');
        if ($kodmo != $_POST['KODMO'])
            throw new Exception('Доступ запрещен', 403);
        /** @var $mo Mo */
        $mo = Mo::model()->findByPk($kodmo);
        $mo->NAMMO = $_POST['NAMMO'];
        if ($mo->save()) {
            Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Название изменено');
        } else {
            Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR, TbHtml::errorSummary($mo));
        }

        $this->redirect(Yii::app()->createAbsoluteUrl('pasport/index'));
    }

    public function actionUpdatePasport()
    {
        if (isset($_POST['Pasport'])) {
            $kodmo = Yii::app()->user->getState('kodmo');
            $pasport = Pasport::model()->find('kodmo=:kodmo', array(":kodmo" => $kodmo));
            $pasport->attributes = $_POST['Pasport'];
            if ($pasport->kodmo != $kodmo)
                throw new Exception('Доступ запрещен', 403);
            if ($pasport->save()) {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Данные внесены');
            } else {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR, TbHtml::errorSummary($pasport));
            }
        }
        $this->redirect(Yii::app()->createAbsoluteUrl('pasport/index'));
    }

    public function loadModel($id)
    {
        $model = PMO::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}