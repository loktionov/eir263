<?php

class PmoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
	}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->pageTitle = 'Подразделение - ' . Yii::app()->name;
        $this->layout='//layouts/column2';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['PMO'])) {
			$model->attributes=$_POST['PMO'];
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}




	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->pageTitle = 'Подразделение - ' . Yii::app()->name;
		$model=new PMO('search');
		$model->unsetAttributes();  // clear any default values
        $model->KODMO = Yii::app()->user->getState('kodmo');
		if (isset($_GET['PMO'])) {
			$model->attributes=$_GET['PMO'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PMO the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PMO::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'Страница не найдена');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PMO $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='pmo-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}