<?php

class ReportController extends Controller
{

    /**
     * @param $report_type
     * @return PHPExcel
     */
    public static function getExcelObject($report_type)
    {
        $objPHPExcel = XPHPExcel::createPHPExcel();
        $xls_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'report_xls' . DIRECTORY_SEPARATOR;
        $xls_path = $xls_dir . self::getName($report_type);
        $serial_path = $xls_dir . self::getSerializedName($report_type);
        if (file_exists($serial_path)) {
            $phpExcel_serialize = file_get_contents($serial_path);
            /** @var  $objPHPExcel PHPExcel */
            $objPHPExcel = unserialize($phpExcel_serialize);
            return $objPHPExcel;
        } else {
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($xls_path);
            $phpExcel_serialize = serialize($objPHPExcel);
            $resource = fopen($serial_path, 'w');
            if ($resource !== false) {
                fwrite($resource, $phpExcel_serialize);
                fclose($resource);
                return $objPHPExcel;
            }
            return $objPHPExcel;
        }
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'mestamo'
                ),
                'expression' => function ($user) {
                        /** @var $user WebUser */
                        if (
                            $user->getState('role') == WebUser::ROLE_MO
                            OR
                            $user->getState('role') == WebUser::ROLE_PMO
                        ) {
                            return true;
                        } else {
                            return false;
                        }
                    }
            ),
            array(
                'allow',
                'expression' => function ($user) {
                        /** @var $user WebUser */
                        if (
                            $user->getState('role') == WebUser::ROLE_TFOMS
                            OR
                            $user->getState('role') == WebUser::ROLE_ADMIN
                        ) {
                            return true;
                        } else {
                            return false;
                        }
                    }
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    const XLS_PLANED = '1_report_planed';
    const XLS_VIOLATION = '2_report_planed_violation_common';
    const XLS_VIOLATION_SMO = '3_report_planed_violation_smo_mo';
    const XLS_VOLUME = '4_report_hospital_volume';
    const XLS_FFOMS = '5_report_ffoms';

    public function actionIndex()
    {

        $rep = new ReportForm(ReportForm::REPORT_ALIAS_DAY, true);
        $this->render('index', array('report' => $rep));
    }

    public function actionGetdaterange()
    {
        if (!Yii::app()->request->isAjaxRequest OR !isset($_POST['category_filter'])) {
            Yii::app()->end();
        }
        $report = new ReportForm();
        echo $report->switchRange($_POST['category_filter'], false);
    }

    public function actionCount_ajax()
    {
        if (!Yii::app()->request->isAjaxRequest OR !isset($_POST['ReportForm'])) {
            Yii::app()->end();
        }
        $report = new ReportForm();
        $report->attributes = $_POST['ReportForm'];
        $report->countAll();
        //$dp = $report->getCommonReportArray(ReportForm::REPORT_CATEGORY_MO_A);
        $this->renderPartial('report_result', array('report' => $report), false, true);
    }

    public function actionGetmolist_ajax()
    {
        if (!Yii::app()->request->isAjaxRequest OR !isset($_POST['ReportForm'])) {
            Yii::app()->end();
        }
        $report = new ReportForm();
        $report->attributes = $_POST['ReportForm'];
        $this->renderPartial('report_selectize_widget', array('report' => $report), false, true);
    }

    public function actionGetpage()
    {
        $cache_id = Yii::app()->request->getParam('cache_id');
        $for = Yii::app()->request->getParam('for');
        $report_attributes = Yii::app()->request->getParam('report_attributes');
        $rawData = Yii::app()->cache->get($cache_id);
        if (empty($report_attributes) && empty($rawData)) {
            Yii::app()->end();
        }
        $report = new ReportForm();
        $report->attributes = unserialize($report_attributes);
        if (!empty($rawData)) {

            $dp = ReportForm::GetDP($rawData);
        } else {
            $dp = $report->getCommonReportArray($for);
        }

        $dp->pagination->params['for'] = $for;
        $dp->pagination->params['cache_id'] = $cache_id;
        $dp->pagination->params['report_attributes'] = serialize($report->attributes);

        $dp->sort->params['for'] = $for;
        $dp->sort->params['cache_id'] = $cache_id;
        $dp->sort->params['report_attributes'] = serialize($report->attributes);

        $this->renderPartial('report_grid_view', array('dp' => $dp, 'for' => $for), false, true);
        Yii::app()->end();
    }

    public function SetFlashAndExitAnalitic($mes, $report_name, $color = TbHtml::ALERT_COLOR_WARNING)
    {
        if ($mes)
            Yii::app()->user->setFlash($color, $mes);
        $this->render('analitic', array('report_name' => $report_name));
        Yii::app()->end();
    }

    public function actionAnaliticMesta()
    {
        $f14 = new ReportF14View();
        if (isset($_GET['ReportF14View']))
            $f14->attributes = $_GET['ReportF14View'];
        $this->render('report_analitic_mesta', array('f14' => $f14));
    }

    public function actionAnalitic($id = 1, $renew_template = true)
    {
        $this->layout = 'column2';
        $anal_types = self::getAnaliticArray();
        if (!isset($anal_types[$id])) {
            throw new Exception("Неверный тип отчета");
        }
        $report_name = $anal_types[$id];
        if (!isset($_POST['report_type'])) {
            $this->SetFlashAndExitAnalitic(false, $report_name);
        }
        $report_type = $_POST['report_type'];
        if (array_search($report_type, array(1, 2, 3, 4, 5)) !== false) {
            if (empty($_POST['kodmo']) and array_search($report_type, array(1, 2, 3)) !== false) {
                $this->SetFlashAndExitAnalitic('Поле СМО не должно быть пустым.', $report_name);
            }
            $report = new ReportForm();
            if (empty($_POST['date_range_alias'])) {
                $this->SetFlashAndExitAnalitic('Поле дата не должно быть пустым.', $report_name);
            }
            if ($_POST['date_range_alias'] == ReportForm::REPORT_ALIAS_OWN)
                $report->date_range = $_POST['report_date_range_picker'];
            else
                $report->date_range = $_POST['date_range_alias'];
            $report->kodmo = @$_POST['kodmo'];
            $report->category = ReportForm::REPORT_CATEGORY_SMO;
            if ($report_type == 3) {
                if (empty($_POST['kodmo_mo_h']) or empty($_POST['kodmo_profk'])) {
                    $this->SetFlashAndExitAnalitic('Все поля должны быть заполнены', $report_name);
                }
                $report->dop_params = array(
                    ReportForm::REPORT_CATEGORY_PROFK => $_POST['kodmo_profk'],
                    ReportForm::REPORT_CATEGORY_MO_H => $_POST['kodmo_mo_h'],
                );
            }
        } else {
            $this->SetFlashAndExitAnalitic('Неверный тип отчета.', $report_name);
        }
        $objPHPExcel = self::getExcelObject($report_type);

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel = self::FillXls($objPHPExcel, $report, $report_type);

        if (!$objPHPExcel) {
            $this->SetFlashAndExitAnalitic('По таким параметрам нет данных', $report_name);
        }
        // Redirect output to a client's web browser (Excel5)$name = $date_range . '_' . $typep . '.xml';


        $name = $this->getName($report_type);
        $path = "outfiles/" . rand(1000, 100000) . $name;

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save($path);
        Helpers::file_force_download($path, $name);

        Yii::app()->end();
    }

    private static function FillFirstReport(PHPExcel $objPHPExcel, ReportForm $report)
    {
        if ($report->category != ReportForm::REPORT_CATEGORY_SMO || empty($report->kodmo))
            return $objPHPExcel;
        /** @var $smo Smo */
        $smo = Smo::model()->findByPk($report->kodmo);
        $namsmo = $report->kodmo == '26000' ? 'Без полиса' : $smo->NAMSMO;
        $report->countAll();
        $objPHPExcel->setActiveSheetIndex()
            ->setCellValue('B2', $namsmo)
            ->setCellValue('C3', $report->date_range_start->format(Helpers::getDateFormat()))
            ->setCellValue('E3', $report->date_range_end->format(Helpers::getDateFormat()))
            ->setCellValue('A6', $report->planed)
            ->setCellValue('B6', $report->blocked + $report->blocked_er)
            ->setCellValue('C6', $report->blocked)
            ->setCellValue('D6', $report->blocked_er)
            ->setCellValue('E6', $report->expired);

        return $objPHPExcel;
    }

    private static function FillSecondReport(PHPExcel $objPHPExcel, ReportForm $report)
    {
        if ($report->category != ReportForm::REPORT_CATEGORY_SMO || empty($report->kodmo))
            return $objPHPExcel;
        $s = $report->getExpiredByMo();
        /** @var $smo Smo */
        $smo = Smo::model()->findByPk($report->kodmo);
        $namsmo = $report->kodmo == '26000' ? 'Без полиса' : $smo->NAMSMO;
        /** @var $sheet  PHPExcel_Worksheet */
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $sheet->setCellValue('C2', $namsmo)
            ->setCellValue('C3', $report->date_range_start->format(Helpers::getDateFormat()))
            ->setCellValue('E3', $report->date_range_end->format(Helpers::getDateFormat()));
        $row_index = 6;
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        foreach ($s as $row) {
            $sheet->setCellValue("B$row_index", $row['nn'])
                ->setCellValue("C$row_index", $row['nammo'])
                ->setCellValue("D$row_index", $row['nampk'])
                ->setCellValue("E$row_index", $row['c']);
            $row_index++;
        }

        return $objPHPExcel;
    }

    private static function FillThirdReport(PHPExcel $objPHPExcel, ReportForm $report)
    {
        if ($report->category != ReportForm::REPORT_CATEGORY_SMO || empty($report->kodmo))
            return $objPHPExcel;
        $s = $report->getExpiredByMo(false);
        if (empty($s))
            return false;
        /** @var $smo Smo */
        $smo = Smo::model()->findByPk($report->kodmo);
        $namsmo = $report->kodmo == '26000' ? 'Без полиса' : $smo->NAMSMO;

        /** @var $mo Mo */
        $mo = Mo::model()->findByPk($report->dop_params[ReportForm::REPORT_CATEGORY_MO_H]);
        $nammo = $mo->NAMMO;

        /** @var $profk Profk */
        $profk = ProfK::model()->findByPk($report->dop_params[ReportForm::REPORT_CATEGORY_PROFK]);
        $namprofk = $profk->NAMPK;

        /** @var $sheet  PHPExcel_Worksheet */
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $sheet->setCellValue('B2', $namsmo)
            ->setCellValue('C5', $report->date_range_start->format(Helpers::getDateFormat()))
            ->setCellValue('E5', $report->date_range_end->format(Helpers::getDateFormat()))
            ->setCellValue('B3', $nammo)
            ->setCellValue('B4', $namprofk);
        $row_index = 8;
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        foreach ($s as $row) {
            $row['DPGOSP'] = empty($row['DPGOSP']) ? '' : date(Helpers::getDateFormat(false, true), strtotime($row['DPGOSP']));
            $row['DNGOSP'] = empty($row['DNGOSP']) ? '' : date(Helpers::getDateFormat(false, true), strtotime($row['DPNOSP']));
            $sheet->setCellValue("A$row_index", $row['FAM'])
                ->setCellValue("B$row_index", $row['IM'])
                ->setCellValue("C$row_index", $row['OT'])
                ->setCellValueExplicit("D$row_index", $row['NPOLIS'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("E$row_index", $row['NNAPR'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("F$row_index", $row['DPGOSP'])
                ->setCellValue("G$row_index", $row['DNGOSP']);
            $row_index++;
        }
        return $objPHPExcel;
    }

    private static function FillFourthReport(PHPExcel $objPHPExcel, ReportForm $report)
    {
        if ($report->category == ReportForm::REPORT_CATEGORY_MO_H and !empty($report->kodmo))
            return $objPHPExcel;
        $s = $report->getVolumeMedHelp();
        if (empty($s))
            return false;


        /** @var $sheet  PHPExcel_Worksheet */
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $sheet
            ->setCellValue('C2', $report->date_range_start->format(Helpers::getDateFormat()))
            ->setCellValue('E2', $report->date_range_end->format(Helpers::getDateFormat()));
        $row_index = 5;
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $i = 0;
        foreach ($s as $row) {
            $i++;
            $sheet->setCellValue("A$row_index", $i)
                ->setCellValue("B$row_index", $row['NAMMO'])
                ->setCellValue("C$row_index", $row['NAMPK'])
                ->setCellValue("D$row_index", $row['c']) //количество госпитализаций
                ->setCellValue("E$row_index", $row['s']); //объем койко-дней
            $row_index++;
        }
        return $objPHPExcel;
        //return false;
    }

    private static function FillFifthReport(PHPExcel $objPHPExcel, ReportForm $report)
    {
        $s = $report->getFomsReport();
        if (empty($s))
            return false;

        list($res, $res_year) = $s;
        /** @var $sheet  PHPExcel_Worksheet */
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $sheet
            ->setCellValue('A3', $report->date_range_start->format(Helpers::getDateFormat())
                . ' — ' .
                $report->date_range_end->format(Helpers::getDateFormat()))
            ->setCellValue('E36', $res['expired'])
            ->setCellValue('E38', $res['avg']);

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        for ($row_index = 10; $row_index <= 29; $row_index++) {

            $x = $sheet->getCell('C' . $row_index)->getValue();
            if (array_search($x, array('x', 'х')) !== false) {
                continue;
            }
            $ix = $sheet->getCell('B' . $row_index)->getValue();

            $sheet->setCellValue("C$row_index", isset($res[$ix]) ? $res[$ix] : 'x')
                ->setCellValue("D$row_index", isset($res_year[$ix]) ? $res_year[$ix] : 'x');

        }
        return $objPHPExcel;
    }

    /**
     * @param \PHPExcel $objPHPExcel
     * @param ReportForm $report
     * @param $report_type
     * @return mixed
     */
    private static function FillXls(PHPExcel $objPHPExcel, ReportForm $report, $report_type)
    {
        switch ($report_type) {
            case 1:
                return self::FillFirstReport($objPHPExcel, $report);
            case 2:
                return self::FillSecondReport($objPHPExcel, $report);
            case 3:
                return self::FillThirdReport($objPHPExcel, $report);
            case 4:
                return self::FillFourthReport($objPHPExcel, $report);
            case 5:
                return self::FillFifthReport($objPHPExcel, $report);
            default:
                return $objPHPExcel;
        }
    }

    static private function getSerializedName($type)
    {
        return self::getName($type, '_serialize.txt');
    }

    static private function getName($type, $ext = '.xls')
    {
        $analitic = self::getAnaliticArray();
        $type = (int)$type;
        if (!isset($analitic[$type]))
            return false;
        return $analitic[$type] . $ext;
    }

    private static function getAnaliticArray()
    {
        return array(
            1 => self::XLS_PLANED,
            2 => self::XLS_VIOLATION,
            3 => self::XLS_VIOLATION_SMO,
            4 => self::XLS_VOLUME,
            5 => self::XLS_FFOMS,
        );
    }

    public function actionMesta()
    {
        $rep = new ReportForm(ReportForm::REPORT_ALIAS_DAY, true);
        $rep->category = ReportForm::REPORT_CATEGORY_MO_H;
        $dp = new CArrayDataProvider(array());
        $sk = 0;
        if (!empty($_POST['kodmo'])) {
            list($dp, $sk) = $rep->getMestaReportArray($_POST['kodmo']);
            $f14 = F14::model()->find('mo=:mo', array(':mo' => $_POST['kodmo']));
        }
        $f14 = empty($f14) ? new F14() : $f14;
        $this->render('mesta', array('report' => $rep, 'dp' => $dp, 'sk' => $sk, 'f14' => $f14));
    }

    public function actionMestaMo()
    {
        $mo = Yii::app()->user->getState('kodmo');
        $rep = new ReportForm(ReportForm::REPORT_ALIAS_DAY, true);
        $rep->category = ReportForm::REPORT_CATEGORY_MO_H;
        //$dp = new CArrayDataProvider(array());
        //$sk = 0;
        list($dp, $sk) = $rep->getMestaReportArray($mo);
        $f14 = F14::model()->find('mo=:mo', array(':mo' => $mo));
        $f14 = empty($f14) ? new F14() : $f14;
        $this->render('mesta', array('report' => $rep, 'dp' => $dp, 'sk' => $sk, 'f14' => $f14));
    }

    public function actionDownloadMesta()
    {
        //todo скачивание мест
        return false;
        $cmd = Yii::app()->db->createCommand("SELECT * FROM track_test LIMIT 10");
        $csv = new ECSVExport($cmd);
        Yii::app()->getRequest()->sendFile('pacient.csv', $csv->toCSV(null, ';'), "text/csv");
    }

    public function actionTest()
    {
        return false;
    }
    /*public function actionF8xls($q = 4, $rd = true, $rt = true, $y = 2014)
    {
        $objPHPExcel = XPHPExcel::createPHPExcel();
        $xls_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'report_xls' . DIRECTORY_SEPARATOR;
        $xls_path = $xls_dir . self::getName(1);

            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($xls_path);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        // Redirect output to a client web browser (Excel5)
        header('Content-Type: text/plain; charset=Windows-1251', true);
        header('Content-Disposition: attachment;filename="f8_.txt"');
        echo 'Привет';
        //header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        //header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        //header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        //header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //header('Pragma: public'); // HTTP/1.0
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save('php://output');
        //$this->render('tmp');
        //Yii::app()->end();
    }*/


}


