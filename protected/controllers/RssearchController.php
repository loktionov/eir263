<?php

/**
 * Class RssearchController
 * @var $p Peoplesearch
 */
class RssearchController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('deny', // allow all users to perform 'index' and 'view' actions
                'users' => array('?'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionIndex()
    {
        $p = Peoplesearch::model();
        $p->setAttributes(@$_POST['Peoplesearch']);

        $r = Reestrsearch::model();
        $r->setAttributes(@$_POST['Reestrsearch']);

        $res = $p->sqlsearch($r);
        $peoplesDataProvider = $res[0];
        $mes = $res[1];
        foreach($mes as $k=>$v)
        {
            Yii::app()->user->setFlash($k,$v);
        }

        $this->render('index', array('peoplemodel' => $p, 'reestrmodel' => $r, 'dataProvider' => $peoplesDataProvider,));
    }

    public function actionAjaxsearch()
    {
        $p = new Peoplesearch();
        $p->setAttributes(@$_POST['Peoplesearch']);

        $r = new Reestrsearch();
        $r->setAttributes(@$_POST['Reestrsearch']);

        $res = $p->sqlsearch($r);
        $peoplesDataProvider = $res[0];
        $mes = $res[1];
        foreach($mes as $k=>$v)
        {
            Yii::app()->user->setFlash($k,$v);
        }
        $this->renderPartial('application.views.rssearch._result_list', array('dataProvider'=>$peoplesDataProvider), false, true);
    }
}