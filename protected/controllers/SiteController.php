<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    private function actionExcelFromTemplate()
    {
        $objPHPExcel = XPHPExcel::createPHPExcel();
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load(
            Yii::app()->basePath . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . "f8_template.xls"
        );
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Html');
        ob_start();
        $objWriter->save('php://output');
        $res = ob_get_clean();
        $doc = new DOMDocument();
        $doc->loadHTML($res);
        $style = '';
        $styleList = $doc->getElementsByTagName('style');
        for ($i = 0; $i < $styleList->length; $i++) {
            $style .= $styleList->item($i)->C14N();
        }
        $body = $doc->getElementsByTagName('table')->item(0)->C14N();

        $this->render('tmp', array('body' => $style . $body));
    }

    private function actionCreateExcel()
    {
        $objPHPExcel = XPHPExcel::createPHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");


// Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

// Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'ГѓВ©Гѓ ГѓВЁГѓВ№ГѓВўГѓВЄГѓВ®ГѓВґГѓВ»ГѓВ«ГѓВЇГѓВјГѓВїГѓВ¤ГѓВ¶ГѓВјГѓВ§');

// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a clientГўв‚¬в„ўs web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        //header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        //header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        //header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        Yii::app()->end();
    }


    public function actionManual()
    {
        $this->layout = 'column2left';
        $this->pageTitle = 'Инструкция - ' . Yii::app()->name;
        $this->render('pages/gosp_info');
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    private function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }


    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            $session = new Sessions();
            $session->remote_addr = $_SERVER['REMOTE_ADDR'];
            $session->http_user_agent = $_SERVER['HTTP_USER_AGENT'];
            $session->login_date = date(Helpers::getDateFormat(true));
            $session->session_id = isset(Yii::app()->request->cookies['PHPSESSID']) ? Yii::app()->request->cookies['PHPSESSID']->value : null;
            $session->save();
            if ($model->validate() && $model->login()) {
                $session->idlogin = Yii::app()->user->id;
                $session->save();
                Yii::app()->user->setState('my_session', serialize($session));
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        if (Yii::app()->user->hasState('my_session')) {
            $session = unserialize(Yii::app()->user->getState('my_session'));
        } else {
            $session = new Sessions();
            $session->idlogin = Yii::app()->user->id;
        }
        $session->logout_date = date(Helpers::getDateFormat(true));
        Yii::app()->user->logout();
        $session->save();
        $this->redirect(Yii::app()->homeUrl);
    }

    private function actionDemologin($id)
    {
        if (!Yii::app()->user->isGuest) {
            if (Yii::app()->user->hasState('my_session')) {
                $session = unserialize(Yii::app()->user->getState('my_session'));
            } else {
                $session = new Sessions();
                $session->idlogin = Yii::app()->user->id;
            }
            $session->logout_date = date(Helpers::getDateFormat(true));
            Yii::app()->user->logout(false);
            $session->save();
        }
        $model = new LoginForm();
        $model->rememberMe = false;
        switch ($id) {
            case WebUser::ROLE_PMO:
                $model->username = 'demo1';
                $model->password = 'demo1';
                break;
            case WebUser::ROLE_SMO:
                $model->username = 'demo2';
                $model->password = 'demo2';
                break;
            case WebUser::ROLE_TFOMS:
                $model->username = 'demo3';
                $model->password = 'demo3';
                break;
        }
        $session = new Sessions();
        $session->remote_addr = $_SERVER['REMOTE_ADDR'];
        $session->http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        $session->login_date = date(Helpers::getDateFormat(true));
        $session->session_id = isset(Yii::app()->request->cookies['PHPSESSID']) ? Yii::app()->request->cookies['PHPSESSID']->value : null;
        $session->save();
        $identity = new UserIdentity($model->username, $model->password);
        $identity->authenticate();
        $identity->errorCode = UserIdentity::ERROR_NONE;
        Yii::app()->user->login($identity);
        $this->redirect(Yii::app()->createAbsoluteUrl('/'));
    }

    private function  actionSetNN()
    {
        $poc = Pacient::model()->findAll("SUBSTRING(nnapr, 1, 6)<>monapr OR '20' + SUBSTRING(nnapr, 7, 2)<>YEAR(Dnapr) or LEN(nnapr)<>14");

        foreach ($poc as $p) {

            $p->NNAPR = '';
            $p->save(false, array('NNAPR'));
        }

    }
}