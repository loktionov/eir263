<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Login;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Login'])) {
            $model->attributes = $_POST['Login'];
            if (!empty($_POST['kodmo']))
                if ($model->TIP == 0)
                {
                    $model->KODP = $_POST['kodmo'];
                    /** @var $mo PmoView */
                    $mo = PMOView::model()->findByPk($model->KODP);
                    $model->KOD = $mo->KODMO;
                }
                else
                {
                    $model->KOD = $_POST['kodmo'];
                    $model->KODP = 0;
                }

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->IDLOGIN));
        }

        $this->render('create', array(
            'model' => $model,
            'selectize_data' => $this->getSelectize($model),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Login'])) {
            $model->attributes = $_POST['Login'];
            if (!empty($_POST['kodmo']))
                if ($model->TIP == 0)
                {
                    $model->KODP = $_POST['kodmo'];
                    /** @var $mo PmoView */
                    $mo = PMOView::model()->findByPk($model->KODP);
                    $model->KOD = $mo->KODMO;
                }
                else
                {
                    $model->KOD = $_POST['kodmo'];
                    $model->KODP = 0;
                }

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->IDLOGIN));
        }

        $this->render('update', array(
            'model' => $model,
            'selectize_data' => $this->getSelectize($model),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Login');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Login('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Login']))
            $model->attributes = $_GET['Login'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Login the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Login::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Login $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetKod()
    {
        if (empty($_POST['Login']))
            Yii::app()->end();
        $form = new Login();
        $form->attributes = $_POST['Login'];
        $options = $this->getSelectize($form);
        $options = json_encode(array(
            'options' =>$options['options'],
            'kod' =>$options['kod'],
        ));
        echo $options;
    }

    public function actionGetLastLogin()
    {
        if (empty($_POST['kodmo']))
            Yii::app()->end();
        $form = new Login();
        $form->attributes = $_POST['Login'];
        $where = "kod = :kod";
        if ($form->TIP == 0) {
            $where = "kodp = :kod";

        }
        $r = Yii::app()->db->createCommand(

            "select top 1 LOGIN, IDLOGIN from Login where TIP = :tip and $where order by LOGIN desc"

        )->queryScalar(array(
                ':tip' => $form->TIP,
                ':kod' => $_POST['kodmo'],
            ));
        echo empty($r) ? '' : $r;
    }
    protected function getSelectize($model){
        $placeholder = 'Выберите организацию';
        $optgroup = false;
        $tip = empty($model->TIP) ? 0 : $model->TIP;
        $kod='';
        $data = array();
        $attr_name = 'User[KOD]';
        switch ($tip) {
            case 0:
                $options = PMOView::GetOptionsArrayForUsers();
                $optgroup = array(
                    array('value' => '1', 'label' => 'Места'),
                    array('value' => '0', 'label' => 'Не места'),
                );
                if(!empty($model->KODP))
                {
                    $kod = $model->KODP;
                    /** @var $name PmoView */
                    $name = PMOView::model()->findByPk($model->KODP);
                    $data = array($kod => $name->NAMPMO);
                }
                break;
            case 1:
                $options = Mo::GetOptionsArray(true);
                if(!empty($model->KOD))
                {
                    $kod = $model->KOD;
                    /** @var $name Mo */
                    $name = Mo::model()->findByPk($model->KOD);
                    $data = array($kod => $name->NAMMO);
                }
                break;
            case 2:
                $options = Smo::GetOptionsArray(true, false);
                if(!empty($model->KOD))
                {
                    $kod = $model->KOD;
                    /** @var $name Smo */
                    $name = Smo::model()->findByPk($model->KOD);
                    $data = array($kod => $name->NAMSMO);
                }
                break;
            case 3:
                $options = array(
                    array(
                        'kod' => Yii::app()->params['okato'],
                        'name' => 'ТФОМС',
                    ));
                $kod = Yii::app()->params['okato'];
                $data = array(Yii::app()->params['okato'] => 'ТФОМС');
                break;
            default:
                return false;
        }
        $data = !empty($data) ? $data : array();
        return array(
            'kod'=>$kod,
            'data'=>$data,
            'options'=>$options,
            'placeholder'=>$placeholder,
            'optgroup'=>$optgroup,
            'model'=>$model,
        );
    }

    public function actionGetCard($id, $format='PDF'){
        $user = $this->loadModel($id);
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        /** @var $mPDF1 mPDF */
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');

        # render (full page)
        $mPDF1->WriteHTML($this->renderPartial('card_view', array('model'=>$user), true));

        # Outputs ready PDF
        $mPDF1->Output($user->KOD .'_' . $user->fam . '_' . $user->im . '_' . $user->ot . '.pdf', 'D');

    }
}
