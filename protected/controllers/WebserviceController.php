<?php
class WebserviceController extends CController implements IWebServiceProvider{

    protected $authenticated = false;

    public function actions(){
        return array(
            'quote'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }

    /**
     * ����������� ������� soap
     * @param string $username
     * @param string $password
     * @return boolean result of auth
     * @soap
     */
    public function auth($username, $password)
    {
        // ������������ ����������� �� yii
        $identity = new SoapIdentity($username, $password);
        if( $identity->authenticate() )
        {
            // ����������� ����������� �� ���� ������ � �� ��� ������������
            $this->authenticated = true;
        }
        return true;
    }
    /**
     * @param string $q
     * @param string $t
     * @return string f
     * @soap
     */
    public function test_soap($q, $t){
        return $q . $t;
    }

    /**
     * This method is invoked before the requested remote method is invoked.
     * @param CWebService $service the currently requested Web service.
     * @return boolean whether the remote method should be executed.
     */
    public function beforeWebMethod($service)
    {

            return true;
        // TODO: Implement beforeWebMethod() method.
    }

    /**
     * This method is invoked after the requested remote method is invoked.
     * @param CWebService $service the currently requested Web service.
     */
    public function afterWebMethod($service)
    {
        // TODO: Implement afterWebMethod() method.
    }
}

/**
 * ����� ������, �������� ��������������� ������
 */
class SoapAuth
{
    /** @var int password */
    public $password;
    /** @var int username */
    public $username;

    public function __construct($l, $p)
    {
        $this->password = $p;
        $this->username = $l;
    }
}