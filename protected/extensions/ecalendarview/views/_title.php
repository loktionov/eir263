<?php
/**
 * _title.php
 *
 * @author Martin Ludvik <matolud@gmail.com>
 * @copyright Copyright &copy; 2014 by Martin Ludvik
 * @license http://opensource.org/licenses/MIT MIT license
 */
$l = setlocale(LC_ALL, Helpers::getLocale());
?>

<?php echo iconv(Helpers::getEncode(),'utf-8',strftime('%B', $pagination->getMiddleRelevantPageDate()->getTimestamp()));?>,
<?php echo $pagination->getMiddleRelevantPageDate()->format('Y'); ?>
