/*
 * jQuery File Upload Plugin Localization Example 6.5.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*global window */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Файл слишком большой",
            "minFileSize": "Файл слишком маленький",
            "acceptFileTypes": "Тип файла не поддерживается",
            "maxNumberOfFiles": "Достигнуто максимальное количество файлов",
            "uploadedBytes": "Загружено больше чем размер файла",
            "emptyResult": "Пустой файл"
        },
        "error": "Ошибка",
        "start": "Старт",
        "cancel": "Отмена",
        "destroy": "Удалить"
    }
};
