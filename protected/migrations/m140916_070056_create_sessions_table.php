<?php

class m140916_070056_create_sessions_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('sessions', array(
            'id'=>'int IDENTITY(1,1) NOT NULL',
            'remote_addr'=>'varchar(50) NULL',
            'http_user_agent'=>'varchar(max) NULL',
            'login_date'=>'datetime NULL',
            'idlogin'=>'int NULL',
            'logout_date'=>'datetime NULL',
        ));
	}

	public function down()
	{
		$this->dropTable('sessions');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}