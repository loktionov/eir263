<?php

class m140917_054043_create_sessionid_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('sessions', 'session_id', 'varchar(50)');
	}

	public function down()
	{
		$this->dropColumn('sessions', 'session_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}