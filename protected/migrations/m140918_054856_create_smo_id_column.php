<?php

class m140918_054856_create_smo_id_column extends CDbMigration
{
	public function up()
	{
        //$this->addColumn('smo', 'id', 'int null');
        $this->update('smo', array('id'=>1), 'kodsmo=:kodsmo', array(':kodsmo'=>'26005'));
        $this->update('smo', array('id'=>2), 'kodsmo=:kodsmo', array(':kodsmo'=>'26002'));
	}

	public function down()
	{
		$this->dropColumn('smo', 'id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}