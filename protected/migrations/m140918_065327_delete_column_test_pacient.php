<?php

class m140918_065327_delete_column_test_pacient extends CDbMigration
{
    public function up()
    {
        try {
            $this->dropColumn('pacient', 'test');
        } catch (Exception $e) {
            var_dump($e);
        }
    }

    public function down()
    {
        $this->addColumn('pacient', 'test', 'int null');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}