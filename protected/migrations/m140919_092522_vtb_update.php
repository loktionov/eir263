<?php

class m140919_092522_vtb_update extends CDbMigration
{
	public function up()
	{
        $this->update('smo', array('NAMSMO'=>'ЗАО ВТБ Медицинское страхование'), 'id=:id', array(':id'=>2));
	}

	public function down()
	{
        $this->update('smo', array('NAMSMO'=>'ЗАО МСК "Солидарность для жизни"'), 'id=:id', array(':id'=>2));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}