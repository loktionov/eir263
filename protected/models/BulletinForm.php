<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class BulletinForm extends CFormModel
{
	public $sname;
	public $name;
	public $pname;
	public $dateman;
	public $sexman;
	public $uid;
	public $gdate;
	public $mo;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			//array('name, email, subject, body', 'required'),
			// email has to be a valid email address
			//array('gdate', 'date', 'format' => 'dd-MM-yyyy'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'sname'=>'Фамилия',
			'name'=>'Имя',
			'pname'=>'Отчество',
			'dateman'=>'Дата рождения',
			'sexman'=>'Пол',
			'uid'=>'Номер направления',
			'gdate'=>'Дата госпитализации',
			'mo'=>'Больница',
		);
	}
}