<?php

/**
 * This is the model class for table "imp_pac_view".
 *
 * The followings are the available columns in table 'imp_pac_view':
 * @property integer $IDIMP
 * @property integer $NZAP
 * @property integer $IDZAP
 * @property string $NNAPR
 * @property string $DNAPR
 * @property string $DPGOSP
 * @property string $DNGOSP
 * @property string $VNGOSP
 * @property string $DOGOSP
 * @property string $DANUL
 * @property integer $PANUL
 * @property integer $IANUL
 * @property integer $KANUL
 * @property integer $PMOANUL
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $TEL
 * @property integer $VPOLIS
 * @property string $SPOLIS
 * @property string $NPOLIS
 * @property string $TER
 * @property string $SMO
 * @property integer $FOMP
 * @property integer $MONAPR
 * @property integer $PMONAPR
 * @property integer $PROFONAPR
 * @property integer $PROFKNAPR
 * @property string $DS
 * @property string $MEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property integer $PROFO
 * @property integer $PROFK
 * @property string $NKART
 * @property string $DSPO
 * @property string $DPOGOSP
 * @property string $DSNAPR
 */
class ImpPacView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'imp_pac_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IDZAP', 'required'),
			array('NZAP, IDZAP, PANUL, IANUL, KANUL, PMOANUL, P, VPOLIS, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, MO, PMO, PROFO, PROFK', 'numerical', 'integerOnly'=>true),
			array('NNAPR, TER, SMO', 'length', 'max'=>50),
			array('VNGOSP', 'length', 'max'=>5),
			array('FAM, IM, OT, NKART', 'length', 'max'=>60),
			array('TEL', 'length', 'max'=>40),
			array('SPOLIS', 'length', 'max'=>10),
			array('NPOLIS', 'length', 'max'=>20),
			array('DS, DSPO, DSNAPR', 'length', 'max'=>6),
			array('MEDRAB', 'length', 'max'=>11),
			array('DNAPR, DPGOSP, DNGOSP, DOGOSP, DANUL, DR, DPOGOSP', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NZAP, IDZAP, NNAPR, DNAPR, DPGOSP, DNGOSP, VNGOSP, DOGOSP, DANUL, PANUL, IANUL, KANUL, PMOANUL, FAM, IM, OT, P, DR, TEL, VPOLIS, SPOLIS, NPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, DS, MEDRAB, MO, PMO, PROFO, PROFK, NKART, DSPO, DPOGOSP, DSNAPR', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NZAP' => 'Nzap',
			'IDZAP' => 'Idzap',
			'NNAPR' => 'Nnapr',
			'DNAPR' => 'Dnapr',
			'DPGOSP' => 'Dpgosp',
			'DNGOSP' => 'Dngosp',
			'VNGOSP' => 'Vngosp',
			'DOGOSP' => 'Dogosp',
			'DANUL' => 'Danul',
			'PANUL' => 'Panul',
			'IANUL' => 'Ianul',
			'KANUL' => 'Kanul',
			'PMOANUL' => 'Pmoanul',
			'FAM' => 'Fam',
			'IM' => 'Im',
			'OT' => 'Ot',
			'P' => 'P',
			'DR' => 'Dr',
			'TEL' => 'Tel',
			'VPOLIS' => 'Vpolis',
			'SPOLIS' => 'Spolis',
			'NPOLIS' => 'Npolis',
			'TER' => 'Ter',
			'SMO' => 'Smo',
			'FOMP' => 'Fomp',
			'MONAPR' => 'Monapr',
			'PMONAPR' => 'Pmonapr',
			'PROFONAPR' => 'Profonapr',
			'PROFKNAPR' => 'Profknapr',
			'DS' => 'Ds',
			'MEDRAB' => 'Medrab',
			'MO' => 'Mo',
			'PMO' => 'Pmo',
			'PROFO' => 'Profo',
			'PROFK' => 'Profk',
			'NKART' => 'Nkart',
			'DSPO' => 'Dspo',
			'DPOGOSP' => 'Dpogosp',
			'DSNAPR' => 'Dsnapr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NZAP',$this->NZAP);
		$criteria->compare('IDZAP',$this->IDZAP);
		$criteria->compare('NNAPR',$this->NNAPR,true);
		$criteria->compare('DNAPR',$this->DNAPR,true);
		$criteria->compare('DPGOSP',$this->DPGOSP,true);
		$criteria->compare('DNGOSP',$this->DNGOSP,true);
		$criteria->compare('VNGOSP',$this->VNGOSP,true);
		$criteria->compare('DOGOSP',$this->DOGOSP,true);
		$criteria->compare('DANUL',$this->DANUL,true);
		$criteria->compare('PANUL',$this->PANUL);
		$criteria->compare('IANUL',$this->IANUL);
		$criteria->compare('KANUL',$this->KANUL);
		$criteria->compare('PMOANUL',$this->PMOANUL);
		$criteria->compare('FAM',$this->FAM,true);
		$criteria->compare('IM',$this->IM,true);
		$criteria->compare('OT',$this->OT,true);
		$criteria->compare('P',$this->P);
		$criteria->compare('DR',$this->DR,true);
		$criteria->compare('TEL',$this->TEL,true);
		$criteria->compare('VPOLIS',$this->VPOLIS);
		$criteria->compare('SPOLIS',$this->SPOLIS,true);
		$criteria->compare('NPOLIS',$this->NPOLIS,true);
		$criteria->compare('TER',$this->TER,true);
		$criteria->compare('SMO',$this->SMO,true);
		$criteria->compare('FOMP',$this->FOMP);
		$criteria->compare('MONAPR',$this->MONAPR);
		$criteria->compare('PMONAPR',$this->PMONAPR);
		$criteria->compare('PROFONAPR',$this->PROFONAPR);
		$criteria->compare('PROFKNAPR',$this->PROFKNAPR);
		$criteria->compare('DS',$this->DS,true);
		$criteria->compare('MEDRAB',$this->MEDRAB,true);
		$criteria->compare('MO',$this->MO);
		$criteria->compare('PMO',$this->PMO);
		$criteria->compare('PROFO',$this->PROFO);
		$criteria->compare('PROFK',$this->PROFK);
		$criteria->compare('NKART',$this->NKART,true);
		$criteria->compare('DSPO',$this->DSPO,true);
		$criteria->compare('DPOGOSP',$this->DPOGOSP,true);
		$criteria->compare('DSNAPR',$this->DSNAPR,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ImpPacView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
