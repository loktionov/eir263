<?php

/**
 * This is the model class for table "imp_view".
 *
 * The followings are the available columns in table 'imp_view':
 * @property integer $KODMO
 * @property integer $IDIMP
 * @property integer $IDLOGIN
 * @property integer $pac_count
 * @property string $NAMF
 * @property integer $TIPDAN
 * @property integer $REZ
 * @property string $D
 * @property string $LOGIN
 */
class ImpView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'imp_view';
	}
    public function primaryKey(){
        return 'IDIMP';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODMO, IDIMP, IDLOGIN', 'required'),
			array('KODMO, IDIMP, IDLOGIN, TIPDAN, REZ', 'numerical', 'integerOnly'=>true),
			array('NAMF', 'length', 'max'=>32),
			array('LOGIN', 'length', 'max'=>255),
			array('D', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODMO, IDIMP, IDLOGIN, NAMF, TIPDAN, REZ, D, LOGIN', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODMO' => 'Код МО',
			'IDIMP' => 'Idimp',
			'IDLOGIN' => 'Idlogin',
			'NAMF' => 'Имя файла',
			'TIPDAN' => 'Тип',
			'REZ' => 'Результат',
			'D' => 'Дата',
			'LOGIN' => 'Логин',
			'pac_count' => 'Количество',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('KODMO',$this->KODMO);
		$criteria->compare('IDIMP',$this->IDIMP);
		$criteria->compare('IDLOGIN',$this->IDLOGIN);
		$criteria->compare('NAMF',$this->NAMF,true);
		$criteria->compare('TIPDAN',$this->TIPDAN);
		$criteria->compare('REZ',$this->REZ);
		$criteria->compare('D',$this->D,true);
		$criteria->compare('LOGIN',$this->LOGIN,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'D' => CSort::SORT_DESC,
                )
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ImpView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
