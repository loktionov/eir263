<?php

/**
 * This is the model class for table "Login".
 *
 * The followings are the available columns in table 'Login':
 * @property integer $IDLOGIN
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property integer $TIP
 * @property integer $KOD
 * @property integer $KODP
 * @property string $fam
 * @property string $im
 * @property string $ot
 * @property string $dr
 * @property integer $sex
 * @property string $mail
 */
class Login extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Login';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TIP, KOD, KODP, sex', 'numerical', 'integerOnly'=>true),
			array('LOGIN, PASSWORD, fam, im, ot, mail', 'length', 'max'=>255),
			array('LOGIN, PASSWORD, KOD, KODP', 'required'),
			array('LOGIN', 'unique'),
			array('mail', 'email'),
			array('dr', 'safe'),
			// The following rule is used by search().
			array('IDLOGIN, LOGIN, PASSWORD, TIP, KOD, KODP, fam, im, ot, dr, sex, mail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IDLOGIN' => 'Idlogin',
			'LOGIN' => 'Логин',
			'PASSWORD' => 'Пароль',
			'TIP' => 'Tip',
			'KOD' => 'Kod',
			'KODP' => 'Kodp',
			'fam' => 'Фамилия',
			'im' => 'Имя',
			'ot' => 'Отчество',
			'dr' => 'День рождения',
			'sex' => 'Пол',
			'mail' => 'Электропочта',
		);
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @param bool|array $order
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
	public function search($order=false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IDLOGIN',$this->IDLOGIN);
		$criteria->compare('LOGIN',$this->LOGIN,true);
		$criteria->compare('PASSWORD',$this->PASSWORD,true);
		$criteria->compare('TIP',$this->TIP);
		$criteria->compare('KOD',$this->KOD);
		$criteria->compare('KODP',$this->KODP);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('im',$this->im,true);
		$criteria->compare('ot',$this->ot,true);
		$criteria->compare('dr',$this->dr,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('mail',$this->mail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$order,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Login the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
