<?php

/**
 * This is the model class for table "LoginView".
 *
 * The followings are the available columns in table 'LoginView':
 * @property integer $IDLOGIN
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property integer $TIP
 * @property integer $KOD
 * @property integer $KODP
 * @property string $NAME_RAY
 * @property string $NAMPMO
 * @property string $ADRESPMO
 * @property string $NAMMO
 * @property string $ADRESMO
 * @property string $OKATO
 * @property string $fam
 * @property string $im
 * @property string $ot
 */
class LoginView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'loginview';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IDLOGIN', 'required'),
			array('IDLOGIN, TIP, KOD, KODP', 'numerical', 'integerOnly'=>true),
			array('LOGIN, PASSWORD', 'length', 'max'=>8),
			array('NAME_RAY', 'length', 'max'=>50),
			array('NAMPMO, NAMMO', 'length', 'max'=>150),
			array('ADRESPMO', 'length', 'max'=>254),
			array('ADRESMO', 'length', 'max'=>250),
			array('OKATO', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('IDLOGIN, LOGIN, PASSWORD, TIP, KOD, KODP, NAME_RAY, NAMPMO, ADRESPMO, NAMMO, ADRESMO, OKATO, fam, im, ot', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IDLOGIN' => 'Idlogin',
			'LOGIN' => 'Login',
			'PASSWORD' => 'Password',
			'TIP' => 'Tip',
			'KOD' => 'Kod',
			'KODP' => 'Kodp',
			'NAME_RAY' => 'Name Ray',
			'NAMPMO' => 'Nampmo',
			'ADRESPMO' => 'Adrespmo',
			'NAMMO' => 'Nammo',
			'ADRESMO' => 'Adresmo',
			'OKATO' => 'Okato',
			'fam' => 'Фамилия',
			'im' => 'Имя',
			'ot' => 'Отчество',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IDLOGIN',$this->IDLOGIN);
		$criteria->compare('LOGIN',$this->LOGIN,true);
		$criteria->compare('PASSWORD',$this->PASSWORD,true);
		$criteria->compare('TIP',$this->TIP);
		$criteria->compare('KOD',$this->KOD);
		$criteria->compare('KODP',$this->KODP);
		$criteria->compare('NAME_RAY',$this->NAME_RAY,true);
		$criteria->compare('NAMPMO',$this->NAMPMO,true);
		$criteria->compare('ADRESPMO',$this->ADRESPMO,true);
		$criteria->compare('NAMMO',$this->NAMMO,true);
		$criteria->compare('ADRESMO',$this->ADRESMO,true);
		$criteria->compare('OKATO',$this->OKATO,true);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('im',$this->im,true);
		$criteria->compare('ot',$this->ot,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LoginView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
