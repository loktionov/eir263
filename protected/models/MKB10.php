<?php

/**
 * This is the model class for table "MKB10".
 *
 * The followings are the available columns in table 'mkb10':
 * @property string $KODMKB
 * @property string $NAMMKB
 */
class MKB10 extends CActiveRecord
{
    public function primaryKey()
    {
        return 'KODMKB';
    }

    public static function GetMkbArray()
    {
        //$sql = "SELECT distinct KODMKB, KODMKB + ': ' + NAMKB as NAMMKB FROM [MKB10] ORDER BY NAMMKB";
        $mkb = Yii::app()->db->cache(3600)->createCommand()
            ->selectDistinct("KODMKB, NAMMKB")
            ->from('mkb10')
            ->order('NAMMKB')
            ->queryAll(true);
        $arr = CHtml::listData($mkb, 'KODMKB', 'NAMMKB');
        return $arr;
    }

    public static function GetMkbArrayLike($q = '')
    {
        if (empty($q) OR @strlen($q) < 3)
            return false;
        $mkb = Yii::app()->db->cache(3600)->createCommand()
            ->select("KODMKB, NAMMKB")
            ->from('mkb10')
            ->where('KODMKB LIKE :Q OR NAMMKB LIKE :N', array(':Q' => "%$q%", ':N' => "%$q%"))
            ->order('NAMMKB');
        $mkb = $mkb->queryAll(true);
        $arr = CHtml::listData($mkb, 'KODMKB', 'NAMMKB');
        return $arr;
    }

    public static function GetMkbRow($q = '')
    {
        if (empty($q))
            return false;
        $mkb = Yii::app()->db->cache(3600)->createCommand()
            ->select("NAMMKB")
            ->from('mkb10')
            ->where('KODMKB = :Q', array(':Q' => "$q"))
            ->queryRow(true);
        return $mkb;
    }

    public static function GetMkbRecentRows($f=1)
    {
        if (!Yii::app()->user->hasState('kodpmo'))
            return array();
        $pmo = Yii::app()->user->getState('kodpmo');
        $ds = 'dsnapr';
        if($f==3)
            $ds = 'dspo';
        $mkb = Yii::app()->db->cache(3600)->createCommand("select top 10 KODMKB, NAMMKB from mkb10 m
            inner join (
                select $ds as DS, count(*) c from Pacient p
                where PMONAPR=:pmo and fomp=:f
                group by $ds
            ) p on p.DS = m.KODMKB
            order by c desc")
            ->queryAll(true, array(':pmo'=>$pmo, ':f'=>$f));
        $arr = CHtml::listData($mkb, 'KODMKB', 'NAMMKB');
        return $arr;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mkb10';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('KODMKB', 'length', 'max' => 10),
            array('NAMMKB', 'length', 'max' => 254),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('KODMKB, NAMMKB', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODMKB' => 'Kodmkb',
            'NAMMKB' => 'Nammkb',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODMKB', $this->KODMKB, true);
        $criteria->compare('NAMMKB', $this->NAMMKB, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MKB10 the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
