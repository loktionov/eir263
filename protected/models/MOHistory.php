<?php

/**
 * This is the model class for table "MO_history".
 *
 * The followings are the available columns in table 'MO_history':
 * @property integer $id
 * @property integer $KODMO
 * @property string $KODOKATO
 * @property string $NAMMO
 * @property string $ADRESMO
 * @property integer $mo_type
 * @property integer $idlogin
 * @property string $idaction
 * @property string $dateaction
 */
class MOHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'MO_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODMO, idlogin, idaction', 'required'),
			array('KODMO, mo_type, idlogin', 'numerical', 'integerOnly'=>true),
			array('KODOKATO', 'length', 'max'=>5),
			array('NAMMO', 'length', 'max'=>150),
			array('ADRESMO', 'length', 'max'=>250),
			array('idaction', 'length', 'max'=>255),
			array('id, KODMO, KODOKATO, NAMMO, ADRESMO, mo_type, idlogin, idaction, dateaction', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'KODMO' => 'Kodmo',
			'KODOKATO' => 'Kodokato',
			'NAMMO' => 'Nammo',
			'ADRESMO' => 'Adresmo',
			'mo_type' => 'Mo Type',
			'idlogin' => 'Idlogin',
			'idaction' => 'Idaction',
			'dateaction' => 'Dateaction',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('KODMO',$this->KODMO);
		$criteria->compare('KODOKATO',$this->KODOKATO,true);
		$criteria->compare('NAMMO',$this->NAMMO,true);
		$criteria->compare('ADRESMO',$this->ADRESMO,true);
		$criteria->compare('mo_type',$this->mo_type);
		$criteria->compare('idlogin',$this->idlogin);
		$criteria->compare('idaction',$this->idaction,true);
		$criteria->compare('dateaction',$this->dateaction,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MOHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
