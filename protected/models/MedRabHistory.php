<?php

/**
 * This is the model class for table "MedRab_history".
 *
 * The followings are the available columns in table 'MedRab_history':
 * @property integer $id
 * @property integer $id_medrab
 * @property string $KODMEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $POST
 * @property integer $idlogin
 * @property string $idaction
 * @property string $dateaction
 */
class MedRabHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'MedRab_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODMEDRAB, idlogin, idaction, id_medrab', 'required'),
			array('MO, PMO, P, idlogin', 'numerical', 'integerOnly'=>true),
			array('KODMEDRAB', 'length', 'max'=>11),
			array('FAM, IM, OT', 'length', 'max'=>40),
			array('POST, idaction', 'length', 'max'=>255),
			array('DR', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, KODMEDRAB, MO, PMO, FAM, IM, OT, P, DR, POST, idlogin, idaction, dateaction', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'KODMEDRAB' => 'Kodmedrab',
			'MO' => 'Mo',
			'PMO' => 'Pmo',
			'FAM' => 'Fam',
			'IM' => 'Im',
			'OT' => 'Ot',
			'P' => 'P',
			'DR' => 'Dr',
			'POST' => 'Post',
			'idlogin' => 'Idlogin',
			'idaction' => 'Idaction',
			'dateaction' => 'Dateaction',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('KODMEDRAB',$this->KODMEDRAB,true);
		$criteria->compare('MO',$this->MO);
		$criteria->compare('PMO',$this->PMO);
		$criteria->compare('FAM',$this->FAM,true);
		$criteria->compare('IM',$this->IM,true);
		$criteria->compare('OT',$this->OT,true);
		$criteria->compare('P',$this->P);
		$criteria->compare('DR',$this->DR,true);
		$criteria->compare('POST',$this->POST,true);
		$criteria->compare('idlogin',$this->idlogin);
		$criteria->compare('idaction',$this->idaction,true);
		$criteria->compare('dateaction',$this->dateaction,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MedRabHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
