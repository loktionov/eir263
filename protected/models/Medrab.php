<?php

/**
 * This is the model class for table "MedRab".
 *
 * The followings are the available columns in table 'MedRab':
 * @property string $KODMEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $id
 */
class Medrab extends CActiveRecord
{
    public static function GetMedRabRow($q, $concat = false)
    {
        if (empty($q))
            return false;
        $select = 'FAM, IM, OT';
        if ($concat)
            $select = 'CONCAT(FAM, IM, OT) as FIO';
        $pmo = Yii::app()->db->cache(3600)->createCommand()
            ->select($select)
            ->from('medrab')
            ->where('KODMEDRAB = :Q', array(':Q' => "$q"))
            ->queryRow(true);
        return $pmo;
    }

    public static function GetOptionsArray()
    {
        //$opts = self::model()->findAll(array('order' => 'NAMPK'));

        if (Yii::app()->user->hasState('kodmo')) {
            $kodmo = Yii::app()->user->getState('kodmo');
            $opts = Yii::app()->db->cache(3600)->createCommand(
                "SELECT distinct concat(FAM , ' ' , IM , ' ' , OT) AS FIO, KODMEDRAB FROM medrab WHERE MO=:MO ORDER BY concat(FAM , ' ' , IM , ' ' , OT)"
            )->queryAll(true, array(':MO' => $kodmo));
        } else
            $opts = Yii::app()->db->cache(3600)->createCommand(
                "SELECT distinct concat(FAM , ' ' , IM , ' ' , OT) AS FIO, KODMEDRAB FROM medrab ORDER BY concat(FAM , ' ' , IM , ' ' , OT)"
            )->queryAll(true);
        $ret = array();
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt['KODMEDRAB'],
                'cat' => $opt['KODMEDRAB'],
                'name' => $opt['FIO'],
            );
        }
        return $ret;
    }

    public static function GetMedrabArray($kodmo = null)
    {
        if (isset($kodmo))
            $medrab = Yii::app()->db->cache(3600)->createCommand(
                "SELECT distinct concat(FAM , ' ' , IM , ' ' , OT) AS FIO, KODMEDRAB FROM medrab WHERE MO=:MO ORDER BY FIO"
            )->queryAll(true, array(':MO' => $kodmo));
        else
            $medrab = Yii::app()->db->cache(3600)->createCommand(
                "SELECT distinct concat(FAM , ' ' , IM , ' ' , OT) AS FIO, KODMEDRAB FROM medrab ORDER BY FIO"
            )->queryAll(true);
        return CHtml::listData($medrab, 'KODMEDRAB', 'FIO');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'medrab';
    }

    /**
     * @return string the associated database table name
     */
    public function primaryKey()
    {
        return 'id';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('KODMEDRAB, P, FAM, IM, OT, DR', 'required'),
            array('KODMEDRAB', 'checkSnils'),
            array('MO, PMO, P', 'numerical', 'integerOnly' => true),
            array('KODMEDRAB', 'length', 'max' => 14),
            array('FAM, IM, OT', 'length', 'max' => 40),
            array('DR', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('KODMEDRAB, MO, PMO, FAM, IM, OT, P, DR', 'safe', 'on' => 'search'),
        );
    }

    public function checkSnils()
    {
        if (!Helpers::checkSNILS($this->KODMEDRAB)) {
            $this->addError('KODMEDRAB', 'Ошибка в СНИЛСе. Неверная контрольная сумма.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODMEDRAB' => 'СНИЛС',
            'MO' => 'Медицинская организация',
            'PMO' => 'Подразделение',
            'FAM' => 'Фамилия',
            'IM' => 'Имя',
            'OT' => 'Отчество',
            'P' => 'Пол',
            'DR' => 'Дата рождения',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODMEDRAB', $this->KODMEDRAB, true);
        $criteria->compare('MO', $this->MO);
        $criteria->compare('PMO', $this->PMO);
        $criteria->compare('FAM', $this->FAM, true);
        $criteria->compare('IM', $this->IM, true);
        $criteria->compare('OT', $this->OT, true);
        $criteria->compare('P', $this->P);
        $criteria->compare('DR', $this->DR, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            //'pagination'=>false,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Medrab the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $this->KODMEDRAB = str_replace('-', '', str_replace(' ', '', $this->KODMEDRAB));
        if (!$this->isNewRecord) {
            $this->historySave();
        }

        return parent::beforeSave();
    }

    public function beforeDelete()
    {
        $this->scenario = 'delete';
        $this->historySave();
        return parent::beforeDelete();
    }

    private function historySave(){
        $old_mo = new MedRabHistory();
        $old_mo->attributes = $this->findByPk($this->id)->attributes;
        $old_mo->idaction = $this->scenario;
        $old_mo->idlogin = Yii::app()->user->id;
        $old_mo->id_medrab = $this->id;
        $old_mo->DR = Yii::app()->dateFormatter->formatDateTime($old_mo->DR, 'medium', null);
        $old_mo->save();
        //$i=0;
    }
}
