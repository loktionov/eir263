<?php

/**
 * This is the model class for table "Mesto".
 *
 * The followings are the available columns in table 'Mesto':
 * @property integer $IDMESTO
 * @property integer $MO
 * @property integer $PMO
 * @property integer $PROFK
 * @property string $D
 * @property integer $NPAC
 * @property integer $IPAC
 * @property integer $OPAC
 * @property integer $PPAC
 * @property integer $SK
 * @property integer $SKM
 * @property integer $SKW
 * @property integer $SKD
 * @property string $DVI
 * @property integer $TEST
 * @property integer $reserve
 */
class Mesto extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mesto';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('SKM, SKW, SKD', 'numerical', 'integerOnly' => true, 'allowEmpty' => true),
            array('SK', 'numerical', 'integerOnly' => true, 'allowEmpty' => false),
            array('MO, PMO, PROFK, D, SK, SKM, SKW, SKD, reserve', 'safe'),
            array('IDMESTO, MO, PMO, PROFK, D, SK, SKM, SKW, SKD', 'safe', 'on' => 'search'),
            array('PMO, PROFK, D, SK', 'required'),
            array('D', 'checkDates'),
            array('reserve', 'checkReserve'),
            array('SK', 'checkSK'),
        );
    }

    public function checkReserve()
    {
        if (empty($this->reserve) or empty($this->SK))
            return;
        if ((int)$this->reserve > (int)$this->SK)
            $this->addError('reserve', 'Резерв не может быть больше общего количества мест.');
    }

    public function checkSK()
    {
        if(empty($this->SK))
            $this->addError('SK', 'Общее количество мест, не может быть пустым');
        if ((int)$this->SK != ((int)$this->SKM + (int)$this->SKW + (int)$this->SKD))
            $this->addError('SK', 'Сумма мест мужские, женские, детские не равна указанному общему количеству мест');
    }

    public function checkDates($attribute, $params)
    {
        if (empty($this->D))
            return false;
        $d = new DateTime($this->D);
        if ($d === false)
            return false;
        $now = new DateTime(date('d-m-Y'));
        if ($d < $now) {
            $this->addError('D', 'Дата должна быть не раньше сегодня. То есть сегодня, завтра и т.д.');
        }
        $maxDate = $now->modify('+' . Yii::app()->params['maxMestaDate'] . ' days');
        if ($d > $maxDate) {
            $this->addError('D', 'Дата должна быть не позже чем ' . $maxDate->format(Helpers::getDateFormat()));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profk_rel' => array(self::HAS_ONE, 'ProfK', array('PROFK'=>'IDPK')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'IDMESTO' => 'Idmesto',
            'MO' => 'Mo',
            'PMO' => 'Pmo',
            'PROFK' => 'Профиль койки',
            'D' => 'Дата',
            'NPAC' => 'Всего пациентов',
            'IPAC' => 'Поступило',
            'OPAC' => 'Выписано',
            'PPAC' => 'Планируется',
            'SK' => 'Количество коек',
            'SKM' => 'Мужские',
            'SKW' => 'Женские',
            'SKD' => 'Детские',
            'DVI' => 'Dvi',
            'TEST' => 'Test',
            'reserve' => 'Резерв',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('IDMESTO', $this->IDMESTO);
        $criteria->compare('MO', Yii::app()->user->getState('kodmo'));
        $criteria->compare('PMO', $this->PMO);
        $criteria->compare('PROFK', $this->PROFK);
        $criteria->compare('D', $this->D, true);
        $criteria->compare('SK', $this->SK);
        $criteria->compare('SKM', $this->SKM);
        $criteria->compare('SKW', $this->SKW);
        $criteria->compare('SKD', $this->SKD);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'IDMESTO' => CSort::SORT_DESC,
                )
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mesto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        foreach ($this->attributes as $attr => $val) {
            if (array_search($attr, array('SK', 'SKM', 'SKW', 'SKD', 'reserve')) !== false)
                if (empty($val))
                    $this->$attr = 0;
        }
        return parent::beforeSave();
    }
}
