<?php

/**
 * This is the model class for table "mesto_view".
 *
 * The followings are the available columns in table 'mesto_view':
 * @property integer $IDMESTO
 * @property integer $SK
 * @property integer $PMO
 * @property integer $PROFK
 * @property string $NAMPMO
 * @property string $NAMMO
 * @property integer $KODMO
 * @property string $KODOKATO
 * @property string $NAMPK
 * @property string $NAMPO
 * @property string $D
 * @property integer $rn
 */
class MestoView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mesto_view';
	}

    public function primaryKey()
    {
        return 'IDMESTO';
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('IDMESTO, KODMO', 'required'),
			array('IDMESTO, SK, SKM, SKD, SKW, PMO, PROFK, KODMO', 'numerical', 'integerOnly'=>true),
			array('NAMPMO, NAMMO, NAMPK', 'length', 'max'=>150),
			array('KODOKATO', 'length', 'max'=>5),
			array('NAMPO', 'length', 'max'=>254),
			array('D', 'safe'),
			array('IDMESTO, SK, PMO, PROFK, NAMPMO, NAMMO, KODMO, KODOKATO, NAMPK, NAMPO, D', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IDMESTO' => 'Idmesto',
			'SK' => 'Sk',
			'PMO' => 'Pmo',
			'PROFK' => 'Profk',
			'NAMPMO' => 'Nampmo',
			'NAMMO' => 'Nammo',
			'KODMO' => 'Kodmo',
			'KODOKATO' => 'Kodokato',
			'NAMPK' => 'Nampk',
			'NAMPO' => 'Nampo',
			'D' => 'D',
		);
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @param null $rn
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
	public function search($rn=null)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IDMESTO',$this->IDMESTO);
		$criteria->compare('SK',$this->SK);
		$criteria->compare('PMO',$this->PMO);
		$criteria->compare('PROFK',$this->PROFK);
		$criteria->compare('NAMPMO',$this->NAMPMO,true);
		$criteria->compare('NAMMO',$this->NAMMO,true);
		$criteria->compare('KODMO',$this->KODMO);
		$criteria->compare('KODOKATO',$this->KODOKATO,true);
		$criteria->compare('NAMPK',$this->NAMPK,true);
		$criteria->compare('NAMPO',$this->NAMPO,true);
		$criteria->compare('D',$this->D,true);
        $criteria->compare('rn', $rn);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>array(
                    'IDMESTO'=>CSort::SORT_DESC,
                )
            ),
		));
	}

    protected static function GetMsMestaCache($_params = array(), $and, $count){
        $key = serialize($_params) . $and . $count;
        return Helpers::check_cache($key, function($args){
            return MestoView::GetMsMesta($args[0], $args[1], $args[2]);
        }, array($_params, $and, $count));
    }
    protected static function GetMsMesta($_params = array(), $and, $count){
        $_params[':kodmo'] = Yii::app()->user->getState('kodmo');
        if( ! $count)
            $diff = Yii::app()->db->createCommand("select * from fnMestaSumReserve(:DT, :kodmo) m where sk > 0" . $and)
                ->queryAll(true,$_params);
        else
        {
            Yii::beginProfile('SQLfnMestaSum');
            $diff = Yii::app()->db->createCommand("select count(*) c,
             isnull(sum(case when sk > 0 then sk end), 0) as s
            ,sum(case when sk < 0 then sk end) as n
            from fnMestaSumReserve(:DT, :kodmo) m where sk > 0" . $and)
                ->queryAll(true, $_params);
            Yii::endProfile('SQLfnMestaSum');
            $diff = $diff[0];
        }
        return $diff;
    }
    protected static function GetMsMestaReserve($_params = array(), $and, $count){
        $_params[':mo'] = Yii::app()->user->getState('kodmo');
        if( ! $count)
            $diff = Yii::app()->db->createCommand("select * from fnMestaSumReserve(:DT, :mo) m where sk > 0" . $and)
                ->queryAll(true,$_params);
        else
        {
            Yii::beginProfile('SQLfnMestaSum');
            $diff = Yii::app()->db->createCommand("select count(*) c,
             isnull(sum(case when sk > 0 then sk end), 0) as s
            ,sum(case when sk < 0 then sk end) as n
            from fnMestaSum(:DT) m where sk > 0" . $and)
                ->queryAll(true, $_params);
            Yii::endProfile('SQLfnMestaSum');
            $diff = $diff[0];
        }
        return $diff;
    }
    protected static function GetMyMesta($_params = array(), $and, $count){

        $q=<<<MYQUERY
SELECT m.NAMMO, m.NAMPMO, m.NAMPK, m.NAMPO, m.D, m.PROFK, m.IDMESTO, m.KODMO AS MO, m.PMO, m.KODOKATO, 0 AS SKM, 0 AS SKW, 0 AS SKD,
  (IFNULL(m.sk, 0) - IFNULL(blocked.pacient_count, 0)) AS SK, :DT AS dt
    FROM (
      SELECT PMO, ProfK, COUNT(*) pacient_count
        FROM pacientview pv
        WHERE typep IN ('planed', 'blocked') AND pv.DPOGOSP IS NOT NULL AND DPOGOSP >= :DT GROUP BY PMO,PROFK
  ) AS blocked
    RIGHT JOIN mesto_view AS m
      ON blocked.PMO = m.PMO
      AND blocked.PROFK = m.PROFK
    INNER JOIN (SELECT m.pmo, m.profk, m.D, MAX(m.IDMESTO) IDMESTO FROM mesto m
  INNER JOIN
  (SELECT
  pmo, profk, MAX(d) D
  FROM mesto
    WHERE D<:DT
  GROUP BY pmo, profk) m1 ON m1.pmo=m.pmo AND m1.profk=m.profk AND m1.D=m.D
  GROUP BY m.pmo, m.profk, m.D) AS cur ON cur.idmesto=m.IDMESTO
  where (IFNULL(m.sk, 0) - IFNULL(blocked.pacient_count, 0)) > 0
MYQUERY;
        $diff = Yii::app()->db->createCommand($q . " " . $and)
            ->queryAll(true,$_params);
        if($count)
            $diff = count($diff);
        return $diff;
    }

    static public function GetMestaSum(DateTime $dt,$params = array(), $count=false)
    {
        $and = '';
        $_params[':DT'] = $dt->format(Helpers::getDateFormat());
        if( ! empty($params['typek'])){
            switch($params['typek'])
            {
                case 0:
                    $and = ' AND SKD>0';
                    break;
                case 1:
                    $and = ' AND SKM>0';
                    break;
                case 2:
                    $and = ' AND SKW>0';
                    break;
                default:
                    break;
            }
        }
        if( ! empty($params['PMO'])){
            $and .= ' AND m.PMO = :PMO';
            $_params[':PMO'] = $params['PMO'];
        }
        if( ! empty($params['MO'])){
            $and .= ' AND m.MO = :MO';
            $_params[':MO'] = $params['MO'];
        }
        if( ! empty($params['PROFK'])){
            $and .= ' AND m.PROFK = :PROFK';
            $_params[':PROFK'] = $params['PROFK'];
        }
        if( ! empty($params['KODOKATO'])){
            $and .= ' AND m.KODOKATO = :KODOKATO';
            $_params[':KODOKATO'] = $params['KODOKATO'];
        }
        if(Helpers::getIsMsSql()){
            return self::GetMsMesta($_params, $and, $count);
        } else {
            return self::GetMyMesta($_params, $and, $count);
        }
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MestoView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
