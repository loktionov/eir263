<?php

/**
 * This is the model class for table "MO".
 *
 * The followings are the available columns in table 'MO':
 * @property integer $KODMO
 * @property string $KODOKATO
 * @property string $NAMMO
 * @property string $ADRESMO
 */
class Mo extends CActiveRecord
{
    public static function GetOptionsArray($with_kod = true)
    {
        /** @var $mo Mo[] */
        $mo = self::model()->findAll(array('order' => 'NAMMO'));
        $ret = array();
        if (!$with_kod)
            $ret = CHtml::listData($mo, 'KODMO', 'NAMMO');
        else {
            foreach ($mo as $m) {
                $ret[] = array(
                    'name' => $m->KODMO . ': ' . $m->NAMMO,
                    'cat' => $m->KODMO,
                    'kod' => $m->KODMO,
                );
            }
        }
        return $ret;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('KODMO', 'required'),
            array('KODMO', 'numerical', 'integerOnly' => true),
            array('KODOKATO', 'length', 'max' => 5),
            array('NAMMO', 'length', 'max' => 150),
            array('ADRESMO', 'length', 'max' => 250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('KODMO, KODOKATO, NAMMO, ADRESMO', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODMO' => 'Kodmo',
            'KODOKATO' => 'Kodokato',
            'NAMMO' => 'Название',
            'ADRESMO' => 'Адрес',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODMO', $this->KODMO);
        $criteria->compare('KODOKATO', $this->KODOKATO, true);
        $criteria->compare('NAMMO', $this->NAMMO, true);
        $criteria->compare('ADRESMO', $this->ADRESMO, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (!$this->isNewRecord) {
            $old_mo = new MOHistory();
            $old_mo->attributes = $this->findByPk($this->KODMO)->attributes;
            $old_mo->idaction = $this->scenario;
            $old_mo->idlogin = Yii::app()->user->id;
            $old_mo->save();
        }
        return parent::beforeSave();
    }
}
