<?php

/**
 * This is the model class for table "rmp.dbo.smo".
 *
 * The followings are the available columns in table 'rmp.dbo.smo':
 * @property string $CODE
 * @property string $NAME
 * @property string $OLD_COD
 * @property string $INN
 * @property string $OGRN
 * @property string $KPP
 * @property string $OKPO
 * @property string $STATUS
 * @property integer $id
 * @property integer $Kod_smo
 * @property string $address
 * @property string $addresss
 * @property string $FNAME
 */
class Orgid extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'smo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CODE, NAME, OLD_COD, STATUS, id', 'required'),
			array('id, Kod_smo', 'numerical', 'integerOnly'=>true),
			array('CODE', 'length', 'max'=>2),
			array('NAME', 'length', 'max'=>50),
			array('OLD_COD', 'length', 'max'=>5),
			array('INN', 'length', 'max'=>12),
			array('OGRN', 'length', 'max'=>15),
			array('KPP', 'length', 'max'=>9),
			array('OKPO', 'length', 'max'=>8),
			array('STATUS', 'length', 'max'=>1),
			array('address, addresss', 'length', 'max'=>250),
			array('FNAME', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CODE, NAME, OLD_COD, INN, OGRN, KPP, OKPO, STATUS, id, Kod_smo, address, addresss, FNAME', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CODE' => 'Code',
			'NAME' => 'Name',
			'OLD_COD' => 'Old Cod',
			'INN' => 'Inn',
			'OGRN' => 'Ogrn',
			'KPP' => 'Kpp',
			'OKPO' => 'Okpo',
			'STATUS' => 'Status',
			'id' => 'ID',
			'Kod_smo' => 'Kod Smo',
			'address' => 'Address',
			'addresss' => 'Addresss',
			'FNAME' => 'Fname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CODE',$this->CODE,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('OLD_COD',$this->OLD_COD,true);
		$criteria->compare('INN',$this->INN,true);
		$criteria->compare('OGRN',$this->OGRN,true);
		$criteria->compare('KPP',$this->KPP,true);
		$criteria->compare('OKPO',$this->OKPO,true);
		$criteria->compare('STATUS',$this->STATUS,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('Kod_smo',$this->Kod_smo);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('addresss',$this->addresss,true);
		$criteria->compare('FNAME',$this->FNAME,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orgid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
