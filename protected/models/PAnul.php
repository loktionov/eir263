<?php

/**
 * This is the model class for table "PAnul".
 *
 * The followings are the available columns in table 'PAnul':
 * @property integer $KODPANUL
 * @property string $NAMPANUL
 */
class PAnul extends CActiveRecord
{
    public function primaryKey()
    {
        return 'KODPANUL';
    }
    public static function GetOptionsArray()
    {
        $panul = self::model()->findAll(array('order' => 'NAMPANUL'));
        return CHtml::listData($panul, 'KODPANUL','NAMPANUL');
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'panul';
	}
    public static function GetPAnulRow($q)
    {
        if(empty($q))
            return false;
        $pmo = PAnul::model()->findByPk($q);
        return $pmo;
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODPANUL', 'numerical', 'integerOnly'=>true),
			array('NAMPANUL', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODPANUL, NAMPANUL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODPANUL' => 'Код причины аннулирования',
			'NAMPANUL' => 'Причина аннулирования',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODPANUL',$this->KODPANUL);
		$criteria->compare('NAMPANUL',$this->NAMPANUL,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PAnul the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
