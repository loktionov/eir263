<?php

/**
 * This is the model class for table "PMO".
 *
 * The followings are the available columns in table 'PMO':
 * @property integer $KODPMO
 * @property integer $KODMO
 * @property string $NAMPMO
 * @property string $ADRESPMO
 */
class PMO extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pmo';
    }

    public static function GetOptionsArray()
    {
        $condition = '';
        $params = array();
        if (Yii::app()->user->getState('tip') < 2) {
            $condition = 'KODMO=:mo';
            $params = array(':mo' => Yii::app()->user->getState('kodmo'));
        }
        /** @var $opts self[] */
        $opts = self::model()->findAll($condition, $params);
        $ret = array();
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt->KODPMO,
                'cat' => $opt->ADRESPMO,
                'name' => $opt->NAMPMO,
            );
        }
        return $ret;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('KODPMO, KODMO', 'numerical', 'integerOnly' => true),
            array('NAMPMO', 'length', 'max' => 150),
            array('ADRESPMO', 'length', 'max' => 254),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('KODPMO, KODMO, NAMPMO, ADRESPMO', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODPMO' => 'Код ПМО',
            'KODMO' => 'Код МО',
            'NAMPMO' => 'Название',
            'ADRESPMO' => 'Адрес',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODPMO', $this->KODPMO);
        $criteria->compare('KODMO', $this->KODMO);
        $criteria->compare('NAMPMO', $this->NAMPMO, true);
        $criteria->compare('ADRESPMO', $this->ADRESPMO, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PMO the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (!$this->isNewRecord) {
            $this->historySave();
        }
        return parent::beforeSave();
    }

    public function beforeDelete(){
        $this->scenario = 'delete';
        $this->historySave();
        return parent::beforeDelete();
    }

    private function historySave(){
        $old_pmo = new PMOHistory();
        $old_pmo->attributes = $this->findByPk($this->KODPMO)->attributes;
        $old_pmo->idaction = $this->scenario;
        $old_pmo->idlogin = Yii::app()->user->id;
        $old_pmo->save();
    }
}
