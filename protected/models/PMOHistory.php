<?php

/**
 * This is the model class for table "PMO_history".
 *
 * The followings are the available columns in table 'PMO_history':
 * @property integer $id
 * @property integer $KODPMO
 * @property integer $KODMO
 * @property string $NAMPMO
 * @property string $ADRESPMO
 * @property integer $idlogin
 * @property string $idaction
 * @property string $dateaction
 */
class PMOHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PMO_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idlogin, idaction', 'required'),
			array('KODPMO, KODMO, idlogin', 'numerical', 'integerOnly'=>true),
			array('NAMPMO', 'length', 'max'=>150),
			array('ADRESPMO', 'length', 'max'=>254),
			array('idaction', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, KODPMO, KODMO, NAMPMO, ADRESPMO, idlogin, idaction, dateaction', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'KODPMO' => 'Kodpmo',
			'KODMO' => 'Kodmo',
			'NAMPMO' => 'Nampmo',
			'ADRESPMO' => 'Adrespmo',
			'idlogin' => 'Idlogin',
			'idaction' => 'Idaction',
			'dateaction' => 'Dateaction',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('KODPMO',$this->KODPMO);
		$criteria->compare('KODMO',$this->KODMO);
		$criteria->compare('NAMPMO',$this->NAMPMO,true);
		$criteria->compare('ADRESPMO',$this->ADRESPMO,true);
		$criteria->compare('idlogin',$this->idlogin);
		$criteria->compare('idaction',$this->idaction,true);
		$criteria->compare('dateaction',$this->dateaction,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PMOHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
