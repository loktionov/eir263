<?php

/**
 * This is the model class for table "PMOView".
 *
 * The followings are the available columns in table 'PMOView':
 * @property integer $KODPMO
 * @property string $NAMPMO
 * @property string $NAMMO
 * @property integer $KODMO
 * @property string $ADRESMO
 * @property string $ADRESPMO
 * @property string $KODOKATO
 */
class PMOView extends CActiveRecord
{
    public static function GetOptionsArray($mesto = true)
    {
        /** @var self[] $opts */
        $opts = self::model()->cache(60 * 60)->findAll(
            array(
                'join' => $mesto ? 'inner join mesto as Mesto on t.kodpmo=Mesto.pmo' : null,
                'select' => 'KODPMO, KODMO, NAMMO, NAMPMO, ADRESPMO',
                'distinct' => true,
                'order' => 't.NAMMO, t.NAMPMO',
            ));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt->KODPMO,
                'cat' => $opt->KODMO . ": " . $opt->NAMMO . ' (' . $opt->ADRESPMO .')',
                'name' => $opt->NAMPMO,
            );
        }
        return $ret;
    }

    public static function GetOptionsArrayForUsers()
    {
        $opts = Yii::app()->db->createCommand('
SELECT DISTINCT [KODPMO], [KODMO], [NAMMO], [NAMPMO], case when ms.idmesto is null then 0 else 1 end as [optgroup]
FROM [pmoview] [t]
left join mesto as ms on t.kodpmo=ms.pmo
ORDER BY [t].[NAMMO], [t].[NAMPMO]')->queryAll();
        $ret = array();
        if (!empty($kod)) {
            $own = Yii::app()->db->createCommand('
SELECT top 1 [KODPMO], [KODMO], [NAMMO], [NAMPMO], case when ms.idmesto is null then 0 else 1 end as [optgroup]
FROM [pmoview] [t]
left join mesto as ms on t.kodpmo=ms.pmo
where kodpmo = :kod')->queryRow(true, array(':kod'=>$kod));


            $ret['own'] = array(
                'kod' => $own['KODPMO'],
                'cat' => $own['KODMO'] . ": " . $own['NAMMO'],
                'name' => $own['NAMPMO'],
                'optgroup' => $own['optgroup'],
            );
        }
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt['KODPMO'],
                'cat' => $opt['KODMO'] . ": " . $opt['NAMMO'],
                'name' => $opt['NAMPMO'],
                'optgroup' => $opt['optgroup'],
            );
        }
        return $ret;
    }

    public static function GetAmbulanceArray($hospital = false)
    {
        $join = !$hospital ? ' left join pacient p on t.kodmo=p.monapr' : ' left join pacient p on p.mo=t.kodmo';
        $order = ' order by optgroup, name';
        $opts = Yii::app()->db->cache(3600)->createCommand("
            select * from (
            select distinct t.KODMO as [kod], t.NAMMO as [name], case when p.IDZAP is null then 'no' else 'yes' end as [optgroup] from MO t " . $join
            . ') as t '
            . $order
        )->queryAll();
        return $opts;
    }

    public static function GetMOArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAMMO, NAMPMO'));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[$opt->KODMO] = $opt->NAMMO;

        }
        return $ret;
    }

    public static function GetPMOArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAMMO, NAMPMO'));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[$opt->KODPMO] = $opt->NAMPMO;
        }
        return $ret;
    }

    public static function GetPMOViewArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAMMO, NAMPMO'));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[$opt->KODPMO] = $opt->NAMPMO;
        }
        return $ret;
    }

    public static function GetPmoRow($q)
    {
        if (empty($q))
            return false;
        $pmo = Yii::app()->db->cache(3600)->createCommand()
            ->select("NAMMO, NAMPMO")
            ->from('pmoview')
            ->where('KODPMO = :Q', array(':Q' => "$q"))
            ->queryRow(true);
        return $pmo;
    }

    public function primaryKey()
    {
        return 'KODPMO';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pmoview';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('KODMO', 'required'),
            array('KODPMO, KODMO', 'numerical', 'integerOnly' => true),
            array('NAMPMO, NAMMO', 'length', 'max' => 150),
            array('ADRESMO', 'length', 'max' => 250),
            array('ADRESPMO', 'length', 'max' => 254),
            array('KODOKATO', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('KODPMO, NAMPMO, NAMMO, KODMO, ADRESMO, ADRESPMO, KODOKATO', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODPMO' => 'Kodpmo',
            'NAMPMO' => 'Nampmo',
            'NAMMO' => 'Nammo',
            'KODMO' => 'Kodmo',
            'ADRESMO' => 'Adresmo',
            'ADRESPMO' => 'Adrespmo',
            'KODOKATO' => 'Kodokato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODPMO', $this->KODPMO);
        $criteria->compare('NAMPMO', $this->NAMPMO, true);
        $criteria->compare('NAMMO', $this->NAMMO, true);
        $criteria->compare('KODMO', $this->KODMO);
        $criteria->compare('ADRESMO', $this->ADRESMO, true);
        $criteria->compare('ADRESPMO', $this->ADRESPMO, true);
        $criteria->compare('KODOKATO', $this->KODOKATO, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PMOView the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
