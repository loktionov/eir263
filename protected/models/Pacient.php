<?php

/**
 * This is the model class for table "Pacient".
 *
 * The followings are the available columns in table 'Pacient':
 * @property integer $IDZAP
 * @property string $NNAPR
 * @property string $DNAPR
 * @property string $DPGOSP
 * @property string $DNGOSP
 * @property string $VNGOSP
 * @property string $DOGOSP
 * @property string $DANUL
 * @property integer $PANUL
 * @property integer $IANUL
 * @property integer $KANUL
 * @property integer $PMOANUL
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $TEL
 * @property integer $VPOLIS
 * @property string $SPOLIS
 * @property string $NPOLIS
 * @property integer $TER
 * @property integer $SMO
 * @property integer $FOMP
 * @property integer $MONAPR
 * @property integer $PMONAPR
 * @property integer $PROFONAPR
 * @property integer $PROFKNAPR
 * @property string $DS
 * @property string $MEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property integer $PROFO
 * @property integer $PROFK
 * @property string $NKART
 * @property string $DSPO
 * @property string $DPOGOSP
 * @property string $DSNAPR
 * @property string $typep
 * @property PacientHistory $pacient_history
 */
class Pacient extends CActiveRecord
{
    const ACTION_INSERT = 1;
    const ACTION_UPDATE = 2;
    public $pacient_history;
    public $file;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pacient';
    }

    public function primaryKey()
    {
        return 'IDZAP';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'mkb10_po' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DSPO')),
            'mkb10_napr' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DSNAPR')),
            'mkb10' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DS')),
            'profk_rel' => array(self::HAS_ONE, 'ProfKView', array('IDPK' => 'PROFK')),
            'pmo_rel' => array(self::HAS_ONE, 'PMOView', array('KODPMO' => 'PMO')),
        );
    }

    public function afterSave()
    {
        parent::afterSave();
        if (!empty($this->pacient_history)) {
            $this->pacient_history->IDZAP = $this->IDZAP;
            $this->pacient_history->save();
        }
    }

    public function beforeSave($save_history = true)
    {
        if (!$this->isChange(true)) {
            //$this->addError('IDZAP', 'Никаких изменений не внесено.');
            //return false;
            //$save_history = false;
        }
        if (empty($this->NNAPR)) {
            if (!$this->setNNAPR())
                return false;
        }
        if ($save_history) {
            $this->pacient_history = new PacientHistory();
            $this->pacient_history->IDACTION = $this->scenario;
            if ($this->isNewRecord) {
                $this->pacient_history->attributes = $this->attributes;
            } else {
                $old_pacient = $this->findByPk($this->IDZAP);
                $this->pacient_history->attributes = $old_pacient->attributes;
            }

            $this->pacient_history->IDLOGIN = Yii::app()->user->id;

            foreach ($this->pacient_history->attributes as $attr => $v) {
                $v = $this->dateMatch($this->pacient_history, $attr);
                if ($v) {
                    $this->pacient_history->$attr = $v;
                }
            }
            $this->pacient_history->DATEACTION = date(Helpers::getDateFormat(true));
            if (!$this->pacient_history->validate()) {
                return false;
            }
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'IDZAP' => 'Idzap',
            'NNAPR' => 'Номер направления',
            'DNAPR' => 'Дата направления',
            'DPGOSP' => 'Дата плановой госпитализации',
            'DNGOSP' => 'Дата начала  госпитализации',
            'VNGOSP' => 'Время начала госпитализации',
            'DOGOSP' => 'Дата окончания госпитализации',
            'DANUL' => 'Дата аннулирования направления',
            'PANUL' => 'Причина аннулирования',
            'IANUL' => 'Источник аннулирования',
            'KANUL' => 'Код источника аннулирования',
            'PMOANUL' => 'Подразделение источника аннулирования',
            'FAM' => 'Фамилия',
            'IM' => 'Имя',
            'OT' => 'Отчество',
            'P' => 'Пол',
            'DR' => 'Дата рождения',
            'TEL' => 'Телефон',
            'VPOLIS' => 'Тип полиса',
            'SPOLIS' => 'Серия полиса',
            'NPOLIS' => 'Номер полиса',
            'TER' => 'Территория страхования',
            'SMO' => 'Код СМО',
            'FOMP' => 'Форма оказания медицинской помощи',
            'MONAPR' => 'Код МО, направившей на госпитализацию',
            'PMONAPR' => 'Код подразделения МО, направившей на госпитализацию',
            'PROFONAPR' => 'Профиль отделения',
            'PROFKNAPR' => 'Профиль койки',
            'DS' => 'Диагноз',
            'MEDRAB' => 'Код мед. работника, направившего больного',
            'MO' => 'Код МО',
            'PMO' => 'Код подразделения МО',
            'PROFO' => 'Профиль отделения',
            'PROFK' => 'Профиль койки',
            'NKART' => 'Номер карты',
            'DSPO' => 'Диагноз приемного отделения',
            'DPOGOSP' => 'Дата планового окончания госпитализации',
            'DSNAPR' => 'Диагноз направившего учреждения',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @param String $where
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($where = null)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('NNAPR', $this->NNAPR, true);
        $criteria->compare('DNAPR', $this->DNAPR, true);
        $criteria->compare('DPGOSP', $this->DPGOSP, true);
        $criteria->compare('DNGOSP', $this->DNGOSP, true);
        $criteria->compare('VNGOSP', $this->VNGOSP, true);
        $criteria->compare('DOGOSP', $this->DOGOSP, true);
        $criteria->compare('DANUL', $this->DANUL, true);
        $criteria->compare('PANUL', $this->PANUL);
        $criteria->compare('IANUL', $this->IANUL);
        $criteria->compare('KANUL', $this->KANUL);
        $criteria->compare('PMOANUL', $this->PMOANUL);
        $criteria->compare('FAM', $this->FAM, true);
        $criteria->compare('IM', $this->IM, true);
        $criteria->compare('OT', $this->OT, true);
        $criteria->compare('P', $this->P);
        $criteria->compare('DR', $this->DR, true);
        $criteria->compare('TEL', $this->TEL, true);
        $criteria->compare('VPOLIS', $this->VPOLIS);
        $criteria->compare('SPOLIS', $this->SPOLIS, true);
        $criteria->compare('NPOLIS', $this->NPOLIS, true);
        $criteria->compare('TER', $this->TER);
        $criteria->compare('SMO', $this->SMO);
        $criteria->compare('FOMP', $this->FOMP);
        $criteria->compare('MONAPR', $this->MONAPR);
        switch ($where) {
            case 'a':
                $criteria->compare('PMONAPR', Yii::app()->user->getState('kodpmo'));
                $criteria->compare('PMO', $this->PMO);
                break;
            case 'h':
                $criteria->compare('PMONAPR', $this->PMONAPR);
                $criteria->compare('PMO', Yii::app()->user->getState('kodpmo'));
                break;
            default:
                $criteria->compare('PMONAPR', $this->PMONAPR);
                $criteria->compare('PMO', $this->PMO);
                break;
        }


        $criteria->compare('PROFONAPR', $this->PROFONAPR);
        $criteria->compare('PROFKNAPR', $this->PROFKNAPR);
        $criteria->compare('DS', $this->DS, true);
        $criteria->compare('MEDRAB', $this->MEDRAB, true);
        $criteria->compare('MO', $this->MO);
        $criteria->compare('PROFO', $this->PROFO);
        $criteria->compare('PROFK', $this->PROFK);
        $criteria->compare('DSPO', $this->DSPO, true);
        $criteria->compare('DPOGOSP', $this->DPOGOSP, true);
        $criteria->compare('DSNAPR', $this->DSNAPR, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'IDZAP' => CSort::SORT_DESC,
                )
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pacient the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('IDZAP', 'is_change', 'on' => 'files'),

            array('NNAPR', 'length', 'is' => 14),
            array('NNAPR', 'unique'),
            array('VNGOSP', 'length', 'max' => 5),
            array('FAM, IM, OT, TEL', 'length', 'max' => 60),
            array('SPOLIS', 'length', 'max' => 10),
            array('NPOLIS', 'length', 'max' => 20),
            array('DS, DSPO, DSNAPR', 'length', 'max' => 6),
            array('MEDRAB', 'length', 'max' => 11),
            array('NKART', 'length', 'max' => 50),
            array('FAM, IM, P, DR, MO, PMO, PROFK, DPOGOSP, FOMP, DNAPR, MONAPR', 'required', 'except' => 'rollback'),
            array('IDZAP, NNAPR, DNAPR, DPGOSP, DNGOSP, VNGOSP, DOGOSP, DANUL, PANUL, IANUL, KANUL, PMOANUL,
            FAM, IM, OT, P, DR, TEL, VPOLIS, SPOLIS, NPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, DS,
            MEDRAB, MO, PMO, PROFO, PROFK, NKART, DSPO, DPOGOSP, DSNAPR', 'safe'),
            array('DPGOSP, DNGOSP, VNGOSP, DOGOSP, DANUL, PANUL, IANUL, KANUL, PMOANUL, FAM, IM, OT, P, DR,
            TEL, VPOLIS, SPOLIS, NPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, DS, MEDRAB, MO, PMO,
            PROFO, PROFK, DSPO, DPOGOSP, DSNAPR', 'safe', 'on' => 'search'),

            array('IDZAP', 'checkFiles', 'on' => 'files'),

            array('FAM, IM, OT', 'TestName'),
            array('DR, DPGOSP, DPOGOSP, DNGOSP, DOGOSP', 'checkDate'),
            array('FOMP', 'isExtr', 'on' => 'insert'),
            array('DPGOSP, DNGOSP', 'isFree', 'except' => 'rollback, files, accept'),
            array('DNGOSP, DPOGOSP, DSPO', 'WithAttributes', 'with' => array('DNGOSP', 'DPOGOSP', 'DSPO'), 'on' => 'accept'),
            array('VPOLIS, NPOLIS, TER, SMO', 'WithAttributes', 'with' => array('VPOLIS', 'NPOLIS', 'TER', 'SMO'), 'except' => 'rollback'),
            array('DOGOSP, DS', 'WithAttributes', 'with' => array('DOGOSP', 'DS'), 'on' => 'out'),
            array('DOGOSP', 'checkDOGOSP', 'on' => 'out'),
            array('DANUL, PANUL', 'WithAttributes', 'with' => array('DANUL', 'PANUL'), 'on' => 'annul'),
            array('PANUL, IANUL, KANUL, PMOANUL, P, VPOLIS, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, MO, PMO, PROFO, PROFK, MEDRAB',
                'numerical', 'integerOnly' => true),
            array('PROFK, PROFKNAPR', 'KodExist', 'class' => 'ProfKView'),
            array('TER', 'KodExist', 'class' => 'Ter'),
            array('SMO', 'KodExist', 'class' => 'Smo'),
            array('MO, MONAPR', 'KodExist', 'class' => 'Mo'),
            array('PMOANUL, PMO, PMONAPR', 'KodExist', 'class' => 'PMOView'),
            array('DS, DSPO, DSNAPR', 'KodExist', 'class' => 'MKB10'),
            array('MEDRAB', 'KodExist',
                'class' => 'Medrab',
                'FindBy' => array(
                    'condition' => 'kodmedrab=:medrab and mo=:mo',
                    'params' => array(':medrab' => $this->MEDRAB, ':mo' => Yii::app()->user->getState('kodmo'))
                )
            ),
            array('PANUL', 'KodExist', 'class' => 'PAnul'),
            array('IANUL', 'KodExist', 'class' => 'IAnul'),
            array(
                'KIANUL',
                'KodExist',
                'class' => 'Login',
                'FindBy' => array(
                    'condition' => 'tip=:tip and kod=:kod',
                    'params' => array(':tip' => $this->IANUL, ':kod' => $this->KANUL)
                )
            ),
            array('NNAPR', 'checkDouble', 'on' => 'insert'),
            array('NNAPR', 'checkUnique', 'on' => 'insert'),
            array('NNAPR', 'checkNnapr', 'except' => 'rollback'),
            array('DNGOSP', 'checkActualRules', 'on' => 'accept'),
            array('DNGOSP', 'checkBeginEndDates', 'end_attr' => 'DPOGOSP', 'on' => 'accept'),
            array('DNGOSP', 'checkBeginEndDates', 'end_attr' => 'DOGOSP', 'on' => 'out'),
            array('DPGOSP', 'checkBeginEndDates', 'end_attr' => 'DPOGOSP', 'on' => 'insert'),
            array('PROFK', 'checkMestaExist'),
            array('MONAPR', 'checkMonapr', 'on' => 'insert'),
        );
    }

    public function checkMonapr(){
        if($this->MONAPR != Yii::app()->user->getState('kodmo'))
            $this->addError('MONAPR', 'В поле MONAPR следует указывать код направившей организации');
    }

    public function checkMestaExist()
    {
        $res = MestoView::model()
            ->findAll('pmo=:pmo and profk=:profk and D<=:d',
                array(':pmo' => $this->PMO, ':profk' => $this->PROFK, ':d' => Yii::app()->dateFormatter->formatDateTime($this->DNAPR, 'medium', null)));
        if (empty($res))
            $this->addError('PROFK', 'Данный профиль коек отсутствует в принимающей медицинской организации');
    }

    public function checkFiles()
    {
        if (empty($this->file)) {
            $this->addError('IDZAP', 'Неверный тип файла.');
            return;
        }
        //$scheme = SXML::model()->findAll('IDXML=:id', array(':id' => $this->file));
        $scheme = Yii::app()->db->createCommand()
            ->from('sxml_view')
            ->where('IDXML=:id', array(':id' => $this->file))
            ->queryAll();
        if (empty($scheme)) {
            $this->addError('IDZAP', 'Неверный тип файла.');
            return;
        }
        foreach ($scheme as $sch) {
            $attr = $sch['NAME'];
            if (empty($this->$attr) AND $sch['require'] AND $attr <> 'NZAP') {
                if ($attr == 'DNAPR')
                    $this->DNAPR = date(Helpers::getDateFormat());
                else
                    $this->addError($attr, 'Отсутствует обязательное поле: ' . $attr . ' ' . $this->getAttributeLabel($attr));
            }
        }
        if ($this->hasErrors())
            return;

        switch ($this->file) {
            case 'T1':
                $this->isExtr();
                $this->checkDouble();
                $this->checkUnique();
                $this->isFree();
                $this->checkActualEq();
                break;
            case 'T2':
                $this->checkActual('DNGOSP', 'now', false);
                $this->checkBeginEndDates('DNGOSP', 'DPOGOSP');
                break;
            case 'T3':
                $this->isExtr();
                $this->checkDouble();
                $this->checkUnique();
                $this->checkActualEq();
                break;
            case 'T4':
                $this->checkActualRules('DANUL');
                break;
            case 'T5':
                $this->checkDOGOSP();
                break;
            case 'T6':
                return;
                break;
            default:
                break;
        }
    }


    public function checkActualRules($attr)
    {
        $this->checkActual($attr, 'now', false);
    }

    /** По умолчанию сравнивает дату плановой госп. с сегодня
     * @param string $attr Имя Аттрибута для проверки
     * @param string $end дата окончания
     * @param bool $more Должнали искомая дата быть больше. false если должна быть меньше
     * @param bool $offset Сдвиг для функции modify
     * @return bool
     */
    public function checkActual($attr = "DPGOSP", $end = 'now', $more = true, $offset = false)
    {
        if (empty($end)) {
            return false;
        }
        $this->checkDate($attr);
        if ($this->hasErrors($attr))
            return false;
        $startdate = new DateTime($this->$attr);
        $now = new DateTime(date('d-m-Y', strtotime($end)));
        if ($offset) {
            $now->modify($offset);
        }
        if ($startdate < $now and $more) {
            $this->addError($attr, "{$this->getAttributeLabel($attr)} ({$this->$attr}) должна быть больше или равна " . $now->format(Helpers::getDateFormat()));
        } else if ($startdate > $now and !$more) {
            $this->addError($attr, "{$this->getAttributeLabel($attr)} ({$this->$attr}) должна быть меньше или равна " . $now->format(Helpers::getDateFormat()));
        }
        return true;
    }

    /** По умолчанию сравнивает дату направления с сегодня
     * @param string $attr Имя Аттрибута для проверки
     * @param bool $offset Сдвиг для функции modify
     * @param string $end дата окончания
     */
    public function checkActualEq($attr = "DNAPR", $end = 'now', $offset = false)
    {
        //TODO Проверка качества заполнения
        if ($attr == "DNAPR" and $end == 'now' and $offset == false)
            return;
        $dpgosp = new DateTime($this->$attr);
        $now = new DateTime(date('d-m-Y', strtotime($end)));
        if ($offset) {
            $now->modify($offset);
        }
        if ($dpgosp != $now) {
            $this->addError($attr, "{$this->getAttributeLabel($attr)} должна быть равна " . $now->format(Helpers::getDateFormat()));
        }
    }

    public function is_change()
    {
        if (!$this->isChange())
            $this->addError('IDZAP', 'Никаких изменений не внесено. Исключите эту запись из пакета.');
    }

    public function checkDOGOSP()
    {
        if (empty($this->DOGOSP)) {
            $this->addError($this->DOGOSP, "Поле «{$this->getAttributeLabel($this->DOGOSP)}» не должно быть пустым.");
            return;
        }
        $this->checkDate('DOGOSP');
        if (strtotime($this->DOGOSP) > strtotime('now')) {
            $this->addError($this->DOGOSP, "«{$this->getAttributeLabel($this->DOGOSP)}» не может быть больше сегодняшней даты");
        }
    }


    /**
     * @param string $attribute имя поля, которое будем валидировать
     * @param array $params дополнительные параметры для правила валидации
     */
    public function WithAttributes($attribute, $params)
    {
        unset($params['with'][$attribute]);
        if (!empty($this->$attribute)) {
            foreach ($params['with'] as $attr) {
                if (empty($this->$attr)) {
                    if (!$this->hasErrors($attr))
                        $this->addError($attr, "Необходимо заполнить поле «{$this->getAttributeLabel($attr)}».");
                }
            }
        }
    }

    public function isFree()
    {
        if ($this->hasErrors())
            return;
        if (strtotime($this->DNGOSP) == strtotime($this->DPGOSP) and !$this->isNewRecord)
            return;
        if ($this->FOMP == 1 AND !empty($this->DPGOSP) AND !empty($this->PROFK) AND !empty($this->PMO)) {
            $c = MestoView::GetMestaSum(new DateTime($this->DPGOSP), array('PMO' => $this->PMO, 'PROFK' => $this->PROFK,), true);
            if (empty($c['c'])) {
                $this->addError('DPGOSP', "На такую дату нет мест.");
            }
        }
    }

    public function TestName($attribute, $params)
    {
        $name = trim($this->$attribute);
        if (empty($name)) {
            return;
        }
        $q = '/^([A-Z\d][A-Za-z\d\.\-\s]*)|([А-ЯЁ\d][А-ЯЁа-яё\d\.\-\s]*)$/iu';

        if (!preg_match($q, $name)) {
            $this->addError($attribute, "Неверный формат поля «{$this->getAttributeLabel($attribute)}».");
        } else {
            $q = '/\.\s*\-|\.\s*\.|\-\s*\-|\-\s*\./iu';
            if (preg_match($q, $name)) {
                $this->addError($attribute, "Неверный формат поля «{$this->getAttributeLabel($attribute)}».");
            } else {
                $q = '/[A-ZА-ЯЁa-zа-яё\d]+$/iu';
                if (!preg_match($q, $name)) {
                    $this->addError($attribute, "Неверный формат поля «{$this->getAttributeLabel($attribute)}».");
                }

            }
        }
    }

    /**
     * Проверяет даты в случае первоначального добавления
     */
    public function isExtr()
    {
        if ($this->hasErrors())
            return;
        if (empty($this->FOMP)) {
            $this->addError('FOMP', "Необходимо заполнить поле «{$this->getAttributeLabel('FOMP')}».");
            return;
        }
        if ($this->FOMP == 1) {
            $this->checkDates_fomp1();
        } else if ($this->FOMP == 3) {
            $this->checkDates_fomp3();
            if ($this->MO <> $this->MONAPR) {
                $this->addError('MONAPR', "При экстренной госпитализации направление можно выдать только в собственную организацию");
            }
        } else {
            $this->addError('FOMP', "Неверное значение поля «{$this->getAttributeLabel('FOMP')}». FOMP = " . $this->FOMP);
        }
    }

    public function checkDates_fomp1()
    {
        if (empty($this->DPGOSP)) {
            $this->addError('DPGOSP', "Необходимо заполнить поле «Дата плановой госпитализации».");
        } else {
            //TODO проверить качество заполнения
            //$this->checkActual();
            if (!empty($this->DPOGOSP)) {
                $this->checkBeginEndDates('DPGOSP', 'DPOGOSP');
            }
        }
        if (empty($this->DSNAPR))
            $this->addError('DSNAPR', "Необходимо заполнить поле «Диагноз направившего учреждения».");
    }

    public function checkDates_fomp3()
    {
        if (empty($this->DNGOSP))
            $this->addError('DNGOSP', "Необходимо заполнить поле «Дата начала госпитализации».");
        else {
            $this->checkActual('DNGOSP', 'now', false);
            if (!empty($this->DPOGOSP)) {
                $this->checkBeginEndDates('DNGOSP', 'DPOGOSP');
            }
        }
        if (empty($this->DSPO)) {
            $this->addError('DSPO', "Необходимо заполнить поле «Диагноз приемного отделения».");
        }
    }

    public function checkNnapr()
    {
        if (empty($this->NNAPR))
            return;
        if (substr($this->NNAPR, 0, 6) != $this->MONAPR)
            $this->addError('NNAPR', 'Первые 6 символов номера направления должны совпадать с кодом направившей медицинской организации');
        if (substr($this->NNAPR, 6, 2) != date('y', strtotime($this->DNAPR)))
            $this->addError('NNAPR', 'Два симовола с 7-го должны быть равны году, в котором выдано направление');
    }

    /**
     * @param $begin_attr
     * @param $end_attr
     * @throws Exception
     */
    public function checkBeginEndDates($begin_attr, $end_attr)
    {
        if (is_array($end_attr)) {
            if (isset($end_attr['end_attr']))
                $end_attr = $end_attr['end_attr'];
            else
                throw new Exception('неверный валидатор');
        }
        if ($begin_attr == 'DPGOSP' and $this->FOMP == 3)
            return;

        $this->checkActual($begin_attr, $this->$end_attr, false);
        //проверка на максимальное кол-во дней до направления
        //$this->checkActual($end_attr, $this->$begin_attr, false, Yii::app()->params['maxNapr'] . ' days');
    }

    public function check()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('NNAPR', $this->NNAPR);
        $criteria->compare('DNAPR', $this->DNAPR);
        $criteria->compare('DPGOSP', $this->DPGOSP);
        $criteria->compare('DNGOSP', $this->DNGOSP);
        $criteria->compare('VNGOSP', $this->VNGOSP);
        $criteria->compare('DOGOSP', $this->DOGOSP);
        $criteria->compare('DANUL', $this->DANUL);
        $criteria->compare('PANUL', $this->PANUL);
        $criteria->compare('IANUL', $this->IANUL);
        $criteria->compare('KANUL', $this->KANUL);
        $criteria->compare('PMOANUL', $this->PMOANUL);
        $criteria->compare('FAM', $this->FAM);
        $criteria->compare('IM', $this->IM);
        $criteria->compare('OT', $this->OT);
        $criteria->compare('P', $this->P);
        $criteria->compare('DR', $this->DR);
        $criteria->compare('TEL', $this->TEL);
        $criteria->compare('VPOLIS', $this->VPOLIS);
        $criteria->compare('SPOLIS', $this->SPOLIS);
        $criteria->compare('NPOLIS', $this->NPOLIS);
        $criteria->compare('TER', $this->TER);
        $criteria->compare('SMO', $this->SMO);
        $criteria->compare('FOMP', $this->FOMP);
        $criteria->compare('MONAPR', $this->MONAPR);
        $criteria->compare('PMONAPR', $this->PMONAPR);
        $criteria->compare('PMO', $this->PMO);
        $criteria->compare('PROFONAPR', $this->PROFONAPR);
        $criteria->compare('PROFKNAPR', $this->PROFKNAPR);
        $criteria->compare('DS', $this->DS);
        $criteria->compare('MEDRAB', $this->MEDRAB);
        $criteria->compare('MO', $this->MO);
        $criteria->compare('PROFO', $this->PROFO);
        $criteria->compare('PROFK', $this->PROFK);
        $criteria->compare('DSPO', $this->DSPO);
        $criteria->compare('DPOGOSP', $this->DPOGOSP);
        $criteria->compare('DSNAPR', $this->DSNAPR);

        return Pacient::model()->findAll($criteria);
    }

    private function setNNAPR()
    {
        if (empty($this->MONAPR)) {
            if (Yii::app()->user->getState('tip') < 2 and Yii::app()->user->hasState('kodmo')) {
                $this->MONAPR = Yii::app()->user->getState('kodmo');
            } else
                return false;
        }
        $cccccc = $this->MONAPR;
        $yy = date('y', strtotime($this->DNAPR));

        if ($yy == 70) {
            $yy = $yy = date('y');
        }

        $n = $this->getHoleNN($cccccc, date('Y', strtotime($this->DNAPR)));
        if (empty($n)) {
            $n = Yii::app()->db->createCommand()
                ->select('substring(NNAPR, 9, 6)')
                ->from('Pacient')
                ->limit(1)
                ->order('Convert(int, substring(NNAPR, 9, 6)) desc')
                ->where('monapr=:monapr', array(':monapr' => $this->MONAPR))
                ->queryScalar();
            $n++;
            if ($n > 999999)
                throw new Exception('Достигнут лимит количества направлений');
        }

        $nnnnnn = str_pad(($n), 6, '0', STR_PAD_LEFT);
        $this->NNAPR = $cccccc . $yy . $nnnnnn;
        return $this->NNAPR;
    }

    private function getHoleNN($monapr, $year)
    {
        return Yii::app()->db->createCommand('SELECT dbo.GetHoleNumNapr (:monapr, :year) as t')
            ->queryScalar(array(':monapr' => $monapr, ':year' => $year));
    }

    public function KodExist($attr, $params)
    {
        if (empty($this->$attr))
            return;
        if (empty($params['class']))
            return;
        if ($this->hasErrors($attr))
            return;
        $class = $params['class'];
        $pk = $this->$attr;
        /** @var $model CActiveRecord */
        $model = call_user_func(array($class, 'model'));
        if (!$model instanceof CActiveRecord)
            return;
        if (empty($params['FindBy'])) {
            $res = $model->findByPk($pk);
        } else {
            foreach ($params['FindBy']['params'] as $k => $v) {
                if (empty($v))
                    return;
            }
            $res = $model->find($params['FindBy']['condition'], $params['FindBy']['params']);
        }
        if (empty($res)) {
            $this->addError($attr, 'Данный код отсутствует в справочнике: ' . $attr . '=' . $this->$attr);
        }
    }

    public function checkDouble()
    {
        if (!$this->isNewRecord)
            return;
        if ($this->hasErrors())
            return;
        $d = $this->check();
        if (count($d)) {
            $this->addError('NNAPR', 'Такой пациент уже был добавлен ранее.');
        }
    }

    public function checkUnique()
    {
        if (!$this->isNewRecord OR empty($this->NNAPR))
            return;
        if ($this->hasErrors())
            return;
        $poc = new Pacient();
        $poc->NNAPR = $this->NNAPR;
        $d = $poc->check();
        if (count($d)) {
            $this->addError('NNAPR', 'Номер направления неуникальный.');
        }
    }

    public function checkDate($attribute)
    {
        if (empty($this->$attribute))
            return;
        $date = true;
        $time = strtotime($this->$attribute);
        if ($time !== false) {
            $this->$attribute = date(Helpers::getDateFormat(false, true), $time);
            $q = '/^(\d\d)\.(\d\d)\.(\d{4})$/';
            if (preg_match($q, $this->$attribute, $matches)) {
                if (!checkdate($matches[2], $matches[1], $matches[3])) {
                    $date = false;
                }
            } else {
                $date = false;
            }
        } else {
            $date = false;
        }
        if (!$date) {
            $this->addError($attribute, "Поле «{$this->getAttributeLabel($attribute)}» не является датой." . $this->$attribute);
        }
    }

    static function getDatesAttr()
    {
        return array('DNAPR', 'DPGOSP', 'DNGOSP', 'DOGOSP', 'DANUL', 'DR', 'DPOGOSP');
    }

    public function isChange($setDate = false)
    {
        $res = array();
        /** @var $old_pacient Pacient */
        if ($this->isNewRecord) {
            $old_pacient = clone($this);
        } else
            $old_pacient = $this->findByPk($this->IDZAP);
        foreach ($old_pacient->attributes as $k => $v) {
            $old_v = empty($v) ? false : $v;
            $this_v = empty($this->$k) ? false : $this->$k;
            $this_date = Pacient::dateMatch($this, $k);
            if ($this_date) {
                $this_v = $this_date;
                $old_v = Pacient::dateMatch($old_pacient, $k);
                if ($setDate)
                    $this->$k = $this_v;
            }
            if ($old_v != $this_v)
                $res[$k] = array($v, $this->$k);
        }
        if (empty($res) and !$this->isNewRecord)
            return false;
        else if ($this->isNewRecord)
            return true;
        return $res;
    }

    /**
     * @param $pacient Pacient|PacientHistory|ImpPacView
     * @param $attr
     * @return bool|null|string
     */
    static function dateMatch($pacient, $attr)
    {
        //Yii::beginProfile('isDate_array_search');
        $match = array_search($attr, self::getDatesAttr());
        //Yii::endProfile('isDate_array_search');
        if ($match !== false) {
            //Yii::beginProfile('isDate_dateformat');
            $ret = empty($pacient->$attr) ? null : date(Helpers::getDateFormat(), strtotime($pacient->$attr));
            //Yii::endProfile('isDate_dateformat');
            return $ret;
        } else return false;
    }

    public function getTypep()
    {
        if ($this->isNewRecord)
            return 'planed';
        /** @var $view PacientView */
        $view = PacientView::model()->findByPk($this->IDZAP);
        if (empty($view))
            return 'planed';
        return $view->typep;
    }

    public static function GetXml($pacients)
    {
        //todo ускорить выгрузку xml
        if (empty($pacients)) return false;
        $sxe = simplexml_load_string("<DAN />");
        $attrs = Pacient::model()->getAttributes();
        TbArray::removeValues(array('IDZAP'), $attrs);
        $i = 1;
        foreach ($pacients as $pacient) {
            $zap = $sxe->addChild('ZAP');
            if ($pacient instanceof ImpPacView)
                $nzap = $pacient->NZAP;
            else
                $nzap = $i++;
            $zap->addChild('NZAP', $nzap);
            foreach ($attrs as $attr => $v) {
                //Yii::beginProfile('isDate()');
                $isDate = self::dateMatch($pacient, $attr);
                //Yii::endProfile('isDate()');

                //Yii::beginProfile('if isDate');
                if (!empty($isDate) AND !empty($pacient->$attr))
                    $pacient->$attr = date('Y-m-d', strtotime($pacient->$attr));
                //Yii::endProfile('if isDate');

                //Yii::beginProfile('addChild');
                $zap->addChild($attr, trim($pacient->$attr));
                //Yii::endProfile('addChild');
            }
        }
        $dom = new DOMDocument('1.0', 'windows-1251');
        $dom->appendChild($dom->importNode(dom_import_simplexml($sxe), true));
        return $dom->saveXML();
    }
}
