<?php

/**
 * This is the model class for table "pacient_history".
 *
 * The followings are the available columns in table 'pacient_history':
 * @property integer $ID
 * @property integer $IDLOGIN
 * @property string $IDACTION
 * @property string $DATEACTION
 * @property integer $IDZAP
 * @property string $NNAPR
 * @property string $DNAPR
 * @property string $DPGOSP
 * @property string $DNGOSP
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $TEL
 * @property integer $VPOLIS
 * @property string $NPOLIS
 * @property string $TER
 * @property string $SMO
 * @property integer $FOMP
 * @property string $MONAPR
 * @property string $PMONAPR
 * @property string $MEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property integer $PROFO
 * @property integer $PROFK
 * @property string $DSPO
 * @property string $DPOGOSP
 * @property string $DSNAPR
 * @property string $DOGOSP
 * @property integer $PROFONAPR
 * @property integer $PROFKNAPR
 * @property string $DS
 * @property string $VNGOSP
 * @property string $DANUL
 * @property integer $PANUL
 * @property integer $IANUL
 * @property integer $KANUL
 * @property integer $PMOANUL
 * @property string $SPOLIS
 * @property string $NKART
 */
class PacientHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pacient_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IDLOGIN, IDZAP, P, VPOLIS, FOMP, MO, PMO, PROFO, PROFK, PROFONAPR, PROFKNAPR, PANUL, IANUL, KANUL, PMOANUL', 'numerical', 'integerOnly'=>true),
			array('NNAPR, IDACTION, FAM, IM, OT, TEL, NPOLIS, TER, SMO, MONAPR, PMONAPR, MEDRAB, DSPO, DSNAPR, DS, VNGOSP, SPOLIS, NKART', 'length', 'max'=>255),
			array('DATEACTION, DNAPR, DPGOSP, DNGOSP, DR, DPOGOSP, DOGOSP, DANUL', 'safe'),
			array('ID, IDLOGIN, IDACTION, DATEACTION, IDZAP, NNAPR, DNAPR, DPGOSP, DNGOSP, FAM, IM, OT, P, DR, TEL, VPOLIS, NPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, MEDRAB, MO, PMO, PROFO, PROFK, DSPO, DPOGOSP, DSNAPR, DOGOSP, PROFONAPR, PROFKNAPR, DS, VNGOSP, DANUL, PANUL, IANUL, KANUL, PMOANUL, SPOLIS, NKART', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'IDLOGIN' => 'Idlogin',
			'IDACTION' => 'Idaction',
			'DATEACTION' => 'Dateaction',
			'IDZAP' => 'Idzap',
            'NNAPR' => 'Номер направления',
            'DNAPR' => 'Дата направления',
            'DPGOSP' => 'Дата плановой госпитализации',
            'DNGOSP' => 'Дата начала  госпитализации',
            'VNGOSP' => 'Время начала госпитализации',
            'DOGOSP' => 'Дата окончания госпитализации',
            'DANUL' => 'Дата аннулирования направления',
            'PANUL' => 'Причина аннулирования',
            'IANUL' => 'Источник аннулирования',
            'KANUL' => 'Код источника аннулирования',
            'PMOANUL' => 'Подразделение источника аннулирования',
            'FAM' => 'Фамилия',
            'IM' => 'Имя',
            'OT' => 'Отчество',
            'P' => 'Пол',
            'DR' => 'Дата рождения',
            'TEL' => 'Телефон',
            'VPOLIS' => 'Тип полиса',
            'SPOLIS' => 'Серия полиса',
            'NPOLIS' => 'Номер полиса',
            'TER' => 'Территория страхования',
            'SMO' => 'Код СМО',
            'FOMP' => 'Форма оказания медицинской помощи',
            'MONAPR' => 'Код МО, направившей на госпитализацию',
            'PMONAPR' => 'Код подразделения МО, направившей на госпитализацию',
            'PROFONAPR' => 'Профиль отделения',
            'PROFKNAPR' => 'Профиль койки',
            'DS' => 'Диагноз',
            'MEDRAB' => 'Код мед. работника, направившего больного',
            'MO' => 'Код МО',
            'PMO' => 'Код подразделения МО',
            'PROFO' => 'Профиль отделения',
            'PROFK' => 'Профиль койки',
            'NKART' => 'Номер карты',
            'DSPO' => 'Диагноз приемного отделения',
            'DPOGOSP' => 'Дата планового окончания госпитализации',
            'DSNAPR' => 'Диагноз направившего учреждения',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('IDLOGIN',$this->IDLOGIN);
		$criteria->compare('IDACTION',$this->IDACTION);
		$criteria->compare('DATEACTION',$this->DATEACTION,true);
		$criteria->compare('IDZAP',$this->IDZAP);
		$criteria->compare('NNAPR',$this->NNAPR,true);
		$criteria->compare('DNAPR',$this->DNAPR,true);
		$criteria->compare('DPGOSP',$this->DPGOSP,true);
		$criteria->compare('DNGOSP',$this->DNGOSP,true);
		$criteria->compare('FAM',$this->FAM,true);
		$criteria->compare('IM',$this->IM,true);
		$criteria->compare('OT',$this->OT,true);
		$criteria->compare('P',$this->P);
		$criteria->compare('DR',$this->DR,true);
		$criteria->compare('TEL',$this->TEL,true);
		$criteria->compare('VPOLIS',$this->VPOLIS);
		$criteria->compare('NPOLIS',$this->NPOLIS,true);
		$criteria->compare('TER',$this->TER,true);
		$criteria->compare('SMO',$this->SMO,true);
		$criteria->compare('FOMP',$this->FOMP);
		$criteria->compare('MONAPR',$this->MONAPR,true);
		$criteria->compare('PMONAPR',$this->PMONAPR,true);
		$criteria->compare('MEDRAB',$this->MEDRAB,true);
		$criteria->compare('MO',$this->MO);
		$criteria->compare('PMO',$this->PMO);
		$criteria->compare('PROFO',$this->PROFO);
		$criteria->compare('PROFK',$this->PROFK);
		$criteria->compare('DSPO',$this->DSPO,true);
		$criteria->compare('DPOGOSP',$this->DPOGOSP,true);
		$criteria->compare('DSNAPR',$this->DSNAPR,true);
		$criteria->compare('DOGOSP',$this->DOGOSP,true);
		$criteria->compare('PROFONAPR',$this->PROFONAPR);
		$criteria->compare('PROFKNAPR',$this->PROFKNAPR);
		$criteria->compare('DS',$this->DS,true);
		$criteria->compare('VNGOSP',$this->VNGOSP,true);
		$criteria->compare('DANUL',$this->DANUL,true);
		$criteria->compare('PANUL',$this->PANUL);
		$criteria->compare('IANUL',$this->IANUL);
		$criteria->compare('KANUL',$this->KANUL);
		$criteria->compare('PMOANUL',$this->PMOANUL);
		$criteria->compare('SPOLIS',$this->SPOLIS,true);
		$criteria->compare('NKART',$this->NKART,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PacientHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
