<?php

/**
 * This is the model class for table "PacientView".
 *
 * The followings are the available columns in table 'PacientView':
 * @property integer $IDZAP
 * @property string $NNAPR
 * @property string $DNAPR
 * @property string $DPGOSP
 * @property string $DNGOSP
 * @property string $VNGOSP
 * @property string $DOGOSP
 * @property string $DANUL
 * @property integer $PANUL
 * @property integer $IANUL
 * @property integer $KANUL
 * @property integer $PMOANUL
 * @property string $FAM
 * @property string $IM
 * @property string $OT
 * @property integer $P
 * @property string $DR
 * @property string $TEL
 * @property integer $VPOLIS
 * @property string $SPOLIS
 * @property string $NPOLIS
 * @property integer $TER
 * @property integer $SMO
 * @property integer $FOMP
 * @property integer $MONAPR
 * @property integer $PMONAPR
 * @property integer $PROFONAPR
 * @property integer $PROFKNAPR
 * @property string $DS
 * @property string $MEDRAB
 * @property integer $MO
 * @property integer $PMO
 * @property integer $PROFO
 * @property integer $PROFK
 * @property string $NKART
 * @property string $DSPO
 * @property string $DPOGOSP
 * @property integer $IDPO
 * @property string $NAMPK
 * @property string $NAMPO
 * @property string $NAMPMO_hospital
 * @property string $NAMMO_hospital
 * @property string $NAMPMO_clinic
 * @property string $NAMMO_clinic
 * @property integer $KODMO_clinic
 * @property integer $KODMO_hospital
 * @property string $typep
 * @property integer $age
 * @property string $DSNAPR
 * @property string $proxy_fam
 * @property string $proxy_im
 * @property string $proxy_ot
 * @property integer $proxy_p
 * @property string $proxy_dr
 */
class PacientView extends CActiveRecord
{
    public function beforeSave()
    {
        $attrs = $this->attributeLabels();
        foreach ($attrs as $attr => $v) {
            $match = preg_match('/дата/ui', $v);
            if ($match) {
                $this->$attr = empty($this->$attr) ? null : date(Helpers::getDateFormat(), strtotime($this->$attr));
            }

        }
        parent::beforeSave();
    }
    public static function GetXml($pacients){
        if(empty($pacients)) return false;
        $sxe = simplexml_load_string("<?xml version='1.0' encoding='Windows-1251'?><DAN />");
        $attrs = Pacient::model()->getAttributes();
        TbArray::removeValues(array('IDZAP'), $attrs);
        $i = 1;
        foreach($pacients as $pacient){
            $zap = $sxe->addChild('ZAP');
            $zap->addChild('NZAP', $i++);
            foreach($attrs as $attr=>$v){
                Yii::beginProfile('isDate()');
                $isDate = Pacient::dateMatch($pacient, $attr);
                Yii::endProfile('isDate()');

                Yii::beginProfile('if isDate');
                if( ! empty($isDate) AND !empty($pacient->$attr))
                    $pacient->$attr = date('Y-m-d', strtotime($pacient->$attr));
                Yii::endProfile('if isDate');

                Yii::beginProfile('addChild');
                $zap->addChild($attr, trim($pacient->$attr));
                Yii::endProfile('addChild');
            }
        }
        return $sxe;
    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pacientview';
    }

    public function primaryKey()
    {
        return 'IDZAP';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IDZAP', 'required'),
            array('IDZAP, PANUL, IANUL, KANUL, PMOANUL, P, VPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, MO, PMO, PROFO, PROFK, IDPO, KODMO_clinic, KODMO_hospital', 'numerical', 'integerOnly' => true),
            array('NNAPR', 'length', 'max' => 12),
            array('VNGOSP', 'length', 'max' => 5),
            array('FAM, IM, OT, TEL', 'length', 'max' => 40),
            array('SPOLIS', 'length', 'max' => 10),
            array('NPOLIS', 'length', 'max' => 20),
            array('DS, DSPO', 'length', 'max' => 6),
            array('MEDRAB', 'length', 'max' => 11),
            array('NKART', 'length', 'max' => 50),
            array('NAMPK, NAMPMO_hospital, NAMMO_hospital, NAMPMO_clinic, NAMMO_clinic', 'length', 'max' => 150),
            array('NAMPO', 'length', 'max' => 254),
            array('typep', 'length', 'max' => 7),
            array('IDZAP, NNAPR, DNAPR, DPGOSP, DNGOSP, VNGOSP, DOGOSP, DANUL, PANUL, IANUL, KANUL, PMOANUL, FAM, IM, OT, P, DR, TEL, VPOLIS, SPOLIS, NPOLIS, TER, SMO, FOMP, MONAPR, PMONAPR, PROFONAPR, PROFKNAPR, DS, MEDRAB, MO, PMO, PROFO, PROFK, NKART, DSPO, DPOGOSP, IDPO, NAMPK, NAMPO, NAMPMO_hospital, NAMMO_hospital, NAMPMO_clinic, NAMMO_clinic, KODMO_clinic, KODMO_hospital, typep, DSNAPR', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'mkb10_po' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DSPO')),
            'mkb10_napr' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DSNAPR')),
            'mkb10' => array(self::HAS_ONE, 'MKB10', array('KODMKB' => 'DS')),
            'profk_rel' => array(self::HAS_ONE, 'ProfKView', array('IDPK' => 'PROFK')),
            'pmo_rel' => array(self::HAS_ONE, 'PMOView', array('KODPMO' => 'PMO')),
        );
    }
    public function AddressClinic()
    {
        if (empty($this->MONAPR))
            return false;
        $MO = Mo::model()->find('KODMO = :MO', array(':MO' => $this->MONAPR));
        if (!empty($MO->ADRESMO))
            return $MO->ADRESMO;
        else return false;
    }
    public function AddressHospital()
    {
        if (empty($this->MO))
            return false;
        $MO = Mo::model()->find('KODMO = :MO', array(':MO' => $this->MO));
        if (!empty($MO->ADRESMO))
            return $MO->ADRESMO;
        else return false;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'IDZAP' => 'Idzap',
            'NNAPR' => 'Номер направления',
            'DNAPR' => 'Дата выдачи направления',
            'DPGOSP' => 'Дата плановой госпитализации',
            'DNGOSP' => 'Дата начала госпитализации',
            'VNGOSP' => 'Время начала госпитализации',
            'DOGOSP' => 'Дата окончания госпитализации',
            'DANUL' => 'Дата аннулирования',
            'PANUL' => 'Причина аннулирования',
            'IANUL' => 'Источник аннулирования',
            'KANUL' => 'Код источника аннулирования',
            'PMOANUL' => 'ПМО аннулирования',
            'FAM' => 'Фамилия',
            'IM' => 'Имя',
            'OT' => 'Отчество',
            'P' => 'Пол',
            'DR' => 'Дата рождения',
            'TEL' => 'Телефон',
            'VPOLIS' => 'Тип полиса',
            'SPOLIS' => 'Серия полиса',
            'NPOLIS' => 'Номер полиса',
            'TER' => 'Регион',
            'SMO' => 'СМО',
            'FOMP' => 'Форма оказания помощи',
            'MONAPR' => 'МО направившая',
            'PMONAPR' => 'ПМО направившего',
            'PROFONAPR' => 'Профиль отделения направишего',
            'PROFKNAPR' => 'Профиль койки направившей',
            'DS' => 'Диагноз',
            'MEDRAB' => 'Мед. работник',
            'MO' => 'Мед. организация',
            'PMO' => 'Профиль мед. организации',
            'PROFO' => 'Профиль отделения',
            'PROFK' => 'Профиль койки',
            'NKART' => 'Номер карты',
            'DSPO' => 'Диагноз приемного отделения',
            'DPOGOSP' => 'Дата планового окончания',
            'IDPO' => 'Профиль отделения',
            'NAMPK' => 'Наименование профиля койки',
            'NAMPO' => 'Наименование профиля отделения',
            'NAMPMO_hospital' => 'Наименование подразделения стационара',
            'NAMMO_hospital' => 'Наименование стационара',
            'NAMPMO_clinic' => 'Наименование подразделения амбулатории',
            'NAMMO_clinic' => 'Наименование подразделения амбулатории',
            'KODMO_clinic' => 'Код амбулатории',
            'KODMO_hospital' => 'Код стационара',
            'typep' => 'Статус пациента',
            'age' => 'Возраст',
            'DSNAPR' => 'Диагноз направившего учреждения',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @param null $where
     * @param null $filter
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($where = null, $filter = null)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        switch ($where) {
            case 'a':
                if (Yii::app()->user->getState('tip') < 2) {
                    $criteria->compare('MONAPR', Yii::app()->user->getState('kodmo'));
                    //$criteria->compare('PMONAPR', Yii::app()->user->getState('kodpmo'));
                } else {
                    //$criteria->compare('PMONAPR', $this->PMONAPR);
                    $criteria->compare('MONAPR', $this->MONAPR);
                    $criteria->compare('SMO', Yii::app()->user->getState('kodmo'));
                }
                //$criteria->compare('PMO', $this->PMO);
                $criteria->compare('MO', $this->MO);
                break;
            case 'h':
                //$criteria->compare('PMONAPR', $this->PMONAPR);
                $criteria->compare('MONAPR', $this->MONAPR);
                if (Yii::app()->user->getState('tip') < 2) {
                    //$criteria->compare('PMO', Yii::app()->user->getState('kodpmo'));
                    $criteria->compare('MO', Yii::app()->user->getState('kodmo'));
                } else if (Yii::app()->user->getState('tip') == 2) {
                    //$criteria->compare('PMO', $this->PMONAPR);
                    //$criteria->compare('MO', $this->MONAPR);
                    $criteria->compare('SMO', Yii::app()->user->getState('kodmo'));
                }
                break;
            default:
                $criteria->compare('MONAPR', $this->MONAPR);
                //$criteria->compare('PMONAPR', $this->PMONAPR);
                $criteria->compare('PMO', $this->PMO);
                $criteria->compare('MO', $this->MO);
                $criteria->compare('SMO', $this->SMO);
                break;
        }
        $criteria->compare('FOMP', $this->FOMP);
        $criteria->compare('typep', $this->typep, true);
        if (!empty($filter)) {
            switch ($filter) {
                case 'extr':
                    $criteria->compare('FOMP', 3);
                    break;
                case 'all':
                    $criteria->compare('typep', null);
                    break;
                default:
                    $criteria->compare('typep', $filter);
                    break;
            }
        }

        $criteria->compare('MO', $this->MO);

        $criteria->compare('IDZAP', $this->IDZAP);
        $criteria->compare('NNAPR', $this->NNAPR);
        $criteria->compare('DNAPR', $this->DNAPR);
        $criteria->compare('DPGOSP', $this->DPGOSP, true);
        $criteria->compare('DNGOSP', $this->DNGOSP, true);
        $criteria->compare('VNGOSP', $this->VNGOSP, true);
        $criteria->compare('DOGOSP', $this->DOGOSP, true);
        $criteria->compare('DANUL', $this->DANUL, true);
        $criteria->compare('PANUL', $this->PANUL);
        $criteria->compare('IANUL', $this->IANUL);
        $criteria->compare('KANUL', $this->KANUL);
        $criteria->compare('PMOANUL', $this->PMOANUL);
        $criteria->compare('FAM', $this->FAM, true);
        $criteria->compare('IM', $this->IM, true);
        $criteria->compare('OT', $this->OT, true);
        $criteria->compare('P', $this->P);
        $criteria->compare('DR', $this->DR, true);
        $criteria->compare('TEL', $this->TEL, true);
        $criteria->compare('VPOLIS', $this->VPOLIS);
        $criteria->compare('SPOLIS', $this->SPOLIS, true);
        $criteria->compare('NPOLIS', $this->NPOLIS, true);
        $criteria->compare('TER', $this->TER);
        $criteria->compare('PROFONAPR', $this->PROFONAPR);
        $criteria->compare('PROFKNAPR', $this->PROFKNAPR);
        $criteria->compare('DS', $this->DS, true);
        $criteria->compare('MEDRAB', $this->MEDRAB, true);
        $criteria->compare('PROFO', $this->PROFO);
        $criteria->compare('PROFK', $this->PROFK);
        $criteria->compare('NKART', $this->NKART, true);
        $criteria->compare('DSPO', $this->DSPO, true);
        $criteria->compare('DPOGOSP', $this->DPOGOSP, true);
        $criteria->compare('IDPO', $this->IDPO);
        $criteria->compare('NAMPK', $this->NAMPK, true);
        $criteria->compare('NAMPO', $this->NAMPO, true);
        $criteria->compare('NAMPMO_hospital', $this->NAMPMO_hospital, true);
        $criteria->compare('NAMMO_hospital', $this->NAMMO_hospital, true);
        $criteria->compare('NAMPMO_clinic', $this->NAMPMO_clinic, true);
        $criteria->compare('NAMMO_clinic', $this->NAMMO_clinic, true);
        $criteria->compare('KODMO_clinic', $this->KODMO_clinic);
        $criteria->compare('KODMO_hospital', $this->KODMO_hospital);
        $criteria->compare('DSNAPR', $this->DSNAPR);
        $criteria->compare('proxy_fam', $this->proxy_fam, false, 'OR');

        $criteria->compare('age', $this->age, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'IDZAP' => CSort::SORT_DESC,
                )
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PacientView the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
