<?php

/**
 * This is the model class for table "pasport".
 *
 * The followings are the available columns in table 'pasport':
 * @property integer $id
 * @property integer $kodmo
 * @property string $fam
 * @property string $im
 * @property string $ot
 * @property string $tel
 * @property string $email
 * @property string $post
 */
class Pasport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pasport';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('fam, im, tel','required'),
			array('kodmo', 'numerical', 'integerOnly'=>true),
			array('fam, im, ot, tel', 'length', 'max'=>50),
			array('email, post', 'length', 'max'=>255),
			array('email', 'email'),
			array('id, kodmo, fam, im, ot, tel, email, post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
            'id' => 'ID',
            'kodmo' => 'Kodmo',
            'fam' => 'Фамилия',
            'im' => 'Имя',
            'ot' => 'Отчество',
            'tel' => 'Телефон',
            'email' => 'Электропочта',
            'post' => 'Должность',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kodmo',$this->kodmo);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('im',$this->im,true);
		$criteria->compare('ot',$this->ot,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('post',$this->post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pasport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
