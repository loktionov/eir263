<?php

/**
 * This is the model class for table "people".
 *
 * The followings are the available columns in table 'people':
 * @property integer $id
 * @property string $ENP
 * @property string $Name
 * @property string $sName
 * @property string $pName
 * @property string $dateMan
 * @property integer $sexMan
 * @property string $pbMan
 * @property string $nationMan
 * @property integer $typeDoc
 * @property string $serDoc
 * @property string $numDoc
 * @property string $dateDoc
 * @property integer $statusMan
 * @property integer $regKladrId
 * @property string $addressReg
 * @property string $dateReg
 * @property string $addressLive
 * @property integer $activPolesId
 * @property string $dateDeath
 * @property integer $liveKladrId
 * @property string $snils
 * @property string $okato_mj
 * @property string $InsDate
 * @property integer $cr
 * @property string $crdate
 * @property integer $lostman
 * @property string $proxy_man
 * @property string $contact
 * @property string $doc_date_end
 * @property integer $livetemp
 * @property integer $refugee
 * @property string $orgdoc
 * @property integer $FFOMS
 * @property integer $wid
 * @property string $okato_reg
 * @property string $kladr_reg
 * @property string $kladr_live
 *
 * The followings are the available model relations:
 * @property PeopleAudit[] $peopleAudits
 * @property PeopleNGrams[] $peopleNGrams
 * @property PeopleAddressNGrams[] $peopleAddressNGrams
 * @property Reestr[] $reestrs
 * @property NgPeopleFioDateman[] $ngPeopleFioDatemen
 * @property StickMO[] $stickMOs
 * @property NgPeopleAddressReg[] $ngPeopleAddressRegs
 * @property Documents[] $documents
 * @property Nationman[] $nationman1
 */
class People extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
        if(Helpers::getIsMsSql())
        {
            return 'rmp.dbo.people';
        }
		return 'people';
	}

    public function addressReg()
    {
        return $this->getAddress($this->addressReg);
    }
    public function addressLive()
    {
        return $this->getAddress($this->addressLive);
    }
    private function getAddress($str)
    {
        $q = "/[;|]/";
        $str = preg_replace($q,'',$str);
        return $str;
    }
    public function GetFullHistory()
    {

        $sql = "SELECT * FROM [fnGetFullHistory] (:pid)";
        $command=Yii::app()->db->createCommand($sql);
        $command->bindParam(':pid', $this->id, PDO::PARAM_INT);
        $history_array = $command->queryAll();
        $history_res = array();
        for($i = 0; $i < count($history_array); $i++)
        {
            if($i == 0)
            {
                $history_res[] = $history_array[$i];
                continue;
            }
            $ch = false;
            foreach($history_array[$i] as $k=>$v)
            {
                $v = trim($v);
                $pre_v = $history_array[$i - 1][$k];
                $q = "/<div .+?>(.+?)<\/div>/";
                preg_match($q,$pre_v,$matches);
                $pre_v = empty($matches[1]) ? $pre_v : $matches[1];
                if($k == 'serdoc')
                {
                    $q1 = '/^(\d\d)(\d\d)$/';
                    $v = preg_replace($q1, '$1 $2', $v);
                    $pre_v = preg_replace($q1, '$1 $2', $pre_v);

                }
                if(trim($pre_v) != $v and strpos($k,'sysuser') === false and strpos($k,'editdate') === false and strpos($k,'host') === false)
                {
                    $history_array[$i][$k] = "<div class='history-diff'>" . $v . "</div>";
                    $ch = true;
                }
            }
            if($ch)
                $history_res[] = $history_array[$i];
        }
        return new CArrayDataProvider($history_res, array('keyField' => 'editdate'));
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name, sName, dateMan, sexMan', 'required'),
			array('sexMan, typeDoc, statusMan, regKladrId, activPolesId, liveKladrId, cr, lostman, livetemp, refugee, FFOMS, wid', 'numerical', 'integerOnly'=>true),
			array('ENP, snils', 'length', 'max'=>40),
			array('Name, sName, pName', 'length', 'max'=>60),
			array('pbMan', 'length', 'max'=>200),
			array('nationMan, okato_mj, okato_reg', 'length', 'max'=>10),
			array('serDoc, numDoc', 'length', 'max'=>20),
			array('addressReg, addressLive, proxy_man, contact, orgdoc', 'length', 'max'=>255),
			array('kladr_reg, kladr_live', 'length', 'max'=>50),
			array('dateDoc, dateReg, dateDeath, InsDate, crdate, doc_date_end', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ENP, Name, sName, pName, dateMan, sexMan, pbMan, nationMan, typeDoc, serDoc, numDoc, dateDoc, statusMan, regKladrId, addressReg, dateReg, addressLive, activPolesId, dateDeath, liveKladrId, snils, okato_mj, InsDate, cr, crdate, lostman, proxy_man, contact, doc_date_end, livetemp, refugee, orgdoc, FFOMS, wid, okato_reg, kladr_reg, kladr_live', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'peopleAudits' => array(self::HAS_MANY, 'PeopleAudit', 'peopleid'),
			//'peopleNGrams' => array(self::HAS_MANY, 'PeopleNGrams', 'PeopleID'),
			//'peopleAddressNGrams' => array(self::HAS_MANY, 'PeopleAddressNGrams', 'PeopleID'),
			'reestrs' => array(self::HAS_MANY, 'Reestr', 'ManId', 'order' => 'reestrs.statusPoles desc, reestrs.begindate desc, reestrs.typepoles desc, reestrs.id desc'),
			//'ngPeopleFioDatemen' => array(self::HAS_MANY, 'NgPeopleFioDateman', 'PeopleID'),
			//'stickMOs' => array(self::HAS_MANY, 'StickMO', 'peopleid'),
			//'ngPeopleAddressRegs' => array(self::HAS_MANY, 'NgPeopleAddressReg', 'PeopleID'),
			'documents' => array(self::HAS_MANY, 'Documents', 'peopleid', 'with' => 'typedoc0'),
			'typedoc1' => array(self::BELONGS_TO, 'Typedocument', array('typeDoc' => 'id')),
            'nationman1' => array(self::BELONGS_TO, 'Nationman', array('nationMan' => 'ALFA3')),
            'wpeople' => array(self::HAS_MANY, 'People', 'wid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ENP' => 'ЕНП',
			'Name' => 'Имя',
			'sName' => 'Фамилия',
			'pName' => 'Отчество',
			'dateMan' => 'Дата рождения',
			'sexMan' => 'пол',
			'pbMan' => 'Место рождения',
			'nationMan' => 'Гражданство',
			'typeDoc' => 'Тип док',
			'serDoc' => 'Ser Doc',
			'numDoc' => 'Num Doc',
			'dateDoc' => 'Date Doc',
			'statusMan' => 'Status Man',
			'regKladrId' => 'Reg Kladr',
			'addressReg' => 'Address Reg',
			'dateReg' => 'Date Reg',
			'addressLive' => 'Address Live',
			'activPolesId' => 'Activ Poles',
			'dateDeath' => 'Date Death',
			'liveKladrId' => 'Live Kladr',
			'snils' => 'Snils',
			'okato_mj' => 'Okato Mj',
			'InsDate' => 'Ins Date',
			'cr' => 'Cr',
			'crdate' => 'Crdate',
			'lostman' => 'Lostman',
			'proxy_man' => 'Proxy Man',
			'contact' => 'Contact',
			'doc_date_end' => 'Doc Date End',
			'livetemp' => 'Livetemp',
			'refugee' => 'Refugee',
			'orgdoc' => 'Orgdoc',
			'FFOMS' => 'Ffoms',
			'wid' => 'Wid',
			'okato_reg' => 'Okato Reg',
			'kladr_reg' => 'Kladr Reg',
			'kladr_live' => 'Kladr Live',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ENP',$this->ENP,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('sName',$this->sName,true);
		$criteria->compare('pName',$this->pName,true);
		$criteria->compare('dateMan',$this->dateMan,true);
		$criteria->compare('sexMan',$this->sexMan);
		$criteria->compare('pbMan',$this->pbMan,true);
		$criteria->compare('nationMan',$this->nationMan,true);
		$criteria->compare('typeDoc',$this->typeDoc);
		$criteria->compare('serDoc',$this->serDoc,true);
		$criteria->compare('numDoc',$this->numDoc,true);
		$criteria->compare('dateDoc',$this->dateDoc,true);
		$criteria->compare('statusMan',$this->statusMan);
		$criteria->compare('regKladrId',$this->regKladrId);
		$criteria->compare('addressReg',$this->addressReg,true);
		$criteria->compare('dateReg',$this->dateReg,true);
		$criteria->compare('addressLive',$this->addressLive,true);
		$criteria->compare('activPolesId',$this->activPolesId);
		$criteria->compare('dateDeath',$this->dateDeath,true);
		$criteria->compare('liveKladrId',$this->liveKladrId);
		$criteria->compare('snils',$this->snils,true);
		$criteria->compare('okato_mj',$this->okato_mj,true);
		$criteria->compare('InsDate',$this->InsDate,true);
		$criteria->compare('cr',$this->cr);
		$criteria->compare('crdate',$this->crdate,true);
		$criteria->compare('lostman',$this->lostman);
		$criteria->compare('proxy_man',$this->proxy_man,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('doc_date_end',$this->doc_date_end,true);
		$criteria->compare('livetemp',$this->livetemp);
		$criteria->compare('refugee',$this->refugee);
		$criteria->compare('orgdoc',$this->orgdoc,true);
		$criteria->compare('FFOMS',$this->FFOMS);
		$criteria->compare('wid',$this->wid);
		$criteria->compare('okato_reg',$this->okato_reg,true);
		$criteria->compare('kladr_reg',$this->kladr_reg,true);
		$criteria->compare('kladr_live',$this->kladr_live,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return People the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function ENPSum($enp)
    {
        // нечетные цифры начниная справа
        $s1 = '';
        // четные цифры начиная справа
        $s2 = '';
        for($i=14; $i>=0; $i--)
        {
            if($i % 2 == 0)
                $s1 .= $enp[$i];
            else
                $s2 .= $enp[$i];
        }
        if($s1 == '') {
            $s1 = 0;
        }
        else {
            $s1 = (int)$s1*2;
        }
        // приписываем "нечетное" число слева от "четного" умноженного на 2
        $s = (int)$s2 . (int)$s1;
        // складываем все цифры полученного числа
        $sum = 0;
        for($i = 0; $i<strlen($s);  $i++)
        {
            $u = (int)$s[$i];
            $sum += $u;
        }
        //  число вычитается из ближайшего большего или равного числа, кратного 10.
        $k = 10 - ($sum % 10);
        if( $k == 10)
            $k = 0;
        return $k;
    }
    public static function CheckENP($enp, DateTime $dateman, $sexman)
    {
        $count = strlen($enp);
        if($count != 16)
        {
            return 1;
            //("Неверное количество символов в енп $enp - $count\n");
        }
        $k = self::ENPSum($enp);
        if($k != $enp[15])
        {
            return 2;
            //iconv_dos("Неверная контрольная сумма $k");
        }
        $firstpartReal = self::GetENPFirstpart($dateman, $sexman);
        $firstpartGet = substr($enp, 2, 8);
        if( $firstpartGet != $firstpartReal )
        {
            return 3;
            //iconv_dos("Неверна вычисляемая часть енп $enp $firstpartReal");
        }
        return true;
    }
    /**
     * @param $DateBirthday
     * @param $sexMan
     * @return string
     */
    public static function GetENPFirstpart(DateTime $DateBirthday, $sexMan)
    {
        $d = $DateBirthday->format("d");
        $m = $DateBirthday->format("m");
        $y = $DateBirthday->format("Y");

        if ($y <= 1950)
            $m = (int)$m + 20;
        if ($y >= 1951 && $y <= 2000)
            $m = $m + 40;
        $m1 = 9 - substr($m, 0, 1);
        $m2 = 9 - substr($m, 1, 1);
        $m = $m1 . $m2;

        $y1 = 9 - substr($y, 0, 1);
        $y2 = 9 - substr($y, 1, 1);
        $y3 = 9 - substr($y, 2, 1);
        $y4 = 9 - substr($y, 3, 1);
        $y = $y4 . $y3 . $y2 . $y1;

        if ($sexMan == 1)
            $d = (int)$d + 50;
        $d1 = 9 - substr($d, 0, 1);
        $d2 = 9 - substr($d, 1, 1);
        $d = $d1 . $d2;

        $firstpart = $m . $y . $d;
        return $firstpart;
    }
    public function GetExportArray($num=1)
    {
        $zl = ($num == 2) ? "Второе ЗЛ:" : "Первое ЗЛ:";
        $col1 = 'col1';
        $col2 = 'col2';
        if($this->isNewRecord)
            return false;
        if( ! isset($this->reestrs))
            return false;
        $polises = array();
        for($i = 0; $i < count($this->reestrs); $i++)
        {

            $col = empty($i) ? 'История страхования' : '';
            $polis = Helpers::getTypepolis($this->reestrs[$i]->typePoles)
                . ' '
                . date(Helpers::getDateFormat(),strtotime($this->reestrs[$i]->beginDate))
                . ' - '
                . date(Helpers::getDateFormat(),strtotime($this->reestrs[$i]->endDate))
                . ' '
                . $this->reestrs[$i]->ENumber
                ;
            $polises[] = array(
                $col1=>$col,
                $col2=>$polis,
            );
        }
        $polises[] = array($col1=>'',$col2=>'',);
        $res = array(
            array(
                $col1=>$zl,
                $col2=>'Данные',
            ),
            array(
                $col1=>'Фамилия',
                $col2=>$this->sName,
            ),
            array(
                $col1=>'Имя',
                $col2=>$this->Name,
            ),
            array(
                $col1=>'Отчество',
                $col2=>$this->pName,
            ),
            array(
                $col1=>'Дата рождения',
                $col2=>date(Helpers::getDateFormat(), strtotime($this->dateMan)),
            ),
            array(
                $col1=>'СНИЛС',
                $col2=>$this->snils,
            ),
            array(
                $col1=>'Тип документа, удостоверяющего личность (УДЛ)',
                $col2=>$this->typeDoc,
            ),
            array(
                $col1=>'Серия документа УДЛ',
                $col2=>$this->serDoc,
            ),
            array(
                $col1=>'Номер документа УДЛ',
                $col2=>$this->numDoc,
            ),
            array(
                $col1=>'ЕНП',
                $col2=>$this->ENP,
            ),
            array(
                $col1=>'Место рождения',
                $col2=>$this->pbMan,
            ),
        );

        return array_merge($res, $polises);

    }
    //выбирает неверно определенных двойников
    public static function GetDouble()
    {
        $q = Yii::app()->db->createCommand()
            ->select('p1.id id1, p1.lostman lostman1, p2.id id2, p2.lostman lostman2')
            ->from('people p1')
            ->join('people p2','p1.id = p2.wid')
            ->queryAll();
        foreach($q as $p)
        {


            $r1 = Yii::app()->db->createCommand()
                ->select('id reestrid1, begindate b1, reasonstm stm1')
                ->limit(1)
                ->from('reestr')
                ->where('manid = :id', array(':id'=>$p['id1']))
                ->order('statuspoles desc, begindate desc, typepoles desc')
                ->queryRow();


            $r2 = Yii::app()->db->createCommand()
                ->select('id reestrid2, begindate b2')
                ->limit(1)
                ->from('reestr')
                ->where('manid = :id', array(':id'=>$p['id2']))
                ->order('statuspoles desc, begindate desc, typepoles desc')
                ->queryRow();

            $b1 = strtotime($r1['b1']);
            $b2 = strtotime($r2['b2']);

            if($b1 < $b2)
            {
                $t = array_merge($p, $r1, $r2);
                if(!empty($t))
                    $res[] = $t;
            }

        }
        if( ! isset($res))
            return;

        foreach($res as $r)
        {
            $q = Yii::app()->db->createCommand('exec [AnnulatePeopleDouble] :id2, :id1')
                ->bindParam(':id1', $r['id1'], PDO::PARAM_INT)
                ->bindParam(':id2', $r['id2'], PDO::PARAM_INT);
            $tmp1 = $q->execute();


                $out = 0;
                $r['stm1'] = empty($r['stm1']) ? 11 : $r['stm1'];
                $q = Yii::app()->db->createCommand('exec [ActivePeople] :id, :uid, :out, :stm1')
                    ->bindParam(':id', $r['id2'], PDO::PARAM_INT)
                    ->bindParam(':uid', Yii::app()->user->id, PDO::PARAM_INT)
                    ->bindParam(':out', $out, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT,2)
                    ->bindParam(':stm1', $r['stm1'], PDO::PARAM_INT);

                $q->execute();
                if(empty($out))
                {
                    $stop = true;
                }

        }
    }

    public function sqlsearch($r)
    {
        return array();
    }
}
