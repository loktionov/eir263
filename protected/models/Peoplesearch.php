<?php
/**

 * User: Loktionov
 * Date: 13.01.14
 * Time: 14:35
 */

class Peoplesearch extends People
{
    private $res_count = 10;

    /**
     * @param $r Reestrsearch
     * @return array
     */
    public function sqlsearch($r)
    {
        $dbo = '';
        if(Helpers::getIsMsSql())
            $dbo = 'rmp.dbo.';
        $sname = $this->sName;
        $name = $this->Name;
        $pname = $this->pName;
        $dateman = $this->dateMan;
        $dateman = empty($dateman) ? null : date(Helpers::getDateFormat(), strtotime($dateman));
        $enp = $this->ENP;
        $enumber = $r->ENumber;

        $criteria = new CDbCriteria();
        //$criteria->with = array(array('documents' => array('condition' => 'documents.actual = 1')), 'typedoc1', 'nationman', 'reestrs', 'reestrs.org', 'wpeople');
        if (!empty($enp)) {
            $criteria->addCondition('t.ENP = :enp');
            $criteria->params[':enp'] = $enp;
        }
        if (!empty($sname)) {
            $criteria->addCondition('t.sName = :sname');
            $criteria->params[':sname'] = $sname;
        }

        if (!empty($name)) {
            $criteria->addCondition('t.Name = :name');
            $criteria->params[':name'] = $name;
        }
        if (!empty($pname)) {
            $criteria->addCondition('t.pName = :pname');
            $criteria->params[':pname'] = $pname;
        }
        if (!empty($dateman)) {
            $criteria->addCondition('t.dateMan = :dateman');
            $criteria->params[':dateman'] = $dateman;
        }
        if (!empty($enumber)) {
            $manid = Yii::app()->db->createCommand()
                ->selectDistinct('t.manid')
                ->from($dbo.'reestr t')
                ->where('t.ENumber = :enumber', array(':enumber' => $enumber))
                ->queryAll();
            $in_array = array();
            foreach ($manid as $id) {
                $in_array[] = $id['manid'];
            }
            $criteria->addinCondition('t.id' , $in_array);
        }
        if (!empty($criteria->condition)) {
            $peoplecount = Yii::app()->db->createCommand()
                ->select('count(distinct t.id)')
                ->from($dbo.'people t')
                ->where($criteria->condition, $criteria->params)
                ->queryScalar();
        }
        else{
            $peoplecount = -1;
        }
        $peoplesDataProvider = null;
        $mes = array();
        if ($peoplecount > 0 and $peoplecount <= $this->res_count) {
            $peoples = new People();
            $peoples = $peoples->findAll($criteria->condition, $criteria->params);
            $peoplesDataProvider = new CArrayDataProvider($peoples, array(
                'id' => 'people-provider',
                'sort' => array(
                    'attributes' => array(
                        'sName',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ));
        } elseif ($peoplecount > $this->res_count) {
            $mes[TbHtml::ALERT_COLOR_WARNING] = TbHtml::b("Количество результатов больше $this->res_count.") . ' Попробуйте сузить поиск.';
        }
        elseif($peoplecount == 0) {
            $mes[TbHtml::ALERT_COLOR_INFO] = TbHtml::b('Никого не найдено.') . ' Попробуйте расширить поиск.';
        }
        return array($peoplesDataProvider, $mes);
    }
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('typeDoc, regKladrId, liveKladrId, wid', 'numerical', 'integerOnly'=>true),
            array('ENP, snils', 'length', 'max'=>40),
            array('Name, sName, pName', 'length', 'max'=>60),
            array('pbMan', 'length', 'max'=>200),
            array('nationMan, okato_mj, okato_reg', 'length', 'max'=>10),
            array('serDoc, numDoc', 'length', 'max'=>20),
            array('addressReg, addressLive, proxy_man, contact, orgdoc', 'length', 'max'=>255),
            array('kladr_reg, kladr_live', 'length', 'max'=>50),
            array('dateDoc, dateReg, dateDeath, InsDate, crdate, doc_date_end, dateMan', 'safe'),
            array('id, ENP, Name, sName, pName, dateMan, typeDoc, serDoc, numDoc, regKladrId, liveKladrId, snils, kladr_reg, kladr_live', 'safe', 'on'=>'search'),
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('ENP',$this->ENP,true);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('sName',$this->sName,true);
        $criteria->compare('pName',$this->pName,true);
        $criteria->compare('dateMan',$this->dateMan,true);
        $criteria->compare('typeDoc',$this->typeDoc);
        $criteria->compare('serDoc',$this->serDoc,true);
        $criteria->compare('numDoc',$this->numDoc,true);
        $criteria->compare('regKladrId',$this->regKladrId);
        $criteria->compare('liveKladrId',$this->liveKladrId);
        $criteria->compare('snils',$this->snils,true);
        $criteria->compare('kladr_reg',$this->kladr_reg,true);
        $criteria->compare('kladr_live',$this->kladr_live,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

} 