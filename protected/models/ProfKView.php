<?php

/**
 * This is the model class for table "ProfKView".
 *
 * The followings are the available columns in table 'ProfKView':
 * @property integer $IDPK
 * @property integer $IDPO
 * @property string $NAMPK
 * @property string $NAMPO
 */
class ProfKView extends CActiveRecord
{
    public function primaryKey()
    {
        return 'IDPK';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'profkview';
    }

    public static function GetOptionsArray()
    {
        /** @var $opts self[] */
        $opts = self::model()->findAll();
        $ret = array();
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt->IDPK,
                'cat' => $opt->NAMPO,
                'name' => $opt->NAMPK,
            );
        }
        return $ret;
    }

    public static function GetOptGroupsArray()
    {
        $join = ' left join pacient p on p.profk=t.idpk';
        $order = ' order by name';
        $opts = Yii::app()->db->cache(3600)->createCommand("
            select * from (
            select distinct t.idpk as [kod], t.nampk as [name], case when p.IDZAP is null then 'no' else 'yes' end as [optgroup] from profk t " . $join
            . ') as t '
            . $order
        )->queryAll();
        return $opts;
    }

    public static function GetProfKArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAMPK'));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[$opt->IDPK] = $opt->NAMPK;
        }
        return $ret;
    }

    public static function GetProfOArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAMPK'));
        $ret = array();
        foreach ($opts as $opt) {
            $ret[$opt->IDPO] = $opt->NAMPO;
        }
        return $ret;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IDPK', 'required'),
            array('IDPK, IDPO', 'numerical', 'integerOnly' => true),
            array('NAMPK', 'length', 'max' => 150),
            array('NAMPO', 'length', 'max' => 254),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('IDPK, IDPO, NAMPK, NAMPO', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'IDPK' => 'Idpk',
            'IDPO' => 'Idpo',
            'NAMPK' => 'Nampk',
            'NAMPO' => 'Nampo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('IDPK', $this->IDPK);
        $criteria->compare('IDPO', $this->IDPO);
        $criteria->compare('NAMPK', $this->NAMPK, true);
        $criteria->compare('NAMPO', $this->NAMPO, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProfKView the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
