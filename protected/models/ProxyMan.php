<?php

/**
 * This is the model class for table "proxy_man".
 *
 * The followings are the available columns in table 'proxy_man':
 * @property integer $id
 * @property integer $idzap
 * @property string $fam
 * @property string $im
 * @property string $ot
 * @property integer $p
 * @property string $dr
 */
class ProxyMan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proxy_man';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fam, im, p, dr', 'required'),
			array('idzap, p', 'numerical', 'integerOnly'=>true),
			array('fam, im, ot', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idzap, fam, im, ot, p, dr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idzap' => 'Idzap',
			'fam' => 'Фамилия представителя',
			'im' => 'Имя представителя',
			'ot' => 'Отчество представителя',
			'p' => 'Пол представителя',
			'dr' => 'Дата рождения представителя',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idzap',$this->idzap);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('im',$this->im,true);
		$criteria->compare('ot',$this->ot,true);
		$criteria->compare('p',$this->p);
		$criteria->compare('dr',$this->dr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProxyMan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
