<?php

/**
 * This is the model class for table "reestr".
 *
 * The followings are the available columns in table 'reestr':
 * @property integer $id
 * @property string $ENumber
 * @property string $OldNumber
 * @property integer $ManId
 * @property integer $orgId
 * @property integer $typePoles
 * @property string $beginDate
 * @property string $endDate
 * @property string $stmDate
 * @property integer $stmType
 * @property integer $formPoles
 * @property integer $reasonStm
 * @property string $numBlank
 * @property string $numCard
 * @property integer $statusPoles
 * @property string $AnnulDate
 * @property string $InsDate
 * @property integer $cr
 * @property string $crdate
 * @property string $crcomment
 * @property integer $PVCode
 * @property integer $reasonG
 *
 * The followings are the available model relations:
 * @property People $man
 * @property Smo $org

 */
class Reestr extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
        if(Helpers::getIsMsSql())
        {
            return 'rmp.dbo.reestr';
        }
        return 'reestr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ManId, orgId, typePoles, statusPoles', 'required'),
			array('ManId, orgId, typePoles, stmType, formPoles, reasonStm, statusPoles, cr, PVCode, reasonG', 'numerical', 'integerOnly'=>true),
			array('ENumber, numCard', 'length', 'max'=>40),
			array('OldNumber', 'length', 'max'=>100),
			array('numBlank', 'length', 'max'=>20),
			array('crcomment', 'length', 'max'=>50),
			array('beginDate, endDate, stmDate, AnnulDate, InsDate, crdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ENumber, OldNumber, ManId, orgId, typePoles, beginDate, endDate, stmDate, stmType, formPoles, reasonStm, numBlank, numCard, statusPoles, AnnulDate, InsDate, cr, crdate, crcomment, PVCode, reasonG', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'man' => array(self::BELONGS_TO, 'People', 'ManId'),
			'org' => array(self::BELONGS_TO, 'Smo', array('orgId'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ENumber' => 'Номер полиса',
			'OldNumber' => 'Old Number',
			'ManId' => 'Man',
			'orgId' => 'Org',
			'typePoles' => 'Type Poles',
			'beginDate' => 'Begin Date',
			'endDate' => 'End Date',
			'stmDate' => 'Stm Date',
			'stmType' => 'Stm Type',
			'formPoles' => 'Form Poles',
			'reasonStm' => 'Reason Stm',
			'numBlank' => 'Num Blank',
			'numCard' => 'Num Card',
			'statusPoles' => 'Status Poles',
			'AnnulDate' => 'Annul Date',
			'InsDate' => 'Ins Date',
			'cr' => 'Cr',
			'crdate' => 'Crdate',
			'crcomment' => 'Crcomment',
			'PVCode' => 'Pvcode',
			'reasonG' => 'Reason G',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ENumber',$this->ENumber,true);
		$criteria->compare('OldNumber',$this->OldNumber,true);
		$criteria->compare('ManId',$this->ManId);
		$criteria->compare('orgId',$this->orgId);
		$criteria->compare('typePoles',$this->typePoles);
		$criteria->compare('beginDate',$this->beginDate,true);
		$criteria->compare('endDate',$this->endDate,true);
		$criteria->compare('stmDate',$this->stmDate,true);
		$criteria->compare('stmType',$this->stmType);
		$criteria->compare('formPoles',$this->formPoles);
		$criteria->compare('reasonStm',$this->reasonStm);
		$criteria->compare('numBlank',$this->numBlank,true);
		$criteria->compare('numCard',$this->numCard,true);
		$criteria->compare('statusPoles',$this->statusPoles);
		$criteria->compare('AnnulDate',$this->AnnulDate,true);
		$criteria->compare('InsDate',$this->InsDate,true);
		$criteria->compare('cr',$this->cr);
		$criteria->compare('crdate',$this->crdate,true);
		$criteria->compare('crcomment',$this->crcomment,true);
		$criteria->compare('PVCode',$this->PVCode);
		$criteria->compare('reasonG',$this->reasonG);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reestr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
