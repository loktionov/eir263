<?php
/**

 * User: Loktionov
 * Date: 13.01.14
 * Time: 15:17
 */

class Reestrsearch extends Reestr {

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('ManId, orgId, typePoles, statusPoles', 'required'),
            array('ManId, orgId, typePoles, stmType, formPoles, reasonStm, statusPoles, cr, PVCode, reasonG', 'numerical', 'integerOnly'=>true),
            array('ENumber, numCard', 'length', 'max'=>40),
            array('OldNumber', 'length', 'max'=>100),
            array('numBlank', 'length', 'max'=>20),
            array('crcomment', 'length', 'max'=>50),
            array('beginDate, endDate, stmDate, AnnulDate, InsDate, crdate', 'safe'),
        );
    }
} 