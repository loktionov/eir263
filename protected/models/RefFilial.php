<?php

/**
 * This is the model class for table "ref_filial".
 *
 * The followings are the available columns in table 'ref_filial':
 * @property string $CODE
 * @property string $NAME
 * @property string $EMAIL
 * @property string $NAME_RAY
 * @property string $OKATO
 * @property string $OKATO_CITY
 */
class RefFilial extends CActiveRecord
{
    public static function GetFilialArray()
    {
        $opts = self::model()->cache(3600)->findAll(array('order' => 'NAME_RAY'));
        $ret = array();
        foreach($opts as $opt){
            $ret[$opt->OKATO] = $opt->NAME_RAY;

        }
        return $ret;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ref_filial';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CODE, NAME, EMAIL, NAME_RAY, OKATO, OKATO_CITY', 'required'),
			array('CODE', 'length', 'max'=>2),
			array('NAME, NAME_RAY', 'length', 'max'=>50),
			array('EMAIL', 'length', 'max'=>30),
			array('OKATO, OKATO_CITY', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CODE, NAME, EMAIL, NAME_RAY, OKATO, OKATO_CITY', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CODE' => 'Code',
			'NAME' => 'Name',
			'EMAIL' => 'Email',
			'NAME_RAY' => 'Name Ray',
			'OKATO' => 'Okato',
			'OKATO_CITY' => 'Okato City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CODE',$this->CODE,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('EMAIL',$this->EMAIL,true);
		$criteria->compare('NAME_RAY',$this->NAME_RAY,true);
		$criteria->compare('OKATO',$this->OKATO,true);
		$criteria->compare('OKATO_CITY',$this->OKATO_CITY,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RefFilial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
