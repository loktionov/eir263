<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class RegisterForm extends CFormModel
{
	public $prof;
	public $mo;
	public $terr;
	public $gdate;
	public $aprox;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			//array('name, email, subject, body', 'required'),
			// email has to be a valid email address
			//array('gdate', 'date', 'format' => 'dd-MM-yyyy'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'prof'=>'Профиль отделения',
			'mo'=>'Стационар',
			'terr'=>'Территория',
			'gdate'=>'Дата планируемой госпитализации',
			'aprox'=>'± 3 дня', //alt+0177
		);
	}
}