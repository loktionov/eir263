<?php

/**
 * This is the model class for table "report_f14_view".
 *
 * The followings are the available columns in table 'report_f14_view':
 * @property integer $KODMO
 * @property string $NAMMO
 * @property integer $sk
 * @property integer $reserve
 * @property integer $f14
 * @property integer $dif
 */
class ReportF14View extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'report_f14_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODMO, f14', 'required'),
			array('KODMO, sk, reserve, f14, dif', 'numerical', 'integerOnly'=>true),
			array('NAMMO', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('KODMO, NAMMO, sk, reserve, f14, dif', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODMO' => 'Kodmo',
			'NAMMO' => 'Nammo',
			'sk' => 'Sk',
			'reserve' => 'Reserve',
			'f14' => 'F14',
			'dif' => 'Dif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODMO',$this->KODMO);
		$criteria->compare('NAMMO',$this->NAMMO,true);
		$criteria->compare('sk',$this->sk);
		$criteria->compare('reserve',$this->reserve);
		$criteria->compare('f14',$this->f14);
		$criteria->compare('dif',$this->dif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReportF14View the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
