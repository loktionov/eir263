<?php

/**
 * ReportForm class.
 * ReportForm is the data structure for keeping
 * report form data.
 * @property string category
 * @property string kodmo
 * @property DateTime date_range_start
 * @property DateTime date_range_end
 * @property string date_range_str
 */
class ReportForm extends CFormModel
{
    const REPORT_ALIAS_WEEK = 'week';
    const REPORT_ALIAS_MONTH = 'month';
    const REPORT_ALIAS_DAY = 'day';
    const REPORT_ALIAS_QUARTER = 'quarter';
    const REPORT_ALIAS_OWN = 'own';

    const REPORT_CATEGORY_FOND = 'fond';
    const REPORT_CATEGORY_SMO = 'smo';
    const REPORT_CATEGORY_PROFK = 'profk';
    const REPORT_CATEGORY_MO_A = 'mo_a';
    const REPORT_CATEGORY_MO_H = 'mo_h';

    public $blocked = 0;

    /**
     * @param string $table
     * @return string
     */
    public static function getExpiredWhere($table = '')
    {
        if (!empty($table))
            $table = $table . '.';
        $where = "DATEADD(day, 30, {$table}DNAPR) < isnull({$table}DNGOSP, getdate()) and {$table}DNAPR between :prev_dt and :dt and {$table}DANUL is null";
        return $where;
    }

    /**
     * @param mixed $date_range_str
     */
    public function setDate_range_str($date_range_str)
    {
        $this->date_range = $date_range_str;
        $this->_date_range_str = $date_range_str;
    }

    /**
     * @return mixed
     */
    public function getDate_range_str()
    {
        return $this->_date_range_str;
    }

    public $blocked_er = 0;
    public $out = 0;
    public $planed = 0;
    public $annuled = 0;

    public $bed_days = 0;
    public $negative = 0;
    public $expired = 0;

    public $date_range_alias = self::REPORT_ALIAS_DAY;
    public $date_range_start;
    public $date_range_end;
    private $_date_range_str;

    public $dop_params;

    public $category = self::REPORT_CATEGORY_FOND;

    public $kodmo;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('date_range', 'required'),
            //array('blocked, blocked_er, out, planed, annuled', 'numeric', 'integerOnly' => true),
            array('date_range_alias,date_range_str, date_range_start, date_range_end, category, kodmo', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'blocked' => 'Занято',
            'blocked_er' => 'Занято экстренно',
            'out' => 'Выписано',
            'planed' => 'Бронь',
            'annuled' => 'Аннулировано',
            'date_range' => 'Диапазон дат',
            'category' => 'В разрезе',
            'kodmo' => 'Код организации',
        );
    }

    /**
     * @param string $date_range
     * @param bool $count
     */
    public function __construct($date_range = null, $count = false)
    {
        if (isset($date_range)) {
            $this->date_range = $date_range;
        } elseif (isset($this->date_range_alias)) {
            $this->date_range = $this->date_range_alias;
        } else {
            $this->date_range = self::REPORT_ALIAS_DAY;
        }
        if ($count)
            $this->countAll();
    }

    public static function getCategoriesArray()
    {
        return array(
            self::REPORT_CATEGORY_FOND => 'ТФОМС',
            self::REPORT_CATEGORY_SMO => 'СМО',
            self::REPORT_CATEGORY_PROFK => 'Койки',
            self::REPORT_CATEGORY_MO_H => 'Стационар',
            self::REPORT_CATEGORY_MO_A => 'Амбулатория',
        );
    }

    public static function getTypepArray()
    {
        return array(
            'blocked',
            'blocked_er',
            'out',
            'planed',
            'annuled',
            'expired',
        );
    }

    public static function getRangesArray()
    {
        return array(
            self::REPORT_ALIAS_DAY => 'День',
            self::REPORT_ALIAS_WEEK => 'Неделя',
            self::REPORT_ALIAS_MONTH => 'Месяц',
            self::REPORT_ALIAS_QUARTER => 'Квартал',
            self::REPORT_ALIAS_OWN => 'Свой',
        );
    }

    /**
     * @param $date_range
     * @throws InvalidArgumentException
     */
    public function setDate_range($date_range)
    {
        if (empty($date_range)) {
            $date_range = $this->date_range_alias;
        }
        if (is_array($date_range)) {
            list($this->date_range_start, $this->date_range_end) = $date_range;
        }
        $ranges = self::getRangesArray();
        $keys = array_search($date_range, array_keys($ranges));
        if ($keys !== false) {
            $this->date_range_alias = $date_range;
        }
        if (is_string($date_range)) {
            list($this->date_range_start, $this->date_range_end) = Helpers::parseRange($this->switchRange($date_range));
        } else {
            throw new InvalidArgumentException('Ошибка разбора диапазона дат');
        }
    }

    /**
     * @return array
     */
    public function getDate_range()
    {
        return array($this->date_range_start, $this->date_range_end);
    }

    public function switchRange($date_range, $get_time = false, $set_var = true)
    {
        $now = date(Helpers::getDateFormat($get_time));
        $format = Helpers::getDateFormat();
        $date_range_str = '';
        $date_range_alias = '';
        switch ($date_range) {
            case self::REPORT_ALIAS_MONTH:
                $date_range_alias = self::REPORT_ALIAS_MONTH;
                $date_range_str = date(str_replace('d', '01', $format)) . ' - ' . $now;
                break;
            case self::REPORT_ALIAS_WEEK:
                $date_range_alias = self::REPORT_ALIAS_WEEK;
                $d = new DateTime();
                $d->modify('-7 days');
                $date_range_str = $d->format($format) . ' - ' . $now;
                break;
            case self::REPORT_ALIAS_DAY:
                $date_range_alias = self::REPORT_ALIAS_DAY;
                $date_range_str = date($format) . ' - ' . $now;
                break;
            case self::REPORT_ALIAS_QUARTER:
                $date_range_alias = self::REPORT_ALIAS_QUARTER;
                $d = new DateTime();
                $d->modify('-120 days');
                $date_range_str = $d->format($format) . ' - ' . $now;
                break;
            /*case self::REPORT_ALIAS_ALL:
                $date_range_alias = self::REPORT_ALIAS_WEEK;
                $d = new DateTime(Yii::app()->params['startDate']);
                return $d->format($format) . ' - ' . $now;
                break;
            */
            default:
                $date_range_str = $date_range;
                $date_range_alias = ReportForm::REPORT_ALIAS_OWN;
                break;
        }
        if ($set_var) {
            $this->_date_range_str = $date_range_str;
            $this->date_range_alias = $date_range_alias;
        }
        return $date_range_str;
    }

    private function getMyQuery()
    {
        return <<<MYCOUNTALL
        select poc.typep, count(poc.typep) c from (
        select
		case
			when p.DANUL between :prev_dt and :dt then 'annuled'
			when p.DNGOSP between :prev_dt and :dt and p.DNGOSP < IFNULL(p.DANUL, NOW()) and IFNULL(p.DOGOSP, NOW()) >= :dt and fomp=1 then 'blocked'
			when p.DNGOSP between :prev_dt and :dt and p.DNGOSP < IFNULL(p.DANUL, NOW()) and IFNULL(p.DOGOSP, NOW()) >= :dt and fomp=3 then 'blocked_er'
			when p.DOGOSP between :prev_dt and :dt then 'out'
			when p.DPGOSP between :prev_dt and :dt and IFNULL(p.DNGOSP, NOW()) >= :dt and IFNULL(p.DOGOSP, NOW()) >= :dt then 'planed'
			else 'error'
		end as typep
		from Pacient p
		where p.DNAPR <= :dt) poc
		group by poc.typep
MYCOUNTALL;
    }

    private function getMsQuery()
    {
        $res = Yii::app()->db->createCommand()
            ->select('poc.typep, count(poc.typep) c')
            ->from('fn_pacient_past(:prev_dt, :dt) poc')
            ->group('poc.typep');
        $res->params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat(true));
        $res->params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));

        if (!empty($this->kodmo)) {
            $attr = $this->getCommonWhere();
            $res->where("poc.{$attr} = :kodmo");
            $res->params[':kodmo'] = $this->kodmo;
        }
        return $res;
    }
    public function getFomsReport()
    {
        if (!$this->date_range)
            return false;
        $params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat(true));
        $params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        $res = Yii::app()->db->createCommand("SELECT * FROM [dbo].[fn_foms_report] (:prev_dt,:dt) order by nn");
        $res->params = $params;
        $s = $res->queryAll();
        $first_january = '01.01.' . $this->date_range_start->format('Y');
        $res_year = Yii::app()->db->createCommand("SELECT * FROM [dbo].[fn_foms_report] (:prev_dt,:dt) order by nn");
        $res_year->params[':prev_dt'] = $first_january;
        $res_year->params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));

        $s_year = $res_year->queryAll();

        //$s_year = $s;

        $res = array();
        $res_year = array();
        foreach($s as $row){
            $res[$row['detail']]=$row['count'];
        }
        foreach($s_year as $row){
            $res_year[$row['detail']]=$row['count'];
        }
        return array($res, $res_year);
    }
    public function getVolumeMedHelp()
    {
        if (!$this->date_range)
            return false;
        $where = '';
        $params[':isnulldt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        $params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat(true));
        $params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        if (!empty($this->kodmo)) {
            $this->category = self::REPORT_CATEGORY_MO_H;
            $attr = $this->getCommonWhere();
            $where .= " and {$attr} = :kodmo";
            $params[':kodmo'] = $this->kodmo;
        }
        $res = Yii::app()->db->createCommand("
select NAMMO, NAMPK, count(*) c, sum(dif) s from (select
DATEDIFF(day, DNGOSP, isnull(DOGOSP, dateadd(day, 1, :isnulldt))) dif
,mo.NAMMO, pk.NAMPK from Pacient p
inner join ProfK pk on pk.IDPK=p.PROFK
inner join mo on mo.KODMO=p.mo
where DNGOSP between :prev_dt and :dt
--and mo='260077'
$where
) a
group by NAMMO, NAMPK
order by s desc
        ");
        $res->params = $params;
        $s = $res->queryAll();
        return empty($s) ? false : $s;
    }

    public function getExpired()
    {
        if (!$this->date_range)
            return false;
        $where = self::getExpiredWhere();
        $params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat(true));
        $params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        if (!empty($this->kodmo)) {
            $attr = $this->getCommonWhere();
            $where .= " and poc.{$attr} = :kodmo";
            $params[':kodmo'] = $this->kodmo;
        }
        $res = Yii::app()->db->createCommand()
            ->select('count(*) as c, sum(DATEDIFF(day, dpgosp, isnull(dngosp, ' . "'{$params[':dt']}'" . '))) as s')
            ->from('Pacient as poc')
            ->where($where);
        $res->params = $params;
        $s = $res->queryAll();
        $this->expired = !empty($s[0]['c']) ? $s[0]['c'] : 0;
        return true;
    }

    public function getExpiredByMo($count = true)
    {
        if (!$this->date_range)
            return false;
        if(empty($this->dop_params) and !$count)
            return false;
        $where = 'where ' . self::getExpiredWhere();
        $params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat(true));
        $params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        if (!empty($this->kodmo)) {
            $attr = $this->getCommonWhere();
            if ($this->kodmo != '26000') {
                $where .= " and poc.{$attr} = :kodmo";
                $params[':kodmo'] = $this->kodmo;
            } else {
                $where .= " and poc.{$attr} is null";
            }
        }
        if ($count)
            $res = Yii::app()->db->createCommand("select ROW_NUMBER() over (order by poc.c desc) as nn, mo.nammo, k.nampk, poc.c from (
	select mo, profk, count(*) c from Pacient as poc
	$where
	group by mo, profk
) poc, mo, profkview as k
where poc.mo = mo.kodmo and k.idpk=poc.profk");
        else {
            $where .= ' and profk=:profk and mo=:mo';
            $params[':profk'] = $this->dop_params[self::REPORT_CATEGORY_PROFK];
            $params[':mo'] = $this->dop_params[self::REPORT_CATEGORY_MO_H];
            $res = Yii::app()->db->createCommand("
	select * from Pacient as poc
	$where
	");
        }
        $res->params = $params;
        $s = $res->queryAll();
        return $s;
    }

    public function countAll($count_bed_days = true)
    {
        if (!$this->date_range)
            return false;
        if (Helpers::getIsMsSql()) {
            $q = $this->getMsQuery();
        } else {
            $q = $this->getMyQuery();
        }
        $key = serialize($q->params);
        $res = Helpers::check_cache($key, function ($q) {
            return $q->queryAll();
        }, $q);
        $attrs = $this->attributeNames();
        foreach ($res as $r) {
            if (in_array($r['typep'], $attrs) !== false) {
                $this->$r['typep'] = $r['c'];
            }
        }
        if ($count_bed_days) {
            $this->countBed_days();
            $this->getExpired();
        }
        return true;
    }

    public static function RearrangeArray($group_array)
    {
        $typep = self::getTypepArray();
        $new_res = array();
        foreach ($group_array as $row) {
            if (!isset($row['kod']))
                $row['kod'] = 0;
            $kod = (string)$row['kod'];
            if (empty($new_res[$kod])) {
                $new_res[$kod]['name'] = $row['name'];
                $new_res[$kod]['optgroup'] = '';
                if (!empty($row['optgroup'])) {
                    if (array_search($row['optgroup'], array('неизвестно', 'yes', 'no')) === false)
                        $new_res[$kod]['optgroup'] = $row['optgroup'];
                }
                $new_res[$kod]['kod'] = $row['kod'];
                $new_res[$kod]['all'] = 0;
                //$new_res[$kod]['optgroup'] = $row['optgroup'];
                foreach ($typep as $v) {
                    $new_res[$kod][$v] = 0;
                }
            }
            if (in_array($row['typep'], $typep) !== false && $row['c'] > 0) {
                $new_res[$kod][$row['typep']] = $row['c'];
                $new_res[$kod]['all'] += (int)$row['c'];
            }

        }
        return array_values($new_res);
    }

    public static function GetDP($rawData)
    {
        return new CArrayDataProvider($rawData, array(
            'id' => '',
            'keyField' => 'kod',
            'sort' => array(
                'attributes' => array(
                    'name', 'blocked', 'blocked_er', 'out', 'planed', 'annuled', 'expired', 'all'
                ),
                'route' => 'report/getpage',
                'defaultOrder' => array(
                    'all' => CSort::SORT_DESC,
                )
            ),
            'totalItemCount' => count($rawData),
            'pagination' => array(
                'route' => 'report/getpage',
                'pageSize' => 10,
            ),

        ));
    }
    public function getMestaReportArray($kodmo)
    {
        $res = Yii::app()->db->createCommand('select * from fnmestareport (:d, :kodmo)')->queryAll(true, array(':d'=>date('d.m.Y'), ':kodmo'=>$kodmo));
        $sk = Yii::app()->db->createCommand('select isnull(sum(sk),0) s from fnmestareport (:d, :kodmo)')->queryScalar(array(':d'=>date('d.m.Y'), ':kodmo'=>$kodmo));
        $dp = new CArrayDataProvider($res, array(
            'keyField'=>'PROFK',
            'pagination' => false,
        ));
        return array($dp, $sk);
    }
    /**
     * @param $for
     * @return CArrayDataProvider
     */
    public function getCommonReportArray($for)
    {
        if ($this->category == self::REPORT_CATEGORY_FOND)
            return new CArrayDataProvider(array());

        $res = $this->getCommonReportDbCommand($for);
        $res->params[':prev_dt'] = $this->date_range_start->format(Helpers::getDateFormat());
        $res->params[':dt'] = $this->date_range_end->format(Helpers::getDateFormat(true));
        $key = $for . serialize($res->params);
        $res = Helpers::check_cache($key, function (CDbCommand $res) {
            return $res->queryAll();
        }, $res);
        try {
            $res = ReportForm::RearrangeArray($res);
        } catch (Exception $e) {
            var_dump($e);
        }
        $cache_id = $for . time();
        Yii::app()->cache->set($cache_id, $res);
        $dp = self::GetDP($res);

        $dp->pagination->params['for'] = $for;
        $dp->pagination->params['cache_id'] = $cache_id;
        $dp->pagination->params['report_attributes'] = serialize($this->attributes);

        $dp->sort->params['for'] = $for;
        $dp->sort->params['cache_id'] = $cache_id;
        $dp->sort->params['report_attributes'] = serialize($this->attributes);

        return $dp;
    }

    public function getCommonWhere()
    {
        if (empty($this->category))
            return false;
        if (empty($this->kodmo))
            return false;
        switch ($this->category) {
            case self::REPORT_CATEGORY_MO_A:
                return 'monapr';
            case self::REPORT_CATEGORY_MO_H:
                return 'mo';
        }
        return $this->category;
    }

    /**
     * @param $who
     * @return CDbCommand
     * @throws Exception
     */
    public function getCommonReportDbCommand($who)
    {
        $res = Yii::app()->db->createCommand();
        $for = $this->category == $who ? false : $this->getCommonWhere();
        $where = '';
        if ($for) {
            $where = 'and p.' . $for . '=:kodmo';
            $res->params[':kodmo'] = $this->kodmo;
        }

        switch ($who) {
            case ReportForm::REPORT_CATEGORY_SMO:
                $query =
                    "select isnull(smo.kodsmo, '0') as kod, isnull(smo.namsmo, 'неизвестно') as name, isnull(ter.namter, 'неизвестно') as optgroup, p.typep, count(p.typep) c
					 from fn_pacient_past(:prev_dt, :dt) as p
					 left join smo on smo.kodsmo = p.smo
					 left join ter on ter.kodter=smo.kodter
					 where p.typep <> 'error' {$where}
				     group by smo.kodsmo, smo.namsmo,ter.namter, p.typep";
                break;
            case ReportForm::REPORT_CATEGORY_PROFK:
                $query =
                    "select profk.idpk as kod, profk.nampk as name, 'yes' as optgroup, p.typep, count(p.typep) c
					 from fn_pacient_past(:prev_dt, :dt) as p
					 left join profk on profk.idpk= p.profk
					 where p.typep <> 'error' {$where}
				     group by profk.idpk, profk.nampk, p.typep";
                break;
            case self::REPORT_CATEGORY_MO_A:
                $query =
                    "select isnull(mo_a.kodmo, '0') as kod, isnull(mo_a.nammo, 'неизвестно') as name, 'yes' as optgroup, p.typep, count(p.typep) c
					 from fn_pacient_past(:prev_dt, :dt) as p
			         left join mo as mo_a on mo_a.kodmo = p.monapr
			         where p.typep <> 'error' {$where}
				     group by mo_a.kodmo, mo_a.nammo, p.typep";
                break;
            case self::REPORT_CATEGORY_MO_H:
                $query =
                    "select mo_h.kodmo as kod, mo_h.nammo as name, 'yes' as optgroup, p.typep, count(p.typep) c
					 from fn_pacient_past(:prev_dt, :dt) as p
			         left join mo as mo_h on mo_h.kodmo = p.mo
			         where p.typep <> 'error' {$where}
				     group by mo_h.kodmo, mo_h.nammo, p.typep";
                break;
            default:
                throw new Exception('неизвестный тип категории ' . $who);
                break;
        }
        $res->text = $query;
        return $res;
    }

    public function getBreadCrumbsArray($html = true)
    {
        $res = array();
        $res['period'] = $this->date_range_str;
        /*if ($this->date_range_alias == self::REPORT_ALIAS_OWN) {
            $res['period'] = $this->date_range_str;
        } else {
            $ranges = self::getRangesArray();
            $keys = array_search($this->date_range_alias, array_keys($ranges));
            if ($keys !== false) {
                $res['period'] = $ranges[$this->date_range_alias];
            }
        }*/

        switch ($this->category) {
            case self::REPORT_CATEGORY_PROFK:
                $res['category'] = 'Профиль койки';
                if (!empty($this->kodmo)) {
                    /** @var $profk ProfK */
                    $profk = ProfK::model()->findByPk($this->kodmo);
                    $res['name'] = $profk->NAMPK;
                }
                break;
            case self::REPORT_CATEGORY_SMO:
                $res['category'] = 'Страховая компания';
                if (!empty($this->kodmo)) {
                    if ($this->kodmo <> '26000') {
                        /** @var $smo Smo */
                        $smo = Smo::model()->findByPk($this->kodmo);
                        $res['name'] = $smo->NAMSMO;
                    } else {
                        $res['name'] = 'Нет полиса';
                    }

                }
                break;
            case self::REPORT_CATEGORY_MO_A:
                $res['category'] = 'Медицинская организация (амбулатория)';
                if (!empty($this->kodmo)) {
                    /** @var $mo_a Mo */
                    $mo_a = Mo::model()->findByPk($this->kodmo);
                    $res['name'] = $mo_a->NAMMO;
                }
                break;
            case self::REPORT_CATEGORY_MO_H:
                $res['category'] = 'Медицинская организация (стационар)';
                if (!empty($this->kodmo)) {
                    /** @var $mo_h Mo */
                    $mo_h = Mo::model()->findByPk($this->kodmo);
                    $res['name'] = $mo_h->NAMMO;
                }
                break;
            case self::REPORT_CATEGORY_FOND:
                $res['category'] = 'Фонд';
                break;
        }
        if ($html) {
            $res = array_flip($res);
            foreach ($res as $k => $v) {
                $res[$k] = 'javascript:void(0)';
            }
        }
        return $res;
    }

    public function countBed_days()
    {
        if (!$this->date_range)
            return false;
        $_params = array();
        if (!empty($this->kodmo)) {
            if ($this->category == self::REPORT_CATEGORY_MO_H) {
                $_params['MO'] = $this->kodmo;
            } else if ($this->category == self::REPORT_CATEGORY_PROFK) {
                $_params['PROFK'] = $this->kodmo;
            }
        }
        $i = clone($this->date_range_start);
        $s = array();
        while ($i <= $this->date_range_end) {
            $d = $i->format(Helpers::getDateFormat());
            $s[$d] = MestoView::GetMestaSum($i, $_params, true);
            $p = $s[$d]['s'];
            $n = $s[$d]['n'];
            $this->bed_days += $p;
            $this->negative += $n;
            $i->modify('1 day');
        }
        $this->negative *= -1;
        return true;
    }


}
