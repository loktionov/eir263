<?php

/**
 * This is the model class for table "sxml_view".
 *
 * The followings are the available columns in table 'sxml_view':
 * @property integer $id
 * @property string $IDXML
 * @property integer $ID_ATTR
 * @property integer $require
 * @property string $NAME
 * @property integer $order
 * @property string $FP
 * @property string $detail
 * @property string $attr_owner
 * @property integer $active
 */
class SXML extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sxml_view';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id', 'required'),
            array('id, ID_ATTR, require, order, active', 'numerical', 'integerOnly' => true),
            array('IDXML, NAME, FP, attr_owner', 'length', 'max' => 50),
            array('detail', 'length', 'max' => 255),
            array('id, IDXML, ID_ATTR, require, NAME, order, FP, detail, attr_owner, active', 'safe', 'on' => 'search'),
        );
    }

    public function primaryKey()
    {
        return 'id';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'IDXML' => 'Idxml',
            'ID_ATTR' => 'Id Attr',
            'require' => 'Require',
            'NAME' => 'Name',
            'order' => 'Order',
            'FP' => 'Fp',
            'detail' => 'Detail',
            'attr_owner' => 'Attr Owner',
            'active' => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('IDXML', $this->IDXML, true);
        $criteria->compare('ID_ATTR', $this->ID_ATTR);
        $criteria->compare('require', $this->require);
        $criteria->compare('NAME', $this->NAME, true);
        $criteria->compare('order', $this->order);
        $criteria->compare('FP', $this->FP, true);
        $criteria->compare('detail', $this->detail, true);
        $criteria->compare('attr_owner', $this->attr_owner, true);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SXML the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function ExampleXML($t)
    {
        if (empty($t)) {
            return '';
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('IDXML=:t');
        $criteria->params[':t'] = $t;
        $criteria->order = 'IDXML, [order]';
        $tags = SXML::model()->findAll($criteria);
        if (empty($tags)) {
            return '';
        }
        $dom = new DOMDocument();
        $dan = $dom->appendChild($dom->createElement('DAN'));
        $zap = $dan->appendChild($dom->createElement('ZAP'));
        /** @var $tags SXML[] */
        foreach($tags as $tag){
            $value = '';
            if($tag->require){
                $value='*';
            }
            $zap->appendChild($dom->createElement($tag->NAME, $value));
        }
        $dom->formatOutput = true;
        $dom_str = TbHtml::encode('<?xml version="1.0" encoding="Windows-1251"?>' . "\n" . $dom->saveXML($dan));
        return str_replace('*', TbHtml::tooltip('*', null, 'Обязательное поле'), $dom_str);

    }
}
