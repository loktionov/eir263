<?php

/**
 * This is the model class for table "sessions".
 *
 * The followings are the available columns in table 'sessions':
 * @property integer $id
 * @property string $remote_addr
 * @property string $http_user_agent
 * @property string $login_date
 * @property integer $idlogin
 * @property string $logout_date
 * @property string $session_id
 */
class Sessions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sessions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idlogin', 'numerical', 'integerOnly'=>true),
			array('remote_addr, session_id', 'length', 'max'=>50),
			array('http_user_agent, login_date, logout_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, remote_addr, http_user_agent, login_date, idlogin, logout_date, session_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'remote_addr' => 'Remote Addr',
			'http_user_agent' => 'Http User Agent',
			'login_date' => 'Login Date',
			'idlogin' => 'Idlogin',
			'logout_date' => 'Logout Date',
            'session_id'=>'session id'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('remote_addr',$this->remote_addr,true);
		$criteria->compare('http_user_agent',$this->http_user_agent,true);
		$criteria->compare('login_date',$this->login_date,true);
		$criteria->compare('idlogin',$this->idlogin);
		$criteria->compare('logout_date',$this->logout_date,true);
		$criteria->compare('session_id',$this->session_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sessions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
