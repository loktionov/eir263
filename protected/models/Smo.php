<?php

/**
 * This is the model class for table "smo".
 *
 * The followings are the available columns in table 'smo':
 * @property string $KODSMO
 * @property string $KODTER
 * @property string $NAMSMO
 * @property integer $id
 */
class Smo extends CActiveRecord
{
    public static function GetSmoArray($ter = null)
    {

        if (empty($ter)) {
            $cond = '';
            $param = array();
        } else {
            $cond = 'KODTER=:KODTER';
            $param = array(':KODTER' => $ter);
        }
        $data = Smo::model()->cache(3600)->findAll($cond, $param);

        return CHtml::listData($data, 'KODSMO', 'NAMSMO');
    }

    public static function GetSmoRow($q)
    {
        if (empty($q))
            return false;
        $data = Smo::model()->cache(3600)->findByPk($q);

        return $data;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'smo';
    }

    public function primaryKey()
    {
        return 'KODSMO';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id', 'numerical', 'integerOnly' => true),
            array('NAMSMO', 'length', 'max' => 150),
            array('KODSMO, KODTER, NAMSMO, id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'KODSMO' => 'Kodsmo',
            'KODTER' => 'Kodter',
            'NAMSMO' => 'Namsmo',
            'id' => 'ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('KODSMO', $this->KODSMO);
        $criteria->compare('KODTER', $this->KODTER);
        $criteria->compare('NAMSMO', $this->NAMSMO, true);
        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Smo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function GetOptionsArray($local_tfoms = true, $without_polis = true)
    {
        $crit = new CDbCriteria();
        $crit->order = 'namsmo';
        if ($local_tfoms) {
            $crit->condition = 'kodter=:okato';
            $crit->params = array(':okato' => Yii::app()->params['okato']);
        }
        /** @var Smo[] $opts */
        $opts = self::model()->cache(3600)->findAll($crit);
        $ret = array();
        if ($without_polis)
            $ret[] = array(
                'kod' => '26000',
                'name' => 'Нет полиса',
                'optgroup' => 'yes',
            );
        foreach ($opts as $opt) {
            $ret[] = array(
                'kod' => $opt->KODSMO,
                'name' => $opt->NAMSMO,
                'optgroup' => 'yes',
            );
        }
        return $ret;
    }
}
