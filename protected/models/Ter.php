<?php

/**
 * This is the model class for table "Ter".
 *
 * The followings are the available columns in table 'Ter':
 * @property integer $KODTER
 * @property string $NAMTER
 */
class Ter extends CActiveRecord
{
    public function primaryKey()
    {
        return 'KODTER';
    }
    public static function GetOptionsArray()
    {
        $polis = self::model()->cache(3600)->findAll(array('order' => 'NAMTER'));
        return CHtml::listData($polis, 'KODTER','NAMTER');
    }
    public static function GetTerRow($q)
    {
        if(empty($q))
            return false;
        $data=Ter::model()->cache(3600)->findByPk($q);

        return $data;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NAMTER', 'length', 'max'=>150),
			array('KODTER, NAMTER', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'KODTER' => 'Kodter',
			'NAMTER' => 'Namter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('KODTER',$this->KODTER);
		$criteria->compare('NAMTER',$this->NAMTER,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
