<?php

/**
 * This is the model class for table "updates".
 *
 * The followings are the available columns in table 'updates':
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $author
 * @property string $insdate
 * @property integer $id_cat
 *
 * The followings are the available model relations:
 * @property UpdatesCategory $idCat
 */
class Updates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'updates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, text', 'required'),
			array('id_cat', 'numerical', 'integerOnly'=>true),
			array('title, author', 'length', 'max'=>255),
			array('id, title, text, author, insdate, id_cat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCat' => array(self::BELONGS_TO, 'UpdatesCategory', 'id_cat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'text' => 'Текст',
            'author' => 'Автор',
            'insdate' => 'Дата',
            'id_cat' => 'Категория',
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('insdate',$this->insdate,true);
		$criteria->compare('id_cat',$this->id_cat);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_DESC,
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Updates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
