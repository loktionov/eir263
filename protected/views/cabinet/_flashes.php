<?php
/**
 * User: локтионов_ав
 * Date: 30.07.14
 * Time: 9:08
 * @var $model Pacient
 * @var $action String
 */
?>
<div class="flashes" id="flash-div-<?php echo $action . '-' . $model->IDZAP; ?>">
    <?php foreach (Yii::app()->user->getFlashes() as $key => $message) {

        echo TbHtml::alert($key, $message);
    }
    ?>
</div>
<div class="flashes-error">
    <?php if($model->hasErrors()) echo '№ направления: ' . @$model->NNAPR . '<br/>'; ?>
    <?php if(!empty($nzap)) echo '№ записи: ' . $nzap . '<br/>'; ?>
    <?php echo TbHtml::errorSummary($model); ?>
</div>