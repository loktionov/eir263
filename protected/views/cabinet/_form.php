<?php
/* @var $this PacientController */
/* @var $model Pacient */
/* @var $form TbActiveForm */
?>
<div class="form">
<div style="float: left; width: 470px; margin: 15px;">
    <?php $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'pacient-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <!--p class="help-block">Поля, отмеченные <span class="required">*</span> обязательны к заполнению.</p-->

    <?php echo $form->errorSummary($model); ?>

    <?php if($model->FOMP == 3) { ?>
    <?php echo $form->label($model, 'DNGOSP');
    $model->DNGOSP = empty($model->DNGOSP) ? null : date(Helpers::getDateFormat(false, true),strtotime($model->DNGOSP)); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'language' => 'ru',
            'model' => $model,
            'attribute' => 'DNGOSP',
        )
    ); ?>
    <?php $model->DPOGOSP = empty($model->DPOGOSP) ? null : date(Helpers::getDateFormat(false, true),strtotime($model->DPOGOSP)); ?>
    <?php echo $form->label($model, 'DPOGOSP') ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'language' => 'ru',
            'model' => $model,
            'attribute' => 'DPOGOSP',
        )
    ); ?>
    <?php echo TbHtml::label($model->getAttributeLabel('DS'), 'ds') ?>
    <?php //Yii::beginProfile('mkb10'); ?>
    <?php //if ($this->beginCache('mkb10_2')) { ?>
        <?php
        $this->widget('YiiSelectize', array(
            'name' => 'ds',
            'data' => array($model->DS=>$model->DS . ': ' . $model->mkb10->NAMMKB),
            'value' => $model->DS,
            'options' => array(
                'valueField' => 'kod',
                'labelField' => 'name',
                'searchField' => ['kod','name'],
            ),
            'callbacks'=>array('load'=>'function(query, callback){MkbLoad(query, callback)}'),
            'htmlOptions' => array(
                'placeholder' => 'Диагноз...',
                'required' => false,
            ),
        ));
        ?>
        <?php //$this->endCache();    } ?>
    <?php //Yii::endProfile('mkb10'); ?>
    <?php } else { ?>
        <?php echo $form->label($model, 'DPGOSP');
        $model->DPGOSP = date(Helpers::getDateFormat(false, true),strtotime($model->DPGOSP)); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'model' => $model,
                'attribute' => 'DPGOSP',
            )
        ); ?>
        <?php $model->DPOGOSP = empty($model->DPOGOSP) ? null : date(Helpers::getDateFormat(false, true),strtotime($model->DPOGOSP)); ?>
        <?php echo $form->label($model, 'DPOGOSP') ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'model' => $model,
                'attribute' => 'DPOGOSP',
            )
        ); ?>
        <?php echo TbHtml::label($model->getAttributeLabel('DSPO'), 'dspo') ?>
        <?php //Yii::beginProfile('mkb10'); ?>
        <?php //if ($this->beginCache('mkb10_2')) { ?>
        <?php
        $this->widget('YiiSelectize', array(
            'name' => 'dspo',
            'data' => !empty($model->DSPO) ? array($model->DSPO=>$model->DSPO . ': ' . $model->mkb10_po->NAMMKB) : array(),
            'value' => $model->DSPO,
            'options' => array(
                'valueField' => 'kod',
                'labelField' => 'name',
                'searchField' => ['kod','name'],
            ),
            'callbacks'=>array('load'=>'function(query, callback){MkbLoad(query, callback)}'),
            'htmlOptions' => array(
                'placeholder' => 'Диагноз...',
                'required' => false,
            ),
        ));
        ?>
        <?php //$this->endCache();    } ?>
        <?php //Yii::endProfile('mkb10'); ?>
    <?php } ?>
    <?php echo TbHtml::label($model->getAttributeLabel('MO'), 'kodmo') ?>
    <?php $this->renderPartial('application.views.clinic.selectize_widget', array(
        'model' => $model->pmo_rel,
        'catname' => '',
        'placeholder' => 'Учреждение...',
        'required' => false,
        'value' => empty($model->PMO) ? '' : $model->PMO,
        'data' => empty($model->PMO) ? array() : array($model->PMO => $model->pmo_rel->NAMPMO),
    ));?>

    <?php echo TbHtml::label($model->getAttributeLabel('PROFK'), 'profk') ?>
    <?php $this->renderPartial('application.views.clinic.selectize_widget', array(
        'model' => empty($model->profk_rel) ? ProfKView::model() : $model->profk_rel,
        'catname' => 'отделение:',
        'placeholder' => 'Профиль койки...',
        'required' => false,
        'value' => empty($model->PROFK) ? '' : $model->PROFK,
        'data' => empty($model->PROFK) ? array() : array($model->PROFK => $model->profk_rel->NAMPK),
    ));?>


    <?php echo $form->dropDownListControlGroup($model, 'FOMP', Helpers::GetFompArray($model->FOMP), array('span' => 5, 'empty' => '')); ?>

    <?php echo $form->dropDownListControlGroup($model, 'MEDRAB', Medrab::GetMedrabArray(Yii::app()->user->getState('tip') < 2 ? Yii::app()->user->GetState('kodmo') : null), array('span' => 5, 'maxlength' => 11, 'empty' => '')); ?>

</div>
<div style="float: right; margin: 15px;">
    <?php echo $form->textFieldControlGroup($model, 'FAM', array('span' => 5, 'maxlength' => 40)); ?>

    <?php echo $form->textFieldControlGroup($model, 'IM', array('span' => 5, 'maxlength' => 40)); ?>

    <?php echo $form->textFieldControlGroup($model, 'OT', array('span' => 5, 'maxlength' => 40)); ?>

    <?php echo $form->dropDownListControlGroup($model, 'P', Helpers::GetSexArray(), array('empty' => '', 'span' => 5)); ?>

    <?php //echo $form->dateFieldControlGroup($model, 'DR', array('span' => 5)); ?>

    <?php if (!empty($model->DR)) {
        $model->DR = date(Helpers::getDateFormat(false, true), strtotime($model->DR));
    } ?>

    <?php echo $form->label($model, 'DR') ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'language' => 'ru',
            'model' => $model,
            'attribute' => 'DR',
        )
    ); ?>

    <?php echo $form->textFieldControlGroup($model, 'TEL', array('span' => 5, 'maxlength' => 40)); ?>

    <?php echo $form->dropDownListControlGroup($model, 'VPOLIS', Helpers::GetPolisArray(), array('empty' => '', 'span' => 5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'SPOLIS',array('span'=>5,'maxlength'=>10)); ?>

    <?php echo $form->textFieldControlGroup($model, 'NPOLIS', array('span' => 5, 'maxlength' => 20)); ?>

    <?php if (empty($model->TER)) $model->TER = Yii::app()->params['okato']; ?>
    <?php echo $form->dropDownListControlGroup($model, 'TER', Ter::GetOptionsArray(), array(
            'empty' => '',
            'span' => 5,
            'ajax' => array(
                'type' => 'POST', //request type
                'url' => Yii::app()->createAbsoluteUrl('clinic/getsmo'), //url to call.
                //Style: CController::createUrl('currentController/methodToCall')
                'update' => '#Pacient_SMO', //selector to update
                'data' => "js:$('#Pacient_TER').serialize()",
                //leave out the data key to pass all form values through
            ),
        )
    ); ?>

    <?php echo $form->dropDownListControlGroup($model, 'SMO', Smo::GetSmoArray($model->TER), array('span' => 5, 'empty' => '')); ?>

    <?php //echo $form->textFieldControlGroup($model,'DNGOSP',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'VNGOSP',array('span'=>5,'maxlength'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'MO',array('span'=>5)); ?>

    <?php // echo $form->textFieldControlGroup($model,'PMO',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PROFO',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PROFK',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'NNAPR',array('span'=>5,'maxlength'=>12)); ?>

    <?php //echo $form->textFieldControlGroup($model,'DNAPR',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'DANUL',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PANUL',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'IANUL',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'KANUL',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PMOANUL',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'MONAPR',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PMONAPR',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PROFONAPR',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'PROFKNAPR',array('span'=>5)); ?>

    <?php //echo $form->textFieldControlGroup($model,'DS',array('span'=>5,'maxlength'=>6)); ?>

    <?php //echo $form->textFieldControlGroup($model,'NKART',array('span'=>5,'maxlength'=>50)); ?>

    <?php //echo $form->textFieldControlGroup($model, 'DSPO', array('span' => 5, 'maxlength' => 6)); ?>

    <?php //echo $form->textFieldControlGroup($model,'TEST',array('span'=>5)); ?>
</div>
<div style="clear: both;"></div>
<div class="form-actions">
    <?php echo TbHtml::submitButton($model->isNewRecord ? 'Выдать' : 'Обновить', array(
        'color' => TbHtml::BUTTON_COLOR_INFO,
        //'size' => TbHtml::BUTTON_SIZE_LARGE,
        'class' => 'pull-right',
    )); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->