<?php
/* @var $this PacientController */
/* @var $data PacientView */
/* @var $proxy ProxyMan */
if (empty($data->PMONAPR) AND !empty($data->MONAPR)) {
    $mo = $mo = Mo::model()->findByPk($data->MONAPR);
    $data->NAMMO_clinic = $mo->NAMMO;
}
?>
<h3>
    <?php echo Helpers::GetPacientLabel($data->typep, ($data->FOMP == 3)); ?> <br/>
    <?php echo CHtml::link(CHtml::encode($data->NNAPR), array('view', 'id' => $data->IDZAP)); ?>
    от <?php echo CHtml::encode(date(Helpers::getDateFormat(false, true), strtotime($data->DNAPR))); ?>
</h3>
<div class="clearfix"></div>
<div class="view">
    <?php echo TbHtml::b('Кого направил'); ?><br/>
    <table>
        <tbody>
        <tr>
            <td style="width: 55%">
                <span class="cat">Карта: </span> <?php echo CHtml::encode($data->NKART); ?>
                <br/>
                <span
                    class="cat">ФИО: </span> <?php echo CHtml::encode($data->FAM . ' ' . $data->IM . ' ' . $data->OT); ?>
                <br/>
                <span
                    class="cat">Метрика: </span> <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DR)) . ' ' . Helpers::GetSex($data->P); ?>
                <br/>
                <span class="cat">Телефон: </span><?php echo CHtml::encode($data->TEL); ?>
                <?php if (!$proxy->isNewRecord): ?>
                    <br/>
                    <span class="cat">Представитель: </span><?php echo CHtml::encode($proxy->fam . ' ' . $proxy->im . ' ' .$proxy->ot); ?>
                <?php endif; ?>
            </td>
            <td>
                <span class="cat">Полис: </span>
                <?php echo TbHtml::tooltip(Helpers::GetTypepolis($data->VPOLIS) . ' ', "javascript:void(0)", Helpers::GetNamepolis($data->VPOLIS), array('class' => 'java-link')); ?>
                <?php if (!empty($data->SPOLIS)) echo $data->SPOLIS . ' № ' ?>
                <?php echo CHtml::encode($data->NPOLIS); ?>
                <br/>
                <span class="cat">СМО: </span>
                <?php
                $smo = Smo::GetSmoRow($data->SMO);
                if (!empty($smo))
                    echo CHtml::encode($smo->NAMSMO);
                ?>
                <br/>
                <span class="cat">Регион: </span> <?php  $ter = Ter::GetterRow($data->TER);
                if (!empty($ter))
                    echo CHtml::encode($ter->NAMTER);
                ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php if ($data->typep == 'annuled') { ?>
    <div class="view">
        <?php echo TbHtml::b('Аннулирован'); ?><br/>
        <table>
            <tbody>
            <tr>
                <td style="width: 55%">
                    <?php $ianul = IAnul::GetIAnulRow($data->IANUL); ?>
                    <span class="cat">Кем: </span> <?php if (!empty($ianul)) echo CHtml::encode($ianul->NAMIANUL); ?>
                    <br/>
                    <?php $pmoanul = PMOView::GetPmoRow($data->PMOANUL); ?>
                    <?php echo CHtml::encode($pmoanul['NAMPMO']); ?>
                    <br/>
                    <span class="cat"><?php echo TbHtml::i(CHtml::encode($pmoanul['NAMMO'])); ?></span><br/>

                </td>
                <td>
                    <span
                        class="cat">Дата: </span> <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DANUL)); ?>
                    <br/>
                    <?php $panul = PAnul::GetPAnulRow($data->PANUL); ?>
                    <span
                        class="cat">Причина: </span> <?php if (!empty($panul)) echo CHtml::encode($panul->NAMPANUL); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php } ?>
<div class="view">
    <b>Кто направил</b><br/>
    <table>
        <tbody>
        <tr>
            <td style="width: 55%">
                <?php echo CHtml::encode($data->NAMPMO_clinic); ?><br/>
                <span class="cat"><?php echo TbHtml::i(CHtml::encode($data->NAMMO_clinic)); ?></span><br/>
                <?php $medrab = Medrab::GetMedRabRow($data->MEDRAB); ?>
                <span
                    class="cat">Направление от: </span> <?php echo $medrab['FAM'] . ' ' . $medrab['IM'] . ' ' . $medrab['OT']; ?>
            </td>
            <td>
                <?php $nammkb = MKB10::GetMkbRow($data->DSNAPR); ?>
                <span class="cat">Диагноз направления:</span>
                <?php if (!empty($nammkb))
                    echo TbHtml::tooltip($data->DSNAPR, "javascript:void(0)", $nammkb['NAMMKB'], array('class' => 'java-link')); ?>
                <br/>
                <span class="cat">Запланированная дата: </span>
                <?php echo !empty($data->DPGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($data->DPGOSP)) : ''; ?>
                <br/>
                <span class="cat">Запланированная дата окончания: </span>
                <?php if (!empty($data->DPOGOSP))
                    echo date(Helpers::getDateFormat(false, true), strtotime($data->DPOGOSP)); ?>

            </td>
        </tr>
        </tbody>
    </table>

</div>
<div class="view">
    <?php echo TbHtml::b('Куда направил'); ?><br/>
    <table>
        <tbody>
        <tr>
            <td style="width: 55%">
                <?php echo CHtml::encode($data->NAMPMO_hospital); ?><br/>
                <span class="cat"><?php echo TbHtml::i(CHtml::encode($data->NAMMO_hospital)); ?></span>
                <br/>
                <br/>
                <span class="cat">Профиль койки: </span> <?php echo CHtml::encode($data->NAMPK); ?><br/>
                <span class="cat"><?php echo TbHtml::i(CHtml::encode($data->NAMPO)); ?></span>
            </td>
            <td>
                <?php $nammkb = MKB10::GetMkbRow($data->DSPO); ?>
                <span class="cat">Диагноз приемного отделения:</span>
                <?php if (!empty($nammkb))
                    echo TbHtml::tooltip($data->DSPO, "javascript:void(0)", $nammkb['NAMMKB'], array('class' => 'java-link')); ?>
                <br/>
                <span class="cat">Окончательный диагноз: </span>
                <?php $nammkb = MKB10::GetMkbRow($data->DS); ?>
                <?php if (!empty($nammkb))
                    echo TbHtml::tooltip($data->DS, "javascript:void(0)", $nammkb['NAMMKB'], array('class' => 'java-link')); ?>
                <br/>
                <span class="cat">Дата госпитализации: </span>
                <?php if (isset($data->DNGOSP)) {
                    echo date(Helpers::getDateFormat(false, true), strtotime($data->DNGOSP)) . ' ' . $data->VNGOSP;
                }?><br/>
                <span class="cat">Дата выписки: </span>
                <?php if (!empty($data->DOGOSP))
                    echo date(Helpers::getDateFormat(false, true), strtotime($data->DOGOSP)); ?>
                <br/>
                <span
                    class="cat">Форма помощи: </span><?php echo TbArray::getValue($data->FOMP, Helpers::GetFompArray($data->FOMP)); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>