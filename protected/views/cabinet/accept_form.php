<?php
/**
 * User: локтионов_ав
 * Date: 22.06.14
 * Time: 22:12
 * @var $model PacientView
 * @var $this CabinetController
 * @var $form TbActiveForm
 * @var $id String
 */
$model->DPGOSP = !empty($model->DPGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($model->DPGOSP)) : null;
$model->DPOGOSP = !empty($model->DPOGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($model->DPOGOSP)) : null;
if(empty($model)){
    $model = new PacientView();
}
$form = $this->beginWidget('TbActiveForm', array(
    'id' => 'accept-form',
    'enableAjaxValidation' => false,
)); ?>
<?php echo $form->hiddenField($model,'IDZAP', array('id'=>'idzap_accept')); ?>
<?php echo TbHtml::hiddenField('source_id', $id); ?>
<?php echo TbHtml::label('Дата госпитализации', 'PacientView_DPGOSP'); ?>
<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'language'=>'ru',
    'model'=>$model,
    'attribute'=>'DNGOSP',
)); ?>
<?php echo TbHtml::label('Дата окончания госпитализации', 'PacientView_DPOGOSP'); ?>
<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'language'=>'ru',
    'model'=>$model,
    'attribute'=>'DPOGOSP',
)); ?>
<?php
$this->widget('YiiSelectize', array(
    'model' => $model,
    'attribute' => 'DSPO',
    'data' => array(),
    'value' => '',
    'options' => array(
        'valueField' => 'kod',
        'labelField' => 'name',
        'searchField' => ['kod','name'],
    ),
    'callbacks'=>array('load'=>'function(query, callback){MkbLoad(query, callback,"' . Yii::app()->createAbsoluteUrl('clinic/getmkb') . '")}'),
    'htmlOptions' => array(
        'placeholder' => 'Диагноз...',
        'required' => false,
    ),
));
?>
<?php $this->endWidget(); ?>
