<?php
/**
 * @var $this Controller
 * @var $id Integer
 * @var $model Pacient
*/
?>
<?php
$this->widget('TbModal', array(
    'id' => 'accept-modal',
    'header' => 'Принять ',
    'content' => $this->renderPartial('accept_form', array('model' => new PacientView(), 'id' => $id), true),
    'footer' => array(
        TbHtml::button('Закрыть', array(
            'data-dismiss' => 'modal',
            'pull' => 'left',
        )),
        TbHtml::ajaxSubmitButton('Принять', Yii::app()->createAbsoluteUrl($this->id . '/accept'), array(
            'id' => 'accept-submit-btn',
            'type' => 'post',
            'async' => false,
            'data' => 'js:$("form#accept-form").serialize()',
            'dataType'=>'json',
            'success' => new CJavaScriptExpression("function(data){

                    UpdatePage('{$this->route}', {$id}, data);
                }
            "),
        ), array(
            'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            'pull' => 'right',
        ))
    ),
)); ?>

<?php
$this->widget('TbModal', array(
    'id' => 'annul-modal',
    'header' => 'Аннулировать ',
    'content' => $this->renderPartial('annul_form', array('model' => $model, 'id' => $id), true),
    'footer' => array(
        TbHtml::button('Закрыть', array(
            'data-dismiss' => 'modal',
            'pull' => 'left',
        )),
        TbHtml::ajaxSubmitButton('Аннулировать', Yii::app()->createAbsoluteUrl($this->id . '/annul'), array(
            'id' => 'annul-submit-btn',
            'type' => 'post',
            'async' => false,
            'data' => 'js:$("form#annul-form").serialize()',
            'dataType'=>'json',
            'success' => new CJavaScriptExpression("function(data){

                    UpdatePage('{$this->route}', {$id}, data);
                }
            "),
        ), array(
            'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            'pull' => 'right',
        ))
    ),
));

?>

<?php
$out_form = $this->widget('TbModal', array(
    'id' => 'out-modal',
    'header' => 'Выписать ',
    'content' => $this->renderPartial('out_form', array('model' => $model, 'id' => $id), true),
    'footer' => array(
        TbHtml::button('Закрыть', array(
            'data-dismiss' => 'modal',
            'pull' => 'left',
        )),
        TbHtml::ajaxSubmitButton('Выписать', Yii::app()->createAbsoluteUrl($this->id . '/out'), array(
            'id' => 'out-submit-btn',
            'type' => 'post',
            'async' => false,
            'data' => 'js:$("form#out-form").serialize()',
            'dataType'=>'json',
            'success' => new CJavaScriptExpression("function(data){

                    UpdatePage('{$this->route}', {$id}, data);
                }
            "),
        ), array(
            'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            'pull' => 'right',
        ))
    ),
));

?>