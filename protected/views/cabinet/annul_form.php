<?php
/**
 * User: локтионов_ав
 * Date: 22.06.14
 * Time: 22:12
 * @var $model PacientView
 * @var $this CabinetController
 * @var $form TbActiveForm
 * @var $id String
 */
$model->DANUL = empty($model->DANUL) ? date(Helpers::getDateFormat(false, true)) : date(Helpers::getDateFormat(false, true), strtotime($model->DANUL));
$form = $this->beginWidget('TbActiveForm', array(
    'id' => 'annul-form',
    'enableAjaxValidation' => false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model, 'IDZAP', array('id'=>'idzap_annul')); ?>
<?php echo TbHtml::hiddenField('source_id', $id); ?>
<?php echo $form->label($model, 'DANUL'); ?>
<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'language'=>'ru',
    'model'=>$model,
    'attribute'=>'DANUL',
    'id'=>'DANUL',
)); ?>
<?php echo $form->dropDownListControlGroup($model, 'PANUL', PAnul::GetOptionsArray(), array('empty'=>'')); ?>
<?php $this->endWidget();?>
