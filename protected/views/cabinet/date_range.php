<?php
/**
 * User: локтионов_ав
 * Date: 16.07.14
 * Time: 14:51
 * @var $this Controller
 * @var $place Integer
 */
$form = $this->beginWidget('TbActiveForm', array(
    'id' => 'range-form',
    'action' => Yii::app()->createAbsoluteUrl('cabinet/download/' . $place),
));
$l = setlocale(LC_ALL, Helpers::getLocale());?>
<?php echo TbHtml::label('Выберите период, за который надо скачать список', 'date_range_picker'); ?>
<?php $this->widget(
    'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
    array(
        'name' => 'date_range_picker',
        'htmlOptions' => array(
            'placeholder' => 'выберите период',
        )
    )
); ?>
    <br />
    <span class="cat">или оставьте поле пустым, чтобы скачать весь список.</span>
<?php echo TbHtml::hiddenField('typep_range', empty($_GET['typep']) ? 'planed' : $_GET['typep']); ?>
<?php $this->endWidget(); ?>