<?php
/* @var $this CabinetController
 * @var $filter_ambulance String
 * @var $filter_hospital String
 * @var $id String
 */

$this->breadcrumbs = array(
    'кабинет',
);
?>
<?php echo TbHtml::well('Раздел «Кабинет» предназначен для сопровождения пациентов.
Вкладка «Исходящие» содержит пациентов, направленных вами. «Входящие» содержит направленных вам.'); ?>
<?php echo TbHtml::well('<i class="fa fa-exclamation-circle text-error"></i>&nbsp;Доступна сортировка по полю «Даты».'); ?>
<div id="flash-place" class="pull-left">
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {

        echo TbHtml::alert($key, $message);
    }
    ?>
</div>

<div style='margin: 10px;' class="pull-right">
    <?php if (array_search(Yii::app()->user->getState('tip'), array(0,1)) !== false) {
        echo TbHtml::buttonGroup(array(
            array(
                'label' => 'Исходящие',
                'color' => TbHtml::BUTTON_COLOR_INFO,
                'class' => $id == 1 ? 'active' : null,
                'url' => Yii::app()->createAbsoluteUrl('cabinet/index', array('id' => 1)),
            ),
            array(
                'label' => 'Входящие',
                'color' => TbHtml::BUTTON_COLOR_INFO,
                'class' => $id == 2 ? 'active' : null,
                'url' => Yii::app()->createAbsoluteUrl('cabinet/index', array('id' => 2)),
            ),
        ), array(
            'toggle' => TbHtml::BUTTON_TOGGLE_RADIO,
        ));
    } else if (array_search(Yii::app()->user->getState('tip'), array(2,3)) !== false) {
        $id = 3;
    } else {
        $id = 2;
    }
    ?>
</div>
<div class="clearfix"></div>
<?php $model = new PacientView(); ?>
<?php
Yii::beginProfile('hosp_view');
switch ($id) {
    case 1:
        $this->renderPartial('pacient_grid_ambulance', array('model' => $model, 'id' => 1, 'filter' => $filter_ambulance));
        break;
    case 2:
        $this->renderPartial('pacient_grid_hospital', array('model' => $model, 'id' => 2, 'filter' => $filter_hospital));
        break;
    case 3:
        $this->renderPartial('pacient_grid_hospital', array('model' => $model, 'id' => 2, 'filter' => $filter_hospital));
        break;
}
Yii::endProfile('hosp_view');
?>
<?php $this->renderPartial('actions_modal', array('model' => $model, 'id' => $id,)); ?>
