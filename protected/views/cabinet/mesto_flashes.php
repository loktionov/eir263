<?php
/**
 * User: локтионов_ав
 * Date: 30.07.14
 * Time: 9:08
 * @var $model Mesto
 * @var $action String
 */
?>
<div class="flashes" id="flash-div-<?php echo $action . '-' . $model->IDMESTO; ?>">
    <?php foreach (Yii::app()->user->getFlashes() as $key => $message) {

        echo TbHtml::alert($key, $message);
    }
    ?>
</div>
<div class="flashes-error">
    <?php if(!empty($nzap)) echo '№ записи: ' . $nzap . '<br/>'; ?>
    <?php echo TbHtml::errorSummary($model); ?>
</div>