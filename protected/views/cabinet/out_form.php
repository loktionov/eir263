<?php
/**
 * User: локтионов_ав
 * Date: 22.06.14
 * Time: 22:12
 * @var $model PacientView
 * @var $this CabinetController
 * @var $form TbActiveForm
 * @var $id String
 */
$form = $this->beginWidget('TbActiveForm', array(
    'id' => 'out-form',
    'enableAjaxValidation' => false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model, 'IDZAP', array('id'=>'idzap_out')); ?>
<?php echo TbHtml::hiddenField('source_id', $id); ?>
<?php echo TbHtml::label('Дата выписки', 'PacientView_DOGOSP'); ?>
<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'language'=>'ru',
    'model'=>$model,
    'attribute'=>'DOGOSP',
    'id'=>'dogosp-out',
)); ?>

<?php
$this->widget('YiiSelectize', array(
    'model' => $model,
    'attribute' => 'DS',
    'data' => array(),
    'value' => '',
    'options' => array(
        'valueField' => 'kod',
        'labelField' => 'name',
        'searchField' => ['kod','name'],
    ),
    'callbacks'=>array('load'=>'function(query, callback){MkbLoad(query, callback,"' . Yii::app()->createAbsoluteUrl('clinic/getmkb') . '")}'),
    'htmlOptions' => array(
        'placeholder' => 'Диагноз...',
        'required' => false,
    ),
));
?>
<?php $this->endWidget(); ?>