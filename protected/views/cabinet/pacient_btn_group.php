<?php
/**
 * User: локтионов_ав
 * Date: 14.07.14
 * Time: 11:05
 * @var $place String
 * @var $typep String
 * @var $filter String
 */
?>
<?php
$getTypep = empty($_GET['typep']) ? 'planed' : $_GET['typep'];
$gridId = ($place == 1) ? 'a' : 'h';
?>
<div style='margin: 10px;' class='pull-right'>
    <?php echo TbHtml::textField('searchFam', '', array(
        'append' => TbHtml::button('Искать по фамилии',
                array(
                    'onclick' => new CJavaScriptExpression("SearchFam()"),
                    'placeholder' => 'Фамилия'
                ))
    )); ?>
</div>
<div class="clearfix"></div>
<span class="cat pull-right" style="margin: -23px 9px 0 0;">или по номеру направления</span>
<div class="clearfix"></div>
<div style='margin: 10px;' class='pull-right'>

    <?php
    $btns = array();
    foreach (Helpers::GetTypepArray() as $typep => $params) {
        $btns[] = array(
            'label' => mb_convert_case($params['rus'], MB_CASE_TITLE, 'utf-8'),
            'color' => TbHtml::BUTTON_COLOR_LINK,
            'class' => 'radio-filter ' . ($typep == $filter ? 'active' : ''),
            'onclick' => new CJavaScriptExpression("$('#typep').val('$typep')"),
            'id' => 'btn_' . $typep,
            'data-value' => $typep,
            'data-place' => $place,
            'data-typep' => $typep,
            'url' => Yii::app()->createAbsoluteUrl('cabinet/index/id/' . $place . '/typep/' . $typep),
        );
    }
    ?>
    <?php echo TbHtml::buttonGroup($btns, array(
        'toggle' => TbHtml::BUTTON_TOGGLE_RADIO,
    )); ?>
    <?php echo TbHtml::hiddenField('typep', $getTypep); ?>

</div>

<?php
echo $this->widget('TbModal', array(
    'id' => 'cabinet-export',
    'header' => 'Период отчета',
    'content' => $this->renderPartial('date_range', array('place' => $place), true),
    'footer' => array(
        TbHtml::button('Закрыть', array('data-dismiss' => 'modal', 'pull' => 'left')),
        TbHtml::button('Скачать', array(
            'id' => 'range-submit-btn',
            //'type' => 'post',
            //'async' => false,
            'onclick' => 'js:$("form#range-form").submit()',
            //'dataType'=>'json',
            //'success' => new CJavaScriptExpression("function(data){UpdatePage('{$this->route}', {$id}, data);}"),
            'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            //'pull' => 'right',
        ))
    ),
), true)
?>
<?php echo TbHtml::link('Скачать текущий список в XML-формате', '#', array(
    'data-toggle' => 'modal',
    'data-target' => '#cabinet-export',
    'onclick' => new CJavaScriptExpression("$('#typep_range').val($('#typep').val())"),
    'class' => 'java-link',
)); ?>
<div class="clearfix"></div>
<?php if (Yii::app()->user->getState('tip') > 1) : ?>
    <div class="pull-right" style="width: 50%; margin: 10px;">
        <?php $this->renderPartial('application.views.clinic.selectize_widget', array(
            'model' => Mo::model(),
            'catname' => 'отделение:',
            'placeholder' => 'Стационар...',
            'required' => false,
            'value' => '',
            'data' => array(),
            'noplugins' => false,
            'callbacks' => array('onChange' => new CJavaScriptExpression('function(value){SearchFam();}'))
        ));?>
    </div>
<?php endif; ?>
<div class="pull-right" style="width: 50%; margin: 10px;">
    <?php $this->renderPartial('application.views.clinic.selectize_widget', array(
        'model' => ProfKView::model(),
        'catname' => 'отделение:',
        'placeholder' => 'Профиль койки...',
        'required' => false,
        'value' => '',
        'data' => array(),
        'noplugins' => false,
        'callbacks' => array('onChange' => new CJavaScriptExpression('function(value){SearchFam();}'))
    ));?>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    function supports_history_api() {
        return !!(window.history && history.pushState);
    }
    function addClicker(link) {
        var jlink = $(link);
        jlink.on("click", function (e) {
            var target = $(e.target);
            UpdateGrid(target.data('place'), target.data('typep'));
            history.pushState(null, null, e.target.href);
            e.preventDefault();

        });
    }

    function setupHistoryClicks() {
        addClicker($('.radio-filter'));
    }

    function SearchFam() {
        $('#pacient-view-<?= $gridId ?>').yiiGridView('update', {
            data: {
                typep_ajax: $('#typep').val(),
                q: $('#searchFam').val(),
                profk: $('#profkview').val(),
                mo: $('#mo').val()
            }
        });
    }
    window.onload = function () {
        if (!supports_history_api()) {
            return;
        }
        setupHistoryClicks();
        window.setTimeout(function () {
            window.addEventListener("popstate", function (e) {
                UpdateGrid(null, null, location.pathname);
            }, false);
        }, 1);

        $('#searchFam').keyup(function (e) {
            if (e.keyCode == 13) {
                SearchFam();
            }
        });
    }

</script>