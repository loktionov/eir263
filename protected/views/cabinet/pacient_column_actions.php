<?php
/**
 * @var $this Controller
 * @var $data PacientView
 * @var $id String
 */
if (Yii::app()->user->getState('tip') > 1) $id = 1;
switch ($data->typep . $id) {
    case 'planed2':
        echo TbHtml::link(TbHtml::icon(TbHtml::ICON_OK) . '&nbsp;Принять', '#', array(
            'class' => 'java-link accept-link',
            'data-toggle' => 'modal',
            'data-target' => '#accept-modal',
            'data-idzap' => $data->IDZAP,
            'data-fio' => $data->FAM . ' ' . $data->IM . ' ' . $data->OT . '.<br />Принять',
            'data-dngosp' => !empty($data->DPGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($data->DPGOSP)) : null,
            'data-dpogosp' => !empty($data->DPOGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($data->DPOGOSP)) : null,
            'data-dsnapr' => !empty($data->mkb10_napr) ? json_encode(array('key' => $data->DSNAPR, 'val' => $data->DSNAPR . ': ' . $data->mkb10_napr->NAMMKB)) : json_encode(array('key', 'val')),
            'id' => 'link-accept-' . $data->IDZAP,
        ));
        echo '<br />'
            . TbHtml::link(TbHtml::icon(TbHtml::ICON_REMOVE) . '&nbsp;Аннулировать', '#', array(
                'class' => 'java-link annul-link',
                'data-toggle' => 'modal',
                'data-target' => '#annul-modal',
                'data-fio' => $data->FAM . ' ' . $data->IM . ' ' . $data->OT . '.<br />Аннулировать',
                'data-idzap' => $data->IDZAP,
                'id' => 'link-annul-' . $data->IDZAP,
            ));
        break;
    case 'planed1':
        echo TbHtml::link(TbHtml::icon(TbHtml::ICON_REMOVE) . '&nbsp;Аннулировать', '#', array(
            'class' => 'java-link annul-link',
            'data-toggle' => 'modal',
            'data-target' => '#annul-modal',
            'data-fio' => $data->FAM . ' ' . $data->IM . ' ' . $data->OT . '.<br />Аннулировать',
            'data-idzap' => $data->IDZAP,
            'id' => 'link-annul-' . $data->IDZAP,
        ));
        break;
    case 'blocked2':
        echo TbHtml::link(TbHtml::icon(TbHtml::ICON_CIRCLE_ARROW_LEFT) . '&nbsp;Выписать', '#', array(
            'class' => 'java-link out-link',
            'data-toggle' => 'modal',
            'data-target' => '#out-modal',
            'data-idzap' => $data->IDZAP,
            'data-fio' => $data->FAM . ' ' . $data->IM . ' ' . $data->OT . '.<br />Выписать',
            'data-dspo' => !empty($data->mkb10_po) ? json_encode(array('key' => $data->DSPO, 'val' => $data->DSPO . ': ' . $data->mkb10_po->NAMMKB)) : json_encode(array('key', 'val')),
            'data-dogosp' => !empty($data->DPOGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($data->DPOGOSP)) : null,
            'id' => 'link-out-' . $data->IDZAP,
        ));
        break;
    default:
        break;
}