<?php
/**
 * User: Локтионов_АВ
 * Date: 11.08.14
 * Time: 18:09
 * @var $data PacientView
 */
?>
<?php if (!empty($data->DPGOSP) and empty($data->DNGOSP)and empty($data->DANUL)): ?>
    <span class="cat">Запланированное начало: </span>
    <br/>
    <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DPGOSP)); ?>
    <?php echo (strtotime($data->DPGOSP) < strtotime('now') ? '&nbsp;<i class="fa fa-exclamation-circle text-error"></i>' : ''); ?>
    <br/>
<?php endif; ?>

<?php if (!empty($data->DNGOSP)and empty($data->DANUL)): ?>
    <span class="cat">Начало гопситализации: </span>

    <br/>
    <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DNGOSP)); ?>
    <br/>
<?php endif; ?>


<?php if (!empty($data->DPOGOSP) and empty($data->DOGOSP) and empty($data->DANUL)): ?>
    <span class="cat">Запланированное окончание: </span>
    <br/>
    <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DPOGOSP)); ?>
    <?php echo (strtotime($data->DPOGOSP) < strtotime('now') ? '&nbsp;<i class="fa fa-exclamation-circle text-error"></i>' : ''); ?>

    <br/>
<?php endif; ?>

<?php if (!empty($data->DOGOSP) and empty($data->DANUL)): ?>
    <span class="cat">Окончание госпитализации: </span>

    <br/>
    <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DOGOSP)); ?>
    <br/>
<?php endif; ?>
<?php if (!empty($data->DANUL)): ?>
    <span class="cat">Аннулирование: </span>

    <br/>
    <?php echo date(Helpers::getDateFormat(false, true), strtotime($data->DANUL)); ?>
    <br/>
<?php endif; ?>