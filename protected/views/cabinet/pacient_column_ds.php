<?php
/**
 * User: локтионов_ав
 * Date: 29.07.14
 * Time: 15:05
 * @var $data PacientView
 */
?>

<table class="table-simple border-bottom" style="width: 170px">
    <tbody>
    <tr>
        <td style="width: 50px; text-align: right"><?php echo empty($data->mkb10_napr) ? $data->DSNAPR : TbHtml::tooltip($data->DSNAPR, "javascript:void(0)", $data->mkb10_napr->NAMMKB, array('class'=>'java-link')); ?></td>
        <td><span class="cat">Направления</span></td>
    </tr>
    <tr>
        <td style="width: 50px; text-align: right"><?php echo empty($data->mkb10_po) ? $data->DSPO : TbHtml::tooltip($data->DSPO, "javascript:void(0)", $data->mkb10_po->NAMMKB, array('class'=>'java-link')); ?></td>
        <td><span class="cat">Приемного отделения</span></td>
    </tr>
    <tr>
        <td style="width: 50px; text-align: right"><?php echo empty($data->mkb10) ? $data->DS : TbHtml::tooltip($data->DS, "javascript:void(0)", $data->mkb10->NAMMKB, array('class'=>'java-link')); ?></td>
        <td><span class="cat">Окончательный</span></td>
    </tr>
    </tbody>
</table>