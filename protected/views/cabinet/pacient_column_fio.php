<?php
/**
 * User: локтионов_ав
 * Date: 29.07.14
 * Time: 14:38
 * @var $data PacientView
 */
?>
<table class="table-simple">
    <tbody>
    <tr>
        <td><?php echo $data->FOMP == 3 ? TbHtml::labelTb('э', array('color' => TbHtml::LABEL_COLOR_WARNING)) : '&nbsp;'; ?></td>
        <td><?php echo Helpers::GetPacientLabel($data->typep); ?></td>
    <tr>
        <td>

        </td>
        <td>
            <?php echo $data->NNAPR; ?> <span class="cat">(<?php echo date(Helpers::getDateFormat(false, true),strtotime($data->DNAPR)); ?>)</span>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo TbHtml::tooltip(TbHtml::icon(TbHtml::ICON_PENCIL), Yii::app()->createAbsoluteUrl('clinic/bulletinupdate', array('id' => $data->IDZAP)), 'Редактировать'); ?>
        </td>
        <td>
            <?php echo TbHtml::link($data->FAM . ' ' . $data->IM . ' ' . $data->OT, Yii::app()->createAbsoluteUrl('cabinet/view', array('id' => $data->IDZAP))); ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <span class="cat"><?php echo Helpers::GetSex($data->P) . '&nbsp;' . date(Helpers::getDateFormat(false, true), strtotime($data->DR)); ?></span>
        </td>
    </tr>
    <?php if(!empty($data->proxy_fam)):?>
        <tr>
            <td></td>
            <td>
                <span class="cat">Представитель:</span><?php echo  '&nbsp;' . $data->proxy_fam . ' ' . $data->proxy_im . ' ' . $data->proxy_ot; ?>
                <span class="cat"><?php echo Helpers::GetSex($data->proxy_p) . '&nbsp;' . date(Helpers::getDateFormat(false, true), strtotime($data->proxy_dr)); ?></span>
            </td>
        </tr>
    <?php endif;?>
    </tbody>
</table>