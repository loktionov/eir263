<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 11.08.14
 * Time: 18:02
 * @var $data PacientView
 */

$pmo = PMOView::GetPmoRow($data->PMONAPR);
$medrab = Medrab::GetMedRabRow($data->MEDRAB);
if (!empty($pmo)) {

    $return = TbHtml::b($pmo['NAMPMO']) . '<br /><span class="cat">' . $pmo['NAMMO'] . '</span>';
} else if (!empty($data->MONAPR)) {
    $mo = Mo::model()->findByPk($data->MONAPR);
    $return = $mo->NAMMO;

} else {
    $return = $data->PMONAPR;
}
if (!empty($medrab)) {
    $return .= '<p>' . TbHtml::i('Направление от: ') . $medrab['FAM'] . ' ' . $medrab['IM'] . ' ' . $medrab['OT'] . '</p>';
} else $return .= '<p>' . $data->MEDRAB . '</p>';

$return .= TbHtml::icon(TbHtml::ICON_ARROW_DOWN) . '<br />';

$pmo = PMOView::GetPmoRow($data->PMO);
if (!empty($pmo)) {

    $return .= TbHtml::b($data->NAMPK) . '<br /><span class="cat">отделение: ' . $data->NAMPO . '</span><br />'
        . TbHtml::b($pmo['NAMPMO']) . '<br /><span class="cat">' . $pmo['NAMMO'] . '</span>';
} else $return .= $data->PMO;

echo $return;