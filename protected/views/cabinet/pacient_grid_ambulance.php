<?php
/**
 * User: локтионов_ав
 * Date: 22.06.14
 * Time: 18:48
 * @var $this CabinetController
 * @var $id String
 * @var $data Pacient
 * @var $filter String
 */
?>
<?php $this->renderPartial('pacient_btn_group', array('place' => 1, 'filter' => $filter)); ?>

<?php $this->widget('TbGridView', array(
    'id' => 'pacient-view-a',
    'dataProvider' => $model->search('a', $filter),
    'columns' => array(
        array(
            //'class'=>'TbFioColumn',
            'header' => 'ФИО',
            'name' => 'FAM',
            'value' => function ($data) {
                    /**
                     * @var $this Controller
                     */
                    return $this->renderPartial('pacient_column_fio', array('data' => $data), true);
                },
            'type' => 'raw',
        ),
        array(
            'value' => function ($data) {
                    return $this->renderPartial('pacient_column_ds', array('data' => $data), true);
                },
            'type' => 'raw',
            'header' => 'Диагноз',
        ),
        array(
            'header' => 'Мед. организации',
            'value' => function ($data) {

                    return $this->renderPartial('pacient_column_mo', array('data' => $data), true);
                },
            'type' => 'html',
        ),
        array(
            'header'=>'Даты',
            'type'=>'html',
            'value'=>function($data){
                    return $this->renderPartial('pacient_column_dates', array('data' => $data), true);
                },
            'name'=>'DPOGOSP',
        ),
        array(
            'header' => 'Действия',
            'value' => function ($data) {
                    /** @var $this Controller */
                    return $this->renderPartial('pacient_column_actions', array('data' => $data, 'id' => 1));
                },
            'type' => 'raw',
            'htmlOptions' => array(
                'width' => '110px',
            ),

        ),
    ),


));
