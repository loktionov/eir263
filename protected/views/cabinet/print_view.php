<?php
/**
 * User: Локтионов_АВ
 * Date: 17.09.14
 * Time: 15:37
 * @var PacientView $data
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<style>
    #napr_table .border_bottom {
        border-bottom: 1px black solid;
    }

    #napr_table .width_10 {
        width: 10cm;
    }

    #napr_table .align_center {
        text-align: center;
    }

    #napr_table .cat {
        color: #B3B3B3;
        font-size: 80%;
    }

    #napr_table td {
        padding: 2px 0 2px 0;
        line-height: 0.8;
    }
    #napr_table .height_35{
        height: 35px;
    }

</style>
<table id="napr_table" style="width: 200mm;">
    <tbody>
    <tr>
        <td class="border_bottom width_10 align_center">
            <?php echo $data->NAMMO_clinic; ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="align_center width_10">
            <span class="cat">(наименование медицинского учреждения)</span>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" class="align_center "><b>НАПРАВЛЕНИЕ</b>
            <?php if(!empty($data->NNAPR)): ?>
            <br /><br /> № <?php echo $data->NNAPR; ?>
            <?php echo !empty($data->DPGOSP) ? ' на ' . date(Helpers::getDateFormat(false, true), strtotime($data->DPGOSP)) : ''; ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="align_center ">на госпитализацию, восстановительное лечение, обследование, консультацию
        </td>
    </tr>
    <tr>
        <td colspan="2" class="align_center cat">(нужное подчеркнуть)</td>
    </tr>
    <tr>
        <td colspan="2" class="align_center border_bottom height_35">
            <?php echo $data->NAMPMO_hospital; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="align_center"><span class="cat">(наименование медицинского учреждения, куда направлен пациент)</span></td>
    </tr>
    <tr>
        <td>1. Номер страхового полиса ОМС</td>
        <td>
            <table style="float: right; width: 0">
                <tbody>
                <tr>
                    <?php
                    $enumber = empty($data->SPOLIS) ? $data->NPOLIS : $data->SPOLIS . ' № ' . $data->NPOLIS;
                    $enumber = preg_split('//', trim($enumber));
                    foreach ($enumber as $char) {
                        ?>
                        <?php if ($char != '') { ?>
                            <td style="border: 1px solid black; padding: 2px">
                                <?php echo $char; ?>
                            </td>
                        <?php } ?>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table style="float: right; width: 0">
                <tbody>
                <tr>
                    <td style="border: none">
                        2. Код льготы&nbsp;
                    </td>
                    <td style="border: 1px solid black">
                        &nbsp;
                        &nbsp;
                    </td>
                    <td style="border: 1px solid black">
                        &nbsp;
                        &nbsp;
                    </td>
                    <td style="border: 1px solid black">
                        &nbsp;
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            3. Фамилия, имя, отчество
        </td>
        <td class="border_bottom">
            <?php echo $data->FAM . ' ' . $data->IM . ' ' . $data->OT; ?>
        </td>
    </tr>
    <tr>
        <td>
            4. Дата рождения
        </td>
        <td class="border_bottom">
            <?php echo date('d.m.Y', strtotime($data->DR)); ?>
        </td>
    </tr>
    <tr>
        <td>
            5. Адрес постоянного места жительства
        </td>
        <td class="border_bottom">

        </td>
    </tr>
    <tr>
        <td>
            6. Место работы, должность
        </td>
        <td class="border_bottom">

        </td>
    </tr>
    <tr>
        <td>
            7. Код диагноза по МКБ
        </td>
        <td>
            <table style="float: left; width: 0">
                <tbody>
                <tr>
                    <?php
                    $dspo = !empty($data->DSNAPR) ? $data->DSNAPR : '     ';
                    $dspo = preg_split('//', ($dspo));
                    foreach ($dspo as $char):
                        ?>
                        <?php if ($char != ''): ?>
                            <td style="border: 1px solid black; padding: 2px;">
                                <?php echo $char; ?>
                            </td>
                    <?php endif; endforeach; ?>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="height_35">
            8. Обоснование направление
        </td>
        <td class="border_bottom">

        </td>
    </tr>
    <tr>
        <td class="height_35">
            Должность медицинского работника,<br />направившего больного
        </td>
        <td class="border_bottom">
            <?php $medrab = Medrab::GetMedRabRow($data->MEDRAB); ?>
            <?php echo $medrab['FAM'] . ' ' . $medrab['IM'] . ' ' . $medrab['OT']; ?>
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td class="align_center">
            <span class="cat">(ФИО)(подпись)</span>
        </td>
    </tr>
    <tr>
        <td class="height_35">
            Заведующий отделением
        </td>
        <td class="border_bottom">

        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td class="align_center">
            <span class="cat">(ФИО)(подпись)</span>
        </td>
    </tr>
    <tr>
        <td>“__” __________ _____ г.</td>
        <td></td>
    </tr>
    <tr>
        <td style="padding-left: 60px">М.П.</td>
        <td></td>
    </tr>
    </tbody>
</table>
</body>
</html>