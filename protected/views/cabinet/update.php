<?php
/* @var $this PacientController */
/* @var $model PacientView */
?>

<?php
$this->breadcrumbs=array(
    'кабинет'=>'/cabinet',
    'редактирование',
);?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo TbHtml::alert($key, $message);
}
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>