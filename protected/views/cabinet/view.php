<?php
/* @var $this PacientController */
/* @var $model PacientView */
/* @var $proxy ProxyMan */
?>

<?php
$this->breadcrumbs = array(
    'кабинет' => '/cabinet',
    'пациент',
); ?>
<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo TbHtml::alert($key, $message);
}
?>
<?php
$out_napr = $model->MONAPR == Yii::app()->user->getState('kodmo');
$in_napr = $model->MO == Yii::app()->user->getState('kodmo');
$typep = $model->typep;
?>
<?php $this->renderPartial('actions_modal', array('model'=>$model, 'id'=>0, )); ?>

<?php $this->menu = array(
    array('label' => 'Действия'),
    TbHtml::menuDivider(),
    array('label' => 'Принять', 'url' => ($in_napr && $typep == 'planed') ? '#' : "",
    'linkOptions'=>array(
        'class' => 'accept-link',
        'data-toggle' => 'modal',
        'data-target' => '#accept-modal',
        'data-idzap' => $model->IDZAP,
        'data-fio' => $model->FAM . ' ' . $model->IM . ' ' . $model->OT . '.<br />Принять',
        'data-dngosp' => !empty($model->DPGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($model->DPGOSP)) : null,
        'data-dpogosp' => !empty($model->DPOGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($model->DPOGOSP)) : null,
        'data-dsnapr' => !empty($model->mkb10_napr) ? json_encode(array('key' => $model->DSNAPR, 'val' => $model->DSNAPR . ': ' . $model->mkb10_napr->NAMMKB)) : json_encode(array('key', 'val')),
        'id' => 'link-accept-' . $model->IDZAP,
    )),
    array('label' => 'Аннулировать', 'url' => ((($in_napr || $out_napr) && $typep == 'planed') || Yii::app()->user->getState('tip') == 2) ? '#' : '',
       'linkOptions' => array(
           'class' => 'annul-link',
           'data-toggle' => 'modal',
           'data-target' => '#annul-modal',
           'data-fio' => $model->FAM . ' ' . $model->IM . ' ' . $model->OT . '.<br />Аннулировать',
           'data-idzap' => $model->IDZAP,
           'id' => 'link-annul-' . $model->IDZAP,
       )),
    array('label' => 'Выписать', 'url' => ($in_napr && $typep == 'blocked') ? '#' : '',
        'linkOptions' =>  array(
            'class' => 'out-link',
            'data-toggle' => 'modal',
            'data-target' => '#out-modal',
            'data-idzap' => $model->IDZAP,
            'data-fio' => $model->FAM . ' ' . $model->IM . ' ' . $model->OT . '.<br />Выписать',
            'data-dspo' => !empty($model->mkb10_po) ? json_encode(array('key' => $model->DSPO, 'val' => $model->DSPO . ': ' . $model->mkb10_po->NAMMKB)) : json_encode(array('key', 'val')),
            'data-dogosp' => !empty($model->DPOGOSP) ? date(Helpers::getDateFormat(false, true), strtotime($model->DPOGOSP)) : null,
            'id' => 'link-out-' . $model->IDZAP,
        )),
    array('label' => 'Редактировать', 'url' => array('clinic/bulletinupdate', 'id' => $model->IDZAP)),
    //array('label' => 'Распечатать', 'url' => 'javascript:printBlock("#napr");'),
    //array('label' => 'Распечатать', 'url' => Yii::app()->createAbsoluteUrl('cabinet/print', array('id'=>$model->IDZAP)), 'linkOptions' =>  array('target'=>'_blank')),
);?>
<?php $this->renderPartial('_view', array('data' => $model, 'proxy' => $proxy)); ?>
<div style="float: right;">
    <?php
    $this->widget('ext.mPrint.mPrint', array(
        'title' => 'napr',          //the title of the document. Defaults to the HTML title
        'tooltip' => 'Печать',        //tooltip message of the print icon. Defaults to 'print'
        'text' => 'Распечатать',   //text which will appear beside the print icon. Defaults to NULL
        'element' => '#napr',        //the element to be printed.
        /*'exceptions' => array(       //the element/s which will be ignored
            '.summary',
            '.search-form'
        ),*/
        'publishCss' => true,       //publish the CSS for the whole page?
        //'visible' => Yii::app()->user->checkAccess('print'),  //should this be visible to the current user?
        'alt' => 'print',       //text which will appear if image can't be loaded
        'debug' => false,            //enable the debugger to see what you will get
        'id' => 'print-div'         //id of the print link
    ));
    ?>
</div>
<div id="napr" style="display: none;">

    <?php $this->renderPartial('print_view', array('data' => $model)); ?>

</div>