<?php
/**
 * User: Loktionov
 * Date: 30.05.14
 * Time: 10:41
 * @var $this ClinicController
 * @var $dt String
 * @var $rs String
 * @var $kodpmo String
 * @var $nampmo String
 * @var $kodprofk String
 * @var $namprofk String
 * @var $proxy ProxyMan
 */
?>
<?php
$this->breadcrumbs = array(
    'поиск мест' => '/clinic',
    'направление',
);
?>
<?php
if (@$isfuture)
    echo TbHtml::well(TbHtml::b('Внимание!') . ' При выдаче направления превышен срок ожидания плановой госпитализации.
                Необходимо согласие пациента.', array('style' => 'background-color: #FDD6D6;'));
?>
<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {

    echo TbHtml::alert($key, $message);
}
?>
<?php $this->widget('TbTabs', array(
    'id' => 'bulletin_tabs',
    'tabs' => array(
        array(
            'label' => 'Направление',
            'content' => $this->renderPartial('bulletin_form', array(
                    'model' => $model,
                    'kodpmo' => $kodpmo,
                    'nampmo' => $nampmo,
                    'kodprofk' => $kodprofk,
                    'namprofk' => $namprofk,
                    'dt' => $dt,
                    'rs' => $rs,
                    'proxy' => $proxy,
                ), true),
            'active' => true,
            'id' => 'form_tab',
            'class' => 'tab-pane'
        ),
        array(
            'label' => 'Поиск застрахованных в Ставропольском крае',
            'content' => $rs,
            'id' => 'zl_tab',
        ),
    ),
)); ?>