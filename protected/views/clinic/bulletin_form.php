<?php
/* @var $this PacientController */
/* @var $model Pacient */
/* @var $form TbActiveForm */
/**
 * @var $kodpmo String
 * @var $nampmo String
 * @var $kodprofk String
 * @var $namprofk String
 * @var $proxy ProxyMan
 *
 */
?>
<div class="form">
    <div style="float: left; width: 45%; margin: 15px;">
        <?php $form = $this->beginWidget('TbActiveForm', array(
            'id' => 'pacient-form',
            'hideInlineErrors' => true,
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            //'enableClientValidation'=>true,
            //'clientOptions'=>array('validateOnSubmit'=>true,'validateOnChange'=>false,),
        )); ?>

        <!--p class="help-block">Поля, отмеченные <span class="required">*</span> обязательны к заполнению.</p-->
        <?php $model->DPGOSP = empty($model->DPGOSP) ? null : date(Helpers::getDateFormat(false, true), strtotime($model->DPGOSP)); ?>
        <?php $model->DPOGOSP = empty($model->DPOGOSP) ? null : date(Helpers::getDateFormat(false, true), strtotime($model->DPOGOSP)); ?>
        <?php $model->DNGOSP = empty($model->DNGOSP) ? null : date(Helpers::getDateFormat(false, true), strtotime($model->DNGOSP)); ?>
        <?php echo $form->errorSummary($model); echo $form->errorSummary($proxy); ?>
        <?php echo Helpers::GetPacientLabel($model->typep, ($model->FOMP == 3)); ?>
        <?php echo $form->hiddenField($model, 'FOMP'); ?>
        <?php if ($model->FOMP == 1) { ?>
            <?php $this->renderPartial('bulletin_form_plan', array('model' => $model, 'form' => $form)); ?>
        <?php } elseif ($model->FOMP == 3) { ?>
            <?php $this->renderPartial('bulletin_form_extr', array('model' => $model, 'form' => $form)); ?>
        <?php
        } else {
            $this->renderPartial('bulletin_form_plan', array('model' => $model, 'form' => $form));
            $this->renderPartial('bulletin_form_extr', array('model' => $model, 'form' => $form));
        } ?>
        <?php echo TbHtml::label($model->getAttributeLabel('MO'), 'kodmo', array('required' => true)) ?>
        <?php $this->renderPartial('selectize_widget', array(
            'model' => PMOView::model(),
            'catname' => '',
            'placeholder' => 'Учреждение...',
            'required' => true,
            'value' => $kodpmo,
            'data' => array($kodpmo => $nampmo),
            'noplugins' => true,
        ));?>

        <?php echo TbHtml::label($model->getAttributeLabel('PROFK'), 'profkview', array('required' => true)) ?>
        <?php $this->renderPartial('selectize_widget', array(
            'model' => ProfKView::model(),
            'catname' => 'отделение:',
            'placeholder' => 'Профиль койки...',
            'required' => true,
            'value' => $kodprofk,
            'data' => array($kodprofk => $namprofk),
            'noplugins' => true,
        ));?>
        <?php echo TbHtml::label($model->getAttributeLabel('MEDRAB'), 'medrab') ?>
        <?php $this->renderPartial('selectize_widget', array(
            'model' => Medrab::model(),
            'catname' => 'код:',
            'placeholder' => 'Медработник...',
            'required' => false,
            'value' => $model->MEDRAB,
            'data' => !empty($model->MEDRAB) ? array($model->MEDRAB => Medrab::GetMedRabRow($model->MEDRAB, true)['FIO']) : array(),
            'noplugins' => true,
        ));?>
        <?php echo $form->error($model, 'MEDRAB') ?>


    </div>
    <div style="float: right; margin: 15px;">
        <?php echo $form->textFieldControlGroup($model, 'FAM', array('span' => 5, 'maxlength' => 40)); ?>

        <?php echo $form->textFieldControlGroup($model, 'IM', array('span' => 5, 'maxlength' => 40)); ?>

        <?php echo $form->textFieldControlGroup($model, 'OT', array('span' => 5, 'maxlength' => 40)); ?>

        <?php echo $form->dropDownListControlGroup($model, 'P', Helpers::GetSexArray(), array('empty' => '', 'span' => 5)); ?>

        <?php //echo $form->dateFieldControlGroup($model, 'DR', array('span' => 5)); ?>

        <?php if (!empty($model->DR)) {
            $model->DR = date(Helpers::getDateFormat(false, true), strtotime($model->DR));
        } ?>
        <?php echo $form->label($model, 'DR', array('required' => true)); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'model' => $model,
                'attribute' => 'DR',
                'options' => array(
                    'maxDate' => 0,
                ),
            )
        ); ?>

        <?php echo $form->textFieldControlGroup($model, 'TEL', array('span' => 5, 'maxlength' => 40)); ?>

        <?php echo $form->dropDownListControlGroup($model, 'VPOLIS', Helpers::GetPolisArray(), array('empty' => '', 'span' => 5)); ?>

        <?php //echo $form->textFieldControlGroup($model,'SPOLIS',array('span'=>5,'maxlength'=>10)); ?>

        <?php echo $form->textFieldControlGroup($model, 'NPOLIS', array('span' => 5, 'maxlength' => 20)); ?>

        <?php //if (empty($model->TER)) $model->TER = Yii::app()->params['okato']; ?>
        <?php echo $form->dropDownListControlGroup($model, 'TER', Ter::GetOptionsArray(), array(
                'empty' => '',
                'span' => 5,
                'ajax' => array(
                    'type' => 'POST', //request type
                    'url' => Yii::app()->createAbsoluteUrl($this->id . '/getsmo'), //url to call.
                    //Style: CController::createUrl('currentController/methodToCall')
                    'update' => '#Pacient_SMO', //selector to update
                    'data' => "js:$('#Pacient_TER').serialize()",
                    //leave out the data key to pass all form values through
                ),
            )
        ); ?>

        <?php echo $form->dropDownListControlGroup($model, 'SMO', Smo::GetSmoArray($model->TER), array('span' => 5, 'empty' => '')); ?>
        <?php echo TbHtml::checkBox('proxy-button', !$proxy->isNewRecord,
            array(
                'class' => 'java-link',
                'onclick' => 'show_div("proxy-man")',
            )); ?>
        <?php echo TbHtml::label('Представитель (для новорожденных)', 'proxy-button', array('style' => 'display: inline;')); ?>
        <div id="proxy-man" style="display: <?= $proxy->isNewRecord ? 'none' : 'block' ?>">
            <?php
            echo $form->textFieldControlGroup($proxy, 'fam', array('span' => 5, 'maxlength' => 40));
            echo $form->textFieldControlGroup($proxy, 'im', array('span' => 5, 'maxlength' => 40));
            echo $form->textFieldControlGroup($proxy, 'ot', array('span' => 5, 'maxlength' => 40));
            echo $form->dropDownListControlGroup($proxy, 'p', Helpers::GetSexArray(), array('empty' => '', 'span' => 5));
            echo $form->label($proxy, 'dr', array('required' => true));
             if (!empty($proxy->dr)) {
                $proxy->dr = date(Helpers::getDateFormat(false, true), strtotime($proxy->dr));
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'model' => $proxy,
                'attribute' => 'dr',
                'options' => array(
                    'maxDate' => 0,
                ),
            ));
            ?>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Выдать' : 'Обновить', array(
            'color' => TbHtml::BUTTON_COLOR_INFO,
            //'size' => TbHtml::BUTTON_SIZE_LARGE,
            'class' => 'pull-right',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->