<?php echo $form->label($model, 'DNGOSP', array('required'=>true)) ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'DNGOSP',
        'options'=>array(
            'minDate' => -2,
            'maxDate' => 0,
        ),
    )
); ?>
<?php //echo $form->error($model, 'DNGOSP'); ?>

<?php echo $form->label($model, 'DPOGOSP', array('required'=>true)) ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'DPOGOSP',
        'options'=>array(
            'minDate' => -2,
            'maxDate' => Yii::app()->params['maxNapr'],
        ),

    )
); ?>
<?php //echo $form->error($model, 'DPOGOSP'); ?>

<?php echo TbHtml::label($model->getAttributeLabel('DSPO'), 'dspo', array('required'=>true)) ?>

<?php
$this->widget('YiiSelectize', array(
    'id'=>'dspo',
    'name' => 'Pacient[DSPO]',
    'data' => !empty($model->DSPO) ? array($model->DSPO=>$model->DSPO . ': ' . MKB10::GetMkbRow($model->DSPO)['NAMMKB']) : MKB10::GetMkbRecentRows(3),
    'value' => $model->DSPO,
    'options' => array(
        'valueField' => 'kod',
        'labelField' => 'name',
        'searchField' => ['kod','name'],
        //'plugins'=> array('clear_selection'),
    ),
    'callbacks' => array('load' => 'function(query, callback){MkbLoad(query, callback,"' . Yii::app()->createAbsoluteUrl('clinic/getmkb') . '")}'),
    'htmlOptions' => array(
        'placeholder' => 'Диагноз...',
        'required' => true,
    ),
));
?>
<?php echo $form->error($model, 'DSPO', array('required'=>true)) ?>