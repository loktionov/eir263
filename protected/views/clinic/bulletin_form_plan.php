<?php $dpgosp = new DateTime($model->DPGOSP); ?>
<?php echo $form->label($model, 'DPGOSP', array('required'=>true)) ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'DPGOSP',
        'options'=>array(
            'minDate' => intval($dpgosp->diff(new DateTime(date('d-m-Y')))->format("%d")),
            'maxDate' => intval($dpgosp->diff(new DateTime(date('d-m-Y')))->format("%d")),
        ),
    )
); ?>
<?php //echo $form->error($model, 'DPGOSP'); ?>
<?php echo $form->label($model, 'DPOGOSP', array('required'=>true)); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'DPOGOSP',
        'options'=>array(
            'minDate' => intval($dpgosp->diff(new DateTime(date('d-m-Y')))->format("%d")),
            'maxDate' => intval($dpgosp->diff(new DateTime(date('d-m-Y')))->format("%d")) + Yii::app()->params['maxNapr'],
        ),
    )
); ?>
<?php echo TbHtml::label($model->getAttributeLabel('DSNAPR'), 'dsnapr', array('required'=>true)) ?>

<?php
$this->widget('YiiSelectize', array(
    'id'=>'dsnapr',
    'name' => 'Pacient[DSNAPR]',
    'data' => !empty($model->DSNAPR) ? array($model->DSNAPR=>$model->DSNAPR . ': ' . MKB10::GetMkbRow($model->DSNAPR)['NAMMKB']) : MKB10::GetMkbRecentRows(),
    'value' => $model->DSNAPR,
    'options' => array(
        'valueField' => 'kod',
        'labelField' => 'name',
        'searchField' => ['kod','name'],
        //'plugins'=> array('clear_selection'),
    ),
    'callbacks' => array('load' => 'function(query, callback){MkbLoad(query, callback,"' . Yii::app()->createAbsoluteUrl('clinic/getmkb') . '")}'),
    'htmlOptions' => array(
        'placeholder' => 'Диагноз...',
        'required' => true,
    ),
));
?>
<?php echo $form->error($model, 'DSNAPR', array('required'=>true)) ?>