<?php
/**

 * User: Loktionov
 * Date: 10.06.14
 * Time: 20:43
 * @var $data ECalendarViewItem
 * @var $this ClinicController
 */
?>
<?php Yii::beginProfile('cal_view');
$now = new DateTime(date('d-m-Y'));
$ispast = $data->date < $now;
$isfuture =  $data->date > $now->modify('+'.Yii::app()->params['maxNapr'].' days');
$modal = '';
if($data->isCurrentDate)
    $color = TbHtml::BADGE_COLOR_IMPORTANT;
else if ($ispast)
    $color = TbHtml::BADGE_COLOR_DEFAULT;
else
    $color = TbHtml::BADGE_COLOR_CUSTOM
?>
    <div class="day-div  <?= $ispast ? ' past' : ''; ?> <?= $isfuture ? ' future' : '' ?>">
        <div class="date">
            <?php echo TbHtml::badge(CHtml::encode($data->date->format('j') . ' / ' . iconv(Helpers::getEncode(), 'utf-8', strftime('%b', $data->date->getTimestamp()))), array(
                'color' => $color,
            )); ?>
        </div>
        <div style="position: relative; top: 50%;padding: 10px; text-align: center">
            <?php //var_dump(array($ispast, $data->isRelevantDate)); ?>
            <?php if (!$ispast) { ?>
                <?php
                Yii::beginProfile('fnMestaSum');
                $s = MestoView::GetMestaSum($data->date, $data->ItemData, true);
                Yii::endProfile('fnMestaSum');
                ?>
                <?php if ($s['c'] > 0) { ?>
                    <?php
                    $url = Yii::app()->createAbsoluteUrl("clinic/getmo");
                    $date = $data->date->format('d-m-Y');
                    $params = json_encode($data->ItemData);
                    $js = new CJavaScriptExpression(
                        'GetMestaAjax("' . $url . '","' . $date . '",' . $params . ')'
                    );
                    $moform = '<div id="IDMESTO' . $date . '"><img src="/css/ajax-loader.gif" class="loading-indicator" /></div>';
                    $sharp = Helpers::getIsWin() ? '%#d' : '%e';
                    $modal = $this->widget('TbModal', array(
                            'id' => 'bulletin-' . $date,
                            'header' => 'Выписка направления на ' . iconv(Helpers::getEncode(), 'utf-8', strftime("%a {$sharp} %b %y", $data->date->getTimestamp())) . ' года',
                            'content' => $moform,
                            'footer' => array(
                                TbHtml::button('Закрыть', array('data-dismiss' => 'modal')),
                            ),
                            'htmlOptions' => array(
                                'style' => "width: 1000px; margin-left: -500px; max-height: 80%; overflow-y: auto;",
                            ),
                        ),
                        true); ?>
                    Найдено:<br />
                    <?php echo TbHtml::link(TbHtml::b($s['s']) . ' ' . Helpers::DeclinationSpace($s['s']), '#', array(
                        'class' => 'java-link',
                        'data-toggle' => 'modal',
                        'data-target' => '#bulletin-' . $data->date->format('d-m-Y'),
                        'onClick' => $js,
                    )); ?>
                    <br /><span class="muted small">по <?php echo $s['c'] . ' ' . Helpers::getNumEnding($s['c'], array('профилю','профилям','профилям',)); ?> коек</span>
                <?php
                } else
                    echo 'Ничего не найдено'; ?>
            <?php } ?>
        </div>
    </div>
<?php echo $modal; Yii::endProfile('cal_view'); ?>