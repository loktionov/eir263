<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 23.03.15
 * Time: 15:41
 * @var $data ECalendarViewItem
 */

$divid = $data->date->format('dmY');

$post = array(
    'ItemData'=>$data->ItemData,
    'date'=>$data->date->format('d.m.Y'),
    'isCurrentDate'=>(int)$data->isCurrentDate,
    'isRelevantDate'=>$data->isRelevantDate,
);
?>
<div id="cal<?=$divid?>">
    
    <img src="<?=Yii::app()->baseUrl?>/css/ajax-loader.gif">
    
</div>
<script>
    $.ajax({
        type: "get",
        url: '<?php echo Yii::app()->createAbsoluteUrl('clinic/getcalajax'); ?>',
        data: <?php echo json_encode($post); ?>,
        success: function (html) {
            $('#cal<?=$divid?>').html(html);
        },
        error: function(data){
            console.log(data);
        }
    });
</script>
