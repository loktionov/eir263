<?php
/**
 * @var $this ClinicController
 * @var $model_reg RegisterForm
 * @var $model_mo Mo
 * @var $model_bulletin BulletinForm
 * @var $mesta MestoView
 * @var $pmo_name PmoView
 * @var $profk_name ProfKView
 * @var $typek_name String
 * @var $okato_name RefFilial
 * */

$this->breadcrumbs = array(
    'поиск мест'
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {

    echo TbHtml::alert($key, $message);
}
?>
    <div>
        <?php echo TbHtml::well('Выберите один или несколько параметров для поиска свободных мест, либо оставьте поля пустыми.'); ?>
    </div>
    <div>
        <?php $this->renderPartial('register_form', array(
            'pmomodel'=>@$pmo_name,
            'profkmodel'=>@$profk_name,
            'tpk'=>@$mesta['typek'],
            'raionmodel'=>@$okato_name,
            'remember_params'=>@$remember_params,
            )); ?>
    </div>
<?php echo TbHtml::well('
        <i class="fa fa-exclamation-circle text-error"></i>&nbsp;
        В поле <b>Куда направить</b> можно ввести только часть названия больницы (код больницы или название улицы), чтобы увидеть список подходящих организаций.
        '); ?>
    <div style="clear: both;"></div>
<?php //$this->renderPartial('search_result', array('model' => $mesta,)); ?>

<?php $this->widget('ecalendarview.ECalendarView', array(
    'id'=>'hospital-cal',
    'dataProvider' => array(
        'pagination' => array(

            'isMondayFirst' => true,
        )
    ),
    'itemView'=>'cal_view_ajax',
    'ajaxUpdate'=>false,
    'myData'=>array(
        'PROFK'=>@$mesta['PROFK'],
        'PMO'=>@$mesta['PMO'],
        'typek'=>@$mesta['typek'],
        'KODOKATO'=>@$mesta['KODOKATO'],
    ) ,
)); ?>