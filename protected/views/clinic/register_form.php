<?php
/**
 * User: Loktionov
 * Date: 30.05.14
 * Time: 9:15
 * @var $form TbActiveForm
 * @var $model RegisterForm
 * @var $model_bulletin BulletinForm
 * @var $this ClinicController
 * @var $pmomodel PMOView
 * @var $profkmodel ProfKView
 * @var $tpk String
 * @var $raionmodel RefFilial
 */
?>

<?php $form = $this->beginWidget('TbActiveForm', array(
    'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
    'id' => 'register-form',
    'method' => 'get',
    'action' => Yii::app()->createAbsoluteUrl($this->id . '/index'),
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    /* 'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>false,
        'successCssClass'=>false,
    ), */

)); ?>
    <table>
        <tbody>
        <tr>
            <th>Куда направить</th>
            <th>Профиль койки</th>
            <th>Район</th>
            <th>&nbsp;</th>
        </tr>


        <tr>
            <td>

                <?php //empty($pmomodel) ? $pmomodel = PMOView::model()->findByPk(Yii::app()->user->getState('kodpmo')) : PMOView::model(); ?>
                <?php $this->renderPartial('selectize_widget', array(
                    'model' => empty($pmomodel) ? PMOView::model() : $pmomodel,
                    'catname' => '',
                    'placeholder' => 'Учреждение...',
                    'required' => false,
                    'value' => @$pmomodel->KODPMO,
                    'data' => empty($pmomodel) ? array() : array($pmomodel->KODPMO => $pmomodel->NAMPMO),
                ));?>
            </td>
            <td>

                <?php $this->renderPartial('selectize_widget', array(
                    'model' => empty($profkmodel) ? ProfKView::model() : $profkmodel,
                    'catname' => 'отделение:',
                    'placeholder' => 'Профиль койки...',
                    'required' => false,
                    'value' => @$profkmodel->IDPK,
                    'data' => empty($profkmodel) ? array() : array($profkmodel->IDPK => $profkmodel->NAMPK),
                ));?>
            </td>
            <td>
                <?php
                $this->widget('YiiSelectize', array(
                    'name' => 'okato',
                    'data' => RefFilial::GetFilialArray(),
                    'value' => @$raionmodel->OKATO,
                    'options' => array(

                        'create' => false,
                        'plugins' => array('clear_selection'),
                    ),
                    'htmlOptions' => array(
                        'placeholder' => 'Район...',
                        'required' => false,
                    ),

                ));
                ?>
            </td>
            <td style="vertical-align: middle; width: 100px;">
                <?php echo TbHtml::submitButton('Найти', array('color'=>TbHtml::BUTTON_COLOR_INFO)); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div style="float: right; margin-left: 3px;"><?php echo TbHtml::checkBox('remember_params', $remember_params); ?> </div>
                <?php echo TbHtml::label('Запомнить параметры поиска','remember_params', array('class'=>'pull-right')); ?>
            </td>
        </tr>
        </tbody>
    </table>
<?php /*
$this->widget('YiiSelectize', array(
    'name' => 'tpk',
    'data'=>array('skm' => 'Мужские', 'skw' => 'Женские', 'skd' => 'Детские'),
    'value'=>@$tpk,
    'options' => array(

        'create' => false,
    ),
    'htmlOptions' => array(
        'placeholder' => 'Тип койки...',
        'required' => false,
    ),
)); */
?>
<?php //echo $form->textFieldControlGroup($model, 'prof'); ?>
<?php //echo $form->textFieldControlGroup($model, 'mo'); ?>
<?php //echo $form->textFieldControlGroup($model, 'terr'); ?>
<?php //echo $form->dateFieldControlGroup($model, 'gdate', array('style'=>'width: 335px;')); ?>
<?php //echo $form->checkBoxControlGroup($model, 'aprox'); ?>
<?php //$this->renderPartial('bulletin', array('model'=>$model_bulletin)); ?>

<?php $this->endWidget(); ?>