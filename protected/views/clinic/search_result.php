<?php
/**

 * User: Loktionov
 * Date: 30.05.14
 * Time: 9:56
 * @var $mesta CArrayDataProvider
 * @var $data Array
 * @var $this ClinicController
 */
?>
<div class="lpu-result">
    <?php $this->widget('TbGridView', array(
        'id' => 'Mesto-grid-' . $mesta->id,
        'dataProvider' => $mesta,
        'hideHeader' => true,
        'template' => '{summary} {pager} {items} ',
        'summaryText' => 'Подразделения {start}—{end} из {count}.',
        'columns' => array(
            array(
                'value' => function ($data) {
                        return "Мед. организация<br />" . TbHtml::b($data["NAMPMO"]) . "<br /><span class=\"cat\">" . $data["NAMMO"] . "</span>";
                    },
                'type' => 'html',
            ),
            array(
                'value' => function ($data) {
                        return 'Профиль койки<br />' . TbHtml::b($data['NAMPK']) . '<br /><span class="cat">отделение:' . $data['NAMPO'] . '</span>';
                    },
                'type' => 'html',
            ),
            array(
                'value' => function ($data) {
                        return 'Мест: ' . TbHtml::b($data['SK']); // . '<br />из них<br />' . $data['SKM'] . ' - мужских<br />' . $data['SKW'] . ' - женских<br />' . $data['SKD'] . ' - детских';
                    },
                'type' => 'html',
                'htmlOptions' => array(
                    'width' => '80px',
                ),
            ),
            array(
                'value' => function ($data) {
                        if ($data['SK'] > 0) {
                            return TbHtml::link('Направить',
                                Yii::app()->createAbsoluteUrl($this->id . '/bulletin',
                                    array(
                                        'idmesto' => $data['IDMESTO'],
                                        'dt' => date('d-m-Y', strtotime($data['dt']))
                                    )
                                )
                            );
                        } else
                            return '';
                    },
                'type' => 'raw',
            ),
        ),
    )); ?>
</div>