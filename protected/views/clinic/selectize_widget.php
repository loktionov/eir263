<?php
/**
 * User: Loktionov
 * Date: 06.06.14
 * Time: 17:17
 * @var $model CActiveRecord
 * @var $catname string
 * @var $placeholder string
 * @var $required bool
 * @var $data Array
 * @var $this Controller
 * @var $name String
 * @var $htmlOptions Array
 */
$callbacks = empty($callbacks) ? array() : $callbacks;
$plugins = empty($noplugins) ? array('clear_selection') : array();
$htmlOptions = isset($htmlOptions) ? $htmlOptions : array();
$name = !empty($name) ? $name : $model->tableName();
$this->widget('YiiSelectize', array(
    //'cssTheme'=>'legacy',
    'name' => $name,
    'value'=> empty($value) ? '' : $value,
    'data'=> $data,
    'options' => array(
        'options' => $model->GetOptionsArray(),
        'valueField' => 'kod',
        'labelField' => 'name',
        'sortField' => 'name',
        'searchField' => array('name', 'cat'),
        'plugins'=> $plugins,
        'create' => false,
        'render' => array(

            'option' => new CJavaScriptExpression('function(item, escape) { return RenderOptionWithOpt(item, escape); }'),

        ),
    ),
    'callbacks' => $callbacks,
    'htmlOptions' => TbArray::merge($htmlOptions, array(
        'placeholder' => $placeholder,
        'required' => $required,
    )),

));