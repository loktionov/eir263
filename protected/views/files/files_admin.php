<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 05.11.14
 * Time: 16:59
 * @var $files ImpView
 * @var $this FilesController
 */
if($files instanceof ImpView)
$this->widget('TbGridView', array(
    'id' => 'files-grid',
    'dataProvider' => $files->search(),
    'columns'=>array(
        array(
            'name'=>'NAMF',
            'value'=>function($data){
                    /** @var $data ImpView */
                    if( ! $data->pac_count)
                        return $data->NAMF;
                    $url = Yii::app()->createAbsoluteUrl('files/download', array('id'=>$data->IDIMP));
                    return TbHtml::link($data->NAMF, $url);
                },
            'type'=>'HTML',
        ),
        array(
            'name'=>'pac_count',
        ),
        array(
            'name'=>'TIPDAN',
        ),
        array(
            'name'=>'REZ',
        ),
        array(
            'name'=>'D',
        ),
        array(
            'name'=>'LOGIN',
        ),
    ),
));