<?php
/**
 * User: локтионов_ав
 * Date: 11.07.14
 * Time: 16:21
 * @var $this FilesController
 */
$this->breadcrumbs = array(
    'файлы'
);?>

<div id="flash-place" class="pull-left">
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {

        echo TbHtml::alert($key, $message);
    }
    ?>
</div>
<div class="clearfix"></div>
<fieldset>
    <div class="pull-right">
        <a href="/NSI/NSI.zip">Скачать справочную информацию одним архивом</a>
    </div>
    <legend>Загрузка файлов</legend>
    <?php
    $this->widget(
        'yiiwheels.widgets.fileupload.WhFileUpload',
        array(
            'name' => 'fileuploadui',
            'url' => $this->createAbsoluteUrl('files/upload', array('type' => 'fine')),
            'multiple' => false,
            'options' => array('always' => new CJavaScriptExpression('function(e,data){FileDone(e,data);}'),),
        )
    );
    ?>
</fieldset>

<?php $this->renderPartial("files_admin", array('files'=>$files)); ?>