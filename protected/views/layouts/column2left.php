<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="span-5">
        <div id="sidebar">
            <?php $this->beginWidget('TbAffix', array('id' => 'affix1','offset' => '-50px',)); ?>
            <div class="well" style="padding: 8px 0">
                <?php
                $this->widget('bootstrap.widgets.TbNav', array(
                    'type' => TbHtml::NAV_TYPE_LIST,
                    'items' => $this->menu,
                    'scrollspy' => '#sidebar',
                ));
                ?>
            </div>
            <?php  $this->endWidget('affix1'); ?>
        </div>
        <!-- sidebar -->
    </div>
    <div class="span-19 last">
        <div id="content">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>
<?php $this->endContent(); ?>