<?php /* @var $this Controller
 * @var $content string
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <!-- blueprint CSS framework -->
    <?php Yii::app()->bootstrap->register(); ?>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/rs.css"/>
    <?php $cs = Yii::app()->clientScript; ?>
    <?php $cs->registerScriptFile(Yii::app()->baseUrl . "/js/jquery.mask.js"); ?>
    <?php $cs->registerScriptFile(Yii::app()->baseUrl . "/js/helper.js", CClientScript::POS_END); ?>
    <?php $cs->registerScript('datePicker', "$('.hasDatepicker').mask('99.99.9999');"); ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<?php
echo TbHtml::hiddenField('baseUrl', Yii::app()->createAbsoluteUrl('/'));
$browser = Yii::app()->browser->getBrowser();
$version = Yii::app()->browser->getVersion();
if($browser = Browser::BROWSER_IE AND ($version == '7.0' OR $version == '8.0' OR $version == '9.0'))
    echo TbHtml::well('Вы используете устаревший браузер. Для корректной работы установите браузер последней версии, например Google Chrome.');
?>
<div class="container" id="page">

    <div id="header">        
        <div id="logo" style="float: left;">
            <a href="/">
                <img src=<?= Yii::app()->baseUrl . "/images/TFOMS_Logotype03-02.jpg"; ?> alt="" style="height: 120px; float: left;"/>
               <span style="float: left; margin: 45px 0 0 45px;font-size: 50px;line-height: 40px;font-weight: bold;text-rendering: optimizelegibility;">ГОСПИТАЛИЗАЦИЯ</span>
            </a>
        </div>
        <div id="user-bar">
            <?php if (!Yii::app()->user->isGuest) {
                echo Yii::app()->user->name;
                if (Yii::app()->user->getState('tip') === 0) {
                    echo '<br /><span class="cat">Подразделение: ' . Yii::app()->user->GetState('nampmo') . '</span>';
                }
            } ?>

        </div>
        <div style="clear: both;"></div>
    </div>
    <!-- header -->

    <div id="mainmenu-strap">
        <?php
        $role = WebUser::ROLE_GUEST;
        if (!Yii::app()->user->hasState('role') and !Yii::app()->user->isGuest) {
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->createAbsoluteUrl('site/login'));
        } elseif (!Yii::app()->user->isGuest) {
            $role = Yii::app()->user->getState('role');
        }
        ?>


        <?php $this->renderPartial('application.views.layouts.menu_' . $role); ?>

    </div>
    <!-- mainmenu -->
    <?php if (isset($this->breadcrumbs)): ?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
            'links' => $this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>


    <div class="clear"></div>
    <div style="margin: 0 22px 4px 22px">
        <hr class="l1">
    </div>
    <div style="padding: 10px; text-align: center; ">
        <span>ТФОМС СК «Госпитализация» <?= date('Y'); ?></span>
        <br/>
        Телефон:  8(8652)94-20-60
        <br/>
        Электропочта: <a href="mailto:skfoms@gmail.com">skfoms@gmail.com</a>
    </div>
    <!-- footer -->
</div>

<!-- page -->

</body>
</html>
