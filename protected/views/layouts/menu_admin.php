<?php
/**
 * User: Тринидад
 * Date: 21.09.14
 * Time: 15:14
 */
$items = Helpers::getCommonMenu($this, array(
    Helpers::getCommonMenuSearchplace($this),
    Helpers::getCommonMenuReport($this),
    Helpers::getCommonMenuAdmintools($this),
));
 $this->widget('TbNavbar', array(
         'brandLabel' => false,
         //'brandOptions'=>array('visible'=>false),
         'display' => TbHtml::NAVBAR_DISPLAY_NONE, // default is static to top
         'items' => $items,
     )
 );