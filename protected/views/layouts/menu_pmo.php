<?php
/**

 * User: локтионов_ав
 * Date: 20.07.14
 * Time: 16:11
 * @var $this MestoController
 */
$show_update = Helpers::isShowUpdate('27.11.2014 23:59:59', '10 days'); //. ($show_update ? '&nbsp;<i class="fa fa-exclamation-circle" style="color: #f89406"></i>' : ''),
$items = Helpers::getCommonMenu($this, array(
    array(
        'class' => 'TbNav',
        'encodeLabel' => false,
        'items' => array(
            array(
                'label' => "Пациент",
                'active' => $this->id == 'clinic',
                'visible' => !Yii::app()->user->isGuest,
                'items' => array(
                    array(
                        'label' =>  TbHtml::icon(TbHtml::ICON_CIRCLE_ARROW_LEFT) . ' Поиск&nbsp;мест',
                        'url' => Yii::app()->createAbsoluteUrl('clinic'),
                    ),
                    TbHtml::menuDivider(array(
                        'style'=>'width: 190px;',
                    )),
                    array(
                        'label' =>  TbHtml::icon(TbHtml::ICON_PLUS_SIGN) . '&nbsp;Добавить&nbsp;экстренно',
                        'url' => ($this->route == 'clinic/bulletin' and intval(isset($_GET['fomp']) ? $_GET['fomp'] : 0)==3) ? false : Yii::app()->createAbsoluteUrl('clinic/bulletin', array('fomp'=>3)),
                    ),
                ),
            ),
        ),
    ),
    array(
        'encodeLabel' => false,
        'class' => 'TbNav',
        'items' => array(
            array(
                'label' => "Места" . ($show_update ? '&nbsp;<i class="fa fa-exclamation-circle" style="color: #f89406"></i>' : ''),
                'active' => @$this->id == 'mesto' OR @$this->route == 'report/mestamo',
                'visible' => !Yii::app()->user->isGuest,
                'items' => array(
                    array(
                        'label' => "Добавить",
                        'active' => @$this->route == 'mesto/index',
                        'visible' => !Yii::app()->user->isGuest,
                        'url' => Yii::app()->createAbsoluteUrl('mesto'),
                    ),
                    array(
                        'label' => "Отчет",
                        'active' => @$this->route == 'report/mestamo',
                        'visible' => !Yii::app()->user->isGuest,
                        'url' => Yii::app()->createAbsoluteUrl('report/mestamo'),
                    ),
                ),
            ),
            array(
                'label' => "Файлы&nbsp;",
                'url' => Yii::app()->createAbsoluteUrl('files'),
                'active' => @$this->id == 'files',
                'visible' => !Yii::app()->user->isGuest
            ),
        ),
    ),
));

 $this->widget('TbNavbar', array(
        'brandLabel' => false,
        //'brandOptions'=>array('visible'=>false),
        'display' => TbHtml::NAVBAR_DISPLAY_NONE, // default is static to top
        'items' => $items,
    )
);