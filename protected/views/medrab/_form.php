<?php
/* @var $this MedrabController */
/* @var $model Medrab */
/* @var $form TbActiveForm */
?>

<div class="search-form" style="background: none">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'medrab-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Отмеченные поля<span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="cell">
        <?php echo $form->labelEx($model, 'KODMEDRAB'); ?>
        <?php $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'KODMEDRAB',
            'placeholder' => '',
            'mask' => '000-000-000 00',
            'htmlOptions' => array('size'=>11,'maxlength'=>14),

        ));?>
        <?php echo $form->error($model, 'KODMEDRAB'); ?>
    </div>
    <div class="cell" style="padding-top: 2px; width: 450px">
        <?php
        echo $form->hiddenField($model, 'MO');
        $pmo = new PMO();
        $pmo->KODMO = Yii::app()->user->getState('kod');
        echo $form->labelEx($model, 'PMO');
        $this->renderPartial('application.views.clinic.selectize_widget', array(
            'name'=>'Medrab[PMO]',
            'model' => $pmo,
            'catname' => 'отделение:',
            'placeholder' => '',
            'required' => false,
            'value' => @$model->PMO,
            'data' => array(),
        ));
        echo $form->error($model, 'PMO');
        ?>
    </div>
	<div class="cell">

        <div class="cell">

            <?php echo $form->textFieldControlGroup($model,'FAM',array('size'=>20,'maxlength'=>40)); ?>

        </div>

        <div class="cell">

            <?php echo $form->textFieldControlGroup($model,'IM',array('size'=>20,'maxlength'=>40)); ?>

        </div>

        <div class="cell">

            <?php echo $form->textFieldControlGroup($model,'OT',array('size'=>20,'maxlength'=>40)); ?>

        </div>
	</div>

    <div class="cell">
        <div class="cell" style="padding-top: 5px">

            <?php echo $form->dropDownListControlGroup($model, 'P', Helpers::GetSexArray(), array('empty' => '', 'span' => 2)); ?>

        </div>

        <div class="cell">
            <?php if (!empty($model->DR)) {
                $model->DR = date(Helpers::getDateFormat(false, true), strtotime($model->DR));
            } ?>
            <?php echo $form->labelEx($model, 'DR', array('required' => true)); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'language' => 'ru',
                    'model' => $model,
                    'attribute' => 'DR',
                    'options' => array(
                        'maxDate' => 0,
                    ),
                )
            ); ?>
            <?php echo $form->error($model, 'DR'); ?>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-actions">
        <?php echo TbHtml::submitButton('Сохранить',array(
            'color'=>TbHtml::BUTTON_COLOR_INFO,
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->