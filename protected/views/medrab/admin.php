<?php
/* @var $this MedrabController */
/* @var $model Medrab */

$this->breadcrumbs=array(
	'Паспорт'=>Yii::app()->createAbsoluteUrl('pasport/index'),
	'Мед. работник',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);


$this->widget('TbGridView', array(
	'id'=>'medrab-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name' => 'KODMEDRAB',
            'value' => 'Helpers::formatSnils($data->KODMEDRAB);'
        ),
		'FAM',
		'IM',
		'OT',

		array(
            'name'=>'DR',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->DR, "medium", null);'
        ),


		array(
			'class'=>'TbButtonColumn',
            'template' => '{update}{delete}'

		),
	),
));
