<?php
/* @var $this MedrabController */
/* @var $model Medrab */

$this->breadcrumbs=array(
	'Паспорт'=>array('pasport/index'),
	'Мед. работник' => array('medrab/admin'),
    'Новый'
);

$this->menu=array(
	array('label'=>'Назад', 'url'=>array('admin')),
);
?>

<h1>Добавить мед.работника</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>