<?php
/* @var $this MedrabController */
/* @var $model Medrab */

$this->breadcrumbs=array(
    'Паспорт'=>array('pasport/index'),
    'Мед. работник' => array('medrab/admin'),
    $model->FAM .' ' . $model->IM . ' ' . $model->OT,
);

$this->menu=array(
    array('label'=>'Назад', 'url'=>array('admin')),
);
?>

<h1>Редактировать мед. работника</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>