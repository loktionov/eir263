<?php
/* @var $this MestoController */
/* @var $model Mesto */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mesto-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'layout' => TbHtml::FORM_LAYOUT_INLINE,
    )); ?>

    <?php echo $form->errorSummary($model); ?>
    <table>
        <tbody>
        <tr>
            <td>
                <label class="control-label required" for="Mesto_PROFK">Профиль койки</label>
                <?php
                $profk_name = ! empty($model->PROFK) ? ProfK::model()->findByPk($model->PROFK) : false;
                $profk_name = $profk_name ? $profk_name->NAMPK : false;
                $this->renderPartial('application.views.clinic.selectize_widget', array(
                    'name'=>'Mesto[PROFK]',
                    'model' => ProfKView::model(),
                    'catname' => 'отделение:',
                    'placeholder' => 'Выберите профиль койки...',
                    'required' => false,
                    'value' => @$model->PROFK,
                    'data' => $profk_name ? array($model->PROFK => $profk_name) : array(),
                ));?>
            </td>
            <td>
                <label class="control-label required" for="Mesto_D">Дата</label>
                <div style="height: 3px;"></div>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'language' => 'ru',
                        'model' => $model,
                        'attribute' => 'D',
                        'options'=>array(
                            'minDate' => 0,
                            'maxDate' => Yii::app()->params['maxMestaDate'],
                        ),
                        'htmlOptions' => array(
                            'placeholder' => 'Дата',
                            'style' => 'height: 27px;'
                        ),
                    )
                ); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label class="control-label required" for="Mesto_PMO">Подраздение мед. организации</label>
                <?php
                $pmo = new PMO();
                $pmo->KODMO = Yii::app()->user->getState('kod');
                $this->renderPartial('application.views.clinic.selectize_widget', array(
                    'name'=>'Mesto[PMO]',
                    'model' => $pmo,
                    'catname' => 'отделение:',
                    'placeholder' => 'Выберите подразделение...',
                    'required' => false,
                    'value' => @$model->PMO,
                    'data' => array(),
                ));?>
            </td>
        </tr>
        </tbody>
    </table>
    <!--fieldset>
        <legend>Пациенты</legend>
        <table>
            <tbody>
            <tr>
                <td>
                    <?php //echo $form->textFieldControlGroup($model, 'NPAC', array('span' => 2, 'placeholder' => 'всего', 'onchange' => 'js:PacSum()')); ?>
                </td>
                <td>
                    <?php //echo $form->textFieldControlGroup($model, 'IPAC', array('span' => 2, 'placeholder' => 'поступило', 'onchange' => 'js:PacSum()')); ?>
                </td>
                <td>
                    <?php //echo $form->textFieldControlGroup($model, 'OPAC', array('span' => 2, 'placeholder' => 'выписано', 'onchange' => 'js:PacSum()')); ?>
                </td>
                <td>
                    <?php //echo $form->textFieldControlGroup($model, 'PPAC', array('span' => 2, 'placeholder' => 'планируется', 'onchange' => 'js:PacSum()')); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </fieldset-->
    <fieldset>
        <legend>Всего коек</legend>
        <table>
            <tbody>
            <tr>
                <td>
                    <?php echo $form->textFieldControlGroup($model, 'SK', array('span' => 2,'onchange' => 'js:KSum()')); ?>
                </td>
                <td>
                    <?php echo $form->textFieldControlGroup($model, 'SKM', array('span' => 2, 'onchange' => 'js:KSum()')); ?>
                </td>
                <td>
                    <?php echo $form->textFieldControlGroup($model, 'SKW', array('span' => 2, 'onchange' => 'js:KSum()')); ?>
                </td>
                <td>
                    <?php echo $form->textFieldControlGroup($model, 'SKD', array('span' => 2, 'onchange' => 'js:KSum()')); ?>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="pull-left">
                        <?php echo $form->textFieldControlGroup($model, 'reserve', array('span' => 4, 'onchange' => 'js:KSum()')); ?>
                    </div>
                    <div class="well well-small pull-left">
                        <i class="fa fa-exclamation-circle" style="color: #f89406"></i>
                        В поле «Резерв» вводится часть коек от общего фонда, которая не будет отображаться для
                        сторонних поликлиник. То есть если вы указали всего 100 коек по одному профилю,
                        из них 30 поставили в резерв, то сторонние поликлиники увидят 70 свободных мест, а специалисты
                        вашей поликлиники все 100. Таким образом, другие поликлиники не смогут направить вам
                        больше 70 человек. Поле можно оставить пустым, если вам не нужен резерв.
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </fieldset>
    <div class="form-actions">
        <?php echo TbHtml::submitButton('Добавить', array(
            'color' => TbHtml::BUTTON_COLOR_INFO,
            //'size' => TbHtml::BUTTON_SIZE_LARGE,
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->