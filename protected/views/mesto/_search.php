<?php
/* @var $this MestoController */
/* @var $model Mesto */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'IDMESTO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'MO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PMO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PROFK',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'D',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'NPAC',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'IPAC',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'OPAC',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PPAC',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SK',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SKM',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SKW',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SKD',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DVI',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'TEST',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_INFO,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->