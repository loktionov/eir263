<?php
/* @var $this MestoController */
/* @var $data Mesto */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('IDMESTO')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDMESTO),array('view','id'=>$data->IDMESTO)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MO')); ?>:</b>
	<?php echo CHtml::encode($data->MO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PMO')); ?>:</b>
	<?php echo CHtml::encode($data->PMO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROFK')); ?>:</b>
	<?php echo CHtml::encode($data->PROFK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('D')); ?>:</b>
	<?php echo CHtml::encode($data->D); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NPAC')); ?>:</b>
	<?php echo CHtml::encode($data->NPAC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IPAC')); ?>:</b>
	<?php echo CHtml::encode($data->IPAC); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('OPAC')); ?>:</b>
	<?php echo CHtml::encode($data->OPAC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PPAC')); ?>:</b>
	<?php echo CHtml::encode($data->PPAC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SK')); ?>:</b>
	<?php echo CHtml::encode($data->SK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SKM')); ?>:</b>
	<?php echo CHtml::encode($data->SKM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SKW')); ?>:</b>
	<?php echo CHtml::encode($data->SKW); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SKD')); ?>:</b>
	<?php echo CHtml::encode($data->SKD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DVI')); ?>:</b>
	<?php echo CHtml::encode($data->DVI); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TEST')); ?>:</b>
	<?php echo CHtml::encode($data->TEST); ?>
	<br />

	*/ ?>

</div>