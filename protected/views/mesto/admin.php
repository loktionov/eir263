<?php
/* @var $this MestoController */
/* @var $model Mesto */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mesto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
    <div class="search-form" style="display:none">
        <?php //$this->renderPartial('_search',array('model'=>$model,)); ?>
    </div><!-- search-form -->

<?php
$mesto = new MestoView();
$mesto->KODMO = Yii::app()->user->getState('kodmo');
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'mesto-grid',
    'dataProvider' => $mesto->search(1),
    'columns' => array(
        //'IDMESTO',
        //'MO',
        array(
            'name' => 'PMO',
            'header' => 'Подразделение',
            'value'=>function($data){

                    $pmo = PMO::model()->findByPk($data->PMO);
                    return empty($pmo) ? $data->PMO : $pmo->NAMPMO;
                },
        ),
        array(
            'name' => 'PROFK',
            'header' => 'Профиль койки',
            'value'=>function($data){

                    $profk = ProfKView::model()->findByPk($data->PROFK);
                    return empty($profk) ? $data->PROFK : $profk->NAMPK;
                },
        ),
        array(
            'name' => 'D',
            'header' => 'Дата',
            'value' => function($data){
                    return date(Helpers::getDateFormat(),strtotime($data->D));
                },
        ),
        array(
            'name' => 'SK',
            'header' => 'Всего мест'
        ),
        array(
            'name' => 'SKM',
            'header' => 'Мужские'
        ),
        array(
            'name' => 'SKW',
            'header' => 'Женские'
        ),
        array(
            'name' => 'SKD',
            'header' => 'Детские'
        ),
        array(
            'name' => 'reserve',
            'header' => 'Резерв'
        ),
        array(
            'class'=>'TbButtonColumn',
            'header'=>'Удалить',
            'template'=>'{delete}'
        )
    ),
)); ?>