<?php
/* @var $this MestoController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php $this->breadcrumbs = array(
    'места',
); ?>
    <div id="ajax_flash">
        <?php
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo TbHtml::alert($key, $message);
        }
        ?>
    </div>
<?php
echo TbHtml::well(TbHtml::b('Внимание!') . ' Общее количество мест следует ввести один раз, затем добавлять только
если количество коек поменялось или добавился новый профиль.', array('style' => 'background-color: #FDD6D6;'));
?>
<?php echo TbHtml::well('В данном разделе вводится '. TbHtml::b('общее') . ' количество коек в разрезе профиля.
Данные можно вводить в любое время.
Количество коек начнет учитываться с указанной даты. Дата не может быть меньше сегодняшней и больше чем ' . Yii::app()->params['maxMestaDate'] . ' дней вперед.'
); ?>
<?php $this->renderPartial('_form', array('model' => $model)); ?>
<?php $this->renderPartial('admin', array('model' => $model)); ?>