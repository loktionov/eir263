<?php
/* @var $this MestoController */
/* @var $model Mesto */
?>

<?php
$this->breadcrumbs=array(
	'места'
);

$this->menu=array(
	array('label'=>'List Mesto', 'url'=>array('index')),
	array('label'=>'Create Mesto', 'url'=>array('create')),
	array('label'=>'View Mesto', 'url'=>array('view', 'id'=>$model->IDMESTO)),
	array('label'=>'Manage Mesto', 'url'=>array('admin')),
);
?>

    <h1>Update Mesto <?php echo $model->IDMESTO; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>