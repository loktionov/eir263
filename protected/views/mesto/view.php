<?php
/* @var $this MestoController */
/* @var $model Mesto */
?>

<?php
$this->breadcrumbs=array(
	'Mestos'=>array('index'),
	$model->IDMESTO,
);

$this->menu=array(
	array('label'=>'List Mesto', 'url'=>array('index')),
	array('label'=>'Create Mesto', 'url'=>array('create')),
	array('label'=>'Update Mesto', 'url'=>array('update', 'id'=>$model->IDMESTO)),
	array('label'=>'Delete Mesto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->IDMESTO),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mesto', 'url'=>array('admin')),
);
?>

<h1>View Mesto #<?php echo $model->IDMESTO; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'IDMESTO',
		'MO',
		'PMO',
		'PROFK',
		'D',
		'NPAC',
		'IPAC',
		'OPAC',
		'PPAC',
		'SK',
		'SKM',
		'SKW',
		'SKD',
		'DVI',
		'TEST',
	),
)); ?>