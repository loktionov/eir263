<?php
/* @var $this PacientController */
/* @var $model Pacient */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'IDZAP',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'NNAPR',array('span'=>5,'maxlength'=>12)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DNAPR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DPGOSP',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DNGOSP',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'VNGOSP',array('span'=>5,'maxlength'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DOGOSP',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DANUL',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PANUL',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'IANUL',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'KANUL',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PMOANUL',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'FAM',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'IM',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'OT',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'P',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'TEL',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'VPOLIS',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SPOLIS',array('span'=>5,'maxlength'=>10)); ?>

                    <?php echo $form->textFieldControlGroup($model,'NPOLIS',array('span'=>5,'maxlength'=>20)); ?>

                    <?php echo $form->textFieldControlGroup($model,'TER',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'SMO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'FOMP',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'MONAPR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PMONAPR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PROFONAPR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PROFKNAPR',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DS',array('span'=>5,'maxlength'=>6)); ?>

                    <?php echo $form->textFieldControlGroup($model,'MEDRAB',array('span'=>5,'maxlength'=>11)); ?>

                    <?php echo $form->textFieldControlGroup($model,'MO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PMO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PROFO',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'PROFK',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'NKART',array('span'=>5,'maxlength'=>50)); ?>

                    <?php echo $form->textFieldControlGroup($model,'DSPO',array('span'=>5,'maxlength'=>6)); ?>

                    <?php echo $form->textFieldControlGroup($model,'TEST',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_INFO,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->