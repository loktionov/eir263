<?php
/* @var $this PacientController */
/* @var $data Pacient */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('IDZAP')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDZAP),array('view','id'=>$data->IDZAP)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NNAPR')); ?>:</b>
	<?php echo CHtml::encode($data->NNAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DNAPR')); ?>:</b>
	<?php echo CHtml::encode($data->DNAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DPGOSP')); ?>:</b>
	<?php echo CHtml::encode($data->DPGOSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DNGOSP')); ?>:</b>
	<?php echo CHtml::encode($data->DNGOSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VNGOSP')); ?>:</b>
	<?php echo CHtml::encode($data->VNGOSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DOGOSP')); ?>:</b>
	<?php echo CHtml::encode($data->DOGOSP); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('DANUL')); ?>:</b>
	<?php echo CHtml::encode($data->DANUL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PANUL')); ?>:</b>
	<?php echo CHtml::encode($data->PANUL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IANUL')); ?>:</b>
	<?php echo CHtml::encode($data->IANUL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KANUL')); ?>:</b>
	<?php echo CHtml::encode($data->KANUL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PMOANUL')); ?>:</b>
	<?php echo CHtml::encode($data->PMOANUL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FAM')); ?>:</b>
	<?php echo CHtml::encode($data->FAM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IM')); ?>:</b>
	<?php echo CHtml::encode($data->IM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OT')); ?>:</b>
	<?php echo CHtml::encode($data->OT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('P')); ?>:</b>
	<?php echo CHtml::encode($data->P); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DR')); ?>:</b>
	<?php echo CHtml::encode($data->DR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TEL')); ?>:</b>
	<?php echo CHtml::encode($data->TEL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VPOLIS')); ?>:</b>
	<?php echo CHtml::encode($data->VPOLIS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SPOLIS')); ?>:</b>
	<?php echo CHtml::encode($data->SPOLIS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NPOLIS')); ?>:</b>
	<?php echo CHtml::encode($data->NPOLIS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TER')); ?>:</b>
	<?php echo CHtml::encode($data->TER); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SMO')); ?>:</b>
	<?php echo CHtml::encode($data->SMO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FOMP')); ?>:</b>
	<?php echo CHtml::encode($data->FOMP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MONAPR')); ?>:</b>
	<?php echo CHtml::encode($data->MONAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PMONAPR')); ?>:</b>
	<?php echo CHtml::encode($data->PMONAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROFONAPR')); ?>:</b>
	<?php echo CHtml::encode($data->PROFONAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROFKNAPR')); ?>:</b>
	<?php echo CHtml::encode($data->PROFKNAPR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DS')); ?>:</b>
	<?php echo CHtml::encode($data->DS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MEDRAB')); ?>:</b>
	<?php echo CHtml::encode($data->MEDRAB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MO')); ?>:</b>
	<?php echo CHtml::encode($data->MO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PMO')); ?>:</b>
	<?php echo CHtml::encode($data->PMO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROFO')); ?>:</b>
	<?php echo CHtml::encode($data->PROFO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROFK')); ?>:</b>
	<?php echo CHtml::encode($data->PROFK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NKART')); ?>:</b>
	<?php echo CHtml::encode($data->NKART); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DSPO')); ?>:</b>
	<?php echo CHtml::encode($data->DSPO); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TEST')); ?>:</b>
	<?php echo CHtml::encode($data->TEST); ?>
	<br />

	*/ ?>

</div>