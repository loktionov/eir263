<?php
/* @var $this PacientController */
/* @var $model Pacient */


$this->breadcrumbs=array(
	'Pacients'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Pacient', 'url'=>array('index')),
	array('label'=>'Create Pacient', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pacient-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pacients</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'pacient-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'IDZAP',
		'NNAPR',
		'DNAPR',
		'DPGOSP',
		'DNGOSP',
		'VNGOSP',
		/*
		'DOGOSP',
		'DANUL',
		'PANUL',
		'IANUL',
		'KANUL',
		'PMOANUL',
		'FAM',
		'IM',
		'OT',
		'P',
		'DR',
		'TEL',
		'VPOLIS',
		'SPOLIS',
		'NPOLIS',
		'TER',
		'SMO',
		'FOMP',
		'MONAPR',
		'PMONAPR',
		'PROFONAPR',
		'PROFKNAPR',
		'DS',
		'MEDRAB',
		'MO',
		'PMO',
		'PROFO',
		'PROFK',
		'NKART',
		'DSPO',
		'TEST',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>