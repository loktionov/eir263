<?php
/* @var $this PacientController */
/* @var $model Pacient */
?>

<?php
$this->breadcrumbs=array(
	'Pacients'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pacient', 'url'=>array('index')),
	array('label'=>'Manage Pacient', 'url'=>array('admin')),
);
?>

<h1>Create Pacient</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>