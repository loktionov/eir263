<?php
/* @var $this PacientController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Pacients',
);

$this->menu=array(
	array('label'=>'Create Pacient','url'=>array('create')),
	array('label'=>'Manage Pacient','url'=>array('admin')),
);
?>

<h1>Pacients</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>