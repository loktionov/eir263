<?php
/* @var $this PacientController */
/* @var $model Pacient */
?>

<?php
$this->breadcrumbs=array(
	'Pacients'=>array('index'),
	$model->IDZAP=>array('view','id'=>$model->IDZAP),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pacient', 'url'=>array('index')),
	array('label'=>'Create Pacient', 'url'=>array('create')),
	array('label'=>'View Pacient', 'url'=>array('view', 'id'=>$model->IDZAP)),
	array('label'=>'Manage Pacient', 'url'=>array('admin')),
);
?>

    <h1>Update Pacient <?php echo $model->IDZAP; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>