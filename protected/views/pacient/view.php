<?php
/* @var $this PacientController */
/* @var $model Pacient */
?>

<?php
$this->breadcrumbs=array(
	'Pacients'=>array('index'),
	$model->IDZAP,
);

$this->menu=array(
	array('label'=>'List Pacient', 'url'=>array('index')),
	array('label'=>'Create Pacient', 'url'=>array('create')),
	array('label'=>'Update Pacient', 'url'=>array('update', 'id'=>$model->IDZAP)),
	array('label'=>'Delete Pacient', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->IDZAP),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pacient', 'url'=>array('admin')),
);
?>

<h1>View Pacient #<?php echo $model->IDZAP; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'IDZAP',
		'NNAPR',
		'DNAPR',
		'DPGOSP',
		'DNGOSP',
		'VNGOSP',
		'DOGOSP',
		'DANUL',
		'PANUL',
		'IANUL',
		'KANUL',
		'PMOANUL',
		'FAM',
		'IM',
		'OT',
		'P',
		'DR',
		'TEL',
		'VPOLIS',
		'SPOLIS',
		'NPOLIS',
		'TER',
		'SMO',
		'FOMP',
		'MONAPR',
		'PMONAPR',
		'PROFONAPR',
		'PROFKNAPR',
		'DS',
		'MEDRAB',
		'MO',
		'PMO',
		'PROFO',
		'PROFK',
		'NKART',
		'DSPO',
		'TEST',
	),
)); ?>