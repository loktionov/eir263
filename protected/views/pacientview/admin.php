<?php
/* @var $this PacientviewController */
/* @var $model PacientView */


$this->breadcrumbs=array(
	'Pacient Views'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PacientView', 'url'=>array('index')),
	array('label'=>'Create PacientView', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pacient-view-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pacient Views</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'pacient-view-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'IDZAP',
		'NNAPR',
		'DNAPR',
		'DPGOSP',
		'DNGOSP',
		'VNGOSP',
		/*
		'DOGOSP',
		'DANUL',
		'PANUL',
		'IANUL',
		'KANUL',
		'PMOANUL',
		'FAM',
		'IM',
		'OT',
		'P',
		'DR',
		'TEL',
		'VPOLIS',
		'SPOLIS',
		'NPOLIS',
		'TER',
		'SMO',
		'FOMP',
		'MONAPR',
		'PMONAPR',
		'PROFONAPR',
		'PROFKNAPR',
		'DS',
		'MEDRAB',
		'MO',
		'PMO',
		'PROFO',
		'PROFK',
		'NKART',
		'DSPO',
		'TEST',
		'DPOGOSP',
		'IDPO',
		'NAMPK',
		'NAMPO',
		'NAMPMO_hospital',
		'NAMMO_hospital',
		'NAMPMO_clinic',
		'NAMMO_clinic',
		'KODMO_clinic',
		'KODMO_hospital',
		'typep',
		'age',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>