<?php
/* @var $this PacientviewController */
/* @var $model PacientView */
?>

<?php
$this->breadcrumbs=array(
	'Pacient Views'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PacientView', 'url'=>array('index')),
	array('label'=>'Manage PacientView', 'url'=>array('admin')),
);
?>

<h1>Create PacientView</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>