<?php
/* @var $this PacientviewController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Pacient Views',
);

$this->menu=array(
	array('label'=>'Create PacientView','url'=>array('create')),
	array('label'=>'Manage PacientView','url'=>array('admin')),
);
?>

<h1>Pacient Views</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>