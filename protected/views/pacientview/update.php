<?php
/* @var $this PacientviewController */
/* @var $model PacientView */
?>

<?php
$this->breadcrumbs=array(
	'Pacient Views'=>array('index'),
	$model->IDZAP=>array('view','id'=>$model->IDZAP),
	'Update',
);

$this->menu=array(
	array('label'=>'List PacientView', 'url'=>array('index')),
	array('label'=>'Create PacientView', 'url'=>array('create')),
	array('label'=>'View PacientView', 'url'=>array('view', 'id'=>$model->IDZAP)),
	array('label'=>'Manage PacientView', 'url'=>array('admin')),
);
?>

    <h1>Update PacientView <?php echo $model->IDZAP; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>