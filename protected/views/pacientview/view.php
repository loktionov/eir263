<?php
/* @var $this PacientviewController */
/* @var $model PacientView */
?>

<?php
$this->breadcrumbs=array(
	'Pacient Views'=>array('index'),
	$model->IDZAP,
);

$this->menu=array(
	array('label'=>'List PacientView', 'url'=>array('index')),
	array('label'=>'Create PacientView', 'url'=>array('create')),
	array('label'=>'Update PacientView', 'url'=>array('update', 'id'=>$model->IDZAP)),
	array('label'=>'Delete PacientView', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->IDZAP),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PacientView', 'url'=>array('admin')),
);
?>

<h1>View PacientView #<?php echo $model->IDZAP; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'IDZAP',
		'NNAPR',
		'DNAPR',
		'DPGOSP',
		'DNGOSP',
		'VNGOSP',
		'DOGOSP',
		'DANUL',
		'PANUL',
		'IANUL',
		'KANUL',
		'PMOANUL',
		'FAM',
		'IM',
		'OT',
		'P',
		'DR',
		'TEL',
		'VPOLIS',
		'SPOLIS',
		'NPOLIS',
		'TER',
		'SMO',
		'FOMP',
		'MONAPR',
		'PMONAPR',
		'PROFONAPR',
		'PROFKNAPR',
		'DS',
		'MEDRAB',
		'MO',
		'PMO',
		'PROFO',
		'PROFK',
		'NKART',
		'DSPO',
		'TEST',
		'DPOGOSP',
		'IDPO',
		'NAMPK',
		'NAMPO',
		'NAMPMO_hospital',
		'NAMMO_hospital',
		'NAMPMO_clinic',
		'NAMMO_clinic',
		'KODMO_clinic',
		'KODMO_hospital',
		'typep',
		'age',
	),
)); ?>