<?php
/**
 * @var $this PasportController
 * @var $mo MO
 * @var $pmo PMO
 * @var $pasport Pasport
 * @var $medrab Medrab
 * @var $form TbActiveForm
 */

$this->breadcrumbs = array(
    'Паспорт',
);
$this->menu = array(
    array('label' => 'Мед. работник', 'url' => array('medrab/admin')),
    array('label' => 'Подразделения', 'url' => array('pmo/admin')),
);
?>

<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo TbHtml::alert($key, $message);
}
?>
<form action="<?= Yii::app()->createAbsoluteUrl('pasport/updatemo') ?>" method="post">
    <fieldset>
        <legend>Название медицинской организации</legend>
        <?= TbHtml::hiddenField('KODMO', $mo->KODMO) ?>
        <?=
        TbHtml::textField('NAMMO', $mo->NAMMO,
            array(
                'span' => 7,
                'append' => TbHtml::submitButton('Сохранить', array(
                        'color' => TbHtml::BUTTON_COLOR_INFO,

                    ))
            )) ?>
    </fieldset>
</form>


<fieldset>
    <legend>Ответственный за ведение госпитализации</legend>
    <div class="form">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'pasport-form',
            'action' => Yii::app()->createAbsoluteUrl('pasport/updatepasport'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        )); ?>

        <?php echo $form->errorSummary($pasport); ?>

        <?php echo $form->hiddenField($pasport, 'kodmo', array('span' => 5)); ?>

        <?php echo $form->textFieldControlGroup($pasport, 'fam', array('span' => 5, 'maxlength' => 50)); ?>

        <?php echo $form->textFieldControlGroup($pasport, 'im', array('span' => 5, 'maxlength' => 50)); ?>

        <?php echo $form->textFieldControlGroup($pasport, 'ot', array('span' => 5, 'maxlength' => 50)); ?>

        <?php echo $form->textFieldControlGroup($pasport, 'post', array('span' => 5, 'maxlength' => 50)); ?>

        <div class="cell">
            <div class="cell">
                <?= $form->labelEx($pasport, 'tel') ?>
                <?php $form->widget('CMaskedTextField', array(
                    'model' => $pasport,
                    'attribute' => 'tel',
                    'placeholder' => '+7(___)___-__-__',
                    'mask' => '+7(000)000-00-00',
                    'htmlOptions' => array(
                        'style' => 'margin-right: 10px;'
                    ),
                ));?>
                <?php echo $form->error($pasport, 'tel'); ?>
            </div>
            <div class="cell" style="margin-left: 18px;">
                <?php echo $form->textFieldControlGroup($pasport, 'email', array('maxlength' => 255)); ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-actions">
            <?php echo TbHtml::submitButton('Сохранить', array(
                'color' => TbHtml::BUTTON_COLOR_INFO,
                'pull' => 'right',
            )); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->
</fieldset>