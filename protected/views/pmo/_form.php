<?php
/* @var $this PmoController */
/* @var $model PMO */
/* @var $form TbActiveForm */

?>

<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'pmo-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>
    <p class="help-block">Отмеченные поля<span class="required">*</span> обязательны.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model, 'KODPMO'); ?>

    <?php echo $form->hiddenField($model, 'KODMO'); ?>

    <?php echo $form->textFieldControlGroup($model, 'NAMPMO', array('span' => 5, 'maxlength' => 150)); ?>

    <?php echo $form->textFieldControlGroup($model, 'ADRESPMO', array('span' => 5, 'maxlength' => 254)); ?>

    <div class="form-actions">
        <?php echo TbHtml::submitButton('Сохранить', array(
            'color' => TbHtml::BUTTON_COLOR_INFO,
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->