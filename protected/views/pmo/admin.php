<?php
/* @var $this PmoController */
/* @var $model PMO */


$this->breadcrumbs=array(
    'Паспорт'=>Yii::app()->createAbsoluteUrl('pasport/index'),
    'Подразделения',
);
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'pmo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'NAMPMO',
		'ADRESPMO',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}'
		),
	),
));