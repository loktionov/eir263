<?php
/* @var $this PmoController */
/* @var $model PMO */
?>

<?php
$this->breadcrumbs=array(
    'Паспорт'=>array('pasport/index'),
    'Подразделения' => array('pmo/admin'),
    $model->NAMPMO,
);
$this->menu=array(
    array('label'=>'Назад', 'url'=>array('admin')),
);
$this->renderPartial('_form', array('model'=>$model)); ?>