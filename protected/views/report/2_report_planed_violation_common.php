<?php
/**
 * User: Локтионов_АВ
 * Date: 20.10.14
 * Time: 17:31
 * @var $this ReportController
 * @var $report ReportForm
 * @var $form TbActiveForm
 */
?>
<div>
    <?php
    $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'range-form-1planed',
        'action' => Yii::app()->createAbsoluteUrl('report/analitic', array('id'=>2)),
    ));
    $l = setlocale(LC_ALL, Helpers::getLocale());
    ?>
    <?= TbHtml::hiddenField('report_type', 2); ?>
    <div class="well well-small">Отчет о пациентах госпитализированных с нарушением сроков в разрезе МО</div>
    <div class="clearfix"></div>
    <div style="width: 300px; float: left">
        <?php $this->widget(
            'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
            array(
                'name' => 'report_date_range_picker',
                'id' => 'report_date_range_picker',
                'htmlOptions' => array(
                    'placeholder' => 'введите период',
                    'disabled' => true,
                ),
                'value' => $report->date_range_str,
            )
        ); ?>
    </div>
    <?php $selectize_id = empty($selectize_id) ? '' : $selectize_id; ?>
    <div style="margin-left: 10px; width: 400px; float: left;">
        <?php echo $this->renderPartial('report_selectize_widget',
            array('report' => $report, 'placeholder' => 'Выберите СМО')); ?>
    </div>
    <div class="clearfix"></div>
    <div id="date_picker" class="uncheck-radio pull-left">
        <?php
        $range_btns = array();
        $ranges = ReportForm::getRangesArray();
        $alias = CHtml::resolveValue($report, 'date_range_alias');
        foreach ($ranges as $value => $label) {
            $range_btns[] = array(
                'label' => $label,
                'data-filter' => $value,
                'class' => strcmp($alias, $value)===0 ? 'active' : null,
                'onclick'=>"js:$('#range-form-1planed #date_range_alias').val('$value');",
                'htmlOptions'=>array(
                    'encode'=>false,
                ),
            );
        }
        echo TbHtml::buttonGroup($range_btns, array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT));
        echo TbHtml::hiddenField('date_range_alias', $alias)
        ?>
    </div>
    <div class="pull-right">
        <?php
        echo TbHtml::button('Скачать', array(
            'id' => 'range-submit-btn',
            //'type' => 'post',
            //'async' => false,
            'onclick' => 'js:$("form#range-form-1planed").submit()',
            //'dataType'=>'json',
            //'success' => new CJavaScriptExpression("function(data){UpdatePage('{$this->route}', {$id}, data);}"),
            //'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            //'pull' => 'right',
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>