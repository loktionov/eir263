<?php
/**
 * User: Локтионов_АВ
 * Date: 20.10.14
 * Time: 17:31
 * @var $this ReportController
 * @var $report ReportForm
 * @var $form TbActiveForm
 */
?>
<div>
    <?php
    $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'range-form-3violation',
        'action' => Yii::app()->createAbsoluteUrl('report/analitic', array('id'=>5)),
    ));
    $l = setlocale(LC_ALL, Helpers::getLocale());
    ?>
    <?= TbHtml::hiddenField('report_type', 5); ?>
    <div class="well well-small">Отчет об информационном сопровождении застрахованных лиц при оказании им медицинской помощи, утвержденный
        приказом ФОМС от 31.12.2013</div>
    <div class="clearfix"></div>
    <div style="width: 300px; float: left">
        <?php $this->widget(
            'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
            array(
                'name' => 'report_date_range_picker',
                'id' => 'report_date_range_picker',
                'htmlOptions' => array(
                    'placeholder' => 'введите период',
                    'disabled' => true,
                ),
                'value' => $report->date_range_str,
            )
        ); ?>
    </div>
    <?php $selectize_id = empty($selectize_id) ? '' : $selectize_id; ?>

    <div class="clearfix"></div>
    <div id="date_picker" class="uncheck-radio pull-left">
        <?php
        $range_btns = array();
        $ranges = ReportForm::getRangesArray();
        $alias = CHtml::resolveValue($report, 'date_range_alias');
        foreach ($ranges as $value => $label) {
            $range_btns[] = array(
                'label' => $label,
                'data-filter' => $value,
                'class' => strcmp($alias, $value)===0 ? 'active' : null,
                'onclick'=>"js:$('#range-form-3violation #date_range_alias').val('$value');",
                'htmlOptions'=>array(
                    'encode'=>false,
                ),
            );
        }
        echo TbHtml::buttonGroup($range_btns, array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT));
        echo TbHtml::hiddenField('date_range_alias', $alias)
        ?>
    </div>
    <div class="pull-right">
        <?php
        echo TbHtml::button('Скачать', array(
            'id' => 'range-submit-btn',
            //'type' => 'post',
            //'async' => false,
            'onclick' => 'js:$("form#range-form-3violation").submit()',
            //'dataType'=>'json',
            //'success' => new CJavaScriptExpression("function(data){UpdatePage('{$this->route}', {$id}, data);}"),
            //'data-dismiss' => 'modal',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            //'pull' => 'right',
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>