<?php
$this->breadcrumbs=array(
    'Аналитик'=>array('analitic'),
);

$this->menu=array(
    array('label'=>'Количество плановых', 'url'=>array('analitic', 'id'=>1)),
    array('label'=>'Количество нарушений', 'url'=>array('analitic', 'id'=>2)),
    array('label'=>'Список нарушений', 'url'=>array('analitic', 'id'=>3)),
    array('label'=>'Объемы мед.помощи', 'url'=>array('analitic', 'id'=>4)),
    array('label'=>'Отчет ФОМС', 'url'=>array('analitic', 'id'=>5)),
);

foreach (Yii::app()->user->getFlashes() as $key => $message) {

    echo TbHtml::alert($key, $message);
}
?>
<?php
/**
 *
 * @var ReportController $this
 */
$report = new ReportForm();
$report->category = ReportForm::REPORT_CATEGORY_SMO;
$this->renderPartial($report_name, array('report' => $report,));
?>

<!--ol>
    <li>
        <?php echo TbHtml::link('Отчет по количеству госпитализированных в плановом порядке', '#', array(
            'data-toggle' => 'modal',
            'data-target' => '#report_1',
            'onclick' => new CJavaScriptExpression("$('#report_type').val(1)"),
            'class' => 'java-link',
        )); ?>
    </li>
    <li>
        <?php echo TbHtml::link('Отчет о пациентах госпитализированных с нарушением сроков в разрезе МО', '#', array(
            'data-toggle' => 'modal',
            'data-target' => '#report_1',
            'onclick' => new CJavaScriptExpression("$('#report_type').val(2)"),
            'class' => 'java-link',
        )); ?>
    </li>
    <li>
        <?php echo TbHtml::link('Список плановых пациентов, госпитализированных с нарушением сроков', '#', array(
            'data-toggle' => 'modal',
            'data-target' => '#report_1',
            'onclick' => new CJavaScriptExpression("$('#report_type').val(3)"),
            'class' => 'java-link',
        )); ?>
    </li>
    <li>
        <?php echo TbHtml::link('Отчет об объемах медицинской помощи, оказанной в условиях круглосуточного стационара', '#', array(
            'data-toggle' => 'modal',
            'data-target' => '#report_3',
            'onclick' => new CJavaScriptExpression("$('#report_type').val(4)"),
            'class' => 'java-link',
        )); ?>
    </li>
    <li>
        <?php echo TbHtml::link('Отчет об информационном сопровождении застрахованных лиц при оказании им медицинской помощи, утвержденный
        приказом ФОМС от 31.12.2013', '#', array(
            'data-toggle' => 'modal',
            'data-target' => '#report_ffoms',
            'onclick' => new CJavaScriptExpression("$('#report_type').val(5)"),
            'class' => 'java-link',
        )); ?>
    </li>
</ol-->