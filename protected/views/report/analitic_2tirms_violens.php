<?php
/**
 * User: Локтионов_АВ
 * Date: 20.10.14
 * Time: 17:31
 * @var $this ReportController
 * @var $report ReportForm
 * @var $form TbActiveForm
 */
?>
<div>
    <?php
    $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'range-form',
        'action' => Yii::app()->createAbsoluteUrl('report/analitic_download', array()),
    ));
    //echo $form->hiddenField($report, 'category');
    //echo $form->hiddenField($report, 'kodmo');
    //echo $form->hiddenField($report, 'date_range_str');
    //echo $form->hiddenField($report, 'date_range_alias');
    $l = setlocale(LC_ALL, Helpers::getLocale());
    ?>

    <div style="width: 300px; float: left">
        <?php $this->widget(
            'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
            array(
                'name' => 'report_date_range_picker',
                'id' => 'report_date_range_picker',
                'htmlOptions' => array(
                    'placeholder' => 'введите период',
                    'disabled' => true,
                ),
                'value' => $report->date_range_str,
            )
        ); ?>
    </div>
    <?php $selectize_id = empty($selectize_id) ? '' : $selectize_id; ?>
    <div style="margin-left: 10px; width: 400px; float: left;">
        <?php echo $this->renderPartial('report_selectize_widget',
            array('report' => $report, 'placeholder' => 'Выберите СМО', 'selectize_id' => 'tirms_violens')); ?>
    </div>
    <?php echo TbHtml::hiddenField('report_type', 2); ?>
    <div class="clearfix"></div>
    <div id="date_picker" class="uncheck-radio">
        <?php
        $range_btns = array();
        $ranges = ReportForm::getRangesArray();
        $alias = CHtml::resolveValue($report, 'date_range_alias');
        foreach ($ranges as $value => $label) {
            $range_btns[] = array('label' => $label, 'data-filter' => $value, 'class' => !strcmp($alias, $value) ? 'active' : null);
        }
        echo TbHtml::buttonGroup($range_btns, array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT)); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>