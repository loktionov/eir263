<?php
/**
 * @var $this ReportController
 * @var $report ReportForm
 */

$this->breadcrumbs = array(
    'ТФОМС',
);
?>
<legend>Сводный отчет о движении пациентов</legend>
<?php $this->renderPartial("report_controls", array('report' => $report)); ?>
<?php $this->renderPartial("report_result", array('report' => $report)); ?>
