<?php
/**
 *
 * @var $this ReportController
 * @var $f14 F14
 */
?>
    <style>
        .td_bot {
            vertical-align: bottom ! important;
        }
    </style>
<legend>Занятость мест в разрезе профиля коек на <?= date('d.m.Y'); ?></legend>
<?php if($this->action->id != 'mestamo'): ?>
    <form method="post">
        <table>
            <tr>
                <td><?php $this->renderPartial('report_selectize_widget', array('report' => $report)); ?></td>
                <td><?php echo TbHtml::submitButton('Показать', array('color' => TbHtml::BUTTON_COLOR_INFO,
                        'size' => TbHtml::BUTTON_SIZE_DEFAULT,)); ?></td>
            </tr>
        </table>
    </form>
<?php endif; ?>
<?php

if ($dp instanceof CArrayDataProvider) {
    ?>
    <?php if (!$f14->isNewRecord): ?>
        <div style="float: right">
            Всего коек: <b><?= $sk ?></b><br/>
            Ф. 14-МЕД (ОМС): <b><?= $f14->f14 ?></b><br/>
            Задание: <b><?= $f14->z15 ?></b>
        </div>
        <div class="clearfix"></div>
    <?php endif; ?>
    <?php $this->widget('TbGridView', array(
        'id' => 'mesta-grid-view',
        'dataProvider' => $dp,
        'columns' => array(
            array(
                'name' => 'NAMMO',
                'header' => 'Медицинская организация',
            ),
            array(
                'name' => 'NAMPMO',
                'header' => 'Подразделение',
            ),
            array(
                'name' => 'NAMPK',
                'header' => 'Профиль&nbsp;коек',
                'type' => 'html',
                'value' => function ($data) {
                        return $data['NAMPK'] . '<br />'
                        . '<span class="cat"> последнее изменение от ' . date('d.m.Y', strtotime($data['d'])) . '</span>';
                    }
            ),
            array(
                'header' => 'Коечный&nbsp;фонд',
                'type' => 'html',
                'cssClassExpression' => function () {
                        return 'td_bot';
                    },
                'value' => function ($data) {
                        return
                            TbHtml::b($data['SKM']) . '&nbsp;-&nbsp;мужских' . '<br />'
                            . TbHtml::b($data['SKW']) . '&nbsp;-&nbsp;женских' . '<br />'
                            . TbHtml::b($data['SKD']) . '&nbsp;-&nbsp;детских' . '<br />'
                            . TbHtml::b($data['reserve']) . '&nbsp;-&nbsp;резерв'

                            . '<hr />'
                            . TbHtml::b($data['SK']) . '&nbsp;-&nbsp;всего&nbsp;коек';
                    }
            ),
            array(
                'header' => 'Пациенты',
                'type' => 'html',
                'cssClassExpression' => function () {
                        return 'td_bot';
                    },
                'value' => function ($data) {
                        return
                            TbHtml::b($data['planed']) . '&nbsp;-&nbsp;бронь' . '<br />'
                            . TbHtml::b($data['extr']) . '&nbsp;-&nbsp;экстренно' . '<br />'
                            . TbHtml::b($data['p_count'] - $data['planed'] - $data['extr']) . '&nbsp;-&nbsp;планово'

                            . '<hr>'
                            . TbHtml::b($data['p_count']) . '&nbsp;-&nbsp;всего&nbsp;пациентов';

                    }
            ),
            array(
                'header' => 'Свободно',
                'type' => 'html',
                'cssClassExpression' => function () {
                        return 'td_bot';
                    },
                'value' => function ($data) {
                        $fact = ($data['SK'] - $data['p_count']);
                        $showed = $fact + $data['expired'];

                        return TbHtml::b($fact) . '&nbsp;-&nbsp;фактически' . '<br />'
                        . TbHtml::b($showed) . '&nbsp;-&nbsp;отображается'

                        . '<hr>'
                        . TbHtml::b($data['expired']) . '&nbsp;-&nbsp;просрочено';
                    }
            ),
        ),
    ));
}
?>