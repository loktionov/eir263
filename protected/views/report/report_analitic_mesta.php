<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 24.03.15
 * Time: 13:09
 * @var $this ReportController
 * @var $f14 ReportF14View
 */
?>
    <style>
        .dif-error {
            background-color: #FDD6D6;
        }
    </style>
    <legend>
        Количество и состав коечного фонда на <?= date('d.m.Y'); ?>
    </legend>
<?php

$this->widget('TbGridView', array(
    'id' => 'reportf14view-grid',
    'dataProvider' => $f14->search(),
    'filter' => $f14,
    'rowCssClassExpression' => function ($data, $row) {

            if ($row->dif > 0)
                return 'dif-error';
            return null;
        },
    'columns' => array(
        array(
            'name' => 'KODMO',
            'header' => 'КОД МО'
        ),
        array(
            'name' => 'NAMMO',
            'header' => 'Название МО',
            'type' => 'raw',
            'value' => function ($data) {
                    return $data->NAMMO
                    . '<br />'
                    . '<span class="cat">'
                    . $data->ADRESMO
                    . '</span>';
                },
        ),
        array(
            'name' => 'sk',
            'header' => 'Всего коек'
        ),
        array(
            'name' => 'reserve',
            'header' => 'Резерв'
        ),
        array(
            'name' => 'z15',
            'header' => 'Задание по ОМС'
        ),
        array(
            'name' => 'dif',
            'header' => 'Отклонение'
        ),
        array(
            'name' => 'f14',
            'header' => 'Ф. 14-МЕД (ОМС)'
        ),
    ),
));

?>