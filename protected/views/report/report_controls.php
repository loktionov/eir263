<?php
/**
 * @var ReportController $this
 * @var ReportForm $report
 */
?>
<style>

</style>
<div class="well well-small">
    <?php /** @var $form TbActiveForm */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'mesto-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'layout' => TbHtml::FORM_LAYOUT_INLINE,
    )); ?>
    <table id="report_filter_table">
        <tbody>
        <tr>
            <th>
                <?php echo $form->labelEx($report, 'date_range', array('class' => 'label-small muted', 'for' => 'report_date_range_picker')); ?>
            </th>
            <th>
                <?php echo $form->labelEx($report, 'category', array('class' => 'label-small muted', 'for' => 'org_picker')); ?>
            </th>
            <th>
                <?php echo $form->labelEx($report, 'kodmo', array('class' => 'label-small muted', 'for' => 'pmoview')); ?>
            </th>
        </tr>
        <tr>
            <td width="250px">
                <div id="date_filter">
                    <?php
                    $l = setlocale(LC_ALL, Helpers::getLocale());
                    $this->widget(
                        'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
                        array(
                            'name' => 'report_date_range_picker',
                            'id' => 'report_date_range_picker',
                            'htmlOptions' => array(
                                'placeholder' => 'введите период',
                                'disabled'=>true,
                            ),
                            'value' => $report->date_range_str,
                        )
                    );
                    ?>
                </div>
            </td>
            <td width="380px">
                <div id="org_picker" class="uncheck-radio">
                    <?php
                    $categories = ReportForm::getCategoriesArray();
                    $cat_btns = array();
                    $sel = CHtml::resolveValue($report, 'category');
                    foreach ($categories as $value => $label) {
                        $cat_btns[] = array('label' => $label, 'data-filter' => $value, 'class' => !strcmp($sel, $value) ? 'active' : null);
                    }
                    echo TbHtml::buttonGroup($cat_btns, array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT)); ?>
                </div>
            </td>
            <td>
                <div id="org_filter">
                    <?php $this->renderPartial('report_selectize_widget', array('report' => $report)); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="date_picker" class="uncheck-radio">
                    <?php
                    $range_btns = array();
                    $ranges = ReportForm::getRangesArray();
                    $alias = CHtml::resolveValue($report, 'date_range_alias');
                    foreach ($ranges as $value => $label) {
                        $range_btns[] = array('label' => $label, 'data-filter' => $value, 'class' => !strcmp($alias, $value) ? 'active' : null);
                    }
                    echo TbHtml::buttonGroup($range_btns, array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT)); ?>
                </div>
            </td>
            <td>
                <?php echo $form->errorSummary($report); ?>
            </td>
            <td style="vertical-align: bottom;">
                <div class="pull-right">
                    <?php //echo TbHtml::link('Очистить', 'javascript:void(0)', array('class' => 'java-link', 'style' => 'margin-right: 15px;')); ?>
                    <?php echo TbHtml::ajaxSubmitButton(
                        'Показать',
                        Yii::app()->createAbsoluteUrl('report/count_ajax'),
                        array(
                            'success' => new CJavaScriptExpression('function(data){$("#report_area").html(data)}'),
                        ),
                        array('color' => TbHtml::BUTTON_COLOR_INFO, 'onClick'=>"$('#report_area').append('<div class=\"overlay_ajax\" />');")
                    ); ?>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="display: none;">
        <?php echo TbHtml::activeTextField($report, 'date_range_str'); ?>
        <?php echo TbHtml::activeTextField($report, 'date_range_alias'); ?>
        <?php echo TbHtml::activeRadioButtonList($report, 'category', ReportForm::getCategoriesArray()); ?>
        <?php echo TbHtml::activeTextField($report, 'kodmo'); ?>
    </div>
    <?php $this->endWidget('mesto-form'); ?>
</div>
<script type="text/javascript">
    $('#report_date_range_picker').mask('99.99.9999 - 99.99.9999');
</script>