<?php
/**
 *
 * @var ReportController $this
 * @var CArrayDataProvider $dp
 * @var ReportForm $report
 * @var String $for
 */
if (empty($dp))
    $dp = $report->getCommonReportArray($for);
if ($dp instanceof CArrayDataProvider)
    $this->widget('TbGridView', array(
        'id' => 'category-grid-view-' . $for,
        'dataProvider' => $dp,
        'rowHtmlOptionsExpression' => function ($data) {

            },
        'columns' => array(
            array(
                'name' => 'kod',
                'header' => 'код'
            ),
            array(
                'name' => 'name',
                'header' => 'название',
                'value' => function ($data) {
                        $name = !empty($data['optgroup']) ? $data['name']
                            . '<br /><span class="muted small">'
                            . $data['optgroup']
                            . '</span>'
                            : $data['name'];
                        return $name;
                    },
                'type' => 'raw',
            ),
            array(
                'name' => 'planed',
                'header' => 'направлено'
            ),
            array(
                'name' => 'blocked',
                'header' => 'принято'
            ),
            array(
                'name' => 'blocked_er',
                'header' => 'экстренно'
            ),
            array(
                'name' => 'annuled',
                'header' => 'аннулировано'
            ),
            array(
                'name' => 'out',
                'header' => 'выписано'
            ),
            array(
                'name' => 'expired',
                'header' => 'нарушение'
            ),
            array(
                'name' => 'all',
                'header' => 'всего'
            ),

        ),
    ));