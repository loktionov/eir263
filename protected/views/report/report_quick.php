<?php
/**
 *
 * @var \ReportForm $report
 */
?>
<table>
    <tbody>
    <tr>
        <td>
            <div class="report-button">
                <div class="text-info">Направлено</div>
            </div>
        </td>

        <td>
            <div class="report-button">
                <div class="text-success">Принято</div>
            </div>
        </td>

        <td>
            <div class="report-button">
                <div class="">Аннулировано</div>
            </div>
        </td>


        <td>
            <div class="report-button">
                <div class="muted">Выписано</div>
            </div>
        </td>

        <td>
            <div class="report-button">
                <div class="text-error">Койко-дней простоя</div>
            </div>
        </td>
        <td>
            <div class="report-button">
                <div class="text-error">Превышение коек</div>
            </div>
        </td>
        <td>
            <div class="report-button">
                <div class="text-error">Нарушения сроков</div>
            </div>
        </td>

    </tr>
    <tr>
        <td>
            <div class="report-button">
                <span class="badge badge-info"><div><?php echo $report->planed; ?></div></span>
            </div>
        </td>
        <td>
            <div class="report-button">
                <span class="badge badge-success">
                    <span><?php echo $report->blocked; ?></span>
                    <?php echo TbHtml::tooltip(TbHtml::badge($report->blocked_er, array('color' => TbHtml::BADGE_COLOR_WARNING)), null, 'Экстренно'); ?>
                </span>

            </div>


        </td>
        <td>
            <div class="report-button">
                <span class="badge badge-inverse"><div><?php echo $report->annuled ?></div></span>
            </div>
        </td>
        <td>
            <div class="report-button">
                <span class="badge"><div><?php echo $report->out ?></div></span>
            </div>
        </td>
        <td>
            <div class="report-button">
                <span class="badge badge-important"><div><?php echo $report->bed_days ?></div></span>
            </div>
        </td>
        <td>
            <div class="report-button">
                <span class="badge badge-important"><div><?php echo $report->negative ?></div></span>
            </div>
        </td>
        <td>
            <div class="report-button">
                <span class="badge badge-important"><div><?php echo $report->expired ?></div></span>
            </div>
        </td>
    </tr>
    </tbody>
</table>