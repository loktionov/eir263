<?php
/**
 * @var ReportController $this
 * @var ReportForm $report
 *
 */
?>
<style>
    #report_area {
        position: relative;
    }

    /* this div is a descendent of the div above */
    div.overlay_ajax {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: .6;
        background: #fff url(/css/ajax-loader.gif) no-repeat center;
    }
    .tab-content{
        /*height: 550px ! important;*/
    }
</style>
<div id="report_area">
    <?php $this->renderPartial('report_breadcrumbs', array('report' => $report)); ?>
    <?php $this->renderPartial('report_quick', array('report' => $report)); ?>
    <?php
    $cat_arr = ReportForm::getCategoriesArray();
    $tabs = array();
     if(!empty($report->kodmo)){
         TbArray::removeValue($report->category, $cat_arr);
         TbArray::removeValue(ReportForm::REPORT_CATEGORY_FOND, $cat_arr);
         foreach ($cat_arr as $cat => $name) {
             $tabs[] = array(
                 'label' => $name,
                 'content' => $this->renderPartial('report_grid_view', array('report' => $report, 'for' => $cat), true),
                 'id' => 'tab-' . $cat,
                 'active'=>count($tabs) == 0,
             );
         }
    }
    else if($report->category != ReportForm::REPORT_CATEGORY_FOND){

        $tabs = array(
            array(
                'label' => $cat_arr[$report->category],
                'content' => $this->renderPartial('report_grid_view', array('report' => $report, 'for' => $report->category), true),
                'id' => 'tab-' . $report->category,
                'active' => true
            ),
        );
    }
     ?>
    <?php if(!empty($tabs)): ?>
    <?php $this->widget('ext.bootstrap.widgets.TbTabs', array(
        'id' => 'report_tabs',
        'tabs' => $tabs,
    )); ?>
    <?php endif; ?>
</div>