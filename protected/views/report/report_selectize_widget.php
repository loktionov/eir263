<?php
/**
 * Created by PhpStorm.
 * User: Локтионов_АВ
 * Date: 08.10.14
 * Time: 15:10
 * @var $report ReportForm
 */ ?>
<style>
    .my-optgroup-header {
        padding: 9px 10px 9px 5px;
        text-align: right;
        border: none;
        margin-bottom: 5px;
    }
    .my-option{
        font-size: 12px;
        border-top: 1px solid #ececec;
    }
    .my-item{
        font-size: 12px;
        border: none;
    }
    .option-yes{

    }
    .option-no{
        color: #CECECE;
    }
</style>
<?php $options = array();
$render = true;
$placeholder = empty($placeholder) ? 'введите название...' : $placeholder;
$optgroup=false;
$category = empty($category) ? $report->category : $category;
switch ($category) {
    case ReportForm::REPORT_CATEGORY_FOND:
        $render = false;
        break;
    case ReportForm::REPORT_CATEGORY_SMO:
        $options = Smo::model()->GetOptionsArray();
        break;
    case ReportForm::REPORT_CATEGORY_PROFK:
        $options = ProfKView::model()->GetOptGroupsArray();
        $optgroup = array(
            array('value' => 'yes', 'label' => 'Профиль койки'),
            array('value' => 'no', 'label' => 'нет пациентов'),
        );
        break;
    case ReportForm::REPORT_CATEGORY_MO_H:
        $options = PMOView::model()->GetAmbulanceArray(true);
        $optgroup = array(
            array('value' => 'yes', 'label' => 'Стационар'),
            array('value' => 'no', 'label' => 'нет пациентов'),
        );
        break;
    case ReportForm::REPORT_CATEGORY_MO_A:
        $options = PMOView::model()->GetAmbulanceArray();
        $optgroup = array(
            array('value' => 'yes', 'label' => 'Амбулатория'),
            array('value' => 'no', 'label' => 'нет пациентов'),
        );
        break;
    default:
        break;
}
$data = !empty($data) ? $data : array();
$selectize_id = empty($selectize_id) ? '' : $selectize_id;
if ($render)
    $this->widget('YiiSelectize', array(
        'name' => 'kodmo' . $selectize_id,
        'value' => '',
        'data' => $data,
        'options' => array(
            'options' => $options,
            'valueField' => 'kod',
            'labelField' => 'name',
            'searchField' => array('name'),
            'sortField' => 'name',
            'plugins' => array('clear_selection'),
            'create' => false,
            'optgroups' => $optgroup,
            'optgroupOrder' => array('yes', 'no'),
            //'dropdownParent'=>'#report_1',
            'render' => array(
                'optgroup_header' => new CJavaScriptExpression( 'function(item, escape) { return RenderOptHeader(item, escape); }' ),
                'option' => new CJavaScriptExpression( 'function(item, escape) { return RenderOption(item, escape); }' ),
                'item' => new CJavaScriptExpression( 'function(item, escape) { return RenderItem(item, escape); }' ),
            ),
        ),
        'callbacks' => array('onChange' => new CJavaScriptExpression('function(value){MoChanged(value);}')),
        'htmlOptions' => array(
            'placeholder' => $placeholder,
            'required' => false,
        ),

    )); ?>