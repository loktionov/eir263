<?php
/**

 * User: Локтионов_АВ
 * Date: 04.07.14
 * Time: 14:50
 * @var $this RssearchController
 * @var $dataProvider CArrayDataProvider
 */
?>

<?php
foreach (Yii::app()->user->getFlashes(false) as $key => $message) {

    echo TbHtml::alert($key, $message);
}
?>
<?php if (isset($dataProvider)) { ?>
    <?php
    $this->widget('TbListView', array(
        'id' => 'rssearch-listview',
        'dataProvider' => $dataProvider,
        'itemView' => 'application.views.rssearch.view',
        'template' => '{summary} {pager} {items}',
        'summaryText' => 'Найдено: {count}'
    ));
} ?>