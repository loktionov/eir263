<div class="search-form">
    <?php
    /* @var $peoplemodel Peoplesearch */
    /* @var $reestrmodel Reestrsearch */
    /* @var $form TbActiveForm */
    $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'search-form',
        'method' => 'post',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));

    ?>


    <div class="cell">
        <div class="cell">
            <?php echo $form->textFieldControlGroup($peoplemodel, 'sName', array('placeholder' => 'Фамилия',)); ?>
        </div>

        <div class="cell">
            <?php echo $form->textFieldControlGroup($peoplemodel, 'Name', array('placeholder' => 'Имя',)); ?>
        </div>
    </div>
    <div class="cell">
        <div class="cell">
            <?php echo $form->textFieldControlGroup($peoplemodel, 'pName', array('placeholder' => 'Отчество',)); ?>
        </div>

        <div class="cell">
            <label class="control-label" for="Peoplesearch_dateMan">Дата рождения</label>
            <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'language' => 'ru',
                    'model' => $peoplemodel,
                    'attribute' => 'dateMan',
                    'htmlOptions' => array(
                        'placeholder' => 'Дата рождения',
                        'style' => 'min-width: inherit ! important;'
                    ),
                )
            ); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="cell">
        <label class="control-label" for="Peoplesearch_ENP">ЕНП</label>
        <?php $this->widget('CMaskedTextField', array(
            'model' => $peoplemodel,
            'attribute' => 'ENP',
            'placeholder' => '',
            'mask' => '0000000000000000',
            'htmlOptions' => array(
                'size' => 40,
                'placeholder' => 'ЕНП',
            ),

        ));?>
        <?php echo $form->error($peoplemodel, 'ENP'); ?>
    </div>
    <div class="cell">
        <?php echo $form->textFieldControlGroup($reestrmodel, 'ENumber', array(
            'size' => 40,
            'maxlength' => 40,
            'placeholder' => 'Полис',
        )); ?>
    </div>

    <div class="clear"></div>

    <div class="cell">
        <?php echo TbHtml::ajaxSubmitButton('Найти', Yii::app()->createAbsoluteUrl('rssearch/ajaxsearch'), array(
            'success' => new CJavaScriptExpression('function(html){$("#result_list").html(html)}'),
        ), array(
            //'size' => TbHtml::BUTTON_SIZE_LARGE,
            'color' => TbHtml::BUTTON_COLOR_WARNING,
            'onclick' => new CJavaScriptExpression('SetLoadingIndicator()'),
        )); ?>
    </div>
    <?php echo TbHtml::link('Очистить', 'javascript:void(0)', array(
        'class' => 'java-link pull-right',
        'style' => 'padding-top: 20px;',
        'onclick' => new CJavaScriptExpression('ClearInputs();'),
    )); ?>

    <?php $this->endWidget(); ?>


</div>

<script type="text/javascript">
    function ClearInputs() {
        var sname = $('#Peoplesearch_sName').val('');
        var name = $('#Peoplesearch_Name').val('');
        var pname = $('#Peoplesearch_pName').val('');
        var dateman = $('#Peoplesearch_dateMan').val('');
        var enp = $('#Peoplesearch_ENP').val('');
        var enumber = $('#Reestrsearch_ENumber').val('');
    }
    function SetLoadingIndicator() {
        $("#result_list").html('<img src="/css/ajax-loader.gif">');
    }
</script>