<?php
/**
 * @var $this RssearchController
 * @var $peoplemodel Peoplesearch
 * @var $reestrmodel Reestrsearch
 * @var $dataProvider CArrayDataProvider
 * */ ?>


<?php $this->breadcrumbs = array(
    'Поиск',
);
?>
    <?php $this->renderPartial('application.views.rssearch._search_form', array(
        'peoplemodel'=>$peoplemodel,
        'reestrmodel'=>$reestrmodel
    )); ?>
<div id="result_list">
<?php $this->renderPartial('application.views.rssearch._result_list', array(
    'dataProvider'=>$dataProvider,
)); ?>
</div>