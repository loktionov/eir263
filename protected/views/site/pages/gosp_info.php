<?php
/**@var $this SiteController */
$this->menu = array(
    array('label' => '1 Общие положения', 'url' => '#main1'),
    array('label' => '2 Главное меню', 'url' => '#menu2'),
    array('label' => '3 Пациент', 'url' => '#pacient3'),
    array('label' => '3.1 Поиск мест', 'url' => '#search3_1'),
    array('label' => '3.2 Выдача направления', 'url' => '#bulletin3_2'),
    array('label' => '3.3 Добавить экстренно', 'url' => '#extr3_3'),
    array('label' => '4 Кабинет', 'url' => '#cabinet4'),
    array('label' => '4.1 Принять', 'url' => '#accept4_1'),
    array('label' => '4.2 Аннулировать', 'url' => '#annul4_2'),
    array('label' => '4.3 Выписать', 'url' => '#out4_3'),
    array('label' => '4.4 Отменить изменения', 'url' => '#rollback4_4'),
    array('label' => '4.5 Подробный вид', 'url' => '#details4_5'),
    array('label' => '5 Места', 'url' => '#mesta5'),
    array('label' => '5.1 Добавить', 'url' => '#mesta_add5_1'),
    array('label' => '5.2 Отчет', 'url' => '#mesta_report5_2'),
    array('label' => '6 Файлы', 'url' => '#files6'),
    array('label' => '6.1 Форматы файлов', 'url' => '#formats6_1'),
    array('label' => '6.2 Скачать файлы', 'url' => '#download6_2'),
    array('label' => '7 Паспорт', 'url' => '#pasport7'),
    array('label' => '8 Обновления', 'url' => '#updates8'),
);
?>
<section id="main1">
    <h4>1 Общие положения</h4>
    Единый информационный ресурс 263 &laquo;Госпитализация&raquo; (далее ЕИР263) предназначен для информационного
    взаимодействия при осуществлении
    информационного сопровождения застрахованных лиц при организации им&nbsp;медицинской помощи в&nbsp;системе
    обязательного
    медицинского страхования (ОМС). Участниками информационного взаимодействия являются: страховые медицинские
    организации (СМО), медицинские организации (МО) (стационары и&nbsp;амбулатории), территориальные фонды ОМС (ТФОМС).
    В&nbsp;состав информационного сопровождения входят следующие данные: сведения о&nbsp;направлении на&nbsp;госпитализацию,
    сведения об&nbsp;аннулировании направления на&nbsp;госпитализацию, сведения о&nbsp;госпитализации по&nbsp;направлению,
    сведения об
    экстренной госпитализации, сведения о&nbsp;выбывших из&nbsp;мед. организаций, сведения о&nbsp;наличии свободных мест
    на&nbsp;госпитализацию.
</section>
<section id="menu2">
    <H4>2 Главное меню ЕИР263</H4>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . "/images/1_menu.jpg"; ?> alt="Главное меню"></p>

    <p>Главное меню состоит из следующих пунктов:</p>

    <div class="well">
        Пациент<br/>
        Места<br/>
        Файлы<br/>
        Кабинет<br/>
        Паспорт<br/>
        Обновления<br/>
        Инструкция<br/>
    </div>


    <p>В инструкции пользователя содержится информация по работе с ЕИР263 (содержание этого документа). Остальные
        пункты меню будут разъяснены ниже.</p>

</section>
<section id="pacient3">
    <H4>3 Пациент</H4>

    <p>
        Данный раздел предназначен для поиска свободных мест и выдачи направления на госпитализацию.
        Он содержит 2 подраздела:
    </p>

    <div class="well">
        Поиск мест<br/>
        Добавить экстренно
    </div>
</section>
<section id="search3_1">
    <H5>3.1 Поиск мест</H5>

    <p>Это основной раздел, с которого начинается выдача направления. Он состоит из двух частей:</p>

    <div class="well">Форма для уточнения поиска свободных мест<br/>Календарь с результатами поиска.</div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/2_search_form.jpg"); ?> alt="Форма поиска">
        <br/>
        <span class="cat">Форма уточнения поиска свободных мест</span>
    </p>

    <div>
        Чтобы уточнить поиск свободных мест, можно выбрать Учреждение, Профиль койки или Район, где стоит
        проводить поиск и&nbsp;нажать кнопку &laquo;Найти&raquo;. Поля можно оставить пустыми, чтобы провести поиск всех
        свободных мест в&nbsp;своем регионе. Для уменьшения списка параметров, их&nbsp;можно отфильтровать, набрав
        несколько символов.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/3_selectize.jpg"); ?> alt="Фильтр параметров">
        <br/>
        <span class="cat">Фильтр параметров</span>
    </p>

    <div>
        Результат поиска отобразится в&nbsp;календаре на&nbsp;этой&nbsp;же странице. Каждая ячейка календаря содержит
        количество найденных подразделений по&nbsp;заданным параметрам на&nbsp;конкретную дату в&nbsp;виде ссылки.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/4_calendar.jpg"); ?> alt="Календарь">
        <br/>
        <span class="cat">Результаты поиска свободных мест</span>
    </p>

    <div>
        Клик на&nbsp;ссылку с&nbsp;количеством найденных подразделений, откроет дополнительное окно, с&nbsp;подробной
        информацией о&nbsp;подходящих местах для направления и&nbsp;кнопкой &laquo;Направить&raquo; рядом с&nbsp;каждым
        из&nbsp;результатов.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/5_modal_result.jpg"); ?> alt="Результат">
        <br/>
        <span class="cat">Результаты поиска свободных мест</span>
    </p>
</section>
<section id="bulletin3_2">

    <h5>3.2 Выдача направления</h5>

    <div>
        После нажатии на&nbsp;ссылку &laquo;Направить&raquo; откроется новое окно с&nbsp;предзаполненной информацией о&nbsp;направлении.
        Поля, обозначенные звездочкой, обязательны для заполнения.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/6_bulletin_form.jpg"); ?> alt="Форма выдачи
                                направления">
        <br/>
        <span class="cat">Форма выдачи направления</span>
    </p>

    <div class="well alert-info">
        Некоторые поля отображают значения только после ввода нескольких символов. Например, поле для ввода
        диагноза.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/7_ds_input.jpg"); ?> alt="Диагноз"></p>

    <div>
        Вкладка &laquo;Поиск застрахованных в&nbsp;Ставропольском крае&raquo; содержит форму для поиска застрахованных в
        Ставропольском крае. Поиск можно вести по&nbsp;любому набору параметров, как по&nbsp;ФИО, так и&nbsp;по&nbsp;номеру
        полиса. Результат отображается ниже, включает персональные данные и&nbsp;данные о&nbsp;текущей страховой принадлежности.
        После нажатия на&nbsp;кнопку &laquo;Открыть&raquo; данные о&nbsp;пациенты автоматически переносятся в&nbsp;форму
        для выдачи направления.
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/8_rssearch.jpg"); ?> alt="Поиск">
        <br/>
        <span class="cat">Поиск застрахованных в&nbsp;Ставропольском крае.</span>
    </p>

    <div>
        После нажатия кнопки &laquo;Выдать&raquo; направление сохраняется и&nbsp;становится доступным
        в&nbsp;разделе &laquo;Кабинет&raquo;.
    </div>
    В&nbsp;случае госпитализации новорожденного без ФИО и&nbsp;номера полиса, следует ввести вместо ФИО слово &laquo;Нет&raquo;,
    а&nbsp;в&nbsp;данные о&nbsp;страховке&nbsp;&mdash; полис представителя, затем отметить галочку &laquo;Представитель
    (для новорожденных)&raquo; и
    в&nbsp;появившиеся поля ввести персональные данные представителя.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/11_proxy_man.jpg"); ?> alt="Представитель">
        <br/>
        <span class="cat">Данные представителя.</span>
    </p>
    Если необходимо удалить данные о&nbsp;представителе при повторном редактировании, следует просто снять галочку
    &laquo;Представитель (для новорожденных)&raquo; и&nbsp;сохранить направление.
</section>
<section id="extr3_3">
    <H5>3.3 Добавить экстренно</H5>

    <div>
        Данный подраздел отличается от&nbsp;предыдущего тем, что исключается шаг с&nbsp;поиском мест. Пользователь сразу
        попадает
        в&nbsp;форму выдачи направления, которая ограничена условиями экстренной госпитализации.
    </div>
</section>
<section id="cabinet4">
    <H4>4 Раздел &laquo;Кабинет&raquo;</H4>

    <div>
        Данный раздел предназначен для сопровождения пациентов. Вкладка &laquo;Исходящие&raquo; содержит пациентов,
        направленных
        вами. &laquo;Входящие&raquo; содержит направленных вам. Кнопки:
        <?php foreach (Helpers::GetTypepArray() as $typep) : ?>
            &laquo;<?php echo mb_convert_case($typep['rus'], MB_CASE_TITLE, 'utf-8'); ?>&raquo;,&nbsp;
        <?php endforeach; ?>
        - фильтруют список пациентов.
    </div>
    <div class="well alert-info">
        <?php foreach (Helpers::GetTypepArray() as $typep => $content) : ?>
            <?php echo Helpers::GetPacientLabel($typep) . ' - ' . $content['detail'] ?>;<br/>
        <?php endforeach; ?>
    </div>
    <div>Для каждого типа пациентов доступен определенный набор действий:</div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/10_cabinet_actions.jpg"); ?> alt="Действия">
        <br/>
        <span class="cat">Действия над пациентом</span>
    </p>

    <div>
        &laquo;Принять&raquo;&nbsp;&mdash; госпитализирует пациента.<br/>
        &laquo;Аннулировать&raquo;&nbsp;&mdash; аннулирует пациента.<br/>
        &laquo;Выписать&raquo;&nbsp;&mdash; выпишет пациента.<br/>
    </div>
</section>
<section id="accept4_1">
    <h5>4.1 Принять</h5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/12_accept.jpg"); ?> alt="Принять">
        <br/>
        <span class="cat">Принять пациента</span>
    </p>
    При госпитализации пациента можно изменить даты и&nbsp;диагноз или оставить предложенные, если они совпадают с&nbsp;действительностью.
</section>
<section id="annul4_2">
    <h5>4.2 Аннулировать</h5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/13_annul.jpg"); ?> alt="Аннулировать">
        <br/>
        <span class="cat">Аннулировать направление</span>
    </p>
    При аннулировании направления, следует указать дату аннулирования и&nbsp;причину из&nbsp;выпадающего списка.
</section>
<section id="out4_3">
    <h5>4.3 Выписать</h5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/15_out.jpg"); ?> alt="Выписать">
        <br/>
        <span class="cat">Выписать пациента</span>
    </p>
    При выписке следует указать дату выписки и&nbsp;окончательный диагноз.
</section>
<section id="rollback4_4">
    <h5>4.4 Отменить изменения</h5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/14_rollback.jpg"); ?> alt="Откатить">
        <br/>
        <span class="cat">Отменить изменения</span>
    </p>
    После каждого действия появляется сообщение об&nbsp;успехе и&nbsp;ссылка для отмены этого действия, если оно было
    ошибочным.
    Пациент перемещается по&nbsp;списку в&nbsp;зависимости от&nbsp;присвоенного статуса, Но&nbsp;всегда доступен в&nbsp;разделе &laquo;Все&raquo;.
</section>
<section id="details4_5">
    <h5>4.5 Подробный вид</h5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/16_view.jpg"); ?> alt="Подробно">
        <br/>
        <span class="cat">Подробно</span>
    </p>
    Детальные данные о&nbsp;направлении можно увидеть нажав на&nbsp;ФИО пациента в&nbsp;общем списке.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/16_view_menu.jpg"); ?> alt="Меню">
        <br/>
        <span class="cat">Меню</span>
    </p>
    Возможные действия дублируются в&nbsp;меню в&nbsp;подробном виде.
</section>
<section id="mesta5">
    <H4>5 Раздел «Места»</H4>

    <div>
        В&nbsp;данном разделе вводится общее количество коек в&nbsp;разрезе профиля. Данные можно вводить в&nbsp;любое
        время.
        Количество коек начнет учитываться с&nbsp;указанной даты. Дата не&nbsp;может быть меньше сегодняшней и&nbsp;больше
        чем
        <?php echo Yii::app()->params['maxMestaDate']; ?> дней вперед.
    </div>
</section>
<section id="mesta_add5_1">
    <H5>5.1 Добавить</H5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/17_mesta.jpg"); ?> alt="Места">
        <br/>
        <span class="cat">Места</span>
    </p>

    <div class="well alert-info">
        Места для направления в&nbsp;разделе &laquo;Пациент&raquo; станут доступны только после того, как они будут
        добавлены в&nbsp;разделе
        &laquo;Места&raquo;.
    </div>
    <a id="mesta_chepter"></a>

    <div>В&nbsp;поле &laquo;Резерв&raquo; вводится часть коек от&nbsp;общего фонда, которая не&nbsp;будет отображаться
        для сторонних поликлиник.
        То&nbsp;есть если вы&nbsp;указали всего 100 коек по&nbsp;одному профилю, из&nbsp;них 30&nbsp;поставили в&nbsp;резерв,
        то&nbsp;сторонние поликлиники
        увидят 70&nbsp;свободных мест, а&nbsp;специалисты вашей поликлиники все 100. Таким образом, другие поликлиники
        не&nbsp;смогут
        направить вам больше 70&nbsp;человек. Поле можно оставить пустым, если вам не&nbsp;нужен резерв.
    </div>

    <div>После добавления новые койки отобразятся в&nbsp;таблице ниже. Нажав на&nbsp;корзину рядом с&nbsp;названием
        можно удалить выбранный профиль коек
    </div>
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/17_mesta_table.jpg"); ?> alt="Места">
        <br/>
        <span class="cat">Список мест</span>
    </p>
</section>
<section id="mesta_report5_2">
    <H5>5.2 Отчет</H5>

    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/18_mesta_report.jpg"); ?> alt="Отчет">
        <br/>
        <span class="cat">Отчет</span>
    </p>
    Отчет представляет из&nbsp;себя сводные данные о&nbsp;количестве мест и&nbsp;количестве пациентов.
    В&nbsp;колонке &laquo;Свободно&raquo; отмечено
    сколько свободных мест по&nbsp;данным ЕИР263 (от&nbsp;количества мест отнимается сумма брони и&nbsp;госпитализаций
    на&nbsp;текущую дату)&nbsp;&mdash; &laquo;фактически&raquo;.
    И&nbsp;количество мест доступных для госпитализации (от&nbsp;количества мест отнимается сумма непросроченных брони и&nbsp;госпитализаций)&nbsp;&mdash; &laquo;отображается&raquo;.
    А&nbsp;также разница между ними&nbsp;&mdash; пациенты, которые должны быть выписаны на&nbsp;текущую
    дату&nbsp;&mdash; &laquo;просрочено&raquo;

    <div class="well">
        Общее количество мест должно совпадать с&nbsp;заданием на&nbsp;текущий год. Число мест по&nbsp;заданию
        отображается над отчетом в&nbsp;строке <strong>Задание</strong>.
        Там&nbsp;же показано сколько мест было использовано в&nbsp;прошлом году <strong>Ф.&nbsp;14&nbsp;-&nbsp;МЕД&nbsp;(ОМС)</strong>
        и&nbsp;количество мест указанных стационаром <strong>Всего коек</strong>.
    </div>
</section>
<section id="files6">
    <H4>6 Раздел &laquo;Файлы&raquo;</H4>

    <div>
        Данный раздел позволяет загружать информацию о&nbsp;пацинентах в&nbsp;следующем формате:
    </div>


    <H5>6.1 Форматы импорта и&nbsp;экспорта данных</H5>

    <p>Наименование файла: GDabc_d.xml, где</p>
    a&nbsp;&mdash; тип данных (номер таблицы);<br/>
    b&nbsp;&mdash; тип источника (0&nbsp;&mdash; ПМО, 1&nbsp;&mdash; МО, 2&nbsp;&mdash; СМО, 3&nbsp;&mdash; ТФОМС);<br/>
    c&nbsp;&mdash; код источника;<br/>
    d&nbsp;&mdash; дата в&nbsp;формате гггг-мм-дд.<br/>
    Файл архивируется в&nbsp;формате ZIP и&nbsp;расширение заменяется на&nbsp;OMS.<br/>
</section>
<section id="formats6_1">
    <H5>6.1 Форматы файлов:</H5>

    <?php

    $n = Yii::app()->db->createCommand()
        ->select('idxml,namxml,owner')
        ->from('nxml')
        ->order('idxml')
        ->queryAll();
    foreach ($n as $t) {
        echo '<p>Таблица ' . $t['idxml'] . ' ' . $t['namxml'] . '.</p>';
        echo TbHtml::codeBlock(SXML::ExampleXML($t['idxml']));
        $s = Yii::app()->db->createCommand()
            ->select('NAME, FP, detail, require, IDXML')
            ->from('sxml_view')
            ->where('idxml=:idxml', array(':idxml' => $t['idxml']))
            ->order('idxml, order')
            ->queryAll();
        $dan = new CArrayDataProvider($s, array(
            'id' => 'sxmlDataProvider' . $t['idxml'],
            'keyField' => 'NAME',
            'totalItemCount' => count($s),
            'pagination' => array(
                'pageSize' => 150,
            ),
        ));
        $this->widget('TbGridView', array(
            'id' => 'sxml-grid' . $t['idxml'],
            'template' => '{items}',
            'dataProvider' => $dan,
            'columns' => array(
                array(
                    'name' => 'IDXML',
                    'header' => 'Тип файла'
                ),
                array(
                    'name' => 'NAME',
                    'header' => 'Название поля',
                ),
                array(
                    'name' => 'FP',
                    'header' => 'Тип данных',
                ),
                array(
                    'name' => 'require',
                    'header' => 'Обязательность',
                ),
                array(
                    'name' => 'detail',
                    'header' => 'Описание',
                ),
            ),
        ));
    }

    ?>
    <div>
        N&nbsp;&mdash; числовые данные;<br/>
        S(X)&nbsp;&mdash; символьные данные максимальной длиной X;<br/>
        D&nbsp;&mdash; дата в&nbsp;формате гггг-мм-дд;<br/>
        V&nbsp;&mdash; время в&nbsp;формате чч:мм;<br/>
        Данные кодируются в&nbsp;соответствии с&nbsp;кодами данных НСИ ТФОМС;<br/>
        Номера направления представляться в&nbsp;виде CCCCCCYYNNNNNN, где<br/>
        CCCCCC&nbsp;&mdash; код направившей медицинской организации&nbsp;6&nbsp;цифр, YY&nbsp;&mdash; 2&nbsp;цифры года выдачи направления,<br/>
        NNNNNN&nbsp;&mdash; уникальный номер направления в&nbsp;пределах направившей медицинской организации;<br/>
        Код мед. работника&nbsp;&mdash; код СНИЛС (только цифры).<br/>
    </div>
</section>
<section id="download6_2">
    <H5>6.2 Скачать файлы</H5>
    Все загруженные файлы можно скачать в&nbsp;том&nbsp;же формате, нажав на имя файла. Например, чтобы применить в&nbsp;собственной
    медицинской информационной системе (МИС) новосгенерированные номара направлений.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/19_download.jpg"); ?> alt="Скачать">
        <br/>
        <span class="cat">Файлы</span>
    </p>
</section>
<section id="pasport7">
    <H4>7 Раздел &laquo;Паспорт&raquo;</H4>
    В этом разделе вы можете изменить название медицинской организации.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/20_pasport_mo.jpg"); ?> alt="Название">
        <br/>
        <span class="cat">Название Медицинской организации</span>
    </p>
    Отредактировать список медицинских работников.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/21_medrab.jpg"); ?> alt="Мед. работник">
        <br/>
        <span class="cat">Медицинский работник</span>
    </p>
    Или уточнить названия подразделений собственной медицинской организации.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/22_pmo.jpg"); ?> alt="Подразделения">
        <br/>
        <span class="cat">Список подразделений</span>
    </p>
</section>
<section id="updates8">
    <H4>8 &laquo;Обновления&raquo;</H4>
    В данном разделе содержится вся оперативная информация касающаяся ЕИР263: список новых возможностей,
    информация о плановых технических работах и прочее.
    <p class="text-center"><img src=<?= Yii::app()->baseUrl . ("/images/23_updates.jpg"); ?> alt="Обновления">
        <br/>
        <span class="cat">Обновления</span>
    </p>
</section>