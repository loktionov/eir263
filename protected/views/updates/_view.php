<?php
/* @var $this UpdatesController */
/* @var $data Updates */
?>

<div class="view">
    <span class="cat">
        <?php echo CHtml::encode(Yii::app()->dateFormatter->formatDateTime($data->insdate, 'medium', 'short')); ?>
    </span>
    <br/>

    <p style="text-align: center"><b><?php echo CHtml::encode($data->title); ?></b></p>
    <br/>

    <?php if (!empty($data->id_cat)): ?>
        <b>Категория: </b> <?php echo CHtml::encode($data->idCat->title_cat); ?>
        <br/>
    <?php endif; ?>
    <?php echo $data->text; ?>
    <br/>

    <!--b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b-->
    <span class="cat pull-right"><?php echo CHtml::encode($data->author); ?></span>
    <br/>
</div>