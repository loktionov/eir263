<?php
/* @var $this UpdatesController */
/* @var $model Updates */
$this->pageTitle = 'Обновления - ' . Yii::app()->name;
$this->breadcrumbs=array(
	'Обновления',
);
?>


<?php $this->widget('TbListView', array(
	'dataProvider'=>$model->search(),
	'itemView'=>'_view',
    'summaryText' => 'Обновления {start} — {end} из {count}',
)); ?>
