<?php
/* @var $this UserController */
/* @var $model Login */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('TbActiveForm', array(
        'id' => 'login-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">

        <?php echo $form->dropDownListControlGroup($model, 'TIP',
            array(0 => 'Юзер МО', 1 => 'Админ МО', 2 => 'СМО', 3 => 'ТФОМС'),
            array(
                'span' => 4,
                'style'=>'font-size: 0.9em;',
                'ajax' => array(
                    'async'=>false,
                    'type' => 'POST', //request type
                    'dataType'=> 'json',
                    'url' => Yii::app()->createAbsoluteUrl($this->id . '/getkod'), //url to call.
                    //Style: CController::createUrl('currentController/methodToCall')
                    //'update' => '#tip_select', //selector to update
                    'data' => "js:$('#Login_TIP').serialize()",
                    'success'=>'function(data){RefillSelectizeUser(data);}'
                    //leave out the data key to pass all form values through
                ),
            )); ?>

    </div>

    <div class="row" id="tip_select">
        <?php echo $this->renderPartial('user_selectize_widget', $_data_); ?>
    </div>

    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'LOGIN', array('span' => 4, 'maxlength' => 255)); ?>

    </div>

    <div class="clearfix">

        <?php echo $form->textFieldControlGroup($model, 'PASSWORD', array('span' => 3,
            'style'=>'margin-top: 0 ! important;',
            'append'=>TbHtml::ajaxButton('gen',
                    Yii::app()->createAbsoluteUrl('admin/getrandpass'),
                    array(
                        'success' => new CJavaScriptExpression('function(data){$("#Login_PASSWORD").val(data);}'),
                    ),
                    array(
                        'style'=>'width: 100px;',
                    )
                )
        )); ?>


    </div>

    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'fam', array('span' => 4, 'maxlength' => 255)); ?>

    </div>
    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'im', array('span' => 4, 'maxlength' => 255)); ?>

    </div>
    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'ot', array('span' => 4, 'maxlength' => 255)); ?>

    </div>
    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'mail', array('span' => 4, 'maxlength' => 255)); ?>

    </div>





    <!--div class="row">

        <?php echo $form->textFieldControlGroup($model, 'KOD', array('span'=>4)); ?>

    </div>

    <div class="row">

        <?php echo $form->textFieldControlGroup($model, 'KODP', array('span'=>4)); ?>

    </div-->

    <div class="row buttons">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
            array(
                'color' => TbHtml::BUTTON_COLOR_INFO,
                //'size' => TbHtml::BUTTON_SIZE_LARGE,
            )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->