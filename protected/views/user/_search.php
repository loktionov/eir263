<?php
/* @var $this UserController */
/* @var $model Login */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'IDLOGIN'); ?>
		<?php echo $form->textField($model,'IDLOGIN'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LOGIN'); ?>
		<?php echo $form->textField($model,'LOGIN',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PASSWORD'); ?>
		<?php echo $form->passwordField($model,'PASSWORD',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TIP'); ?>
		<?php echo $form->textField($model,'TIP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KOD'); ?>
		<?php echo $form->textField($model,'KOD'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KODP'); ?>
		<?php echo $form->textField($model,'KODP'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->