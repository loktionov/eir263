<?php
/* @var $this UserController */
/* @var $data Login */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDLOGIN')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IDLOGIN), array('view', 'id'=>$data->IDLOGIN)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LOGIN')); ?>:</b>
	<?php echo CHtml::encode($data->LOGIN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PASSWORD')); ?>:</b>
	<?php echo CHtml::encode($data->PASSWORD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fam')); ?>:</b>
	<?php echo CHtml::encode($data->TIP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Im')); ?>:</b>
	<?php echo CHtml::encode($data->KOD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ot')); ?>:</b>
	<?php echo CHtml::encode($data->KODP); ?>
	<br />


</div>