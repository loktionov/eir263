<?php
/* @var $this UserController */
/* @var $model Login */

$this->breadcrumbs=array(
	'Logins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Login', 'url'=>array('index')),
	array('label'=>'Create Login', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#login-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Logins</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php /** @var $model Login */
$this->widget('TbGridView', array(
	'id'=>'login-grid',
	'dataProvider'=>$model->search(array('defaultOrder'=>array('IDLOGIN'=>CSort::SORT_DESC))),
	'filter'=>$model,
	'columns'=>array(
		'fam',
		'im',
		'ot',
        'LOGIN',
		'PASSWORD',
		'IDLOGIN',
		'TIP',
		'KOD',
		'KODP',
        array(
            'header'=>'Карта',
            'type'=>'html',
            'value'=>function($data){
                    $url =  Yii::app()->createAbsoluteUrl('user/getcard', array('id'=>$data->IDLOGIN));
                    return TbHtml::link('PDF', $url);
                }
        ),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
