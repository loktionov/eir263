<?php
/* @var $this UserController */
/* @var $model Login */
/*
$this->breadcrumbs=array(
	'Logins'=>array('index'),
	$model->IDLOGIN=>array('view','id'=>$model->IDLOGIN),
	'Update',
);*/

$this->menu=array(
	array('label'=>'List Login', 'url'=>array('index')),
	array('label'=>'Create Login', 'url'=>array('create')),
	array('label'=>'View Login', 'url'=>array('view', 'id'=>$model->IDLOGIN)),
	array('label'=>'Manage Login', 'url'=>array('admin')),
);
?>

<h1>Update Login <?php echo $model->IDLOGIN; ?></h1>

<?php $this->renderPartial('_form',  $selectize_data); ?>