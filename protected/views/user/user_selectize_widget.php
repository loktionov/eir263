<?php
/**
 * @var $model Login
 */
?>
    <style>
        .my-optgroup-header {
            padding: 9px 10px 9px 5px;
            text-align: right;
            border: none;
            margin-bottom: 5px;
        }

        .my-option {
            font-size: 12px;
            border-top: 1px solid #ececec;
        }

        .my-item {
            font-size: 12px;
            border: none;
        }

        .option-yes {

        }

        .option-no {
            color: #CECECE;
        }
    </style>
<?php
    $name = empty($name) ? 'kodmo' : $name;
    $this->widget('YiiSelectize', array(
        'name' => $name,
        'value' => @$kod,
        'data' => @$data,
        'options' => array(
            'options' => @$options,
            'valueField' => 'kod',
            'labelField' => 'name',
            'searchField' => array('name', 'cat'),
            'sortField' => 'kod',
            'plugins' => array('clear_selection'),
            'create' => false,
            'optgroups' => @$optgroup,
            'optgroupOrder' => array('1', '0'),
            //'dropdownParent'=>'#report_1',
            'render' => array(
                'optgroup_header' => new CJavaScriptExpression('function(item, escape) { return RenderOptHeader(item, escape); }'),
                'option' => new CJavaScriptExpression('function(item, escape) { return RenderOptionWithOpt(item, escape); }'),
                'item' => new CJavaScriptExpression('function(item, escape) { return RenderItem(item, escape); }'),
            ),
        ),
        'callbacks' => array('onChange' => new CJavaScriptExpression('function(value){
            $.ajax({
                type: "POST",
                url: "'.Yii::app()->createAbsoluteUrl($this->id . '/getlastlogin') .'",
                data: $("#login-form").serialize(),
                success: function(data){$("#Login_LOGIN").val(data);}
            });
        }')),
        'htmlOptions' => array(
            'placeholder' => @$placeholder,
            'required' => false,
            'style' => 'width: 370px;'
        ),

    )); ?>