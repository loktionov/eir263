<?php
/* @var $this UserController */
/* @var $model Login */

$this->breadcrumbs=array(
	'Logins'=>array('index'),
	$model->IDLOGIN,
);

$this->menu=array(
	array('label'=>'List Login', 'url'=>array('index')),
	array('label'=>'Create Login', 'url'=>array('create')),
	array('label'=>'Update Login', 'url'=>array('update', 'id'=>$model->IDLOGIN)),
	array('label'=>'Delete Login', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->IDLOGIN),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Login', 'url'=>array('admin')),
);
?>

<div id="card">
    <?php $this->renderPartial('card_view', array('model'=>$model)); ?>
</div>
<div class="clearfix"></div>
<div style="float: right;">
    <?php
    $this->widget('ext.mPrint.mPrint', array(
        'title' => 'user',          //the title of the document. Defaults to the HTML title
        'tooltip' => 'Печать',        //tooltip message of the print icon. Defaults to 'print'
        'text' => 'Распечатать',   //text which will appear beside the print icon. Defaults to NULL
        'element' => '#card',        //the element to be printed.
        /*'exceptions' => array(       //the element/s which will be ignored
            '.summary',
            '.search-form'
        ),*/
        //'publishCss' => true,       //publish the CSS for the whole page?
        //'visible' => Yii::app()->user->checkAccess('print'),  //should this be visible to the current user?
        'alt' => 'print',       //text which will appear if image can't be loaded
        'debug' => false,            //enable the debugger to see what you will get
        'id' => 'print-div'         //id of the print link
    ));
    ?>
</div>
